"use strict"
var base_url = $('html').attr('base-url');
// +==============================================+ //
// +==== load scripts upon loading the module ====+ //
// +==============================================+ //
$(document).ready(function() {
	
    $("#submit_property").on("click", function() {
    	$('#addnew_property').modal({
    	    backdrop: 'static',
    	    keyboard: false
    	})

        var intCostumer_id = $('#addnew_property').find('#customerID_property').val();

        var chrProperty_name = $('#addnew_property').find('#txtChrProperty_name').val();
        var chrProperty_unit = $('#addnew_property').find('#txtChrProperty_unit').val();
        var chrProperty_street = $('#addnew_property').find('#txtChrProperty_street').val();
        var chrProperty_building = $('#addnew_property').find('#txtChrProperty_building').val();
        var chrProperty_postal = $('#addnew_property').find('#txtChrProperty_postal').val();
        if((chrProperty_name.replace(/ /g,"")) == "")
        {
            $("._no_property_name").show();
            return false;
        }
        if(parseInt(chrProperty_postal) == undefined || chrProperty_postal == "")
        {
            $("._no_postal_code").show();
            return false;
        }
        if (chrProperty_name.length <= 0) {
            $('#addnew_property').modal("hide");
            return;
        }
        var post_url = base_url + 'admin/property/insertProperty/' + intCostumer_id;

        var aRproperty_details = [];
        aRproperty_details.push({
            'customer_id': intCostumer_id,
            'chrProperty_unit': chrProperty_unit,
            'chrProperty_name': chrProperty_name,
            'chrProperty_street': chrProperty_street,
            'chrProperty_building': chrProperty_building,
            'chrProperty_postal': chrProperty_postal
        });


        var fd = new FormData(document.getElementById("add-property-form"));
        fd.append('aRproperty_details', JSON.stringify(aRproperty_details));
        $.ajax({
            url: post_url,
            type: "POST",
            data: fd,
            processData: false,
            contentType: false,
            success: function(data) {
                var data = JSON.parse(data);
                //<a href="<?php echo base_url('admin/joborder/showJobOrderForm/').'/'.$properties_value->id.'/'.$customer_id; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i>Add Job Order</a>
                $('#propertyTab').find('table tbody').append('<tr><td style="width:40%"><img class="img-responsive img-thumbnail pull-left" src="' + base_url + 'img/properties/' + data.file_name + '" width="20%" /><span class="pull-left" style="margin-left:10px;">' + chrProperty_name + '</span></td><td>' + chrProperty_unit + ' ' + chrProperty_street + ' ' + chrProperty_building + '  ' + chrProperty_postal + '</td></tr>');
                $('#no-property').hide();
                $('#addnew_property').modal("hide");
                $('#add-property-form *').filter(':input').each(function() {
                    $(this).val('');
                });
            },
            error: function(x) {
                console.log(x.responseText);
            }

        });
    });

});
