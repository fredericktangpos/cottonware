"use strict"
var base_url = $('html').attr('base-url');
// +==============================================+ //
// +==== load scripts upon loading the module ====+ //
// +==============================================+ //
$(document).ready(function() {
	


});


/***----this part is the function processing-----***/

var searchSubTypeAction = function (elem)
{
    var value = $(elem).val();
    if(value!=0&& value != null )
    {
        var post_url = base_url+"admin/joborder/typesSubType/"+value;
        $.post(post_url,{},function(result)
            {
                var result = $.parseJSON(result);
                result = result.content;
                append("subsub_style","option",result);
            });
    }
}
/***
*@param parentID, the ID of parent object/element where the element is to be appended
*@param appendElement, the element to be appended like div , option, value..
*@param dataArray, json encoded array
*@param displayName, the arrays variable name that needs to be viewed
*applicable for option tag only 
**/
var append = function(parentID, appendElement, dataArray)
{
    console.log(dataArray);
    $("#"+parentID).empty();
    var open = "<"+appendElement+">";
    var close = "</"+appendElement+">";
    var parent = $("#"+parentID);
    parent.append("<"+appendElement+" value='0' id=''>-- Style --"+close);
    for(var i=0 ; i < dataArray.length ; i++)
    {
        parent.append("<"+appendElement+" value='"+dataArray[i].job_class_id+"'>"+dataArray[i].type_name+" ---- "+dataArray[i].type_code+close);
        console.log(dataArray[i]);
    }
}

var getSubTypeDetailAction = function(elem)
{
    var value = $(elem).val();
    if(value!=0&& value != null )
    {
        var post_url = base_url+"admin/joborder/subTypeInfo/"+value;
        $.post(post_url,{},function(result)
            {
                var result = $.parseJSON(result);
                result = result.content;
                changeDetails(result,"subsub_style");
            });
    }
}

var changeDetails = function(dataArray,parentID){
    $(".order-item-submit").attr("theName",dataArray[0].type_name);

}


function getMainTypeAction(elem)
{
    
}
/** ----- this part is the functions invocation---- **/

function searchSubType(elem)
{
    searchSubTypeAction(elem);
}

function getSubTypeDetail(elem)
{
    getSubTypeDetailAction(elem);
}

function getMainType(elem)
{
    getMainTypeAction(elem);
}
/**
var window_counter = 0 ;
function addNewWindow()
{   
    var get_url = base_url+"admin/joborder/getTypes";
    var app = $(".type_sel").parent().find(".chosen-container").find(".chosen-drop").find(".chosen-results");
    $(".type_sel").attr("data-rel","chosen");

    $.get(get_url, function(result){
        var result = $.parseJSON(result);
        for (var i = 0; i < result.length; i++) {
            var id = result[i].id;
            var  name = result[i].job_type_name;
            //var data_append = '<li class="active-result" data-option-array-index="'+id+'">'+name+'</li>';
            //$(".type_sel").parent().find(".chosen-container").find(".chosen-drop").find(".chosen-results").append(data_append);
        };
        console.log(result);
    });

    window_counter++;
    $(".window-"+window_counter).show();
    $(".chosen-container").css("width","100%");
    $(".chosen-container-single").css("width","100%");
}

**/
function chooseStyle(elem)
{
    var value = $(elem).val();

    var get_url = base_url + "admin/joborder/getSwatch/"+value;
    $(elem).parent().parent().find(".type_sel_swatch").empty();
    $(elem).parent().parent().find(".type_sel_swatch").append('<option value="">--- swatch ---</option>');

    $.get(get_url, function(result)
        {
            var result = $.parseJSON(result);
            for (var i = 0; i < result.length; i++) {
                var id = result[i].job_class_id;
                var name = result[i].type_name;
                var code  = result[i].type_code;
                //console.log('id = '+id +'   name' +name);
                var swatch = $(elem).parent().parent().find(".type_sel_swatch");
                var append_data = '<option value="'+id+'" style="height:50px;">'+name+' -- ' + code+'</option>';
                swatch.append(append_data);
            };
        });
}
function showNewRoom()
{
    var clone = $(".clone-src").clone();
    clone.removeClass("clone-src");
    clone.addClass("room");
    $(".room-area").append(clone);
    clone.show();
}
function showNewDimension(elem)
{
    var clone_src = $(elem).parent().parent().parent().find(".dimension-area").find(".dclone-src").clone();
    var appendElem = $(elem).parent().parent().parent().find(".dimension-area");
    clone_src.find("#order_width").addClass("order_width");
    clone_src.find("#order_height").addClass("order_height");
    clone_src.find(".basic_type").addClass("basic_type2");
    clone_src.find(".secondary_type").addClass("secondary_type2");
    clone_src.removeClass("dclone-src");
    clone_src.addClass("one-dimension");
    appendElem.append(clone_src);
    clone_src.show();
}
var location_counter  = 0 ;
function getOrder(elem)
{
    var _rooms = $(".room-area").find(".room");
    var room_count = _rooms.length;
    var _parent = $(elem).parent().parent().find(".modal-body");
    var primaryData  = {};
    var location_id = _parent.find(".order_location").val();
    var location_name = _parent.find(".order_location option:selected").text();
    var _other_location = _parent.find("._other_location").val();
    var t_other_location = _other_location.trim();
    //alert(_other_location);
    if($(".order_location").val() == "" && t_other_location == "") {
        $("._no_location").show();
        return false;
    } else {
        $("._no_location").hide();
    }
    $(".saveOrderBtn").show();
    location_counter ++;



    if(location_id == "" && t_other_location != "")
    {
        location_id = 0 ;
        location_name = _other_location;
    }
    primaryData = {
        "location_id"   : location_id,
        "location_name" : location_name,
        "room_count"    : room_count
    }
    var rooms = [];
    _rooms.each(function (e) {

            var data  = {};
            var room_id = $(this).find("#order_room").val();
            var room_name = $(this).find("#order_room option:selected").text();
            var _other_room = $(this).find("._other_room").val();
            var t_other_room = _other_room.trim();
            var _no_room_err = $(this).find("._no_room_err");
            
            if(room_id == "" && t_other_room == "")
            {
                _no_room_err.show();
                return false;
            }else {
                _no_room_err.hide();
            }

            if(room_name == "" && t_other_room != "") 
            {
                room_id = 0 ;
                room_name = _other_room;
            }

            var width_list = []; 
            $(this).find(".order_width").each(function() {
                width_list.push($(this).val());
            });
            var height_list = [];
            $(this).find(".order_height").each(function() {
                //alert($(this).val());
                height_list.push($(this).val());
            });
            var basic_type = [];
            var basic_type_name = [];
            $(this).find(".basic_type2").each(function() {
                //alert($(this).val());
                basic_type.push($(this).val());
                basic_type_name.push($(this).find("option:selected").text());
            });
            var secondary_type = [];
            var secondary_type_name = [];
            $(this).find(".secondary_type2").each(function() {
                //alert($(this).val());
                secondary_type.push($(this).val());
                secondary_type_name.push($(this).find("option:selected").text());
            });

            data = {
                "width_list" : width_list,
                "height_list" : height_list,
                "basic_type_list" : basic_type,
                "basic_type_name_list" : basic_type_name,
                "secondary_type_list" : secondary_type,
                "secondary_type_name_list" : secondary_type_name,
                "room_id" : room_id,
                "room_name" : room_name 
            }

            rooms.push(data);
    });
 

    displayChoices(rooms,primaryData);

    $(".room-area").children().each( function () {$(this).detach();});
    
}

function displayChoices(roomData, primaryData)
{
    var clone  = $(".mock_clone_src").clone();
    clone.addClass("location_number_"+location_counter);

    var roomData = roomData;
    var primaryData = primaryData;

    var _ROOM_COUNT = primaryData.room_count;

    /** primary Data assignment */

    var location_name = primaryData.location_name;
    var location_id   = primaryData.location_id;
    clone.find(".location_area").find(".location_name").empty().html(location_name);
    clone.find(".location_area").find(".location_id").val("").val(location_id);
    clone.find(".location_area").find(".location_id").attr("name","locations[]");
    /** -- end --  */


    /** order list assignment */
    for (var i = 0; i < roomData.length; i++) {

        var room_id = roomData[i].room_id; //finalize
        var room_name = roomData[i].room_name; // finalize
        console.log("rooms " + room_name);
        var room_clone = clone.find(".room_clone_src").clone();
        room_clone.addClass("room");
        room_clone.find(".room_name").empty().html(room_name);
        room_clone.find(".room_id").val("").val(room_id);
        $(".room_area").append(room_clone);
        
        var _arRoomOrders = []; 

        var width_list = roomData[i].width_list;
        var height_list = roomData[i].height_list;
        var basic_type_list = roomData[i].basic_type_list;
        var basic_type_name_list = roomData[i].basic_type_name_list;
        var secondary_type_list = roomData[i].secondary_type_list;
        var secondary_type_name_list = roomData[i].secondary_type_name_list;


        var count = 0 ;
        

        for (var x = 0; x < width_list.length; x++) {
            var temp_data = {};

            var width = width_list[x];
            var height = height_list[x];

            var dimension_clone  = room_clone.find(".dimension_clone_src").clone();

            dimension_clone.find(".room_width").html(width);
            dimension_clone.find(".room_height").html(height);
            room_clone.find(".dimension_area").append(dimension_clone);
            var _order = [];
            var reverse_count = 5;
            var inc = basic_type_list.length;
            if (count < inc)
            {
                inc = count+5;
            }
            for (var z = count; z < inc; z++) {
               // console.log(z + " = " + basic_type_list[z]);
                if (basic_type_list[z]!="") {
                    var _temp = {
                            "basic_type_id" : basic_type_list[z],
                            "basic_type_name" : basic_type_name_list[z],
                            "secondary_type" : secondary_type_list[z],
                            "secondary_type_name_list" : secondary_type_name_list[z]
                    };

                    _order.push(_temp);
                }
                
            }
            
            dimension_clone.removeClass("dimension_clone_src");
            

            count+=5;

             temp_data  = {
                "width" : width,
                "height" : height,
                "orders" : _order
             }

             var d_c = dimension_clone.find(".mock_order_list");
             d_c.empty();
             for (var o = 0; o < temp_data.orders.length; o++) {
                 console.log(temp_data.orders[o]);
                 var _append = '<li class="'+temp_data.orders[o].basic_type_id+'" id = "'+temp_data.orders[o].secondary_type+'">'+temp_data.orders[o].basic_type_name+' ----------- '+temp_data.orders[o].secondary_type_name_list+'</li>';
                 d_c.append(_append);
             }
             d_c.removeClass("mock_order_list");
             console.log("+++++----------------------------------------------------------++++");

           // var dimension_clone  = room_clone.find(".dimension_area").find(".dimension_clone_src").clone();
            //dimension_clone.find(".room_width").empty().html(temp_data.width);
            //dimension_clone.find(".rooom_height").empty().html(temp_data.height);
            //dimension_clone.find(".mock_order_list").empty();
            //for (var b = 0; b < temp_data.orders.length; b++) {
             //   var append_data = '<ol style="height:50px;">'+temp_data.orders[b].secondary_type_name_list+'</ol>';
             //   dimension_clone.find(".mock_order_list").append(append_data);
            //};
            dimension_clone.show();
            clone.find(".room_area").append(room_clone);
            room_clone.show();
        };
        room_clone.removeClass("room_clone_src");
        count = 0 ;
        /***
        console.log("basic type list = " + basic_type_list.length);
        console.log("secondary type list = " + secondary_type_list.length);
        console.log("number of dimension = " + width_list.length);
        console.log(" +++-------------------------------------+++"); 
        ****/



    }
    $(".order_area").append(clone);
    clone.removeClass("mock_clone_src");
    clone.show();
    $("#addOrd").modal("hide");
    /** -- end -- */
}

function hideModal()
{

    $("#addOrd").modal("hide");
    $(".room-area").empty();
}

function  saveOrder()
{

    if($("#contact_person_id").val() == "") {
        $("._no_contact_person").show();
        return false;
    }

    var container = $(".order_area");
    var children = container.children();
    var contact_person_id = $("#contact_person_id").val();
    var asset_id = $(".asset_id").val();
    var customer_id = $(".customer_id").val();
    var user_id = $(".user_id").val();
    var orders  = [];
    children.each(function () {
        var location = $(this);
        var location_id = location.find(".location_id").val();
        var location_name = location.find(".location_name").html();
        var room_data = [];
        location.find(".room_area").children().filter(".room").each( function () {
            var room_id  = $(this).find(".room_id").val();
            var room_name  = $(this).find(".room_name").html();
            var dimensions = [];
            $(this).find(".dimension_area").children().not(".dimension_clone_src").each( function () {
                var width = $(this).find(".room_width").html();
                var height = $(this).find(".room_height").html();

                var _dimension_orders = [];
                    $(this).find("ol li").each( function () {
                        var _basic_type = $(this).attr("class");
                        var _secondary_type = $(this).attr("id");
                        var or = {
                            "basic_type" :_basic_type,
                            "secondary_type" : _secondary_type
                        }
                        _dimension_orders.push(or);
                    }); 
                var dimensions_info = {
                        "width": width,
                        "height" : height,
                        "orders" : _dimension_orders
                }
                dimensions.push(dimensions_info);
            });
            var data = {
                "room_id" : room_id,
                "room_name" : room_name,
                "dimension" : dimensions
            }

            room_data.push(data);
        });
        var order_data = {
            "location_id" : location_id,
            "location_name" : location_name,
            "data" : room_data
        }
        orders.push(order_data);
    });

    console.log(orders);

    var post_url  = base_url + "admin/joborder/getOrder";

    $.post(post_url , {
            orders:orders,
            contact_person:contact_person_id,
            asset_id:asset_id,
            customer_id:customer_id,
            user_id:user_id}, function (result)
        {
            var res = $.parseJSON(result);
            console.log(res);
           if(res.success == true)
           {
                window.location.replace(base_url + "admin/joborder/edit/"+res.transaction_item_id);
           }
        });

}

function createQuotation(elem)
{
    
    var transaction_id = $(elem).attr("theTransactionID");
    var post_url = base_url + "admin/joborder/createQuotation/"+transaction_id;
    location.reload();
}
function showConfirmModal(elem)
{
    var close_btn = $(elem);
    var transaction_item_id = close_btn.attr("transactionItemID");
    $(".removeAreaConfirmBtn").attr("transactionItemID",transaction_item_id );
    $("#removeAssetFromListModal").modal("show");
    
}
function removeTransactionItem(elem)
{

    var close_btn = $(elem);
    var transaction_item_id = close_btn.attr("transactionItemID");
    
    var post_url = base_url + "admin/joborder/removeTransactionItem";

    $.post( post_url , {transaction_item_id: transaction_item_id} , function (result)
         {
            var result = $.parseJSON(result);
            if(result.trans_item_id == transaction_item_id) 
            {
                location.reload();
            }
         }) ;
}
function addOrderOnEdit(elem)
{
    var _parent = $(elem).parent().parent().find(".modal-body");
    var primaryData  = {};
    var location_id = _parent.find(".order_location").val();
    var location_name = _parent.find(".order_location option:selected").text();
    var _other_location = _parent.find("._other_location").val();
    var transaction_id = $("#addOnOrderTransaction_id").val();
    var contact_person = $(".order_contact_person").val();
    var asset_id = $(".order_asset_id").val();  
    var t_other_location = _other_location.trim();
    //alert(_other_location);
    if($(".order_location").val() == "" && t_other_location == "") {
        $("._no_location").show();
        return;
    } else {
        $("._no_location").hide();
    }

    var _validation_room_count = $(".room-area").find(".room").length;
    if (_validation_room_count == 0 )
    {
        $(".no-room-level-validation").show();
        throw new Error("no Room added for this level");
    } else {
        $(".no-room-level-validation").hide();
        $(".room-area").find(".room").each( function (e) {
            var _validation_room_dimension = $(this).find(".dimension-area").find(".one-dimension").length;
            if ( _validation_room_dimension == 0 )
            {
                $(this).find(".no-dimension-room-validation").show();
               throw new Error("no dimension for this room");
            } else {
                $(this).find(".no-dimension-room-validation").hide();
                $(this).find(".dimension-area").find(".one-dimension").each( function (e)
                    {
                        var width = $(this).find(".order_width").val();
                        var height = $(this).find(".order_height").val();
                        var orders=0;
                        if (width == "" || width == 0)
                        {
                            $(this).find(".order_width").css("border-color","red");
                            throw new Error("width is empty");
                        } else {
                            $(this).find(".order_width").removeAttr("style");
                        }
                        if (height == "" || height == 0)
                        {
                            $(this).find(".order_height").css("border-color","red");
                            throw new Error("height is empty");
                        } else {
                            $(this).find(".order_height").removeAttr("style");
                        }
                        $(this).find(".basic_type").each( function (e)
                            {
                               var selected = $(this).find("option:selected").val();
                               if (selected)
                               {
                                orders +=1;
                               }
                               
                            });

                       if (orders == 0 )
                       {
                            $(this).find(".no-order-dimension-validation").show();
                            throw new Error("no order selected");
                       } else {
                        $(this).find(".no-order-dimension-validation").hide();
                            $(this).find(".basic_type").each( function (e)
                            {
                               var selected = $(this).find("option:selected").val();
                               if (selected)
                               {
                                    var this_swatch = $(this).parent().parent().find(".secondary_type").val();
                                    if (this_swatch == "")
                                    {
                                        $(this).parent().parent().find(".secondary_type").css("border-color","red");
                                        throw new Error("no swatch selected");
                                    } else {
                                        $(this).parent().parent().find(".secondary_type").removeAttr("style");  
                                    }
                               }
                               
                            });
                       }

                    });
            }
        });
    }

    $(".saveOrderBtn").show();
    location_counter ++;

    var room_count = ($(".room-area").find(".room")).length;
    if(location_id == "" && t_other_location != "")
    {
        location_id = 0 ;
        location_name = _other_location;
    }
    primaryData = {
        "location_id"   : location_id,
        "location_name" : location_name,
        "room_count"    : room_count,
        "transaction_id" : transaction_id,
        "contact_person" : contact_person,
        "asset_id"       : asset_id
    }

   // dump(primaryData);
    var rooms = [];
    $(".room-area").find(".room").each(function ()
        {
            var data  = {};
            var room_id = $(this).find("#order_room").val();
            var room_name = $(this).find("#order_room option:selected").text();
            var _other_room = $(this).find("._other_room").val();
            var t_other_room = _other_room.trim();
            var _no_room_err = $(this).find("._no_room_err");
            
            if(room_id == "" && t_other_room == "")
            {
                _no_room_err.show();
                return;
            }else {
                _no_room_err.hide();
            }

            if(room_name == "" && t_other_room != "") 
            {
                room_id = 0 ;
                room_name = _other_room;
            }

            var width_list = []; 
            $(this).find(".order_width").each(function() {
                width_list.push($(this).val());
            });
            var height_list = [];
            $(this).find(".order_height").each(function() {
                //alert($(this).val());
                height_list.push($(this).val());
            });
            var basic_type = [];
            var basic_type_name = [];
            $(this).find(".basic_type2").each(function() {
                //alert($(this).val());
                basic_type.push($(this).val());
                basic_type_name.push($(this).find("option:selected").text());
            });
            var secondary_type = [];
            var secondary_type_name = [];
            $(this).find(".secondary_type2").each(function() {
                //alert($(this).val());
                secondary_type.push($(this).val());
                secondary_type_name.push($(this).find("option:selected").text());
            });

            data = {
                "width_list" : width_list,
                "height_list" : height_list,
                "basic_type_list" : basic_type,
                "basic_type_name_list" : basic_type_name,
                "secondary_type_list" : secondary_type,
                "secondary_type_name_list" : secondary_type_name,
                "room_id" : room_id,
                "room_name" : room_name 
            }

            rooms.push(data);
        });
    console.log("order data");
    console.log(rooms);
    console.log(primaryData);
    //displayChoices(rooms,primaryData);
    
    var post_url = base_url + "admin/joborder/getOrderOnEdit";
    $.post(post_url , {rooms: rooms , primaryData : primaryData} , function (result)
         {
            var  result = $.parseJSON(result);
            if(result.success == true)
            {
                location.reload();
            }
           
         });


    return false;

    //$(".room-area").children().each( function () {$(this).detach();});
    // $("#addOrdOnEdit").modal("hide");
   // $(".room-area").empty();
    room_count = 0 ;
}

function hideAddOnEdit()
{
     $("#addOrdOnEdit").modal("hide");
    $(".room-area").empty();
}

function _REMOVEROOM(elem)
{
    $(elem).parent().parent().parent().remove();
}
function _REMOVEDIMENSION(elem)
{
    $(elem).parent().parent().remove();
}
function newMisc(elem)
{
    var parent = $(elem).parent().parent();
    var transaction_id = parent.find(".misc-transaction_id").val();
    var misc_cost = parent.find(".misc-cost").val();
    if (misc_cost == "" || misc_cost <= 0 )
    {
        parent.find(".misc-cost").css("border-color","red");
        return false;
    } else {
        parent.find(".misc-cost").css("border-color","black");
    }
    
    var misc_description = parent.find(".misc-description").val();
    var misc_description_clean = misc_description.replace(/ /g,'');
    if (misc_description_clean == "")
    {
        parent.find(".misc-description").css("border-color","red");
        return false;
    } else {
        parent.find(".misc-description").css("border-color","black");
    }
    //misc_description
    
    var misc_id = parent.find(".misc-id").val();
    var post_url = base_url + "admin/joborder/newMiscJobOrder";

    $.post( post_url , 
                    {
                        transaction_id : transaction_id,
                        misc_cost : misc_cost,
                        misc_description : misc_description,
                        misc_id : misc_id
                    },function (result) {
                       location.reload();
                    });
}




