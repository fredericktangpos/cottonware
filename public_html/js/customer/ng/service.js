'use strict'
var app = angular.module('cw');

app
.factory('Ajax',['$rootScope','$http',function($rs,$ht){
	
	var fac = {};
	
	fac.restAction = function(obj){
		
		var ht = $ht
				(
					{
						method:obj.method,
						url   :obj.url,
						data  :obj.data,
						params:obj.params,
						transaformRequest:angular.identity,
						headers:{'Content-Type':undefined}
					}
				);

		return ht;
	}
	
	return fac;
	
}])
.factory('Map',['$rootScope','$http',function($rs,$ht){
	var fac = {};

	fac.geocode = function(obj){
		var ht  = $ht(
						{
							method : "GET",
							url    : "https://maps.googleapis.com/maps/api/geocode/json",
							params : obj.params,
							transaformRequest : angular.identity,
							headers : {'Content-Type':undefined}
						}
					);
		return ht;
	}

	return fac;
}])
.factory('Rooms',['$rootScope',function($rs){
	var fac = {};

	fac.rooms = function(){
		var roomsList = [
							{ 1: "Aerary"},
							{ 2: "Air Shower"},
							{ 3: "AirCraft Cabin"},
							{ 4: "Airport Lounge"},
							{ 5: "Aisle"},
							{ 6: "Alcove"},
							{ 7: "Almonry"},
							{ 8: "Anatomic Theatre"},
							{ 9: "Andron"},
							{ 10: "Anechoic Chamber"},
							{ 11: "Antechamber"},
							{ 12: "Apodyterium"},
							{ 13: "Arizona Room"},
							{ 14: "Assembly Hall"},
							{ 15: "Atelier"},
							{ 16: "Atrium"},
							{ 17: "Attic"},
							{ 18: "Attic Style"},
							{ 19: "Auditorium"},
							{ 20: "Aula regia"},
							{ 21: "Ball Room"},
							{ 22: "Bang"},
							{ 23: "Banishment Room"},
							{ 24: "Basement"},
							{ 25: "Bath room"},
							{ 26: "Bed room"},
							{ 27: "Billiard Room"}
						];
	}

	return fac;
}]);