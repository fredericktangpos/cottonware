'use strict'
var app =angular.module('cw');
app
.controller('JobOrder',['$scope','$rootScope','Ajax','$compile','$sce',function($s,$rs,Ajax,$compile,sce){
	$s.sub_type = '';
	$s.showItemOrderDialog = function(trans_item_id,type_id)
	{
		$s.transactionType = type_id;
		$s.trans_item_id = trans_item_id;
		console.log($s.trans_item_id);
		var fd = new FormData(); //post data
		fd.append("transaction_type",$s.transactionType);

		var params      = new Object(); //parameters
		var obj    = new Object();
		obj.method = "POST";
		obj.url    = $rs.base_url+"admin/settings/getSubType";
		obj.data   = fd;
		obj.params = params;

		Ajax
		.restAction(obj)
		.then(
			function(res){
				$s.sub_type = res.data.sub_type;
				$(".order-item-submit").attr("theID", $s.trans_item_id);
				$("#item-order-dialog").modal("show");
			},
			function(res){
				$rs.dialogContent = "Could not load customers due to :"+res.data;
				$rs.dialogColor   = "danger";
				$s.loading        = false;
			}
		);
	}

	console.log($s.sub_type);
	
}])
app
.controller('SupplierList',['$scope','$rootScope','Ajax','$compile','$sce',function($s,$rs,Ajax,$compile,sce){

	$s.per_page      = 10;	//limits number of items per page
	$s.filter        = ""; //search filter
	$s.customer_type = "customer"; //customer type
	$s.start         = 0; //start
	$s.orderCol      = "id"; //order by which column
	$s.orderVal      = true; //order alignment
	$s.loading       = false; //loading limiter
	$s.maxPerPage    = 10;

	$s.customers     = []; //customers
	$s.page_links    = ""; //page links
	$s.searching     = ""; // search
	//load customer
	$s.loadCustomer = function(){
		var fd = new FormData(); //post data
		fd.append("customerType",$s.customer_type);
		fd.append("orderCol",$s.orderCol);
		fd.append("orderVal",$s.orderVal);
		fd.append("searchTerm",$s.filter);
		fd.append("perPage",$s.per_page);

		var params      = new Object(); //parameters
		params.per_page = $s.start;

		var obj    = new Object();
		obj.method = "POST";
		obj.url    = $rs.base_url+"admin/supplier/json_supplier";
		obj.data   = fd;
		obj.params = params;

		if($s.loading===false){
			$s.loading = true;

			Ajax
			.restAction(obj)
			.then(
				function(res){
					$s.loading = false;

					$s.customers = res.data.msg;
					$s.page_links = res.data.page_links;

					$('.customer-pagination-container').empty().append($s.page_links);
					$('.customer-pagination-container').find('a').each(function(i,e){
						var href_split = $(e).attr('href');
						var num        = null;

						if(href_split!==undefined){
							num = parseInt(href_split.split("=")[1]);
							$(e).attr("ng-click","start="+num+"; loading=false;");
						}else{
							num = 0;
						}

						$(e).attr('href','javascript:void(0);');
					});
					$compile($('.customer-pagination-container'))($s);
				},
				function(res){
					$rs.dialogContent = "Could not load customers due to :"+res.data;
					$rs.dialogColor   = "danger";
					$s.loading        = false;
				}
			);
		}
	}

	$s.$watch('per_page',function(){
		$s.per_page = parseInt($s.per_page);
		$s.start    = 0;
		if(!isNaN($s.per_page)){
			$s.loadCustomer();
		}
	});

	$s.$watch('orderVal',function(){
		$s.loadCustomer();
	});

	$s.$watch('customer_type',function(){
		$s.loadCustomer();
	});

	$s.$watch('start',function(){
		$s.loadCustomer();
	});
}])
.controller('CurtainList',['$scope','$rootScope','Ajax','$compile','$sce',function($s,$rs,Ajax,$compile,sce){

	$s.per_page      = 10;	//limits number of items per page
	$s.filter        = ""; //search filter
	$s.customer_type = "customer"; //customer type
	$s.start         = 0; //start
	$s.orderCol      = "id"; //order by which column
	$s.orderVal      = true; //order alignment
	$s.loading       = false; //loading limiter
	$s.maxPerPage    = 10;

	$s.curtains      = []; //curtains
	$s.page_links    = ""; //page links
	$s.searching     = ""; // search
	//load customer
	$s.loadCurtains = function(){
		var fd = new FormData(); //post data
		fd.append("customerType",$s.customer_type);
		fd.append("orderCol",$s.orderCol);
		fd.append("orderVal",$s.orderVal);
		fd.append("searchTerm",$s.filter);
		fd.append("perPage",$s.per_page);

		var params      = new Object(); //parameters
		params.per_page = $s.start;

		var obj    = new Object();
		obj.method = "POST";
		obj.url    = $rs.base_url+"admin/curtaincontroller/json_curtain";
		obj.data   = fd;
		obj.params = params;

		if($s.loading===false){
			$s.loading = true;

			Ajax
			.restAction(obj)
			.then(
				function(res){
					$s.loading = false;
					 console.log(res.data);
					$s.curtains = res.data.msg;
					$s.page_links = res.data.page_links;

					$('.customer-pagination-container').empty().append($s.page_links);
					$('.customer-pagination-container').find('a').each(function(i,e){
						var href_split = $(e).attr('href');
						var num        = null;

						if(href_split!==undefined){
							num = parseInt(href_split.split("=")[1]);
							$(e).attr("ng-click","start="+num+"; loading=false;");
						}else{
							num = 0;
						}

						$(e).attr('href','javascript:void(0);');
					});
					$compile($('.customer-pagination-container'))($s);
				},
				function(res){
					$rs.dialogContent = "Could not load curtains due to :"+res.data;
					$rs.dialogColor   = "danger";
					$s.loading        = false;
				}
			);
		}
	}

	$s.$watch('per_page',function(){
		$s.per_page = parseInt($s.per_page);
		$s.start    = 0;
		if(!isNaN($s.per_page)){
			$s.loadCurtains();
		}
	});

	$s.$watch('orderVal',function(){
		$s.loadCurtains();
	});

	$s.$watch('customer_type',function(){
		$s.loadCurtains();
	});

	$s.$watch('start',function(){
		$s.loadCurtains();
	});
}])
.controller('CustomerList',['$scope','$rootScope','Ajax','$compile','$sce',function($s,$rs,Ajax,$compile,sce){

	$s.per_page      = 10;	//limits number of items per page
	$s.filter        = ""; //search filter
	$s.customer_type = "customer"; //customer type
	$s.start         = 0; //start
	$s.orderCol      = "id"; //order by which column
	$s.orderVal      = true; //order alignment
	$s.loading       = false; //loading limiter
	$s.maxPerPage    = 10;

	$s.customers     = []; //customers
	$s.page_links    = ""; //page links

	//load customer
	$s.loadCustomer = function(){
		var fd = new FormData(); //post data
		fd.append("customerType",$s.customer_type);
		fd.append("orderCol",$s.orderCol);
		fd.append("orderVal",$s.orderVal);
		fd.append("searchTerm",$s.filter);
		fd.append("perPage",$s.per_page);

		var params      = new Object(); //parameters
		params.per_page = $s.start;

		var obj    =new Object();
		obj.method = "POST";
		obj.url    = $rs.base_url+"admin/customer/getCustomers";
		obj.data   = fd;
		obj.params = params;

		if($s.loading===false){
			$s.loading = true;

			Ajax
			.restAction(obj)
			.then(
				function(res){
					$s.loading = false;

					$s.customers = res.data.content.msg;
					$s.page_links = res.data.content.page_links;

					$('.customer-pagination-container').empty().append($s.page_links);
					$('.customer-pagination-container').find('a').each(function(i,e){
						var href_split = $(e).attr('href');
						var num        = null;

						if(href_split!==undefined){
							num = parseInt(href_split.split("=")[1]);
							$(e).attr("ng-click","start="+num+"; loading=false;");
						}else{
							num = 0;
						}

						$(e).attr('href','javascript:void(0);');
					});
					$compile($('.customer-pagination-container'))($s);
				},
				function(res){
					$rs.dialogContent = "Could not load customers due to :"+res.data;
					$rs.dialogColor   = "danger";
					$s.loading        = false;
				}
			);
		}
	}
	$s.removeCustomerModal = function (elem)
	{
		var elem = elem.target;
		var intCustomer_id = $(elem).attr("id");
		$("#removeCustomerModal").find(".removeCustomerConfirmBtn").attr("id",intCustomer_id);
		$("#removeCustomerModal").modal("show");
	}
	$s.removeCustomer = function (elem)
	{
		var elem = elem.target;
		var intCustomer_id = $(elem).attr("id");

		var fd = new FormData();
		fd.append("intCustomer_id",intCustomer_id);

		var obj = new Object();
		obj.method = "POST";
		obj.url = $rs.base_url+"admin/customer/removeCustomer";
		obj.data = fd;

		if($s.loading===false){ 
			$s.loading = true;
			Ajax
			.restAction(obj)
			.then(
				function(res){
					$("#removeCustomerModal").modal("hide");
					$s.loading = false;
					$s.loadCustomer();
				},
				function(res){
				}
			);
		}
	}
	$s.$watch('per_page',function(){
		$s.per_page = parseInt($s.per_page);
		$s.start    = 0;
		if(!isNaN($s.per_page)){
			$s.loadCustomer();
		}
	});

	$s.$watch('orderVal',function(){
		$s.loadCustomer();
	});

	$s.$watch('customer_type',function(){
		$s.loadCustomer();
	});

	$s.$watch('start',function(){
		$s.loadCustomer();
	});
}])
.controller('CustomerEdit',['$scope','$rootScope','Ajax','$sce','Map',function($s,$rs,Ajax,sce,Map){
	$s.customerId         = 0;
	$s.customer           = {};
	$s.customer.maps      = "";
	$s.duplicates = [];
	$s.loading            = true;

	//add customer contact
	$s.addCustomerContact = function(){
		$s.customer.contact_persons.push({id:0,name:'',contact:'',role:'',email:''});
	}

	//save /update customer
	$s.saveCustomer = function(){
		var fd     = new FormData(document.getElementById('customer-registration-form'));

		var obj    = new Object();
		obj.url    = $rs.base_url+"admin/customer/customerCreationAjax";
		obj.method = "POST";
		obj.params = {};
		obj.data   = fd;

		$('.customer-registration .btn-save').attr('disabled','disabled');

		$rs.dialogColor   = "";
		$rs.dialogContent = "";
		Ajax
		.restAction(obj)
		.then(
			function(res){
				console.log(res.data);
				if(res.data.error==false){
					$rs.dialogColor             = sce.trustAsHtml("success");
					$rs.dialogContent           = sce.trustAsHtml("Customer '"+res.data.content[0].cus_name+"' has been saved.");
					$s.customer                 = res.data.content[0];
					$s.customerId               = res.data.content[0].id;
					window.location.href=$rs.base_url+"admin/customer/edit/"+$s.customerId;
					if($s.customer.contact_persons.length===0){
						$s.customer.contact_persons = [{id:0,name:"",contacts:'',role:'',email:''}];
					}
				}else{
					$rs.dialogColor             = sce.trustAsHtml("danger");
					$rs.dialogContent           = sce.trustAsHtml(res.data.content);
				}

				$('.customer-registration .btn-save').removeAttr('disabled');
			},
			function(res){
				$rs.dialogColor   = sce.trustAsHtml("danger");
				$rs.dialogContent = sce.trustAsHtml("An error occurred due to : "+res.data);
				$('.customer-registration .btn-save').removeAttr('disabled');
			}
		);
	}

	//save the contacts
	$s.saveContacts = function(){
		var fd     = new FormData(document.getElementById('customer-contact-person-form'));
		var obj    = new Object();
		obj.url    = $rs.base_url+"admin/customer/customerContactPersonAjax";
		obj.method = "POST";
		obj.params = {};
		obj.data   = fd;

		$rs.dialogColor   = "";
		$rs.dialogContent = "";

		Ajax
		.restAction(obj)
		.then(
			function(res){
				if(res.data.error==false){
					$rs.customerTabColor   = sce.trustAsHtml("success");
					$rs.customerTabContent = sce.trustAsHtml("Contact person(s) saved.");
					$s.customer.contact_persons = res.data.content[0].contact_persons;
				}else{
					$rs.customerTabColor   = "danger";
					$rs.customerTabContent = sce.trustAsHtml(res.data.content);
				}
			},
			function(res){
				$rs.customerTabColor   = "danger";
				$rs.customerTabContent = sce.trustAsHtml("Unable to save contact person(s) due to : "+res.data);
			}
		);
	}

	//init function
	$s.init = function(){

		$s.loading       = true;

		$s.customerId  = parseInt($("[ng-controller='CustomerEdit']").attr('customerid'));
		if($s.customerId!==0 && !isNaN($s.customerId)){
			var obj = new Object();

			obj.method = "GET";
			obj.data   = {};
			obj.params = {customerID:$s.customerId};
			obj.url    = $rs.base_url+"admin/customer/getCustomer";

			Ajax
			.restAction(obj)
			.then(
				function(res){
					$s.customer       = res.data.content[0];
					if($s.customer.contact_persons.length===0){
						$s.customer.contact_persons = [{id:0,name:"",contacts:'',role:'',email:''}];
					}
					$s.mapLoader();
					$s.loading       = false;
				},
				function(res){
					$rs.dialogContent = sce.trustAsHtml("Could not load company information due to :"+res.data);
					$rs.dialogColor   = sce.trustAsHtml("danger");
					$s.customer.cus_address_postal = "";
					$s.mapLoader();
					$s.loading       = false;
				}
			);
		}else{
			$s.customer                    = {};
			$s.customer.id                 = $s.customerId;
			$s.customer.cus_hom_num        = [''];
			$s.customer.cus_off_num        = [''];
			$s.customer.cus_fax_num        = [''];
			$s.customer.cus_han_num        = [''];
			$s.customer.cus_email          = [''];
			$s.customer.contact_persons    = [{id:0,name:"",contacts:'',role:'',email:''}];
			$s.customer.cus_address_postal = "";
			$s.mapLoader();
			$s.loading       = false;
		}
	}

	//load the maps!
	$s.mapLoader = function(){
		var addresss = "";
		var zooms     = 12;

		if($s.customer.cus_address_postal!=undefined && $s.customer.cus_address_postal!=""){
			addresss = "sinapore+"+$s.customer.cus_address_postal;
			zooms     = 15;
		}else{
			addresss = "singapore";
		}

		var obj = new Object();
		 //obj.params = {address:addresss,key:"AIzaSyB-TsGZH_Uid250iKuxoIGIIa1wDBMcaHs"}; // local
		obj.params = {address:addresss,key:"AIzaSyCifS-cGoG5c5ybyHd-WQYqo18ip0tCo8I"}; // remote

		Map
		.geocode(obj)
		.then(
			function(res){
				console.log (res);
				if(res.data.status=="OK"){
					var lats   = res.data.results[0].geometry.location.lat;
					var lngs   = res.data.results[0].geometry.location.lng;
					var latLng =  new google.maps.LatLng(lats,lngs);

					if($s.customer.maps!=undefined){
		    			$s.customer.maps.panTo(latLng);
					}else{
						var mapOptions = {
				          center: { lat: lats, lng: lngs},
				          zoom: zooms
				        };
				        $s.customer.maps = new google.maps.Map(document.getElementById('map-container'),
				            mapOptions);
					}

					var marker = new google.maps.Marker({
						position:latLng
					});

					marker.setMap($s.customer.maps);
				}else{
					console.log("could not loadthe map");
				}
			},
			function(res){
				console.log("could not properly load the calendar");
			}
		);
	}


	//check if th e customer already exists
	$s.checkCustomerDuplicity = function(){
		$s.customer.cus_name = $s.customer.cus_name == undefined ? "" : $s.customer.cus_name;

		if($s.customer.cus_name!="" && $s.customer.cus_name.length>2){
			var obj = new Object();
			obj.method = "GET";
			obj.data   = {};
			obj.params = {cus_name:$s.customer.cus_name,cus_id:$s.customer.id};
			obj.url    = $rs.base_url+"admin/customer/checkDuplicate";

			Ajax
			.restAction(obj)
			.then
			(
				function(res){
					$s.duplicates = res.data.content;
				},
				function(res){

				}
			);
		}
	}

	//redirect the user to antoher customer
	$s.redirectCustomer = function($customerid){
		window.open($rs.base_url+"admin/customer/edit/"+$customerid);
	}

	//init funnction
	$s.init();
}])
.controller('ChatController',['$scope','$rootScope','Ajax','$sce', function($s,$rs,Ajax,sce){
	$s.cr       = new Object();
	$s.cr.chats = [];

	$s.cr.chatText = "";
	$s.cr.isStoring = false;
	$s.cr.isLoading = false;

	$s.pushChat = function(){

		var obj = new Object();
		var  fd = new FormData();
		fd.append("content",$s.cr.chatText);
		fd.append("userid",0);

		obj.url    = $rs.base_url+"chatroom/ChatroomController/storeChat";
		obj.data   = fd;
		obj.params = {};
		obj.method = "POST";

		$s.cr.isStoring = true;

		Ajax
		.restAction(obj)
		.then(
			function(res){
				$s.cr.isStoring = false;
				$s.cr.chats.push(res.data.content);
				$s.cr.chatText = "";
				var container = $('.mainchat-table .item-container').height();
				$('.mainchat-table').animate({scrollTop:(container+100)+"px"},100);
			},
			function(res){
				$s.cr.isStoring = false;
			}
		);
		return false;
	}


	$s.loadChat = function(){
		var obj    = new Object();
		obj.url    = $rs.base_url+"chatroom/ChatroomController/getChat";
		obj.data   = {};
		obj.params = {};
		obj.method = "POST";

		$s.cr.isLoading = true;

		Ajax
		.restAction(obj)
		.then(
			function(res){
				$s.cr.chats = res.data.content;
				setTimeout(function(){
					var container = $('.mainchat-table .item-container').height();
					$('.mainchat-table').animate({scrollTop:(container)+"px"},100);
					$s.cr.isLoading = false;
				},100);
			},
			function(res){
				$s.cr.isLoading = false;
			}
		);

		$s.intervalChat();
		setInterval(function(){
			$s.intervalChat();
		},1000);
	}

	$s.intervalChat = function(){
		if($s.cr.isStoring==false && $s.cr.isLoading == false){
			var lastId = $('.item-container .chat-item:last-child').attr('chatid');
			lastId = lastId==undefined ? 0 : lastId;


			var obj    = new Object();
			obj.url    = $rs.base_url+"chatroom/ChatroomController/intervalChat";
			obj.data   = {};
			obj.params = {chatid:lastId};
			obj.method = "GET";

			Ajax
			.restAction(obj)
			.then(
				function(res){
					if(res.data.content.length!==0){

						for(var i = 0 ;i< res.data.content.length;i++){
							$s.cr.chats.push(res.data.content[i]);
						}
					}
				},
				function(res){

				}
			);
		}
	}

	$s.loadChat();
}]);