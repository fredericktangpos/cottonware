"use strict"
var base_url = $('html').attr('base-url');

$(document).ready(function()
{

    $('#show_subcat').click(function(){
      $( "#add_subCat" ).toggle( "slow", function() {
      });
    });

    //display sub category based on the selected category
    $('#job_category_id').change(function(){
      var jobcat_id = $('#job_category_id').val();
      var process_url = base_url + 'admin/settings/GET_SUBCATEGORY';

        if(jobcat_id.length > 0)
        {

          $('#disp').show().append('<i class="fa fa-spinner fa-spin"></i> fetching data...');

          $.post(process_url, {jobcat_id:jobcat_id}, function(result){
            try {
                var result = $.parseJSON(result);
                var theresult = "";

                if(result.subcat_list.length > 0)
                {
                  for (var i=0; i<result.subcat_list.length; i++)
                  {
                    theresult += '<option value='+result.subcat_list[i].id+'>'+result.subcat_list[i].job_type_name+'</option>';
                    $('#sub_category').empty().append(theresult);
                  }
                }
            }
            catch(e) {
                console.log(result);
            }
          })
          .fail(function(test) {
            alert(test.responseText);
          });

          setTimeout(function(){
            $('#disp').empty().hide().fadeIn();
          },2000);

        }
    });


    // ajax pagination in supplier
    $('#supplier').on('click','.pagination li > a',function(e){
      var url = $(this).attr('href');
        if(url == null){
          return false;
        }
        $.get(url, function(data){
          var html_element = $(data).find('#table-container');
          $("#supplier").html(html_element);
        });
      e.preventDefault();
    });

    // ajax pagination in job category
    $('#jo_order_cat').on('click','.pagination li > a',function(e){
      var url = $(this).attr('href');
        if(url == null){
          return false;
        }
        $.get(url, function(data){
          var html_element = $(data).find('#table-container1');
          $("#jo_order_cat").html(html_element);
        });
      e.preventDefault();
    });

    // ajax pagination in job classification
    $('#job_classification').on('click','.pagination li > a',function(e){
      var url = $(this).attr('href');
        if(url == null){
          return false;
        }
        $.get(url, function(data){
          var html_element = $(data).find('#table-container2');
          $("#job_classification").html(html_element);
        });
      e.preventDefault();
    });

});
// end of doc ready

// adding new supplier
function ADD_NEW_SUPPLIER()
{
  var supp_name         = $('#supp_name').val();
  var supp_contact_name = $('#supp_contact_name').val();
  var supp_contact_num  = $('#supp_contact_num').val();
  var supp_email        = $('#supp_email').val();

  var post_url          = base_url + 'admin/settings/ADD_SUPPLIER';

  if(supp_name.length > 0)
  {
    $('#add_supplier_btn').show( function(){
      $('#add_supplier_btn').empty().append('<i class="fa fa-spinner fa-spin"></i> saving...');
    });

    $.post(post_url, { supp_name:supp_name, supp_contact_name:supp_contact_name, supp_contact_num:supp_contact_num, supp_email:supp_email }, function(result){
      var result = $.parseJSON(result);
      console.log(result);
      if(result.stat == true)
      {
          $("#add-supplier-dialog").modal("hide");
          bootbox.alert('New Supplier successfully added');
          /**
         $('#supplier-list-body').append('<tr><td>'+result.content.id+'</td>'+
          '<td>'+result.content.supp_co_name+'</td><td>'+result.content.supp_contact_person+'</td>'+
          '<td>'+result.content.supp_contact_num+'</td><td>'+result.content.supp_email+'</td>'+
          '<td class="text-center"><a href=""><i class="fa fa-edit"></i></a></td>'+
          '<td class="text-center"><a href=""><i class="fa fa-times"></i></a></td>'+
          '</tr>'); **/

          $('#supplier-list-body').append('<tr><td>'+result.content.id+'</td>'+
          '<td><a href="'+base_url+'admin/supplier/supplier_swatches/'+result.content.id+'">'+result.content.supp_co_name+'</a></td><td>'+result.content.supp_contact_person+'</td>'+
          '<td>'+result.content.supp_contact_num+'</td><td>'+result.content.supp_email+'</td>'+
          '<td><a href="" theName="'+supp_name+'" theSupplierID="'+result.content.id+'" theContactNumber="'+supp_contact_num+'" theContactPerson="'+supp_contact_name+'" theContactEmail="'+supp_email+'" onclick="showEditSupplierModal(this)"><i class="fa fa-edit"></i></a></i></a></td>'+
          '<td><a href="" theName="'+supp_name+'" theSupplierID="'+result.content.id+'" theContactNumber="'+supp_contact_num+'" theContactPerson="'+supp_contact_name+'" theContactEmail="'+supp_email+'" onclick="showConfirmDeleteDialog(this,1)"><i class="fa fa-times red"</i></a></td>'+
          '</tr>');
      }
      else
      {
        alert('Error saving new supplier');
      }
      // window.location.reload();
    });

    setTimeout(function(){
      $('#add_supplier_btn').empty().hide().fadeIn().append('Add');
    },3000);
  }
  else
  {
    document.getElementById("supp_name").style.borderColor = "red";
    document.getElementById("supp_name").style.backgroundColor = "rgb(255, 187, 187)";
    /*var error = 'error';
    $('#validate').html( error );*/
    /*alert('Please enter supplier name');*/
    return false;
  }
}

// adding new job category
function ADD_JOB_CATEGORY()
{

  var job_name = $('#category_name').val();
  var job_desc = $('#cat_description').val();
  var cat_stat = $('.cat_stat:checked').val();
  var post_url = base_url + 'admin/settings/ADD_JOB_ORDER';

  var subcat_array = new Object;
  var tm_arr = [];

  $(".subcat_list ul li span.thesubcat").each(function(){
    tm_arr.push($(this).html());
  });

  subcat_array.subCategory = tm_arr;
  subcat_array.job_name   = job_name;
  subcat_array.job_desc   = job_desc;
  subcat_array.cat_stat   = cat_stat;

  // console.log(subcat_array);

  if(job_name.length > 0)
  {

    $('#add_jobcat_btn').show( function(){
      $('#add_jobcat_btn').empty().append('<i class="fa fa-spinner fa-spin"></i> saving...');
    });

    $.post(post_url, subcat_array, function(result){

      try {
        var result = $.parseJSON(result);
        alert(result.msg);
        window.location.reload();
      }
      catch(e) {
        console.log(result);
      }
    })
    .fail(function(test){
      alert(test.responseText);
    });

    setTimeout(function(){
      $('#add_jobcat_btn').empty().hide().fadeIn().append('Add');
    },3000);
  }

  else
  {
    return false;
  }
}

// adding new job classification
function ADD_JOB_CLASSIFICATION()
{
  var job_class_name  = $('#job_class_name').val();
  var tbl_supplier_id = $('#tbl_supplier_id').val();
  var job_category_id = $('#job_category_id').val();
  var sub_category    = $('#sub_category').val();
  var job_class_desc  = $('#job_class_desc').val();

  var post_url = base_url + 'admin/settings/ADD_JOB_CLASSIFICATION';

  if(job_class_name.length > 0)
  {

    $('#add_jobclass_btn').show( function(){
      $('#add_jobclass_btn').empty().append('<i class="fa fa-spinner fa-spin"></i> saving...');
    });

    $.post(post_url, {job_class_name:job_class_name, tbl_supplier_id:tbl_supplier_id, job_category_id:job_category_id, sub_category:sub_category, job_class_desc:job_class_desc}, function(result){

      try {
        var result = $.parseJSON(result);
        alert(result.msg);
        window.location.reload();
      }
      catch(e) {
        console.log(result);
      }
    })
    .fail(function(test){
      alert(test.responseText);
    });

    setTimeout(function(){
    $('#add_jobclass_btn').empty().hide().fadeIn().append('Add');
    },3000);
  }

  else
  {
    return false;
  }
}


// adding new sub category
function get_subcat(e)
{
    var subCat_name = $('#subCat_name').val();
    if (e.keyCode == 13)
    {
      if(subCat_name.length == "")
      {
        return false;
      }

      var thedata = '<li onclick="remove(this);"style="cursor:pointer;font-size:11px; margin-right:5px;" class="label label-success"><span class="thesubcat">'+subCat_name+'</span>&nbsp;&nbsp;<i class="fa fa-times"></i></li>';
      $(".subcat_list ul").append(thedata);
      $('#subCat_name').val('');
    }
}

function search_item(value)
{

  var chrKeyword = $("#item_name").val();
  var post_url = base_url + 'admin/itemcontroller/ajaxSearchItem';

  $('#htxtItem-id').val('');//set the item id to zero value

  if($.trim(chrKeyword).length == 0)
  {
    $(value).parent().parent().parent().parent().find('.result').slideUp("slow",function() {
      $('').val('');
    });
  }else
  {
    $.post(post_url,{keyword:chrKeyword},function(result)
      {
        var result = $.parseJSON(result);

        var elResult = "";

          if(result.success == true || result.data !=null)
          {
            $(value).parent().parent().parent().parent().find(".result").slideDown("slow", function()
              {

                var length = result.data.length;

                for(var i = 0; i < length ; i++)
                {

                  elResult += '<li onclick = "select(this);" style="cursor:pointer" class="list-group-item text-right" theName="'+result.data[i].job_type_name+'" '+
                        'id="'+result.data[i].id+'">'+result.data[i].job_type_name+'&nbsp;&nbsp;&nbsp;&nbsp;<i style="color:#EBC137;" class="fa fa-arrow-circle-o-up"></i></li>';

                  $(value).parent().parent().find('.result ul').empty().append(elResult);

                }
              });
          }else
          {
            $(value).parent().parent().parent().find('.result').slideUp("slow");
          }
      });
  }

}

function select (elem)
{

  var item_id = elem.id;
  $('#htxtItem-id').val(item_id); //set the item value [-hidden input type-]
  var theName = $(elem).attr('theName'); //get the name of the item
  $('#item_name').val("").val(theName).parent().parent().parent().find(".result").slideUp("slow"); //set the value of the input area and hide the result display

}

function addItemSupplier()
{
    var item_id = $("#htxtItem-id").val(); //get the item id, if item exist

    var intItem_id = item_id; //item id
    var intSupplier_id = $("#slctSupplier_id").val(); //suppliers id
    var chrItem_category = $("#slctItem_category").val(); //item category
    var chrPrice_amount = $("#cost_amount").val(); //item amount on the supplier
    var chrItem_name = $("#item_name").val();
    var chrType_code = $("#chrType_code").val();

    var partial_url =  base_url + 'admin/settings/';

    if($.trim(item_id).length == 0) //if item does not exist
    {

      var post_url = partial_url+'addItem'

      $.post(post_url,
        {
          chrItem_name        :  chrItem_name,
          intSupplier_id      :  intSupplier_id,
          chrItem_category    :  chrItem_category,
          chrPrice_amount     :  chrPrice_amount,
          chrType_code        :  chrType_code
        },function(result)
        {
          alert("item added");
          var result = $.parseJSON(result);
          if(result.success=="true"||result.success==true)
          {

            $('#add_item_form *').filter(':input').each(function()
              {
                $(this).val('');
              }
            );
          }
        }
      );

    }else{ //if item exist

      var post_url = partial_url+'addItemSupplier';

      $.post(post_url,
        {
          item_id             :  item_id,
          intSupplier_id      :  intSupplier_id,
          chrItem_category    :  chrItem_category,
          chrPrice_amount     :  chrPrice_amount,
          chrType_code        :  chrType_code
        },function(result)
        {
          var result = $.parseJSON(result);
          if(result.success=="true"||result.success==true)
          {
            alert("item supplier added");
            $('#add_item_form *').filter(':input').each(function()
              {
                $(this).val('');
              }
            );
          }
        }
      );

    }

}

function editItem(elem)
{

  $("#edit_item").modal("show"); //show modal
  $("#edit_item #edit-item-form select").find("option:eq(0)").attr("selected","selected");

  var intItem_id = $(elem).attr('theItemID');
  var chrItem_name = $(elem).attr('theName');
  var item_type = $(elem).attr('theType');

  item_type = $.trim(item_type.toLowerCase());

  $("#edit_item").find("#txtChrItem_name").val("").val(chrItem_name);
  $("#edit_item").find("#htxtItem_id").val("").val(intItem_id);

  $("#edit_item #edit-item-form select").find("option").each(function()
    {
      var option_value = $.trim($(this).attr("value"));
      console.log(item_type+"=="+option_value);
      if(option_value==item_type)
      {
        $(this).attr("selected","selected");
      }

    });
}

function saveEditedItem(elem,sel)
{
  if(sel==1)
  {
    $("#edit_item").modal("hide");
  }else if(sel==2)
  {
      var intItem_id = $(elem).parent().parent().parent().find("#htxtItem_id").val();
      var chrItem_name = $(elem).parent().parent().parent().find("#txtChrItem_name").val();
      var chrItem_type = $(elem).parent().parent().parent().find("#slctItem_category").val();

      var post_url = base_url+'admin/itemcontroller/saveItemUpdate';

      $.post(post_url,{
        intItem_id   : intItem_id,
        chrItem_name : chrItem_name,
        chrItem_type : chrItem_type
      },function(result)
      {

        var result = $.parseJSON(result);
        if(result.success==true || result.success=="true")
        {
          $("#edit_item").modal("hide");
          $("#"+intItem_id).find("#s_job_item_name").empty().append(chrItem_name);
          $("#"+intItem_id).find("#s_job_item_type").empty().append(chrItem_type);
          $("#edit-item-form *").find(":input, select").each(function()
            {
              $(this).val("");
            });
        }

      });
  }


}
function showConfirmDeleteDialog(elem,selector)
{
  if(selector==2) //item
  {
    var item_name = $(elem).attr("theName");
    var item_id = $(elem).attr("theItemID");
    $("#cancel-delete-item").attr("theItemID",item_id);
    $("#persist-delete-item").attr("theItemID",item_id);

    $("#delete_item").find("#modal-item-name").empty().append(item_name);
    $("#delete_item").modal("show");

  }else if(selector==1)//supplier
  {
    var supplier_id=$(elem).attr("theSupplierID");
    var supplier_name=$(elem).attr("theName");
    var contact_number = $(elem).attr("theContactNumber");
    var contact_person = $(elem).attr("theContactPerson");
    var contact_email = $(elem).attr("theContactEmail"); //
    $("#cancel-delete-supplier").attr("theSupplierID",supplier_id);
    $("#persist-delete-supplier").attr("theSupplierID",supplier_id);
     $("#delete_supplier").find("#modal-supplier-name").empty().append(supplier_name);
    $("#delete_supplier").modal("show");
  }

}
function cancelDelete(elem,selector)
{
  if(selector==2)//item
  {
    $("#delete_item").modal("hide");
  }else if(selector==1)//supplier
  {
    $("#delete_supplier").modal("hide");
  }else if(selector==3)
  {
    $("#deleteCurtainModal").modal("hide");
  }else if(selector==4)
  {
    $("#delete-supplier-swatch-modal").modal("hide");
  }else if (selector == 5)
  {
    $("#delete-supplier-swatch-modal").modal("hide");
  }

}

function persistDelete(elem,selector)
{
  if(selector==2)//item
  {
    var id = $(elem).attr("theItemID");
      var post_url = base_url+"admin/itemcontroller/softDeleteItem";
      $.post(post_url,
        {
          item_id:id
        },function(result){
          var result = $.parseJSON(result);
          if(result.success==true || result.success=="true")
          {
            $("#"+id).hide();
            $("#delete_item").modal("hide");
            location.reload();
          }else
          {
            console.log("error on updating");
          }
        });
  }else if(selector==1)//supplier
  {
    var id = $(elem).attr("theSupplierID");
     var post_url = base_url+"admin/supplier/softDeleteSupplier";
     console.log(id);
    $.post(post_url,
      {
        supplier_id:id
      },function(result){
        var result = $.parseJSON(result);
        if(result.success==true || result.success=="true")
        {
          $("#supplier-list-body").find("#sup-"+id).hide();
          $("#delete_supplier").modal("hide");
          location.reload();
        }else
        {
          console.log("error on updating");
        }
      });

  }else if(selector==3)
  {
    var id = $(elem).attr("theID");
    var post_url = base_url+"admin/curtaincontroller/deleteType/"+id;

    $.post(post_url,{},function(result)
      {
        var result = $.parseJSON(result);
        $(".tr-type-id-"+result.result_id).hide();
        $("#deleteCurtainModal").modal("hide");
        location.reload();
      });
  }else if(selector == 4)
  {
    var id = $(elem).attr("theID");
    var post_url = base_url + "admin/supplier/deleteSupplierSwatch/"+id;

    $.post(post_url,{}, function(result)
      {
        var result = $.parseJSON(result);
        $("#co-"+id).hide();
        $("#delete-supplier-swatch-modal").modal("hide");
          
        location.reload();
      });
  }else if(selector == 5)
  {
    console.log("here!..");
    var id = $(elem).attr("theID");
    var post_url = base_url + "admin/curtaincontroller/deleteCurtainSwatch";

    $.post(post_url,{id:id}, function(result)
      {
        var result = $.parseJSON(result);

        $(".curtain-swatch-"+id).hide();
        $("#delete-supplier-swatch-modal").modal("hide");
        location.reload();
      });
  }

}

function showEditSupplierModal(elem)
{
  var supplier_id=$(elem).attr("theSupplierID");
  var supplier_name=$(elem).attr("theName");
  var contact_number = $(elem).attr("theContactNumber");
  var contact_person = $(elem).attr("theContactPerson");
  var contact_email = $(elem).attr("theContactEmail"); //

  $("#edit_supplier_form").modal("show");

  $("#edit_supplier_form").find("#edit_supp_name").val("").val(supplier_name);
  $("#edit_supplier_form").find("#edit_supp_contact_name").val("").val(contact_person);
  $("#edit_supplier_form").find("#edit_supp_contact_num").val("").val(contact_number);
  $("#edit_supplier_form").find("#edit_supp_email").val("").val(contact_email);
  $("#edit_supplier_form").find("#edit_supplier_id").val("").val(supplier_id);

}

function editSupplier(elem,sel)
{
  if(sel==1)
  {
    $("#edit_supplier_form").modal("hide");
  }else if(sel==2)
  {

    var chrSupplier_name  = $("#edit_supplier_form").find("#edit_supp_name").val();
    var chrContact_name   = $("#edit_supplier_form").find("#edit_supp_contact_name").val();
    var chrContact_number = $("#edit_supplier_form").find("#edit_supp_contact_num").val();
    var chrSupplier_email = $("#edit_supplier_form").find("#edit_supp_email").val();
    var intSupplier_id    = $("#edit_supplier_form").find("#edit_supplier_id").val();
    /**
    console.log(chrSupplier_name);
    console.log(chrContact_name);
    console.log(chrContact_number);
    console.log(chrSupplier_email);
    console.log(intSupplier_id); **/

    var post_url = base_url+"admin/supplier/updateSupplierDetails";

    $.post(post_url,
      {
        chrSupplier_name : chrSupplier_name,
        chrContact_name   : chrContact_name,
        chrContact_number : chrContact_number,
        chrSupplier_email : chrSupplier_email,
        intSupplier_id    : intSupplier_id
      },function(result)
      {
        var result = $.parseJSON(result);
        if(result.success==true ||result.success=="true")
        {
          $("#edit_supplier_form").modal("hide");
          $("#sup-"+intSupplier_id).find("#s-supplier-name").empty().append('<a href="#" target="_blank">'+chrSupplier_name+'</a>');
          $("#sup-"+intSupplier_id).find("#s-contact-person").empty().append(chrContact_name);
          $("#sup-"+intSupplier_id).find("#s-contact-number").empty().append(chrContact_number);
          $("#sup-"+intSupplier_id).find("#s-contact-email").empty().append(chrSupplier_email);
          $("#edit-supplier-form *").find(":input").each(function()
            {
              $(this).val("");
            });
        }else
        {
          console.log("error on adding");
        }
      });
  }
}

function getSubType(elem)
{

  var type = $(elem).val();
  $("#sub_type_input option").not('option:eq(0)').remove();
  var get_url = base_url+"admin/supplier/getSubType/"+type;

  $.get(get_url,function(result)
      {
        for(var i=0; i<result.length;i++)
        {
          console.log(result);
          $("#cmbIntSupplier_swatch_sub_type").append('<option value="'+result[i].id+'">'+result[i].job_type_name+'</option>');
        }

      },'json');
}

function supplierAddSwatch(elem)
{
  var intSwatch_type = $(elem).parent().parent().find("#cmbIntSupplier_swatch_type").val();
  var intSwatch_sub_type = $(elem).parent().parent().find("#cmbIntSupplier_swatch_sub_type").val();
  var chrSwatch_name = $(elem).parent().parent().find("#txtChrSwatch_name").val();
  var chrSwatch_type_code = $(elem).parent().parent().find("#txtChrSwatch_type_code").val();
  var intSwatch_width = $(elem).parent().parent().find("#txtChrSwatch_width").val();
  var fltSwatch_price_per_meter = $(elem).parent().parent().find("#txtChrSwatch_pricer_per_meter").val();
  var intSupplier_id = $(elem).parent().parent().find("#htxtIntSupplier_id").val();

  /**  ---- DO NOT DELETE!. use for future debugging ---
  console.log(intSwatch_type);
  console.log(intSwatch_sub_type);
  console.log(chrSwatch_name);
  console.log(chrSwatch_type_code);
  console.log(intSwatch_width);
  console.log(fltSwatch_price_per_meter);
  console.log(intSupplier_id);
  ***/

  var post_url = base_url+"admin/supplier/addSwatch";

  $.post(post_url,
    {
      intSupplier_id:intSupplier_id,
      intSwatch_type:intSwatch_type,
      intSwatch_sub_type:intSwatch_sub_type,
      chrSwatch_name:chrSwatch_name,
      chrSwatch_type_code:chrSwatch_type_code,
      intSwatch_width:intSwatch_width,
      fltSwatch_price_per_meter:fltSwatch_price_per_meter
    },function(result)
    {

      if(result.success=='true' || result.success==true)
      {
        console.log(result);
        var clone  = $(".clone-origin").clone();
        clone.attr("id","co-"+result.result_id);
        clone.find(".co-swatch-type-code").html(chrSwatch_type_code);
        clone.find(".co-swatch-width").html(intSwatch_width);
        clone.find(".co-swatch-cost").html(fltSwatch_price_per_meter);
        console.log(clone);
        $("#supplier-swatches-body").append(clone);
        clone.show();
        $("#add-supplier-dialog").modal("hide");
      }
    },'json');

}

function addType(elem)
{
  var chrType_name = $(elem).parent().parent().find("#txtChrType_name").val();
  var post_url = base_url+"admin/curtaincontroller/addType";
  $.post(post_url,
    {
      chrType_name:chrType_name
    },function(result)
    {
      if(result.success=="true" || result.success==true)
      {
        var clone = $("#type-origin").clone();
        clone.attr("class","tr-type-id-"+result.result_id);
        clone.attr("id","type-"+result.result_id);
        clone.find("#type_id").html(result.result_id).attr("class","type-id-"+result.result_id);
        clone.find("#type_name").html("<a href='"+base_url+"admin/curtaincontroller/typesSubType/"+result.result_id+"' target='_blank'>"+chrType_name+"</a>").attr("class","type-name-"+result.result_id);
        $(".type-table-body").append(clone);
        clone.show();
        console.log(clone);
        $("#addType").modal("hide");
        location.reload();
      }
    },'json');
}

function addSwatch(elem)
{
  var chrSwatch_name            = $(elem).parent().parent().find("#txtChrSwatch_name").val();
  var chrSwatch_type_code       = $(elem).parent().parent().find("#txtChrSwatch_type_code").val();
  var intSwatch_width           = $(elem).parent().parent().find("#txtChrSwatch_width").val();
  var fltSwatch_price_per_meter = $(elem).parent().parent().find("#txtChrSwatch_pricer_per_meter").val();
  var intSupplier_id            = $(elem).parent().parent().find("#cmbIntSupplier_id").val();
  var main_sub_type_id          = $("#htxtMain_type_id").val();
  var sub_type_name = $("#htxtMain_type_id").attr("theName");
  var min_width_panel = 0;
  if (sub_type_name.toLowerCase() == "blinds")
  {
    min_width_panel = $(elem).parent().parent().find("#intBlindsMinWidth").val();
  }
  /**
  console.log(chrSwatch_name);
  console.log(chrSwatch_type_code);
  console.log(intSwatch_width);
  console.log(fltSwatch_price_per_meter);
  console.log(intSupplier_id);
  console.log(main_sub_type_id); **/
  var post_url = base_url+"admin/curtaincontroller/addSwatch";

  $.post(post_url,
      {
        chrSwatch_name :chrSwatch_name,
        chrSwatch_type_code:chrSwatch_type_code,
        intSwatch_width:intSwatch_width,
        fltSwatch_price_per_meter:fltSwatch_price_per_meter,
        intSupplier_id:intSupplier_id,
        main_sub_type_id:main_sub_type_id,
        min_width_panel : min_width_panel
      },function(result)
      {
        if(result.success==true || result.success=="true")
        {
          $("#add-new-style").modal("hide");
          location.reload();

        }
      },'json');

}

function showEditCurtainModal(elem)
{
  var typeID = $(elem).attr("theID");
  var theName = $(elem).attr("theName");
  $(".curtain-type-name").empty().val(theName);
  $(".update-curtain-confirm").attr("theID",typeID);
  $("#editCurtainModal").modal("show");
}

function saveCurtainEdit(elem)
{
  var newName = $(elem).parent().parent().find(".curtain-type-name").val();
  var ID = $(elem).attr("theID");

  var post_url = base_url+"admin/curtaincontroller/updateType";

  $.post(post_url,
    {
      intTypeID : ID,
      chrTypeName : newName
    }, function(result)
    {
      var result = $.parseJSON(result);
      $(".tr-type-id-"+result.result_id).find(".type-name-"+result.result_id).empty().html("<a href='"+base_url+"admin/curtaincontroller/typesSubType/"+result.result_id+"' target='_blank'>"+newName+"</a>");
      $("#editCurtainModal").modal("hide");
      location.reload();
    });
}

function showCurtainDeleteModal(elem)
{
  var typeID = $(elem).attr("theID");
  var typeName = $(elem).attr("theName");
  $("#persist-delete-type").attr("theID",typeID);
  $("#persist-delete-type").attr("theName",typeName);
  $("#modal-type-name").empty().html(typeName);
  $("#deleteCurtainModal").modal("show");

}

function showEditSupplierSwatchModal(elem)
{
  var id  = $(elem).attr("theID");
  var name = $(elem).attr("theName");
  var type_code = $("#co-"+id).find(".co-swatch-type-code").html();
  var type_width = $("#co-"+id).find(".co-swatch-width").html();
  var type_cost = $("#co-"+id).find(".co-swatch-cost").html();
  /**
  console.log(id);
  console.log(name);
  console.log(type_code);
  console.log(type_width);
  console.log(type_cost); **/
  $("#edit_swatch_name").empty().val(name);
  $("#edit_swatch_code").empty().val(type_code);
  $("#edit_swatch_width").empty().val(type_width);
  $("#edit_swatch_cost").empty().val(type_cost);
  $("#persist-edit-swatch").attr("theID",id);
  $("#cancel-edit-swatch").attr("theID",id);
  $("#edit-supplier-swatch-modal").modal("show");
}

function confirmEditSwatch(elem)
{
  var id = $(elem).attr("theID");
  var new_swatch_name = $("#edit_swatch_name").val();
  var new_swatch_code = $("#edit_swatch_code").val();
  var new_swatch_width = $("#edit_swatch_width").val();
  var new_swatch_cost = $("#edit_swatch_cost").val();

  var post_url = base_url+"admin/supplier/updateSupplierSwatch";

  $.post(post_url,
      {
        id : id,
        new_swatch_name : new_swatch_name,
        new_swatch_code : new_swatch_code,
        new_swatch_width: new_swatch_width,
        new_swatch_cost : new_swatch_cost
      }, function(result) {
        var result = $.parseJSON(result);
        if(result.success=='true'||result.success==true){
            $("#co-"+id).find(".co-swatch-type-code").html(new_swatch_code);
            $("#co-"+id).find(".co-swatch-width").html(new_swatch_width);
            $("#co-"+id).find(".co-swatch-cost").html(new_swatch_cost);
            $("#edit-supplier-swatch-modal").modal("hide");
        }
      });
}

function showDeleteSupplierSwatchModal(elem)
{
  var swatch_id = $(elem).attr("theID");
  var swatch_name = $(elem).attr("theName");
  $("#modal-swatch-code").empty().html(swatch_name);
  $("#persist-delete-swatch").attr("theID",swatch_id);
  $("#delete-supplier-swatch-modal").modal("show");
}

function viewEditCurtainSwatchModal(elem)
{
  var swatch_id = $(elem).attr("theID");
  var sub_type_name = $(elem).attr("theName");
  var chrSwatch_name = $(".curtain-swatch-"+swatch_id).find(".cu-sw-name").html();
  var chrSwatch_code = $(".curtain-swatch-"+swatch_id).find(".cu-sw-code").html();
  var chrSwatch_width = $(".curtain-swatch-"+swatch_id).find(".cu-sw-width").html();
  var chrSwatch_cost = $(".curtain-swatch-"+swatch_id).find(".cu-sw-cost").html();
  var intSwatch_supplier = $(".curtain-swatch-"+swatch_id).find(".cu-sw-supplier").attr("theSupplier_id");
  
  if (sub_type_name.toLowerCase() == "blinds")
  {
    var min_width = $(".curtain-swatch-"+swatch_id).find(".cu-sw-min-width").html();
    $("#intBlindsMinWidth_edit").empty().val(min_width);
  }
  /********************
  console.log(swatch_id);
  console.log(chrSwatch_name);
  console.log(chrSwatch_code);
  console.log(chrSwatch_width);
  console.log(chrSwatch_cost);
  console.log(intSwatch_supplier);
  *********************/
  //console.log(intSwatch_supplier);
  $("#txtChrSwatch_name_edit").empty().val(chrSwatch_name);
  $("#txtChrSwatch_type_code_edit").empty().val(chrSwatch_code);
  $("#txtChrSwatch_width_edit").empty().val(chrSwatch_width);
  $("#txtChrSwatch_pricer_per_meter_edit").empty().val(chrSwatch_cost);
  $("#cmbIntSupplier_id_edit > option").each(function(i){
      var supplier_id = $(this).val();
      if(supplier_id == intSwatch_supplier)
      {
        $(this).attr("selected","selected");
      }
    });
  //console.log(swatch_id);
  $(".confirm-curtain-swatch-update").attr("theID",swatch_id);
  $("#edit-curtain-swatch-modal").modal("show");

}

function confirmCurtainSwatchEdit(elem)
{
  var swatch_id = $(elem).attr("theID");
  var sub_type_name = $(elem).attr("theName");

  var swatch_name = $("#txtChrSwatch_name_edit").val();
  var swatch_code = $("#txtChrSwatch_type_code_edit").val();
  var swatch_width = $("#txtChrSwatch_width_edit").val();
  var swatch_cost = $("#txtChrSwatch_pricer_per_meter_edit").val();
  var swatch_supplier_id = $("#cmbIntSupplier_id_edit").val();
  var swatch_supplier_name = $("#cmbIntSupplier_id_edit").text();
  var min_width = 0;

  if (sub_type_name.toLowerCase() == "blinds")
  {
    min_width = $("#intBlindsMinWidth_edit").val();
  }

  var post_url = base_url + "admin/curtaincontroller/updateCurtainSwatch";

  $.post(post_url,
    {
      swatch_id : swatch_id,
      swatch_name : swatch_name,
      swatch_code : swatch_code,
      swatch_width : swatch_width,
      swatch_cost : swatch_cost,
      swatch_supplier_id : swatch_supplier_id,
      min_width:min_width
    }, function(result)
    {
      var result = $.parseJSON(result);
      $(".curtain-swatch-"+swatch_id).find(".cu-sw-name").empty().html(swatch_name);
      $(".curtain-swatch-"+swatch_id).find(".cu-sw-code").empty().html(swatch_code);
      $(".curtain-swatch-"+swatch_id).find(".cu-sw-width").empty().html(swatch_width);
      $(".curtain-swatch-"+swatch_id).find(".cu-sw-cost").empty().html(swatch_cost);
       $(".curtain-swatch-"+swatch_id).find(".cu-sw-supplier").attr("href",base_url+"admin/supplier/supplier_swatches/"+swatch_supplier_id);
       $("#cmbIntSupplier_id_edit > option").each(function(i){
          var supplier_id = $(this).val();
          if(supplier_id == swatch_supplier_id)
          {
             $(".curtain-swatch-"+swatch_id).find(".cu-sw-supplier").empty().html($(this).text());
          }
       });
       if (sub_type_name.toLowerCase() == "blinds")
          {
            $(".curtain-swatch-"+swatch_id).find(".cu-sw-min-width").empty().html(min_width);
          }
      $("#edit-curtain-swatch-modal").modal("hide");

    });

}

function viewDeleteCurtainSwatchModal(elem)
{
  var swatch_id = $(elem).attr("theID");
  var swatch_name = $(elem).attr("theName");
  $("#persist-delete-supplier").attr("theID",swatch_id);
  $("#modal-curtain-swatch-name").empty().html(swatch_name);
  $("#delete-supplier-swatch-modal").modal("show");
}

function showUserDeleteModal(elem)
{
  var intUserID = $(elem).attr("theID");
  var chrUserFullName = $(elem).attr("theName");
  console.log(chrUserFullName);
  $("#deleteUserModal").find('.del_user_name').empty().html(chrUserFullName);
  $("#deleteUserModal").find('.delete-user-confirm').attr("theID",intUserID);
  $("#deleteUserModal").modal("show");
}

function deleteUser(elem)
{
  var intUserID = $(elem).attr("theID");
  console.log(intUserID);

  var post_url = base_url+ "admin/user/deleteUser/"+intUserID;

  $.post(post_url, function(result)
    {
      var result = $.parseJSON(result);
      console.log(result);
      if(result.status == "true")
      {
        location.reload();
      }
    });
}



