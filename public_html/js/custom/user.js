"use strict"
var base_url = $('html').attr('base-url'); 

$(document).ready( function () {



});

function validate()
{
    var password  = $("#txtChrUser_password").val();
    var confirm_password  = $("#txtChrUser_password_confirm").val();

    if (password != confirm_password)
    {
        $("#passwordDontMatch").show();
        return false;
    }

    
}

function updateValidate()
{

	var password = $("#edit_txtChrUser_password").val();
	var confirm_password = $("#edit_txtChrUser_password_confirm").val();
	
	if (password != "" && password != confirm_password )
    {
        $(".passwordDontMatch").show();
        return false;
    }
}

function getMenuData(elem)
{
    var menu_id = $(elem).attr("theMenuID");

    var post_url = base_url + "admin/menu/menuInformation/"+menu_id;

    $.post(post_url, function(result) {
        var res = $.parseJSON(result);
        var data = res[0];
        var mod = $("#viewMenuDetailModal");
        mod.find("#m_menu_id").val("").val(data.id);
        mod.find("#m_display_name").val("").val(data.display_name);
        mod.find("#m_slug").val("").val(data.slug);
        mod.find("#m_icon_class").val("").val(data.icon_class);
        $("#viewMenuDetailModal").modal("show");
    });
}
function saveChanges(elem) 
{ 
    var mod = $("#viewMenuDetailModal");
    var menu_id = mod.find("#m_menu_id").val();
    var menu_display_name = mod.find("#m_display_name").val();
    var menu_slug = mod.find("#m_slug").val();
    var menu_icon_class = mod.find("#m_icon_class").val();

    var post_url = base_url + "admin/menu/saveChanges";

    $.post(post_url , {
                        menu_id : menu_id,
                        display_name: menu_display_name,
                        slug : menu_slug,
                        icon_class : menu_icon_class
                        } , function (result)
                        {
                            var res = $.parseJSON(result);
                            if (res.success == true)
                            {
                                $(".menu_id_tag_"+menu_id).parent().find(".menu_name").val("").val(menu_display_name);
                                $("#viewMenuDetailModal").modal("hide");
                                window.location.reload();
                            } else {
                                $("._menu_update_err").show();
                            }
                        });
}

function closeMenuModal()
{
    $("#viewMenuDetailModal").modal("hide");
    $("._menu_update_err").hide();
}

/*
* script for uploading picture
 */
 function toggleDownButton(elem)
 {
    $(elem).parent().parent().find(".toggle-upload").css();
 }
 function toggleUploadButton(elem)
 {
    $(elem).parent().parent().find(".toggle-upload").css("visibility","visible");
 }

 var postSrc = "";
 $(document).ready( function () {
    $(".toggle-upload").change( function () {
        postSrc = $('.preview-image-panel').attr('src');
         readURL(this);
    });
 });
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            console.log(e.target.result);
            $('.preview-image-panel').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
        $(".saveNewImage").css("visibility","visible");
        $(".removeNewImage").css("visibility","visible");
    }
}
function removeImage(elem)
{
    $(elem).parent().find(".preview-image-panel").attr("src",postSrc);
}
function saveImagetoDB(elem)
{
    var image_src = $('.preview-image-panel').attr('src');
    var user_id = $(elem).parent().find("#_img_user_id").val();
    var post_url = base_url + "admin/user/uploadImage";
    $.post(post_url  , {image_src:image_src,user_id:user_id} , function (res) {
        var res = $.parseJSON(res);
        console.log(res);
        if (res.success == true)
        {
              $(".saveNewImage").css("visibility","hidden");
              $(".removeNewImage").css("visibility","hidden");
        }
    });
}
 /*
 end scripts
  */