"use strict"
var base_url = $('html').attr('base-url');      
$(window).load(function() {
    var post_url = base_url + "admin/dashboard/transactionActivity";
    var list = $(".dashboard-list-unique");
    
    $.post(post_url , function (result) {
        var transactions = $.parseJSON(result);
        console.log(transactions);
        for (var i = 0; i < transactions.transactions.length; i++) {
            var clone  = list.find(".list-src").clone();
            clone.removeClass("list-src");
            var app = transactions.transactions[i]
            clone.find(".users_name").html(app.u_fname+"  " + app.u_lname);
            clone.find(".users_name").attr("href",base_url + "admin/user/updateUser/"+app.USERS_ID);
            clone.find(".date").html(app.job_trans_created);
            clone.find(".transaction_id").html(app.id);
            clone.find(".transaction_id").parent().attr("href",base_url+ "admin/joborder/edit/"+app.TRANSACTION_ID);

            var status = "Pending";
            if (app.job_trans_status == 0)
            {
                status = "Done";
            }
            clone.find(".trans_status").html(status);
            list.append(clone);
            clone.show();
        };
    });
});

$(document).ready( function () {


    if($('#calendar').length!=0)
    {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events: initialization,
            eventClick: function(calEvent, jsEvent, view) {
                            //window.open(base_url+"admin/calendar/event/"+calEvent.id);
                            //na.a ko gi change diri, peter ni
                            window.location.href=base_url+"admin/calendar/event/"+calEvent.id+"/"+calEvent.customer_id;

                        },            
            eventLimit: 2,
            eventLimitText: "more"
        });        
        // $('#calendar').fullCalendar({
        //     header: {
        //         left: 'prev,next today',
        //         center: 'title',
        //         right: 'month,basicWeek,basicDay'
        //     },
        //     height:'auto',                        
        //     events: initialization,
        //     eventClick: function(calEvent, jsEvent, view) {
        //                     window.open(base_url+"admin/calendar/event/"+calEvent.id);
        //                 },
        //     eventLimit:2        
        // });
        //$('#calendar').find('.fc-toolbar').find('.fc-button').addClass('btn btn-primary').removeClass('fc-button fc-state-default');
    }

} );

//load dashboard events
//fetch the transaction events
function initialization(start, end, timezone, callback)
{
    //fetch the transaction events dynamically
    $.post(base_url+"admin/user/userSchedule",function(data){
        try{
            data = $.parseJSON(data);
            console.log(data.schedules);
            console.log(data.schedules.length);
            var tmp_events  = [];
            var events      = [];
            var events_data = data.schedules;
            if(data.schedules.length!=0)
            {
                for(var i=0; i<events_data.length; i++)
                {
                    tmp_events = events_data[i];
                    events.push({
                                    title: tmp_events.user_fname+' - '+tmp_events.sales_fname,
                                    start: tmp_events.date,
                                    id   : tmp_events.id,
                                    // try2x rani by peter dungag ug customer ID
                                    customer_id:tmp_events.customer_id
                                });
                }
            }
            callback(events);
        }catch(e){
            console.log(e);
        }
    })
    //catch fail
    .fail(function(x){
        alert("Events could not be loaded! Please ensure that your device is connected to the internet. To reload the event, click the 'refetch' button.");
        console.log("error : calendar initialization error");
        console.log(x.responseText);
    });
}

function scheduleList ()
{
	var post_url = base_url + "admin/user/userSchedule";
	var test;
	$.post(post_url ,{}, function (result) {
		test = $.parseJSON(result);
	});
	return test;
}


