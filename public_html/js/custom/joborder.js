"use strict"
var base_url = $('html').attr('base-url'); 
// +==============================================+ //
// +==== load scripts upon loading the module ====+ //
// +==============================================+ //
$(document).ready(function() 
{

	//date picker on the set appointment modal
	
	$("#chrDateAppointment_date").datetimepicker({
            lang:'ch',
            timepicker:false,
            format:'d/m/Y',
            formatDate:'Y/m/d'
            
        });

	//timepicker on the set appointment modal
	$("#chrTimeAppointment_time").datetimepicker({
              datepicker:false,
              format:'H:i'
            });

	// job order modal
	$('#newWindow').off('show.bs.modal').on('show.bs.modal', function () {
		var propID 	 = $('#propoertyID').val();
		var itemType = $('#selector').val(); 
		$('#newWindow').attr('asset_prop_id',propID);
		$('#newWindow').attr('itemType',itemType);
	});

	// process when the modal is close
	$('#newWindow').on('hidden.bs.modal', function (event) {
        event.preventDefault();
        var propID 		= $('#newWindow').attr('asset_prop_id');
        var asset_type 	= $('#newWindow').attr('itemType');
        var location 	= $('#newWindow').find('#newLocation').val();
        var room 		= $('#newWindow').find('#newRoom').val();
        var short_desc	= $('#newWindow').find('#newDesc').val();
        //var newFH		= $('#newWindow').find('#newFH').val();
        var newFH 		= 0;
        var newWH		= $('#newWindow').find('#newWH').val();
        var newWW		= $('#newWindow').find('#newWW').val();

        var newWindow  = [];
        newWindow.push({
			'asset_type':asset_type,
			'propID'	:propID,
			'location'	:location,
			'room'		:room,
			'short_desc':short_desc,
			'fh'		:newFH,
			'wh'		:newWH,
			'ww'		:newWW
		});

        var post_url 	= base_url + 'admin/joborder/save_update_assetdtl/2';
        $.post(post_url, {newWindow:newWindow}, function(result) {
		    // try {
			      	var result = $.parseJSON(result);
			      	if(result.success == true)
			      	{
			      		console.log(result.data);
			      		// if curtain is added
			      		if(result.data.asset_type == '1')
			      		{
			      			var toclone = $('#property_window table#windowTable tr.tableItems:eq(0)').clone();
			      			toclone.find('.joborderTable_style.location').empty().removeClass('joborderTable_style').append(result.data.location);
			      			toclone.find('.joborderTable_style.room').empty().removeClass('joborderTable_style').append(result.data.room);
			      			toclone.find('.joborderTable_style.short_desc').empty().removeClass('joborderTable_style').append(result.data.short_desc);
			      			toclone.find('.joborderTable_style.fh').empty().removeClass('joborderTable_style').append(result.data.full_height);
			      			toclone.find('.joborderTable_style.wh').empty().removeClass('joborderTable_style').append(result.data.window_height);
			      			toclone.find('.joborderTable_style.ww').empty().removeClass('joborderTable_style').append(result.data.window_width);
			      			toclone.attr('assetDtlid',result.data.id);
			      			$('#property_window tbody.thetbody').append(toclone.show());
			      		}
			      		// if sofa is added
			      		else if(result.data.asset_type == '2')
			      		{
			      			var toclone = $('#property_sofa table#sofaTable tr.tableItems:eq(0)').clone();
			      			toclone.find('.joborderTable_style.location').empty().removeClass('joborderTable_style').append(result.data.location);
			      			toclone.find('.joborderTable_style.room').empty().removeClass('joborderTable_style').append(result.data.room);
			      			toclone.find('.joborderTable_style.short_desc').empty().removeClass('joborderTable_style').append(result.data.short_desc);
			      			toclone.find('.joborderTable_style.fh').empty().removeClass('joborderTable_style').append(result.data.full_height);
			      			toclone.find('.joborderTable_style.wh').empty().removeClass('joborderTable_style').append(result.data.window_height);
			      			toclone.find('.joborderTable_style.ww').empty().removeClass('joborderTable_style').append(result.data.window_width);
			      			toclone.attr('assetDtlid',result.data.id);
			      			$('#property_sofa tbody.thetbody').append(toclone.show());
			      		}
			      	}
		    // 	}
		    // catch(e) {
		    //   		console.log(result);
		    // 	}
		})
		.fail(function(test) {
			alert(test.responseText);
		});
    	
	});

	// when the modal is closes
	$('#createnew_joborder').on('hidden.bs.modal', function (event) {
        event.preventDefault();
    	$('#windowTable tbody.thetbody').empty();
    	$('#sofaTable tbody.thetbody').empty();
        $('#createnew_joborder').removeData('bs.modal');
	});

	$('#newWindow').off('hidden.bs.modal', function (event) {
        event.preventDefault();
        $('#newWindow').removeData('bs.modal');
	});

});
// +==============================================+ //
// +==== joborder processing functionalities =====+ //
// +==============================================+ //

function getpropID(value)
{
	var theID = $(value).attr('prop_id'); // property id
	$('#createnew_joborder').find('input.test').empty().val(theID);
	var post_url	= base_url + 'admin/joborder/fetching_asset_dtl';

    	$.post(post_url, {property_id:theID}, function(result) {
		    try {
		      	var result = $.parseJSON(result);
		        if(result.success == true || result.data != null) {
					var length = result.data.length;
					for(var i = 0; i < length; i++) {
						if(result.data[i].asset_type == 1) // get all the curtain items
						{
							var toClone = $('#createnew_joborder div#property_window').find('.tableItems:eq(0)').clone();

							toClone.find('.joborderTable_style.location').empty().removeClass('joborderTable_style').append(result.data[i].location);
							toClone.find('.joborderTable_style.room').empty().removeClass('joborderTable_style').append(result.data[i].room);
							toClone.find('.joborderTable_style.short_desc').empty().removeClass('joborderTable_style').append(result.data[i].short_desc);
							toClone.find('.joborderTable_style.fh').empty().removeClass('joborderTable_style').append(result.data[i].full_height);
							toClone.find('.joborderTable_style.wh').empty().removeClass('joborderTable_style').append(result.data[i].window_height);
							toClone.find('.joborderTable_style.ww').empty().removeClass('joborderTable_style').append(result.data[i].window_width);
							toClone.attr('assetDtlid',result.data[i].id);
						 	$('#windowTable tbody.thetbody').append(toClone.show());
						}
						else if(result.data[i].asset_type == 2) // get all the sofa items
						{
							var toClone = $('#createnew_joborder div#property_sofa').find('.tableItems:eq(0)').clone();

							toClone.find('.joborderTable_style.location').empty().removeClass('joborderTable_style').append(result.data[i].location);
							toClone.find('.joborderTable_style.room').empty().removeClass('joborderTable_style').append(result.data[i].room);
							toClone.find('.joborderTable_style.short_desc').empty().removeClass('joborderTable_style').append(result.data[i].short_desc);
							toClone.find('.joborderTable_style.fh').empty().removeClass('joborderTable_style').append(result.data[i].full_height);
							toClone.find('.joborderTable_style.wh').empty().removeClass('joborderTable_style').append(result.data[i].window_height);
							toClone.find('.joborderTable_style.ww').empty().removeClass('joborderTable_style').append(result.data[i].window_width);
							toClone.attr('assetDtlid',result.data[i].id);
						 	$('#sofaTable tbody.thetbody').append(toClone.show());
						}
					}
		         }
		         else 
	         	{
	         		$('#windowTable tbody.thetbody').append('<tr class="noResult"><td colspan="4">No curtain has been recorded for this property.</td></tr>');
	         		$('#sofaTable tbody.thetbody').append('<tr class="noResult"><td colspan="4">No sofa has been recorded for this property.</td></tr>');
	         	}
		    }
		    catch(e) {
		      	console.log(result);
		    }
		})
		.fail(function(test) {
			alert(test.responseText);
		});
}

// remove item in the window
function remove_item(value, pointer)
{
	$(value).closest(pointer).remove();
}

// searching customer
function search_customer(value,sel)
{
	var keyword 	= $(value).parent().parent().find('#theCustomer').val();
	var selector 	= sel;
	var keyMatch 	= 'cus_name';
	var post_url	= base_url + 'admin/joborder/search_module';

	// start the process
	if($.trim(keyword).length == 0) {
		$(value).parent().parent().parent().parent().find('.showResult').slideUp( "slow", function() {
			$('#customer_id').val('');
		});
	}
	else
	{
		$.post(post_url, {keyword:keyword,selector:selector,keyMatch:keyMatch}, function(result) {
		    try {
			      	var result = $.parseJSON(result);
			      	var theResult = "";

				        if(result.success == true || result.data != null) {
				      		$(value).parent().parent().parent().parent().find(".showResult").slideDown( "slow", function() {
				          	   var length = result.data.length;

					           for(var i = 0; i < length; i++) {
					             	theResult = '<li onclick="selected_item(this,1);" class="list-group-item text-right" theName="'+result.data[i].cus_name+'" '+
					             	'theID="'+result.data[i].id+'">'+result.data[i].cus_name+'&nbsp;&nbsp;&nbsp;&nbsp;<i style="color:#EBC137;" class="fa fa-arrow-circle-o-up"></i></li>';
					             	$(value).parent().parent().parent().parent().find('.showResult ul').empty().append(theResult);
					           }
					        });
				         }

						 else {
							return false;
						 }
					
		    	}
		    catch(e) 
		    	{
		      		console.log(result);
		    	}
		})
		.fail(function(test) {
			alert(test.responseText);
		});	
	}
}

// searching customer's contact person
function search_customer_contact(value, sel)
{
	var keyword 	= $(value).parent().parent().find('.contact_key').val();	// the keyword to search
	var selector 	= sel;						// 2 value means querying in the customer contact
	var keyMatch 	= 'name';					// what column to match in the keyword
	var whereCol	= 'customer_id';			// fliter the search in this column
	var where 		= $('.customerID').val();	// search and filter in this value
	var post_url	= base_url + 'admin/joborder/search_module';

	// start the process
	if($.trim(keyword).length == 0 || where.length == 0) {
		$(value).parent().parent().parent().parent().find('.showResult').slideUp( "slow", function() {
			$('.customer_contact_id').val('');
			return false;
		});
	}

	else
	{
		$.post(post_url, {keyword:keyword,selector:selector,keyMatch:keyMatch,whereCol:whereCol,where:where}, function(result) {
		    try {
			      	var result = $.parseJSON(result);
			      	var theResult = "";

			      	$(value).parent().parent().parent().parent().find(".showResult").slideDown( "slow", function() {
				        if(result.success == true || result.data != null) {
			          	   var length = result.data.length;
				           $(value).parent().parent().parent().find('.showResult ul').empty();

				           for(var i = 0; i < length; i++) {
				             	theResult = '<li onclick="selected_item(this,2);" class="list-group-item text-right" theName="'+result.data[i].name+'" '+
				             	'theID="'+result.data[i].id+'">'+result.data[i].name+'&nbsp;&nbsp;&nbsp;&nbsp;<i style="color:#EBC137;" class="fa fa-arrow-circle-o-up"></i></li>';
				             	$(value).parent().parent().parent().parent().find('.showResult ul').append(theResult);
				           }
				         }

						 else {
							$('.customer_contact_id').val('');
							return false;
						 }
					 });
		    	}
		    catch(e) 
		    	{
		      		console.log(result);
		    	}
		})
		.fail(function(test) {
			alert(test.responseText);
		});
	}
}

// selected item display
function selected_item(value,selector)
{
	var theID 	= $(value).attr('theID');
	var theName = $(value).attr('theName');

	// when selecting customer
	if(selector == 1)
	{
		if(theID.length == 0)
			alert("Please select customer to proceed");
		else
		{
			$(value).parent().parent().parent().find('.showResult').slideUp( "slow", function() {
				$(value).parent().parent().parent().parent().find('#customer_id').val(theID);
				$(value).parent().parent().parent().parent().find('.thekeyword').val(theName);
			});
		}	
	}

	//when selecting contact person of a customer
	else if(selector == 2)
	{
		if(theID.length == 0)
			alert("Please select customer contact person to proceed");
		else
		{
			$(value).parent().parent().parent().find('.showResult').slideUp( "slow", function() {
				$(value).parent().parent().parent().parent().find('.customer_contact_id').val(theID);
				$(value).parent().parent().parent().parent().find('.contact_key').val(theName);
			});	
		}
	}
}

// ________________________________________________________________________________________________________________________

function get_suppliers(value)
{
	var post_url	= base_url + 'admin/joborder/get_supplier';
	var sub_cat_id	= $(value).parent().find('[name="the_curtain[]"]').val();

	if(sub_cat_id != null)
	{
		$(value).parent().find('.curtain_supplier').slideDown(function() {
			$.post(post_url, {sub_cat_id:sub_cat_id}, function(result) {
			    try {
				      	var result = $.parseJSON(result);
				      	var theResult = "";

				      	console.log(result);
				      	return false;

				      	$(value).parent().parent().find(".showResult").slideDown( "slow", function() {
					        if(result.success == true || result.data != null) {
				          	   var length = result.data.length;
					           $(value).parent().parent().find('.showResult ul').empty();

					           for(var i = 0; i < length; i++) {
					             	theResult = '<li onclick="selected_item(this,2);" class="list-group-item text-right" theName="'+result.data[i].name+'" '+
					             	'theID="'+result.data[i].id+'">'+result.data[i].name+'&nbsp;&nbsp;&nbsp;&nbsp;<i style="color:#EBC137;" class="fa fa-arrow-circle-o-up"></i></li>';
					             	$(value).parent().parent().find('.showResult ul').append(theResult);
					           }
					         }

							 else {
								theResult = '<li style="color:red;" class="list-group-item text-right">Supplier Not Found</li>';
								$(value).parent().parent().find('.showResult ul').empty().append(theResult);
							 }
						 });
			    	}
			    catch(e) 
			    	{
			      		console.log(result);
			    	}
			})
			.fail(function(test) {
				alert(test.responseText);
			});
		});

	}

	else {
		bootbox.alert("<strong>Please select item to load supplier.</strong>");
	}
}

function addNewItem(value, selector)
{
	if(selector == 1)
	{
		var toclone = $(value).parent().find('#property_window table#windowTable tr.tableItems:eq(0)').clone();
		toclone.find('.remover').show();
		$(value).parent().find('#property_window tbody.thetbody tr.noResult').remove();
		$(value).parent().find('#property_window tbody.thetbody').append(toclone.show());
		
	}
	if(selector == 2)
	{
		var toclone = $(value).parent().find('#property_sofa table#sofaTable tr.tableItems:eq(0)').clone();
		toclone.find('.remover').show();
		$(value).parent().find('#property_sofa tbody.thetbody tr.noResult').remove();
		$(value).parent().find('#property_sofa tbody.thetbody').append(toclone.show());
		
	}
}

function switchButton(selector)
{
	if(selector == 1)
	{
		$('#new_itemcurtain').show();
		$('#new_itemsofa').hide();
		$('#selector').empty().val('0');
	}
	if(selector == 2)
	{
		$('#new_itemcurtain').hide();
		$('#new_itemsofa').show();
		$('#selector').empty().val('1');
	}
}

function addDropDown(value)
{
	var dropdownselector = $(value).parent().parent().parent().parent().find('.classdropdown:eq(0)').clone();
	$('.cloned').append(dropdownselector);
	dropdownselector.find('.forclose').show();
	dropdownselector.find('.foradd').hide();
}

function removeDropDown(value) {
	$(value).parent().parent().remove();
}

// event trigger for editable table in customer property create new joborder
function updateData(value) {
	var theValue = $(value).text();
	$(value).removeClass('joborderTable_style').css('background-color','#ECECF0').attr('contenteditable', true);
}
var counter = 0;
function addToTransaction(value)
{
	var inputValue = $(value).parent().find('input').val();
	
	if(inputValue == '0' )
	{
		$(value).parent().find('input').val('1');
		$(value).parent().parent().css('background-color','#E7E7E7');
		$(value).empty().append('<i class="fa fa-dot-circle-o"></i>')
		counter++;
	}
	else
	{
		$(value).parent().find('input').val('0');
		$(value).parent().parent().css('background-color','#fff');
		$(value).empty().append('<i class="fa fa-check-circle"></i>')
		counter--;
	}
	if(counter > 0)
		$('#createBtn').attr("disabled", false);
	else
		$('#createBtn').attr("disabled", true);
}

function SaveJobOrderTransaction()
{
	var contact_id 	 = $('.customer_contact_id').val();
	var property_id  = $('#propoertyID').val();
	var customer_id  = $('.customerID').val();

	// getting curtain data
	var curtain_location 	= $('#property_window table#windowTable tbody.thetbody tr.tableItems').find('.location');
	var curtain_room 		= $('#property_window table#windowTable tbody.thetbody tr.tableItems').find('.room');
	var curtain_short_desc 	= $('#property_window table#windowTable tbody.thetbody tr.tableItems').find('.short_desc');
	var curtain_fh		 	= $('#property_window table#windowTable tbody.thetbody tr.tableItems').find('.fh');
	var curtain_wh		 	= $('#property_window table#windowTable tbody.thetbody tr.tableItems').find('.wh');
	var curtain_ww		 	= $('#property_window table#windowTable tbody.thetbody tr.tableItems').find('.ww');
	var curtain_isinTrans 	= $('#property_window table#windowTable tbody.thetbody tr.tableItems').find('.inTransaction');
	var curtain_assetDTLid 	= $('#property_window table#windowTable tbody.thetbody tr.tableItems');

	// getting sofa data
	var sofa_location 	= $('#property_sofa table#sofaTable tbody.thetbody tr.tableItems').find('.location');
	var sofa_room 		= $('#property_sofa table#sofaTable tbody.thetbody tr.tableItems').find('.room');
	var sofa_short_desc = $('#property_sofa table#sofaTable tbody.thetbody tr.tableItems').find('.short_desc');
	var sofa_fh		 	= $('#property_sofa table#sofaTable tbody.thetbody tr.tableItems').find('.fh');
	var sofa_wh		 	= $('#property_sofa table#sofaTable tbody.thetbody tr.tableItems').find('.wh');
	var sofa_ww		 	= $('#property_sofa table#sofaTable tbody.thetbody tr.tableItems').find('.ww');
	var sofa_isinTrans 	= $('#property_sofa table#sofaTable tbody.thetbody tr.tableItems').find('.inTransaction');
	var sofa_assetDTLid = $('#property_sofa table#sofaTable tbody.thetbody tr.tableItems');

	var curtain_item = $('#property_window table#windowTable tbody.thetbody tr.tableItems');
	var sofa_item 	 = $('#property_sofa table#sofaTable tbody.thetbody tr.tableItems');
	
	// creating variable array
	var item 		 = new Object();
		item.curtain = new Object();
		item.sofa 	 = new Object();
	var curtainArrays    = [];
	var sofaArrays    	 = [];

	if($.trim(contact_id.length) != 0)
	{
		// start fetching the data in the curtain list and store it in the array..
		// fetching only those who has a status of 1
		for (var i = 0; i < curtain_item.length; i++) 
		{
			if(curtain_isinTrans[i].value == '1')
			{
				// console.log(curtain_assetDTLid[i].attributes[0].value);
				curtainArrays.push({
					'assetDTLid'	:curtain_assetDTLid[i].attributes[0].value,
					'location'		:curtain_location[i].innerHTML,
					'room'			:curtain_room[i].innerHTML,
					'short_desc'	:curtain_short_desc[i].innerHTML,
					'wh'			:curtain_wh[i].innerHTML,
					'ww'			:curtain_ww[i].innerHTML
				});
			}
		};
		item.curtain = curtainArrays;

		// start fetching the data in the sofa list and store it in the array..
		// fetching only those who has a status of 1
		for (var i = 0; i < sofa_item.length; i++) 
		{
			if(sofa_isinTrans[i].value == '1')
			{
				sofaArrays.push({
					'assetDTLid'	:sofa_assetDTLid[i].attributes[0].value,
					'location'		:sofa_location[i].innerHTML,
					'room'			:sofa_room[i].innerHTML,
					'short_desc'	:sofa_short_desc[i].innerHTML,
					'fh'			:sofa_fh[i].innerHTML,
					'wh'			:sofa_wh[i].innerHTML,
					'ww'			:sofa_ww[i].innerHTML
				});
			}
		};
		item.sofa = sofaArrays;

		var post_url = base_url + 'admin/joborder/createnew';
		
		// start the processing of the data
		$.post(post_url,{contact_id:contact_id,
						 customer_id:customer_id,
						 property_id:property_id,
						 item:JSON.stringify(item)}, function(result) {
		    try {
		      	var result = $.parseJSON(result);
		      	// console.log(result);
		      	if(result.success = true) {
		      		var path = base_url+'admin/'+result.redirect+result.trans_id;
		      		console.log(path);
		      		window.location.href=path;
		      	}
		    }
		    catch(e) {
	      		console.log(result);
	    	}
		})

		.fail(function(test) {
			alert(test.responseText);
		});	
	}
	else
		bootbox.alert('Contact Person must not be empty. Please select one...');
}

function Update_asset_details(value)
{
	var post_url 	= base_url + 'admin/joborder/save_update_assetdtl/1';
	var location 	= $(value).parent().parent('.tableItems').find('.location');
	var room 		= $(value).parent().parent('.tableItems').find('.room');
	var short_desc 	= $(value).parent().parent('.tableItems').find('.short_desc');
	var fh 			= $(value).parent().parent('.tableItems').find('.fh');
	var wh 			= $(value).parent().parent('.tableItems').find('.wh');
	var ww 			= $(value).parent().parent('.tableItems').find('.ww');
	var assetdtl_ID = $(value).parent().parent('.tableItems').attr('assetDtlid');
	var property_id = $('#propoertyID').val();

	var asset_data  = [];

	asset_data.push({
		'propertyID'	:property_id,
		'assetDTLid'	:assetdtl_ID,
		'location'		:location[0].innerHTML,
		'room'			:room[0].innerHTML,
		'short_desc'	:short_desc[0].innerHTML,
		'wh'			:wh[0].innerHTML,
		'ww'			:ww[0].innerHTML
	});

	$.post(post_url, {asset_data:asset_data}, function(result) {
	    try {
		      	var result = $.parseJSON(result);
		      	if(result.success == true)
		      		bootbox.alert('Data successfully Updated...');
		      	else
		      		bootbox.alert('Details did not save. Contact System Administrator..');
	    	}
	    catch(e) 
	    	{
	      		console.log(result);
	    	}
	})
	.fail(function(test) {
		alert(test.responseText);
	});
}

function showItemOrderDialog(elem)
{
	var trans_item_id = $(elem).attr("theTransactionItemID");
	var main_type_input = $("#order-main-type");
	//$(".order-item-submit").attr("theID", trans_item_id);
	console.log(trans_item_id);
	$("#item-order-dialog").modal("show");

}
function addJobOrder (elem)
{
  var trans_item_id = $(elem).attr("theID");
  var subsub_type_id = $(elem).parent().parent().find("#subsub_style").val();
  var chrOrder_description = $("#txtChrOrder_description").val();
  console.log(chrOrder_description);
  if( subsub_type_id > 0 )
  {
  	var post_url = base_url+"admin/joborder/ajaxAddJobOrder";

  	$.post(post_url,
  		{
  			trans_item_id : trans_item_id,
  			subsub_type_id : subsub_type_id,
  			chrOrder_description : chrOrder_description
  		}, function (result) {
  			var result = $.parseJSON(result);
  			if(result.result_id != null)
  			{
  				location.reload();
  			}
  		});
  }  else {
  	$(".show-add-order-edit").show();
  }
  console.log(trans_item_id);
}
function deleteOrderItem(elem)
{
	var desc = $(elem).parent().parent().find(".item_order_desc").html();
	var itemOrderID = $(elem).attr("theOrderID");
	var theItemID = $(elem).attr("theItemID");
	$("#delete-order-item-confirm").find(".confirm-delete-order-item").attr("theOrderID",itemOrderID);
	$("#delete-order-item-confirm").find(".confirm-delete-order-item").attr("theItemID",theItemID);
	$("#delete-order-item-confirm").find(".del-item-show").html(desc);
	$("#delete-order-item-confirm").modal("show");
}

function confirmDeleteItem(elem)
{
	var itemOrderID = $(elem).attr("theOrderID");
	var theItemID = $(elem).attr("theItemID");
	var post_url = base_url+"admin/joborder/deleteOrderItem";

	$.post(post_url,
		{
			itemOrderID : itemOrderID
		},function(result)
		{
			var result = $.parseJSON(result);
			console.log(result.success);
			if(result.success == 'true' || result.success == true)
			{
				console.log("hi");
				var order_amount = parseFloat($(".order-item-"+itemOrderID).find(".order_total").html());
				var prev_total = parseFloat($("#item_total_"+theItemID).html());
				var curr_total = (prev_total - order_amount).toFixed(2);

				console.log("order_amount = "+order_amount);
				console.log("prev_total = "+prev_total);
				console.log("curr_total = "+curr_total);

				$("#item_total_"+theItemID).empty().html(curr_total);
				$(".order-item-"+itemOrderID).hide();
				$("#delete-order-item-confirm").modal("hide");
				location.reload();
			}
		});
}

function showEditItemDescription(elem)
{
	var intOrderID = $(elem).attr("theOrderID");
	var chrOrderDescription = $(elem).attr("theOrderDescription");

	console.log(intOrderID);
	console.log(chrOrderDescription);

	$(".edit-order-desc-submit").attr("theOrderID",intOrderID);
	$(".order-description-edit").empty().val(chrOrderDescription);

	$("#edit-order-description-dialog").modal("show");

}

function editOrderDescription(elem)
{
	var intOrderID = $(elem).attr("theOrderID");
	var chrOrderDescription = $(elem).parent().parent().find(".order-description-edit").val();

	/***** FOR DEBUGGING PURPOSES, DELETE AT YOUR OWN RISK!!.. *******/
	console.log("editOrderDescription {");
	console.log(" var intOrderID = "+intOrderID);
	console.log(" var chrOrderDescription = "+chrOrderDescription);
	console.log("}");
	/****** ENDS HERE ************************************************/

	var post_url = base_url + "admin/joborder/editOrderDescription";

	$.post(post_url, 
			{
				intOrderID : intOrderID,
				chrOrderDescription : chrOrderDescription
			},function(result)
			{
				var result = $.parseJSON(result);
				if(result.result_id!=null)
				{
					$(".with_order_desc_"+intOrderID).attr("data-content",chrOrderDescription);
					$("#edit-order-description-dialog").modal("hide");
				}else
				{

				}
			});

}

function createInvoice(elem)
{
	var transaction_id = $(elem).attr("theTransactionID");

	var post_url = base_url + "admin/joborder/createInvoice/"+transaction_id;

	$.post(post_url,function(result){

		var result = $.parseJSON(result);
		$("#create_quotation").hide();
		$(elem).hide();
		location.reload();
		console.log(result);
	});

	$(".success-alert").show();

};

function saveAppointment(elem)
{
	var this2 = $(elem);
	var intCustomer_property = this2.parent().parent().find("#selIntCustomer_property").val();
	if (intCustomer_property == "")
	{
		this2.parent().parent().find("#selIntCustomer_property").css("border-color","red");
		return false;
	} else {
		this2.parent().parent().find("#selIntCustomer_property").css("border-color","black");
	}
	var intSales_id = this2.parent().parent().find("#selIntSales_representative").val();
	if(intSales_id == 0 )
	{
		this2.parent().parent().find("#selIntSales_representative").css("border-color","red");
		return false;
	}  else {
		this2.parent().parent().find("#selIntSales_representative").css("border-color","black");
	}
	var chrAppointment_date = this2.parent().parent().find("#chrDateAppointment_date").val();
	if (chrAppointment_date == "")
	{
		this2.parent().parent().find("#chrDateAppointment_date").css("border-color","red");
		return false;
	} else {
		this2.parent().parent().find("#chrDateAppointment_date").css("border-color","black");
	}
	var chrAppointment_time = this2.parent().parent().find("#chrTimeAppointment_time").val();
	if (chrAppointment_time == "")
	{
		this2.parent().parent().find("#chrTimeAppointment_time").css("border-color","red");
		return false;
	} else {
		this2.parent().parent().find("#chrTimeAppointment_time").css("border-color","black");
	}
	var customer_id = this2.parent().parent().find("#hdnIntCustomer_id").val();
	

	var post_url = base_url + "admin/customer/saveAppointment";

	$.post(post_url, 
		{
			selIntRepresentative_id : intSales_id,
			appointment_date : chrAppointment_date,
			appointment_time : chrAppointment_time,
			customer_id : customer_id,
			customer_property : intCustomer_property
		},function(result)
		{
			var result  = $.parseJSON(result);
			if(result.success == true)
			{
				console.log(result);
				var clone = $("#clone-origin").clone();
				clone.addClass("appointment-"+result.appointment_id);
				clone.find(".name").html(result.sales_name[0].u_fname + "  " + result.sales_name[0].u_lname);
				chrAppointment_date = chrAppointment_date.split("/");
				clone.find(".date").html(chrAppointment_date[0]+'-' +chrAppointment_date[1] + '-'+chrAppointment_date[2]);
				clone.find(".time").html(chrAppointment_time);
				clone.find(".status").html(result.added_by[0].u_fname);
				clone.find(".added_by").html("Pending");
				
				

				$(".appointment_list_body").append(clone);
				clone.show();
				this2.parent().parent().find("#selIntSales_representative").val("");
				this2.parent().parent().find("#chrDateAppointment_date").val("");
				this2.parent().parent().find("#chrTimeAppointment_time").val("");
				$("#addappointmentmodal").modal("hide");

			} else if (result.success == false) {
				$(".show_in_warning").show();
			}
			
		});
};

//--------------------------------------------------------------------------------------------------------------
function updateAppointment(elem)
{
	var this2 = $(elem);
	var intCustomer_property = this2.parent().parent().find("#selIntCustomer_property").val();
	var intSales_id = this2.parent().parent().find("#selIntSales_representative").val();
	var chrAppointment_date = this2.parent().parent().find("#chrDateAppointment_date").val();
	var chrAppointment_time = this2.parent().parent().find("#chrTimeAppointment_time").val();
	var peter_id = this2.parent().parent().find("#hdnIntCustomer_id").val();
	

	if(intSales_id == 0 || chrAppointment_date == "" || chrAppointment_time == "" || intCustomer_property == "")
	{
		$(".show_in_warning").show();
		return false;
	}
	var post_url = base_url + "admin/customer/saveAppointment2";

	$.post(post_url, 
		{
			selIntRepresentative_id : intSales_id,
			appointment_date : chrAppointment_date,
			appointment_time : chrAppointment_time,
			peter_id : peter_id,
			customer_property : intCustomer_property
		},function(result)
		{
			var result  = $.parseJSON(result);
			if(result.success == true)
			{
				this2.parent().parent().find("#selIntSales_representative").val("");
				this2.parent().parent().find("#chrDateAppointment_date").val("");
				this2.parent().parent().find("#chrTimeAppointment_time").val("");
				$("#addappointmentmodal").modal("hide");
				location.reload();
			} else if (result.success == false) {
				$(".show_in_warning").show();
			}
			console.log(result);
		});	
	
};


function deleted(elem) {
	var peter_id = $("#hdnIntCustomer_id").val();
		$.post (base_url + "admin/customer/deleteAppointment",
		{
			peter_id:peter_id
		},function (result) {
			var ids = $.parseJSON(result);
			if(ids.id)
			{
				location.reload();				
			}
		});

}


function updatestatus(elem) {
	var peter_id = $("#hdnIntCustomer_id").val();
	
		$.post (base_url + "admin/customer/UpdateStatusAppointment",
		{
			peter_id:peter_id
		},function (result) {
			//console.log (result);
			var success = $.parseJSON(result);
			if(success.success)
			{
				location.reload();				
			}
		});

}

function editMisc(elem)
{
	var misc_id = $(elem).parent().parent().attr("id");
	var misc_description = $(elem).parent().parent().find(".misc_description").html();
	var misc_cost = $(elem).parent().parent().find(".misc_cost").html();
	$("#add-misc-dialog").find(".misc-cost").val(misc_cost);
	$("#add-misc-dialog").find(".misc-description").val(misc_description);
	$("#add-misc-dialog").find(".misc-id").val(misc_id);
	$("#add-misc-dialog").find(".modal-title").html("Edit Miscellanous");
	$("#add-misc-dialog").modal("show");
}

function deleteMisc(elem)
{
	$("#delete-misc-confirm").modal("show");
	var misc_id = $(elem).parent().parent().attr("id");
	$("#delete-misc-confirm").find(".confirm-delete-order-misc").attr("theID",misc_id);
	$("#delete-misc-confirm").find(".del-misc-show").html($(elem).parent().parent().find(".misc_description").html());
}

function confirmedDeleteMisc(elem)
{
	var misc_id = $(elem).attr("theID");
	
	var post_url = base_url + 'admin/joborder/removeMisc';
	$.post( post_url , {misc_id:misc_id} , function (res) {
		var res = $.parseJSON(res);
		var id = res.misc_id;
		$(".misc-order-container").find("#"+misc_id).remove();
		$("#delete-misc-confirm").modal("hide");
		location.reload();
	}); 

}


