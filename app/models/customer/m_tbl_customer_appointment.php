<?php
class M_tbl_customer_appointment extends MY_Model {
	protected $_table_name = 'tbl_customer_appointment';
	protected $_order_by   = 'id ASC';

	public function __construct()
	{
		parent::__construct();
	}	

	public function getAppointment($customer_id)
	{
		if($customer_id != null) {
			$this->db->select("tbl_customer_appointment.*, sales_rep.u_fname AS sales_fname, sales_rep.u_lname AS sales_lname,user.u_fname AS user_fname, user.u_lname ");
			$this->db->from("tbl_customer_appointment");
			$this->db->where("customer_id",$customer_id);
			$this->db->join("tbl_users AS sales_rep","tbl_customer_appointment.sales_rep_id = sales_rep.id","left");
			$this->db->join("tbl_users AS user","tbl_customer_appointment.user_id = user.id","left");

			return $this->db->get()->result();
		} else {
			$this->db->select("tbl_customer_appointment.*, sales_rep.u_fname AS sales_fname, sales_rep.u_lname AS sales_lname,user.u_fname AS user_fname, user.u_lname ");
			$this->db->from("tbl_customer_appointment");
			$this->db->join("tbl_users AS sales_rep","tbl_customer_appointment.sales_rep_id = sales_rep.id","left");
			$this->db->join("tbl_users AS user","tbl_customer_appointment.user_id = user.id","left");

			return $this->db->get()->result();
		}
		
	}


	public function viewAppointment($id)
	{
		if($id != null) {
			$this->db->select("tbl_customer_appointment.*, sales_rep.u_fname AS sales_fname, sales_rep.u_lname AS sales_lname,user.u_fname AS user_fname, user.u_lname,tbl_customers.id AS ID_customer,tbl_customers.* ");
			$this->db->from("tbl_customer_appointment");
			$this->db->where("tbl_customer_appointment.id",$id);
			$this->db->join("tbl_users AS sales_rep","tbl_customer_appointment.sales_rep_id = sales_rep.id","left");
			$this->db->join("tbl_users AS user","tbl_customer_appointment.user_id = user.id","left");
			$this->db->join("tbl_customers","tbl_customer_appointment.customer_id = tbl_customers.id","left");


			return $this->db->get()->result();
		}
		else 
		{
			return "ERROR";
		}
		
	}

	public function userSchedule($data) {
		if($data['u_role'] == 1)
		{

		} else if ($data['u_role'] == 2) {
			$this->db->select("tbl_customer_appointment.*, tbl_customer_appointment.id AS APPOINTMENT_ID, tbl_customers.* , tbl_customers.id AS CUSTOMERS_ID, tbl_asset.*, tbl_asset.id AS ASSET_ID");
			$this->db->from("tbl_customer_appointment");
			$this->db->where("tbl_customer_appointment.status",0);
			$this->db->join("tbl_customers","tbl_customers.id  =  tbl_customer_appointment.customer_id","left");
			$this->db->join("tbl_asset","tbl_asset.id  = tbl_customer_appointment.customer_property","left");
			return $this->db->get()->result();
		} else if ($data['u_role'] == 3) {
			$this->db->select("tbl_customer_appointment.*, tbl_customer_appointment.id AS APPOINTMENT_ID, tbl_customers.* , tbl_customers.id AS CUSTOMERS_ID, tbl_asset.*, tbl_asset.id AS ASSET_ID");
			$this->db->from("tbl_customer_appointment");
			$this->db->where("tbl_customer_appointment.sales_rep_id",$data['user_id']);
			$this->db->where("tbl_customer_appointment.status",0);
			$this->db->join("tbl_customers","tbl_customers.id  =  tbl_customer_appointment.customer_id","left");
			$this->db->join("tbl_asset","tbl_asset.id  = tbl_customer_appointment.customer_property","left");
			return $this->db->get()->result();
		}
	}
	public function deleteItem($id)
    {
        $this->db->delete("tbl_customer_appointment", array('id' => $id));
    }


}