<?php
class M_customer_contact extends MY_Model {
	protected $_table_name = 'tbl_customers_contact';
	protected $_order_by   = 'id ASC';
	protected $_timestamps = TRUE;

	public function __construct()
	{
		parent::__construct();
	}	

	function getCustomersContact($customer_id)
	{
		$intCustomer_id = (int)$customer_id;
		$arContacts = $this->get_by(array('customer_id'=>$intCustomer_id));
		return $arContacts;
	}
}