<?php
class M_customer extends MY_Model {
	protected $_table_name = 'tbl_customers';
	protected $_order_by   = 'id ASC';
	protected $_timestamps = TRUE;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('customer/m_customer_contact');
	}

	public function countcustomers()
	{

		// $this->db->select("*");
		// $this->db->from("tbl_customers");
		// $Count=count($this->db->get()->result());
		return ($this->db->get("tbl_customers")->result());
		//return $Count;
	}

	public function describeCustomer($customers)
	{	
		$arr = array();

		if(!is_array($customers)):
			$arr[] = $customers;
		else:	
			$arr   = $customers;
		endif;
		
		for($i=0;$i<count($arr);$i++):
			$arr[$i]->cus_fax_num     = json_decode($arr[$i]->cus_fax_num);
			$arr[$i]->cus_hom_num     = json_decode($arr[$i]->cus_hom_num);
			$arr[$i]->cus_han_num     = json_decode($arr[$i]->cus_han_num);
			$arr[$i]->cus_off_num     = json_decode($arr[$i]->cus_off_num);
			$arr[$i]->cus_email       = json_decode($arr[$i]->cus_email);
			$arr[$i]->contact_persons = $this->m_customer_contact->get_by(array('customer_id'=>$arr[$i]->id,'status'=>1));
		endfor;
		
		return $arr;
	}

	public function getCustomer($transaction_id)
	{
		$this->db->select("tbl_job_transaction.tbl_customers_id,tbl_customers.id,tbl_customers.cus_name,tbl_asset.asset_name");
		$this->db->from("tbl_job_transaction");
		$this->db->where("tbl_job_transaction.id",$transaction_id);
		$this->db->join("tbl_customers","tbl_customers.id = tbl_job_transaction.tbl_customers_id","join");
		$this->db->join("tbl_asset","tbl_asset.id = tbl_job_transaction.asset_id","join");
		return $this->db->get()->result_array();
	}
	
}	