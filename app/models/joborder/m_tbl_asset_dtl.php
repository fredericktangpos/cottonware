<?php
class M_tbl_asset_dtl extends MY_Model 
{
    protected $_table_name  = 'tbl_asset_dtl';
    protected $_order_by  	= 'tbl_asset_dtl.id';

    function __construct() {
        parent::__construct();
    }

    function insertAsset($arAssetDetail)
    {
    	$id = $this->save($arAssetDetail);
    	if($id!=null)
    	{
    		return $id;
    	}else{
    		return null;
    	}
    	
    }
    public function deleteItem($id)
    {
        $this->db->delete("tbl_asset_dtl", array('id' => $id)); 
    }

} # end of class


?>