<?php
class M_tbl_rooms extends MY_Model 
{

    protected $_table_name  = 'tbl_rooms';
    protected $_order_by  	= 'tbl_rooms.id';

    function __construct() {
        parent::__construct();
    }

    function insertRoom($room_name)
    {
    	$id = $this->save(array('room_name'=>$room_name));
    	if($id!=null)
    	{
    		return $id;
    	}else{
    		return null;
    	}
    	
    }

    function getRooms()
    {
        $dbArRooms = $this->get();
        return $dbArRooms;
    }

} # end of class


?>