<?php
class M_tbl_miscellanous extends MY_Model 
{
    protected $_table_name  = 'tbl_miscellanous_job';
    protected $_order_by  	= 'tbl_miscellanous_job.id';

    function __construct() {
        parent::__construct();
    }

    public function getMisc($transaction_id)
    {
    	$this->db->select("tbl_miscellanous_job.*");
    	$this->db->from("tbl_miscellanous_job");
        $this->db->where("tbl_miscellanous_job.transaction_id",$transaction_id);
    	$this->db->where("tbl_miscellanous_job.status",1);
    	return $this->db->get()->result_array();
    }
    public function getTransBasicDetails($transaction_id)
    {
    	
    }
    public function deleteItem($id)
    {
        $this->db->delete("tbl_miscellanous_job", array('id' => $id)); 
    }

} # end of class


?>