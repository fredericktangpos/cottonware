<?php
class M_tbl_job_transaction extends MY_Model 
{
    protected $_table_name  = 'tbl_job_transaction';
    protected $_order_by  	= 'tbl_job_transaction.id';

    function __construct() {
        parent::__construct();
    }

    function insertTransaction($arTransactionData)
    {
        $intTransaction_id =  $this->save($arTransactionData);
        if($intTransaction_id != null)
        {
            return $intTransaction_id;
        }else {
            return null;
        }
    }
    public function countTransaction() {
        $this->db->Select('*');
        $this->db->from($this->_table_name);
        $this->db->where("job_trans_status",0);
        return $this->db->get()->result();
    }
    public function dashboardJobSummary()
    {
    	$this->db->select($this->_table_name.'.*,tbl_customers.cus_name, tbl_asset.asset_name');
    	$this->db->from($this->_table_name);
    	$this->db->join('tbl_customers', 'tbl_customers.id=tbl_job_transaction.tbl_customers_id', 'left');
    	$this->db->join('tbl_transaction_item', 'tbl_transaction_item.tbl_job_transaction_id=tbl_job_transaction.id', 'left');
    	$this->db->join('tbl_asset', 'tbl_asset.id=tbl_transaction_item.tbl_asset_id', 'left');
    	return $this->db->get()->result();
    }

    public function getCostumerTransactions($customer_id)
    {
        $this->db->select($this->_table_name.'.*');
        $this->db->from($this->_table_name);
        $this->db->where("tbl_customers_id",$customer_id);       
        $this->db->order_by($this->_order_by,"DESC");       
        return $this->db->get()->result();
    }

    public function createInvoice($transaction_id)
    {
        $intTransaction_id = (int)$transaction_id;
        $result_id = $this->save(array('job_trans_status'=>1),$intTransaction_id);
        if($result_id != NULL)
        {
            return $result_id;
        }else{
            return NULL;
        }
    }

    public function transactionLatestTen()
    {
        $this->db->select("tbl_job_transaction.* , tbl_job_transaction.id AS TRANSACTION_ID,tbl_users.*,tbl_users.id AS USERS_ID");
        $this->db->from("tbl_job_transaction");
        $this->db->join("tbl_users","tbl_job_transaction.tbl_users_id = tbl_users.id","LEFT");
        $this->db->order_by("tbl_job_transaction.id","DESC");
        $this->db->limit(15);
        return $this->db->get()->result();
    }

    public function deleteItem($id)
    {
        $this->db->delete("tbl_job_transaction", array('id' => $id)); 
    }
    public function getTransactionsCustomerData($transaction_id)
    {
        $this->db->select("tbl_job_transaction.tbl_customers_id,tbl_customers.cus_salutations,tbl_customers.cus_name,tbl_customers.cus_email,tbl_customers.cus_address_blk,tbl_customers.cus_address_street,tbl_customers.cus_address_unit,tbl_customers.cus_address_postal,tbl_customers.cus_fax_num,tbl_customers.cus_hom_num,tbl_customers.cus_han_num,tbl_customers.cus_off_num");
        $this->db->from("tbl_job_transaction");
        $this->db->where("tbl_job_transaction.id",$transaction_id);
        $this->db->join("tbl_customers","tbl_customers.id = tbl_job_transaction.tbl_customers_id","left");
        return $this->db->get()->result();
    }
} # end of class
?>