<?php
class M_tbl_locations extends MY_Model 
{

    protected $_table_name  = 'tbl_locations';
    protected $_order_by  	= 'tbl_locations.id';

    function __construct() {
        parent::__construct();
    }

    function insertLocation($location_name)
    {
    	$id = $this->save(array('location_name'=>$location_name));
    	if($id!=null)
    	{
    		return $id;
    	}else{
    		return null;
    	}
    	
    }

    function getLocations()
    {
        $dbArLocations = $this->get();
        return $dbArLocations;
    }

} # end of class


?>