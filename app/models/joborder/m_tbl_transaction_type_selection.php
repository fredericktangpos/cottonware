<?php
class M_tbl_transaction_type_selection extends MY_Model 
{
    protected $_table_name  = 'tbl_transaction_type_selection';
    protected $_order_by  	= 'id';

    function __construct() {
        parent::__construct();
    }
    function insertNewSelection($arType_selection_data)
    {
    	$intType_selection_id =  $this->save($arType_selection_data);

        if($intType_selection_id != null)
        {
            return $intType_selection_id;
        }else {
            return null;
        } 
    }
    public function deleteItem($id)
    {
        $this->db->delete("tbl_transaction_type_selection", array('id' => $id)); 
    }
} # end of class


?>