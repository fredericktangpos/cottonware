<?php
class M_tbl_transaction_images extends MY_Model 
{

    protected $_table_name  = 'tbl_transaction_images';
    protected $_order_by  	= 'tbl_transaction_images.id';

    function __construct() {
        parent::__construct();
    }
    public function deleteItem($id)
    {
        $this->db->delete("tbl_transaction_images", array('id' => $id)); 
    }

} # end of class


?>