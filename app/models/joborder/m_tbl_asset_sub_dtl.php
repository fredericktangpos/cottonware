<?php
class M_tbl_asset_sub_dtl extends MY_Model 
{
    protected $_table_name  = 'tbl_asset_sub_dtl';
    protected $_order_by  	= 'tbl_asset_sub_dtl.id';

    function __construct() {
        parent::__construct();
    }

    /*
	==========================================
	This function will get the property and other
	details given only the window id
	==========================================
    */
    public function get_property($window_id)
    {
    	$this->db->select("tbl_asset.id AS asset_id, tbl_asset_dtl.id AS aset_dtl_id");
    	$this->db->from($this->_table_name);
    	$this->db->where($this->_table_name.'.id', $window_id);
    	$this->db->join('tbl_asset_dtl', 'tbl_asset_dtl.id='.$this->_table_name.'.tbl_asset_dtl_id','left');
    	$this->db->join('tbl_asset', 'tbl_asset.id=tbl_asset_dtl.tbl_asset_id','left');
    	return $this->db->get()->result();
    }

} # end of class


?>