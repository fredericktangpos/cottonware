<?php
class M_tbl_transaction_item extends MY_Model 
{
    protected $_table_name  = 'tbl_transaction_item';
    protected $_order_by  	= 'tbl_transaction_item.id';

    function __construct() {
        parent::__construct();
    }

    function insertTransactionItem($arTransaction_item_data){

        $intTransaction_item_id =  $this->save($arTransaction_item_data);

        if($intTransaction_item_id != null)
        {
            return $intTransaction_item_id;
        }else {
            return null;
        } 
        
    }

    public function get_subitem($item_id, $subcat_type)
    {
		$this->db->select('tbl_job_subcat.id, tbl_job_subcat.job_type_name');    	
		$this->db->from('tbl_transaction_type_selection');
		$this->db->where('tbl_transaction_type_selection.tbl_transaction_item_id',$item_id);
		$this->db->where('tbl_job_subcat.sub_cat_type',$subcat_type);
		$this->db->join('tbl_job_subcat','tbl_job_subcat.id=tbl_transaction_type_selection.curtain_type', 'left');
		return $this->db->get()->result();
    }

    public function property_count($id)
    {
        $this->db->select('*');
        $this->db->from($this->_table_name);
        $this->db->where($this->_table_name.'.tbl_job_transaction_id', $id);
        $this->db->group_by($this->_table_name.'.tbl_asset_id');
        return $this->db->get()->result();
    }

    public function getTransactionItems($transaction_id)
    {
        $this->db->select("tbl_transaction_item.id");
        $this->db->from($this->_table_name);
        $this->db->where("tbl_transaction_item.tbl_job_transaction_id",$transaction_id);
        $this->db->where("tbl_transaction_item.status",1);  
        return $this->db->get()->result();
    }

     public function getTransactionItemsAll($transaction_id)
    {
        $this->db->select("tbl_transaction_item.*, tbl_transaction_item.id AS ID_transaction_item");
        $this->db->select("tbl_asset_dtl.*, tbl_asset_dtl.id AS ID_asset_dtl");
        $this->db->select("tbl_asset.*, tbl_asset.id AS ID_asset");
        $this->db->select("tbl_customers_contact.*, tbl_customers_contact.id AS ID_customers_contact");
        $this->db->select("tbl_rooms.room_name");
        $this->db->select("tbl_locations.location_name");
        $this->db->select("tbl_customers_contact.*, tbl_customers_contact.id AS ID_customers_contact");
        $this->db->from($this->_table_name);
        $this->db->where("tbl_transaction_item.tbl_job_transaction_id",$transaction_id);
        $this->db->join('tbl_asset_dtl','tbl_transaction_item.asset_dtl_id=tbl_asset_dtl.id', 'left');
        $this->db->join('tbl_asset','tbl_transaction_item.asset_id=tbl_asset.id', 'left');
        $this->db->join('tbl_customers_contact','tbl_transaction_item.tbl_customer_contact_id=tbl_customers_contact.id', 'left');
        $this->db->join('tbl_rooms','tbl_asset_dtl.room=tbl_rooms.id', 'left');
        $this->db->join('tbl_locations','tbl_asset_dtl.location=tbl_rooms.id', 'left');
        return $this->db->get()->result();
    }

    public function getBasics($transaction_id)
    {
        $this->db->select("tbl_transaction_item.asset_id, tbl_transaction_item.tbl_customer_contact_id");
        $this->db->from("tbl_transaction_item");
        $this->db->where("tbl_job_transaction_id",$transaction_id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }

    public function deleteItem($id)
    {
        $this->db->delete("tbl_transaction_item", array('id' => $id)); 
    }
} # end of class


?>