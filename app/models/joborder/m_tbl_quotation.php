<?php
class M_tbl_quotation extends MY_Model 
{
    protected $_table_name  = 'tbl_quotation';
    protected $_order_by  	= 'tbl_quotation.id';

    function __construct() {
        parent::__construct();
    }
    public function deleteItem($id)
    {
        $this->db->delete("tbl_quotation", array('id' => $id)); 
    }

} # end of class


?>