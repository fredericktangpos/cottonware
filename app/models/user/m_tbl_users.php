<?php
class M_tbl_users extends MY_Model 
{
	
	protected $_table_name = 'tbl_users';
	protected $_order_by = 'id';
    protected $_timestamps = TRUE;

    public $rules = array(
        'u_username' => array('field' => 'username', 
                            'label' => 'Username', 
                            'rules' => 'trim|required|xss_clean'),
        'u_pass'     => array('field' => 'password', 
                            'label' => 'Password', 
                            'rules' => 'trim|required'),
    ); 

    # user form validation

    function __construct() {
        parent::__construct();
    }

    # login and create session
    public function login() {
        $user = $this->get_by(array(
            'u_username' => $this->input->post('username'),
            'u_pass' => $this->hash($this->input->post('password')),
            'u_role'=>1
            ), TRUE);

        if(count($user))
        {
            $data = array(
                'id'        => $user->id,
                'name'      => $user->u_fname.' '.$user->u_lname,
                'username'  => $user->u_username,
                'u_role'    => $user->u_role,
                'image'     => $user->image,
                'loggedin'  => TRUE,
            );
            $this->session->set_userdata($data);
        }
    }

    # kill session
    public function logout() {
        $this->session->sess_destroy();
    }

    #check if alreaday logged in or not
    public function loggedin() {
        return (bool) $this->session->userdata('loggedin');
    }

    # security
    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

} #end of Class


?>