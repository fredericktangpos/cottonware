<?php
class M_tbl_user_access extends MY_Model 
{
	
	protected $_table_name = 'user_access';
	protected $_order_by = 'id';


    function __construct() {
        parent::__construct();
    }

    function _isExist($data)
    {
    	$this->db->select('user_access.*');    	
   		$this->db->from('user_access');
   		$this->db->where('menu_id',$data['menu_id']);
   		$this->db->where('tbl_users_id',$data['tbl_users_id']);
   		return $this->db->get()->result();
    }

    function _removeAccess($intUser_id)
    {
      return $this->db->delete('user_access', array('tbl_users_id' => $intUser_id)); 
    }

} #end of Class


?>