<?php
class M_tbl_job_classification extends MY_Model 
{
    protected $_table_name  = 'tbl_job_classification';
    protected $_order_by    = 'tbl_job_classification.id';

    function __construct() {
        parent::__construct();
    }

    public function get_classification($tableName,$per_pg2,$offset2)
    {
    	$this->db->select('*, tbl_job_classification.id AS job_class_id');
    	$this->db->from('tbl_job_classification');
    	$this->db->join('tbl_supplier','tbl_supplier.id = tbl_job_classification.tbl_supplier_id', 'left');
    	// $this->db->join('tbl_job_category','tbl_job_category.id = tbl_job_classification.tbl_job_category_id', 'left');

    	return $this->db->get('',$per_pg2,$offset2)->result();
    }

    public function getSubType($main_type_id)
    {
        $this->db->select('*, tbl_job_classification.id AS job_class_id');
        $this->db->where("tbl_job_classification.tbl_job_subcat_id",$main_type_id);
        $this->db->from('tbl_job_classification');
        $this->db->join('tbl_supplier','tbl_supplier.id = tbl_job_classification.tbl_supplier_id', 'left');
        $this->db->order_by("tbl_job_classification.type_name", "asc");
        $res = $this->db->get();

        return $res->result_array();
    }

    public function subTypeDetail($sub_id)
    {
        $this->db->select('*, tbl_job_classification.id AS job_class_id');
        $this->db->where("tbl_job_classification.id",$sub_id);
        $this->db->from('tbl_job_classification');
        $this->db->join('tbl_supplier','tbl_supplier.id = tbl_job_classification.tbl_supplier_id', 'left');
        $res = $this->db->get();

        return $res->result_array();
    }
} # end of class


?>