<?php
class M_tbl_job_subcat extends MY_Model 
{
    protected $_table_name  = 'tbl_job_subcat';
    protected $_order_by    = 'tbl_job_subcat.id';

    function __construct() {
        parent::__construct();
    }


    function getSubType($sub_type_id)
    {
    	$this->db->where("status",1);
        $this->db->where("transaction_type_id",$sub_type_id);
        $this->db->from('tbl_job_subcat');
        $res = $this->db->get();
        return $res->result_array();
    }

    function getSwatchDetails ($swatch_id,$supplier_id)
    {
        $this->db->select("tbl_job_classification.*, 
                            tbl_job_classification.id AS classification_id,
                            tbl_supplier.*,
                            tbl_supplier.id AS supplier_id,
                            tbl_transaction_type.*,
                            tbl_transaction_type.id AS trans_type_id");
        $this->db->where("tbl_job_classification.id",$swatch_id);
        $this->db->where("tbl_job_classification.status",1);
        $this->db->from('tbl_job_classification');  
        $this->db->join("tbl_supplier","tbl_supplier.id = tbl_job_classification.tbl_supplier_id","left");
        $this->db->join("tbl_job_subcat","tbl_job_subcat.id = tbl_job_classification.tbl_job_subcat_id","left");
        $this->db->join("tbl_transaction_type","tbl_transaction_type.id = tbl_job_subcat.transaction_type_id","left");
        $res = $this->db->get();
        return $res->result_array();
    }

    function getType()
    {
        $dbArTypes = $this->get_by(array('status' => 1));
        return $dbArTypes;
    }

} # end of class


?>