<?php

class Sm_transactions_model extends My_Model
{	
	protected $_table_name = 'tbl_job_transaction';
	protected $_order_by = 'id';
    protected $_timestamps = TRUE;

   
	public function __construct() {
		parent::__construct();

	}

	public function index()
	{

	}
	public function adminVariables()
	{
		$this->db->select("tbl_admin_variables.*");
		$this->db->from("tbl_admin_variables");
		return $this->db->get()->result();
	}
	public function salesRepCustomerTransactions($_filter_data)
	{
		$intUserId = (int)$_filter_data['intUserId'];
		$intCustomerId = (int)$_filter_data['intCustomerId'];
		
		$this->db->select($this->_table_name.'.*');
    	$this->db->from($this->_table_name);
    	$this->db->where("tbl_users_id",$intUserId);
    	$this->db->where("tbl_customers_id",$intCustomerId);

    	//$this->db->join('tbl_transaction_item', 'tbl_job_transaction.id=tbl_transaction_item.tbl_job_transaction_id', 'left');    
    	//$this->db->join('tbl_asset', 'tbl_asset.id=tbl_transaction_item.tbl_asset_id', 'left');
    	return $this->db->get()->result();
	}
	public function getOrderDetails($transaction_item_id)
	{
		$this->db->select("tbl_transaction_type_selection.*,tbl_job_classification.*");
		$this->db->from("tbl_transaction_type_selection");
		$this->db->where("tbl_transaction_item_id",$transaction_item_id);
		$this->db->join("tbl_job_classification","tbl_transaction_type_selection.tbl_job_classification_id = tbl_job_classification.id","left");
		return $this->db->get()->result();
	}	

	public function userSchedule($data) {
		if($data['u_role'] == 1)
		{

		} else if ($data['u_role'] == 2) {
			$this->db->select("tbl_customer_appointment.*, tbl_customer_appointment.id AS APPOINTMENT_ID, tbl_customers.* , tbl_customers.id AS CUSTOMERS_ID, tbl_asset.*, tbl_asset.id AS ASSET_ID");
			$this->db->from("tbl_customer_appointment");
			$this->db->where("tbl_customer_appointment.status",0);
			$this->db->join("tbl_customers","tbl_customers.id  =  tbl_customer_appointment.customer_id","left");
			$this->db->join("tbl_asset","tbl_asset.id  = tbl_customer_appointment.customer_property","left");
			$this->db->order_by("tbl_customer_appointment.id", "desc"); 
			return $this->db->get()->result();

		} else if ($data['u_role'] == 3) {
			$this->db->select("tbl_customer_appointment.*, tbl_customer_appointment.id AS APPOINTMENT_ID, tbl_customers.* , tbl_customers.id AS CUSTOMERS_ID, tbl_asset.*, tbl_asset.id AS ASSET_ID");
			$this->db->from("tbl_customer_appointment");
			$this->db->where("tbl_customer_appointment.sales_rep_id",$data['user_id']);
			$this->db->where("tbl_customer_appointment.status",0);
			$this->db->join("tbl_customers","tbl_customers.id  =  tbl_customer_appointment.customer_id","left");
			$this->db->join("tbl_asset","tbl_asset.id  = tbl_customer_appointment.customer_property","left");
			return $this->db->get()->result();
		}
	}
	public function viewAppointment($id)
	{
		if($id != null) {
			$this->db->select("tbl_customer_appointment.*, sales_rep.u_fname AS sales_fname, sales_rep.u_lname AS sales_lname,user.u_fname AS user_fname, user.u_lname,tbl_customers.id AS ID_customer,tbl_customers.*,tbl_asset.* ");
			$this->db->from("tbl_customer_appointment");
			$this->db->where("tbl_customer_appointment.id",$id);
			$this->db->join("tbl_users AS sales_rep","tbl_customer_appointment.sales_rep_id = sales_rep.id","left");
			$this->db->join("tbl_users AS user","tbl_customer_appointment.user_id = user.id","left");
			$this->db->join("tbl_customers","tbl_customer_appointment.customer_id = tbl_customers.id","left");
			$this->db->join("tbl_asset","tbl_customer_appointment.customer_property = tbl_asset.id","left");
			$this->db->order_by("tbl_customer_appointment.id", "desc"); 

			return $this->db->get()->result();
		}
		else 
		{
			return "ERROR";
		}
		
	}

	public function getContactPerson($customer_id) {
		$this->db->select("tbl_customers_contact.*");
		$this->db->from("tbl_customers_contact");
		$this->db->where("customer_id",(int)$customer_id);
		return $this->db->get()->result();
	}


	public function getAllLevel()
	{
		$this->db->select("tbl_locations.*");
		$this->db->from("tbl_locations");
		return $this->db->get()->result();
		
	}
	public function getAllRooms()
	{
		$this->db->select("tbl_rooms.*");
		$this->db->from("tbl_rooms");
		return $this->db->get()->result();
	}
	//
	public function finishedJobOrder($data) {
		if($data['u_role'] == 1)
		{

		} else if ($data['u_role'] == 2) {
			$this->db->select("tbl_customer_appointment.*, tbl_customer_appointment.id AS APPOINTMENT_ID, tbl_customers.* , tbl_customers.id AS CUSTOMERS_ID, tbl_asset.*, tbl_asset.id AS ASSET_ID");
			$this->db->from("tbl_customer_appointment");
			$this->db->where("tbl_customer_appointment.status",1);
			$this->db->join("tbl_customers","tbl_customers.id  =  tbl_customer_appointment.customer_id","left");
			$this->db->join("tbl_asset","tbl_asset.id  = tbl_customer_appointment.customer_property","left");
			$this->db->order_by("tbl_customer_appointment.id", "desc"); 
			return $this->db->get()->result();
		} else if ($data['u_role'] == 3) {
			$this->db->select("tbl_customer_appointment.*, tbl_customer_appointment.id AS APPOINTMENT_ID, tbl_customers.* , tbl_customers.id AS CUSTOMERS_ID, tbl_asset.*, tbl_asset.id AS ASSET_ID");
			$this->db->from("tbl_customer_appointment");
			$this->db->where("tbl_customer_appointment.sales_rep_id",$data['user_id']);
			$this->db->where("tbl_customer_appointment.status",1);
			$this->db->join("tbl_customers","tbl_customers.id  =  tbl_customer_appointment.customer_id","left");
			$this->db->join("tbl_asset","tbl_asset.id  = tbl_customer_appointment.customer_property","left");
			$this->db->order_by("tbl_customer_appointment.id", "desc"); 
			return $this->db->get()->result();
		}
	}

	public function getUserInformation($user_id)
	{
		$this->db->select("tbl_users.*");
		$this->db->from("tbl_users");
		$this->db->where("id",$user_id);
		return $this->db->get()->result();
	}

	public function getOrderType()
	{
		$this->db->select("tbl_job_subcat.*");
		$this->db->from("tbl_job_subcat");
		return $this->db->get()->result();
	}
	public function getOrderSwatches($ordertype_id)
	{
		$this->db->select("tbl_job_classification.*");
		$this->db->from("tbl_job_classification");
		$this->db->where("tbl_job_subcat_id",$ordertype_id);
		$this->db->order_by("tbl_job_classification.type_name", "asc");
		return $this->db->get()->result();
	}

	public function addNewTransaction($data)
	{
		$this->db->insert("tbl_job_transaction",$data);
		return $this->db->insert_id(); 
	}
	public function updateTransaction($data,$id)
	{
		$this->db->where("id",$id);
		return $this->db->update("tbl_job_transaction",$data);
	}
	public function addNewAssetDetail($data)
	{
		$this->db->insert("tbl_asset_dtl",$data);
		return $this->db->insert_id(); 
	}
	public function addNewTransactionItem($data)
	{
		$this->db->insert("tbl_transaction_item",$data);
		return $this->db->insert_id(); 
	}
	public function addNewOrderSelection($data)
	{
		$this->db->insert("tbl_transaction_type_selection",$data);
		return $this->db->insert_id(); 
	}
	public function addNewLevel($data)
	{
		$this->db->insert("tbl_locations",$data);
		return $this->db->insert_id(); 
	}
	public function addNewRoom($data)
	{
		$this->db->insert("tbl_rooms",$data);
		return $this->db->insert_id(); 
	}
	public function updateAppointment($id)
	{
		$data = array("status"=>1);
		$this->db->where('id', $id);
		return $this->db->update('tbl_customer_appointment', $data); 
	}
	public function updateAppointmentToFinish($transaction_id ,$appointment_id)
	{
		$this->db->where("id",$appointment_id);
		return $this->db->update("tbl_customer_appointment",array("transaction_id"=>$transaction_id));
	}

	public function insertMiscJob($data)
	{
		$this->db->insert("tbl_miscellanous_job",$data);
		return $this->db->insert_id(); 
	}
	public function getMisc($transaction_id)
	{
		$this->db->select("*");
		$this->db->from("tbl_miscellanous_job");
		$this->db->where("transaction_id",$transaction_id);
		return $this->db->get()->result();
	}

	public function saveImagePath($imageData)
	{
		$this->db->insert("tbl_transaction_images",$imageData);
		return $this->db->insert_id(); 
	}

	public function getTransactionImage($transaction_id)
	{
		$this->db->select("*");
		$this->db->from("tbl_transaction_images");
		$this->db->where("transaction_id",$transaction_id);
		return $this->db->get()->result();
	}	

	public function getAllCustomersContact()
	{
		$data = $this->db->query("SELECT id, REPLACE(cus_email,' ','') AS cus_email, REPLACE(cus_fax_num , ' ','') AS cus_fax_num, REPLACE(cus_hom_num, ' ','') AS cus_hom_num, REPLACE(cus_han_num,' ','') AS cus_han_num, REPLACE(cus_off_num , ' ','') AS  cus_off_num FROM tbl_customers WHERE cus_status=1");
		return $data->result();
	}

	public function getCustomer($id)
	{
		$this->db->select("id, cus_name, cus_address_blk, cus_address_street, cus_address_unit, cus_address_bldg, cus_address_postal, cus_remark");
		$this->db->from("tbl_customers");
		$this->db->where("id",$id);
		return $this->db->get()->result();
	}

	public function getProperties($id)
	{
		$this->db->select("*");
		$this->db->from("tbl_asset");
		$this->db->where("tbl_customer_id",$id);
		return $this->db->get()->result();
	}

	public function createIndirectAppointment($data) {
		$this->db->insert("tbl_customer_appointment", $data);
		return $this->db->insert_id(); 
	}
}	

?>