<?php
class Sm_user_model extends MY_Model 
{
	
	protected $_table_name = 'tbl_users';
	protected $_order_by = 'id';
    protected $_timestamps = TRUE;

    

    function __construct() {
        parent::__construct();
    }

    public function logIn($login_data)
    {
        if($login_data['_system_secret'] === $login_data['_login_secret'])
        {
            $user = $this->get_by(array(
                'u_username' => $login_data['username'],
                'u_pass' => $this->hash($login_data['password']),
            ), TRUE);

            if(count($user))
            {
                $data = array(
                    'id'        => $user->id,
                    'name'      => $user->u_fname.' '.$user->u_lname,
                    'username'  => $user->u_username,
                    'u_role'    => $user->u_role
                );
                return $data;
            }
        }
    }

    public function loggedin() {
        return (bool) $this->session->userdata('loggedin');
    }
    public function userId()
    {
        return $this->session->userdata('id');
    }
    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

} #end of Class


?>