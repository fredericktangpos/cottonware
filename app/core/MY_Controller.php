<?php
class MY_Controller extends CI_Controller 
{
	public $data = array();

	function __construct() {
		parent::__construct();
            header('Access-Control-Allow-Origin: *'); 
            header('Access-Control-Allow-Headers: content-type'); 
		$this->data['errors'] 	 = array();
		$this->data['site_name'] = config_item('site_name');
	}

	public function __ajax_paginate($base_url, $total_row, $per_page){
      $config['base_url'] = $base_url;
      $config['total_rows'] = $total_row;
      $config['per_page'] = $per_page;

      $config['full_tag_open'] = '<div class="text-center"><ul id="cust-pagi" class="pagination pagination-sm">';
      $config['full_tag_close'] = '</ul></div>';
      
      #TO LAST 
      $config['last_link'] = htmlspecialchars_decode('&raquo;');
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';

      #TO FIRST 
      $config['first_link'] = htmlspecialchars_decode("&laquo;");
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';

      #TO NEXT
      $config['next_link'] = htmlspecialchars_decode("&gt;");
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';

      #TO PREVIOUS
      $config['prev_link'] = htmlspecialchars_decode("&lt;");
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';

      #CURRENT PAGE
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';

      #Pagination format
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';

      $this->pagination->initialize($config);
      return $this->pagination->create_links();
}


}
?>