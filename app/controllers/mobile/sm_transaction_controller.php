<?php

class Sm_transaction_controller extends My_Controller 
{
	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *'); 
		header('Access-Control-Allow-Headers: content-type'); 
		$this->load->model('mobile/sm_transactions_model');
	}

	public function index() { }

	public function userSchedule()
	{
		$mobile = @$this->input->post('mobile');
		if($mobile)
		{
			$u_role   = $this->input->post('u_role',TRUE);
			$user_id  = $this->input->post('user_id',TRUE);
		} else
		{
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata);
			$u_role = $request->u_role;
			$user_id = $request->user_id;			
		}
		$data = array('u_role'=>(int)$u_role, 'user_id'=> (int)$user_id);
		$this->load->model("customer/m_tbl_customer_appointment");
		$result = $this->m_tbl_customer_appointment->userSchedule($data);
 
		if (count($result) > 0)
		{	
			die(json_encode(array("success"=>true , "result" => $result)));
		} else {
			die(json_encode(array("success"=>false, "result" => null)));
		}
	}
	
	public function salesRepCustomerTransactions ()
	{
		$intUserId = $this->input->post("intUserId",TRUE);
		$intCustomerId = $this->input->post("intCustomerId",TRUE);
		//$intUserId = 1;
		//$intCustomerId = 1;

		$_filter_data = array(
							"intUserId" => $intUserId,
							"intCustomerId" => $intCustomerId);
		$transaction_list = $this->sm_transactions_model->salesRepCustomerTransactions($_filter_data);
		$this->load->model("joborder/m_tbl_transaction_item");
		$transaction_item = array();
		
		foreach ($transaction_list as $trans_list) {
			$trans_id = $trans_list->id;

			$temp_items = $this->m_tbl_transaction_item->getTransactionItemsAll($trans_id);
			
			$transaction_item[] = $temp_items;
		}
		$this->load->model('joborder/m_tbl_asset');

        $properties= $this->m_tbl_asset->get_by(array('tbl_customer_id'=>$intCustomerId));


		die(json_encode(array("transaction_list"=>$transaction_list,"transaction_item"=>$transaction_item,"properties"=>$properties)));

	}

	public function transactionOrders($id)
	{
		$intTransaction_id = (int)$id;
		//$intTransaction_id = 2;
		$this->load->model("joborder/m_tbl_transaction_item");
		$this->load->model("joborder/m_tbl_transaction_type_selection");
		$this->load->model("mobile/sm_transactions_model");
		$dbArAdmin_variables = $this->sm_transactions_model->adminVariables();
		$item_list = $this->m_tbl_transaction_item->getTransactionItemsAll($intTransaction_id);
		$order_list = array();
		foreach ($item_list as $list) {
			$transaction_item_id = (int)$list->ID_transaction_item;
			$orders = $this->sm_transactions_model->getOrderDetails($transaction_item_id);
			$_order = array();
			$_item = array();
			foreach ($orders as $order) {
				$_order[] = array(
									"classification_id"=>$order->tbl_job_subcat_id,
									"swatch_id"=>$order->id,
									"transaction_item_id"=>$order->tbl_transaction_item_id);
			}
			
			$_item[] = array(
								"item_id"=>(string)$transaction_item_id,
								"height"=>$list->window_height,
								"width"=>$list->window_width);
			
			$orders[] = $this->calculateBasicCost($_item,$_order);
			//dump($orders);
			$order_list[] = $orders;	

			
			
		}
		die(json_encode(array("order_list"=>$order_list,"admin_variables"=>$dbArAdmin_variables)));
	}

	public function swatchDetails($swatch_id)
	{
		//load job classification database
		$this->load->model('settings/m_tbl_job_classification');
		$dbArSwatch_data = $this->m_tbl_job_classification->get_by(array('id'=>$swatch_id));

		return $dbArSwatch_data;
	}

	function calculateBasicCost($itemList, $orderList)
	{
		$arItem_list = $itemList;
		$arOrder_list = $orderList;
		$costs_list = array();
		foreach ($arItem_list as $item) {
			$temp_price = 0; 
			foreach ($arOrder_list as $order) {
				if ($item['item_id'] == $order['transaction_item_id']) {
					
					if($order['classification_id'] == 1)	
					{
						$order_cost = $this->nightCurtainCalculation($item['width'], $item['height'], $order['swatch_id']);
						//$temp_price += $order_cost ; 
						//print_r("item width" . $item['width']);
						$sewing_cost = $this->sewingCost($item['width'], 1);
						$temp_price += ($sewing_cost+$order_cost) ; 
						$costs_list[] = array(
											'swatch_id' => $order['swatch_id'],
											'basic_price' => ($order_cost+$sewing_cost),
											'transaction_item_id' => $order['transaction_item_id']);
					} else if($order['classification_id'] == 2)
					{
						$order_cost = $this->dayCurtainCalculation($item['width'], $item['height'], $order['swatch_id']);
						//$temp_price += $order_cost;
						$sewing_cost = $this->sewingCost($item['width'], 2);

						$temp_price += ($sewing_cost+$order_cost) ; 
						$costs_list[] = array(
											'swatch_id' => $order['swatch_id'],
											'basic_price' => ($order_cost+$sewing_cost),
											'transaction_item_id' => $order['transaction_item_id']);

					}else if($order['classification_id'] == 3)
					{
						$order_cost = $this->trackCost($order['swatch_id'], $item['width']);
						$temp_price += $order_cost;
						$costs_list[] = array(
											'swatch_id' => $order['swatch_id'],
											'basic_price' => $order_cost,
											'transaction_item_id' => $order['transaction_item_id']);

					}else if($order['classification_id'] == 4)
					{
						$order_cost = $this->blindsCost($item['width'], $item['height'], $order['swatch_id']);
						$temp_price += $order_cost;
						$costs_list[] = array(
											'swatch_id' => $order['swatch_id'],
											'basic_price' => $order_cost,
											'transaction_item_id' => $order['transaction_item_id']);
					}

				}
			}
		}

		//dump($costs_list);

		return $costs_list;
	}

	public function adminVariables()
	{
		//load the database
		$this->load->model('joborder/m_tbl_admin_variables');

		$dbArAdmin_variables = $this->m_tbl_admin_variables->get();

		return $dbArAdmin_variables;
	}
	public function dayCurtainCalculation($width, $height, $swatch_id)
	{

		$_WIDTH = (float)$width;
		$_HEIGHT = (float)$height;

		/****    get the GST FACTOR, DAY CURTAIN FABRIC BLEED (INCH), CURTAIN FACTOR    ****/

		$dbArAdmin_variables = $this->adminVariables();
		$_GST_FACTOR =	(float)$dbArAdmin_variables[0]->gst_factor; 
		$_DAY_CURTAIN_FABRIC_BLEED = (float)$dbArAdmin_variables[0]->day_curtain_fabric_bleed;
		$_CURTAIN_FACTOR = (float)$dbArAdmin_variables[0]->curtain_factor;
		
		/****************---------------------------------------------*******************/


		/******   get the swatch details from supplier i.e. width, price per meter ******/

		$dbArSwatch_data = $this->swatchDetails($swatch_id);
		$_CURTAIN_WIDTH = (float)$dbArSwatch_data[0]->width;
		$_PRICE_PER_METER = (float)$dbArSwatch_data[0]->cost_amount;
		//$_PRICE_PER_METER = (float)7.00;

		/**********-----------------------------------------------------------************/

		/**********      get day curtain fabric calculation   *********/

		$_DAY_CURTAIN_FABRIC_CALCULATION = (float)(($_WIDTH*2)+$_DAY_CURTAIN_FABRIC_BLEED)/$_CURTAIN_FACTOR;

		/***************************************************************/

		/**********   get day curtain fabric required   ************/
		$_DAY_CURTAIN_FABRIC_REQUIRED = ceil($_DAY_CURTAIN_FABRIC_CALCULATION);
		/***********************************************************/

		$_DAY_CURTAIN_COST = round(($_DAY_CURTAIN_FABRIC_REQUIRED * $_PRICE_PER_METER * $_GST_FACTOR),2);
		//dump($_DAY_CURTAIN_COST);
		return $_DAY_CURTAIN_COST;

	}

	public function nightCurtainCalculation($width, $height, $swatch_id)
	{
		$_WIDTH = (float)$width;
		$_HEIGHT = (float)$height;

		$dbArSwatch_data = $this->swatchDetails($swatch_id);
		$_CURTAIN_WIDTH = (float)$dbArSwatch_data[0]->width;
		//$_CURTAIN_WIDTH = (float)145.00;
		$_PRICE_PER_METER = (float)$dbArSwatch_data[0]->cost_amount;
		//$_PRICE_PER_METER = (float)8.00;

		$_NIGHT_CURTAIN_FABRIC_WIDTH = round(($_CURTAIN_WIDTH/2.54/2),2);
		$_NIGHT_PANELS = ceil($_WIDTH/$_NIGHT_CURTAIN_FABRIC_WIDTH);

		$dbArAdmin_variables = $this->adminVariables();
		$_GST_FACTOR = $dbArAdmin_variables[0]->gst_factor;
		$_NIGHT_CURTAIN_FABRIC_BLEED = $dbArAdmin_variables[0]->night_curtain_fabric_bleed;
		$_CURTAIN_FACTOR = $dbArAdmin_variables[0]->curtain_factor;
		$_NIGHT_CURTAIN_FABRIC_CALCULATION = (($_HEIGHT + $_NIGHT_CURTAIN_FABRIC_BLEED)*$_NIGHT_PANELS)/$_CURTAIN_FACTOR;	
		$_NIGHT_CURTAIN_FABRIC_REQUIRED = ceil($_NIGHT_CURTAIN_FABRIC_CALCULATION);
		$_NIGHT_CURTAIN_COST = round(($_NIGHT_CURTAIN_FABRIC_REQUIRED * $_PRICE_PER_METER * $_GST_FACTOR),2);
		//dump($_NIGHT_CURTAIN_COST);
		return $_NIGHT_CURTAIN_COST;
	}

	/***
	*@param $type, 1 or 2. 1 for night, and 2 for day
	*/
	public function sewingCost($window_width, $type)
	{	
		$_SEWING_COST = 0;

		$dbArAdmin_variables = $this->adminVariables();
		$_GST_FACTOR = (float)$dbArAdmin_variables[0]->gst_factor;
		$_DAY_SEWING_FACTOR = (float)$dbArAdmin_variables[0]->day_sewing_factor;
		$_DAY_SEWING_PRICE_PER_PANEL = (float)$dbArAdmin_variables[0]->day_sewing_price_per_panel;
		$_NIGHT_SEWING_PRICE_PER_PANEL = (float)$dbArAdmin_variables[0]->night_sewing_price_per_panel;

		$_SEWING_PANELS = ceil($window_width/$_DAY_SEWING_FACTOR);

		if($type==1)
		{	
			$_SEWING_COST = round(($_SEWING_PANELS * $_DAY_SEWING_PRICE_PER_PANEL * $_GST_FACTOR),2);
		}else if($type==2)
		{
			$_SEWING_COST = round(($_SEWING_PANELS * $_NIGHT_SEWING_PRICE_PER_PANEL * $_GST_FACTOR),2);
		}

		//dump($_SEWING_COST);

		return $_SEWING_COST;
	}

	public function trackCost($track_id, $window_width)
	{
		$track_id = (int)$track_id;
		$_WIDTH = (float)$window_width;
		$dbArSwatch_data = $this->swatchDetails($track_id);	
		$_PRICE_PER_FEET = 	$dbArSwatch_data[0]->cost_amount;
		//$_PRICE_PER_FEET = 	(float)5.00;

		$_TRACK_COST = 0;

		$_TRACK = round(($_WIDTH/12),2);

		$_TRACK_COST = round(($_TRACK * $_PRICE_PER_FEET),2);

		//dump($_TRACK_COST);

		return $_TRACK_COST;
	}

	public function blindsCost($window_width, $window_height, $blinds_id)
	{
		$_BLINDS_ID = $blinds_id;
		$_WIDTH = $window_width;
		$_HEIGHT = $window_height;
		$dbArAdmin_variables = $this->adminVariables();
		$_GST_FACTOR = $dbArAdmin_variables[0]->gst_factor;
		$dbArSwatch_data = $this->swatchDetails($_BLINDS_ID);
		//$_FABRIC_WIDTH = $dbArSwatch_data[0]->width;
		//$_PRICE_PER_SQUARE_FEET = $dbArSwatch_data[0]->cost_amount;
		$_PRICE_PER_SQUARE_FEET = (float)2.40;

		$_BLINDS_DIMENSION = ceil(($_WIDTH * $_HEIGHT) / 144);

		$_BLINDS_COST = round(($_BLINDS_DIMENSION * $_PRICE_PER_SQUARE_FEET  * $_GST_FACTOR) ,2);

		//dump($_BLINDS_COST);

		return $_BLINDS_COST;
	}

	public function scheduleInformation () 
	{
		$this->load->model("mobile/sm_transactions_model");
		//$request = json_decode($postdata);
		$id = $this->input->post("id");

		$result = $this->sm_transactions_model->viewAppointment($id);

		if( count($result) > 0 ) 
		{
			die(json_encode(array("success"=>true, "result"=>$result)));
		} else {
			die(json_encode(array("success"=>false,"result"=>null)));
		}
	}

	public function insertOrder ()
	{
		// $basic = $this->input->post("basic_info");
		// $temp = json_decode($basic,TRUE);
		// dump($temp);
		// dump($temp['user_id']);
		// die();
		$orders   = @$this->input->post("information");
		$basic    = @$this->input->post("basic_info");
		$misc_job = @$this->input->post("misc_job");
		$mobile   = @$this->input->post("mobile");

		if($mobile)
		{
			$orders = (array)json_decode($orders);
			$basic  = (array)json_decode($basic);
			$misc_job = (array)json_decode($misc_job);
			// dump($misc_job);
			// dump($misc_job[0]->description);
		} 
		$transaction_data = array(
								"transaction_id"=>0,
								"user_id"=>$basic['user_id'],
								"customer_id"=>$basic['customer_id'],
								"asset_id"=>$basic["asset_id"]
								);	

		$transaction_id = $this->addTransaction($transaction_data);
		$this->load->model("mobile/sm_transactions_model");
		$this->sm_transactions_model->updateAppointmentToFinish($transaction_id ,$basic['appointment_id']);
		if($transaction_id > 0)
		{
		 	$this->updateAppointment($basic['appointment_id']);
		 	$this->insertMiscJob($misc_job, $transaction_id);
		}
		if($orders) {
			// dump('-------------------------------------------------');
			// $temp = $mobile ? @$orders[0]->room_information : @$orders[0]["room_information"];
			// $temp2 = $mobile ? $temp[0]->dimensions : $temp[0]["dimensions"];
			// $temp3 = $mobile ? $temp2[1]->dimension_height : $temp2[0]["dimension_height"];
			// $temp4 = $mobile ? $temp2[0]->dimension_orders : $temp2[0]["dimension_orders"];
			// dump($temp4);
			// die();
			for($i = 0 ; $i < count($orders); $i++) {
				$location = $mobile ? $orders[$i]->level_description->level_name : $orders[$i]["level_description"]["level_name"];
				if((int)$location == 0)
				{
					$location = (int)$this->addNewLevel(array("location_name"=>$mobile ? @$orders[$i]->level_description->level_name_other : @$orders[$i]["level_description"]["level_name_other"]));
				}
				$rooms = $mobile ? @$orders[$i]->room_information : @$orders[$i]["room_information"];
				if(count($rooms) > 0) {
					for ($x=0; $x < count($rooms); $x++) { 
						$room = $mobile ? $rooms[$x]->room_description : $rooms[$x]["room_description"];
						if((int)$room == 0)
						{
							$room = (int)$this->addNewRoom(array("room_name"=>$mobile ? $rooms[$x]->room_description_other : $rooms[$x]["room_description_other"]));
						}
						$dimensions = $mobile ? $rooms[$x]->dimensions : $rooms[$x]["dimensions"];
						if(count($dimensions) > 0)
						{
							for ($z=0; $z < count($dimensions); $z++) {
								//dump($dimensions[$z]);
								$tbl_asset_dtl_data = array(
														"tbl_asset_id"=>(int)$basic["asset_id"],
														"location"=>(int)$location,
														"room"=>(int)$room,
														"window_height"=>$mobile ? (int)$dimensions[$z]->dimension_height : (int)$dimensions[$z]["dimension_height"],
														"window_width"=>$mobile ? (int)$dimensions[$z]->dimension_width : (int)$dimensions[$z]["dimension_width"]);
								//dump($tbl_asset_dtl_data);
								$asset_dtl_id = $this->addNewAssetDetail($tbl_asset_dtl_data);
								//dump($asset_dtl_id);
								$job_transaction_item_data = array(
															"tbl_job_transaction_id"=>$transaction_id,
															"tbl_customer_contact_id"=>$basic["contact_person"],
															"asset_id"=>$basic["asset_id"],
															"asset_dtl_id"=>$asset_dtl_id);
								$transaction_item_id = $this->addNewTransactionItem($job_transaction_item_data);
								//dump($transaction_item_id);
								//dump($dimensions[$z]["dimension_width"]);
								//dump($dimensions[$z]["dimension_height"]);
								$dimension_orders = $mobile ? $dimensions[$z]->dimension_orders : $dimensions[$z]["dimension_orders"];
								if(count($dimension_orders) > 0){
									for ($s=0; $s < count($dimension_orders); $s++) {
										$selection_data = array(
																"tbl_transaction_item_id"=>$transaction_item_id,
																"tbl_job_classification_id"=>$mobile ? $dimension_orders[$s]->swatch_id : $dimension_orders[$s]["swatch_id"],
																"color"=>$mobile ? $dimension_orders[$s]->swatch_color : $dimension_orders[$s]["swatch_color"]
																);
										$order_selection_id = $this->addNewOrderSelection($selection_data);
										//dump($order_selection_id);
										//dump($dimension_orders[$s]);
										//dump($orders[$s]["swatch_id"]);
										//dump($orders[$s]["swatch_color"]);
									}
								}
							}
						}
					}
				}
			}
		}
		die(json_encode(array("success"=>true,"transaction_id"=>$transaction_id)));
	}
	public function insertMiscJob($misc_data , $transaction_id)
	{
		if($misc_data)
		{
			$this->load->model("mobile/sm_transactions_model");
			foreach ($misc_data as $misc) {
				$data = array(
							"transaction_id"=>$transaction_id,
							"description"=>$misc->description,
							"cost"=>$misc->cost);

				$this->sm_transactions_model->insertMiscJob($data);
			}
		}
	}
	public function updateAppointment($appointment_id)
	{
		$this->load->model("mobile/sm_transactions_model");
		return $this->sm_transactions_model->updateAppointment($appointment_id);
	}
	/**
	 * add new transaction to table tbl_job_transaction
	 * @param [assoc array] $data , contains three ids, 
	 * transaction_id, default to 0 (zero)
	 * user_id , sytems users id
	 * customer_id, customer id   
	 */
	public function addTransaction($data) 
	{
		$this->load->model("mobile/sm_transactions_model");
		if((int)$data['transaction_id'] == 0) //insert new transaction
		{
			$data2 = array(
							"tbl_users_id"=>(int)$data['user_id'],
							"tbl_customers_id"=>(int)$data['customer_id'],
							"job_trans_created"=>date('Y-m-d H:i:s'),
							"asset_id"=>(int)$data['asset_id']
						);
			return $this->sm_transactions_model->addNewTransaction($data2);
		} else if ((int)$data['transaction_id'] > 0) //update transaction 
		{
			$data2 = array(
							"tbl_users_id"=>$data['transaction_id'],
							"tbl_customers_id"=>$data['customer_id'],
							"job_trans_status"=>2
						);
			return $this->sm_transactions_model->updateTransaction($data2,$data['id']);
		}
	}

	public function addNewAssetDetail($data)
	{
		$this->load->model("mobile/sm_transactions_model");
		return $this->sm_transactions_model->addNewAssetDetail($data);
	}

	public function addNewTransactionItem($data)
	{
		$this->load->model("mobile/sm_transactions_model");
		return $this->sm_transactions_model->addNewTransactionItem($data);
	}
	public function addNewLevel($data)
	{
		$this->load->model("mobile/sm_transactions_model");
		return $this->sm_transactions_model->addNewLevel($data);
	}
	public function addNewRoom($data)
	{
		$this->load->model("mobile/sm_transactions_model");
		return $this->sm_transactions_model->addNewRoom($data);
	}
	public function addNewOrderSelection($data)
	{
		$this->load->model("mobile/sm_transactions_model");
		return $this->sm_transactions_model->addNewOrderSelection($data);
	}
	public function pendingJobOrders()
	{
		$this->load->model("mobile/sm_transactions_model");
		$user_id = $this->input->post("user_id");
		$u_role = $this->input->post("u_role");
		$data = array("user_id" => $user_id, "u_role"=>$u_role);
		$result = $this->sm_transactions_model->userSchedule($data);
		die(json_encode(array("success"=>true,"joborder"=>$result)));
	}

	public function viewAppointment()
	{
		$this->load->model("mobile/sm_transactions_model");
		$appointment_id = $this->input->post("appointment_id");
		$result = $this->sm_transactions_model->viewAppointment($appointment_id);
		die(json_encode(array("success"=>true,"joborder"=>$result)));
	}

	public function getContactPerson($id)
	{	
		$this->load->model("mobile/sm_transactions_model");
		$contact_persons = $this->sm_transactions_model->getContactPerson($id);
		die(json_encode(array("status"=>true,"contact_persons"=>$contact_persons)));
	}

	public function getAllLevel()
	{
		$this->load->model("mobile/sm_transactions_model");
		$levels = $this->sm_transactions_model->getAllLevel();
		die(json_encode(array("success"=>true,"levels"=>$levels)));
	}

	public function getAllRooms()
	{
		$this->load->model("mobile/sm_transactions_model");
		$rooms = $this->sm_transactions_model->getAllRooms();
		die(json_encode(array("success"=>true,"rooms"=>$rooms)));
	}

	public function getOrderType()
	{
		$this->load->model("mobile/sm_transactions_model");
		$order_types = $this->sm_transactions_model->getOrderType();
		die(json_encode(array("success"=>true,"order_types"=>$order_types)));
	}
	//finishedJobOrders
	public function finishedJobOrders()
	{
		$this->load->model("mobile/sm_transactions_model");
		$user_id = $this->input->post("user_id");
		$u_role = $this->input->post("u_role");
		$data = array("user_id" => $user_id, "u_role"=>$u_role);
		$result = $this->sm_transactions_model->finishedJobOrder($data);
		die(json_encode(array("success"=>true,"joborder"=>$result)));
	}

	public function getUserInformation($user_id)
	{
		$this->load->model("mobile/sm_transactions_model");
		$user_data = $this->sm_transactions_model->getUserInformation((int)$user_id);
		die(json_encode(array("success"=>true,"user_data"=>$user_data)));
	}

	public function getOrderSwatches($ordertype_id)
	{
		$this->load->model("mobile/sm_transactions_model");
		$swatches = $this->sm_transactions_model->getOrderSwatches((int)$ordertype_id);
		die(json_encode(array("success"=>true,"swatches"=>$swatches)));
	}
	public function viewTransaction($transaction_id) {

		$this->load->model("joborder/m_tbl_transaction_item");
		$this->load->model("joborder/m_tbl_quotation");
		$this->load->model("joborder/m_tbl_job_transaction");
		$this->load->model("mobile/sm_transactions_model");
		$_DATA = array();
		$intTransaction_id = $transaction_id;
		
		$item_list = $this->m_tbl_transaction_item->getTransactionItems($intTransaction_id);
		$misc_job = $this->sm_transactions_model->getMisc($transaction_id);
		$misc_total_cost = 0 ;
		if(count($misc_job) > 0)
		{
			foreach ($misc_job as $misc) {
				$_each_cost = (int)$misc->cost;
				$misc_total_cost+=$_each_cost;
			}
		}
		$items = array();

			foreach ($item_list as $item) {
						$items[] = (int)$item->id;
			}

		$item_json = json_encode($items);

		$data = array(
					'tbl_job_transaction_id' => $intTransaction_id,
					'items'=>$item_json);

		$_is_exist = $this->m_tbl_quotation->get_by(array('tbl_job_transaction_id'=>$intTransaction_id));

		if(count($_is_exist) > 0 )
		{
			$update_trans_type_data = array('job_trans_type'=>2);
			$this->m_tbl_job_transaction->save($update_trans_type_data,$intTransaction_id);
		}

		$result_id = $this->m_tbl_quotation->save($data);

		$test = $this->m_tbl_quotation->get();

		//dump(json_decode($test[1]->items));

		if($result_id!=NULL)
		{
			//$status_change = array('job_trans_status'=>1);

			//$this->m_tbl_job_transaction->save($status_change, $intTransaction_id);

			$select_join = 'tbl_transaction_item.*, 
						tbl_transaction_item.id AS trans_id, 
						tbl_job_transaction.*, 
						tbl_asset_dtl.*, 
						tbl_customers.*, 
						tbl_customers_contact.name,
						tbl_asset.asset_name,
						tbl_asset.asset_photo,
						tbl_rooms.*,
						tbl_locations.*';

			$joining_array = array();
			$joining_array[0] = new stdClass();
			$joining_array[1] = new stdClass();
			$joining_array[2] = new stdClass();
			$joining_array[3] = new stdClass();
			$joining_array[4] = new stdClass();
			$joining_array[5] = new stdClass();
			$joining_array[6] = new stdClass();
			

			$joining_array[0]->table 		= 'tbl_transaction_item';
			$joining_array[0]->condition 	= 'tbl_job_transaction.id = tbl_transaction_item.tbl_job_transaction_id';
			$joining_array[0]->method 		= 'left';

			$joining_array[1]->table 		= 'tbl_asset_dtl';
			$joining_array[1]->condition 	= 'tbl_asset_dtl.id = tbl_transaction_item.asset_dtl_id';
			$joining_array[1]->method 		= 'left';

			$joining_array[2]->table 		= 'tbl_customers';
			$joining_array[2]->condition 	= 'tbl_customers.id = tbl_job_transaction.tbl_customers_id';
			$joining_array[2]->method 		= 'left';

			$joining_array[3]->table 		= 'tbl_customers_contact';
			$joining_array[3]->condition 	= 'tbl_customers_contact.id = tbl_transaction_item.tbl_customer_contact_id';
			$joining_array[3]->method 		= 'left';

			$joining_array[4]->table 		= 'tbl_asset';
			$joining_array[4]->condition 	= 'tbl_asset.id = tbl_asset_dtl.tbl_asset_id';
			$joining_array[4]->method 		= 'left';

			$joining_array[5]->table 		= 'tbl_rooms';
			$joining_array[5]->condition 	= 'tbl_asset_dtl.room = tbl_rooms.id';
			$joining_array[5]->method 		= 'left';

			$joining_array[6]->table 		= 'tbl_locations';
			$joining_array[6]->condition 	= 'tbl_asset_dtl.location = tbl_locations.id';
			$joining_array[6]->method 		= 'left';

			$_DATA['job_transaction_item'] = $this->m_tbl_job_transaction->get_by(array('tbl_job_transaction.id'=>$transaction_id,"tbl_transaction_item.status"=>1),FALSE,$joining_array,$select_join);

			$item_array[0] = new stdClass();
			$item_array[1] = new stdClass();
			$item_array[2] = new stdClass();

			$item_array[0]->table 		= 'tbl_transaction_type_selection';
			$item_array[0]->condition 	= 'tbl_transaction_type_selection.tbl_transaction_item_id = tbl_transaction_item.id';
			$item_array[0]->method 		= 'left';

			$item_array[1]->table 		= 'tbl_job_classification';
			$item_array[1]->condition 	= 'tbl_job_classification.id = tbl_transaction_type_selection.tbl_job_classification_id';
			$item_array[1]->method 		= 'left';

			$item_array[2]->table 		= 'tbl_supplier';
			$item_array[2]->condition 	= 'tbl_supplier.id = tbl_job_classification.tbl_supplier_id';
			$item_array[2]->method 		= 'left';


			$item_join = 	"tbl_transaction_item.*,
							tbl_transaction_item.id AS trans_item_id,
							tbl_transaction_type_selection.id AS trans_type_sel_id,
							tbl_transaction_type_selection.*,
							tbl_job_classification.*,
							tbl_job_classification.id AS job_class_id,
							tbl_supplier.*,
							tbl_supplier.id AS supplier_id";

			$_DATA['item_order_list'] = $this->m_tbl_transaction_item->get_by(array('tbl_job_transaction_id'=>$transaction_id,"tbl_transaction_item.status"=>1),FALSE,$item_array,$item_join);
			
			$order_basic_info = array();
			$item_basic_info = array();
			
			foreach ($_DATA['item_order_list'] as $order) {
				$temp_array = array(
									'classification_id' => $order->tbl_job_subcat_id,
									'swatch_id' => $order->job_class_id,
									'transaction_item_id' => $order->tbl_transaction_item_id,
									'selection_id' =>$order->trans_type_sel_id );
				$order_basic_info[] = $temp_array;
			}
			//dump($this->data['job_transaction_item']);
			//dump($order_basic_info);
			foreach($_DATA['job_transaction_item'] as $order_item)
			{	
				//dump($order_item);
				$temp_array = array(
									'item_id' => $order_item->trans_id,
									'height' => $order_item->window_height,
									'width' => $order_item->window_width);
				$item_basic_info[] = $temp_array;
			}

			//$this->load->helper(array('dompdf','file'));
			$this->load->helper('file');
			$costing_list = $this->calculateBasicCost($item_basic_info, $order_basic_info);
			//$html = $this->load->view('backend/admin/customer/transaction-form');
			//$html = "just a test1...";
			//$data = pdf_create($html, '', false);
    	    //$test = write_file("quotation/test.pdf", $data);
    	    //dump($costing_list);
			$_DATA['costing_list'] = $costing_list;
			$images = $this->sm_transactions_model->getTransactionImage($intTransaction_id);
			die(json_encode(array("success"=>true,"images"=>$images,"transaction_data"=>$_DATA,"misc_job"=>$misc_job,"misc_total"=>$misc_total_cost)));


			//$this->load->view('backend/admin/customer/transaction-form', $this->data);
			/***
			$c_name = $this->data['job_transaction_item'][0]->cus_name;
			$date_today = date('g:ia l jS F Y');
			$pdf_name = $c_name.'[ '.$date_today.' ].pdf';
			$this->load->library('pdf');
			****/
			//redirect(base_url('backend/admin/customer/transaction-form'), $this->data);
			
			/***
			$this->pdf->set_paper( "letter", "portrait");
			$this->pdf->load_view('backend/admin/customer/transaction-form',$this->data);
			$this->pdf->render();
			$this->pdf->stream($pdf_name);
			***/
			//$this->load->view('backend/admin/customer/transaction-form',$this->data);
			//$this->load->view('backend/adminVariablesin/customer/transaction-form', $this->data);
			//$save = write_file("quotation/quotation.pdf", $test);
			//dump($test);
			
			//pdf_create($this->load->view('backend/admin/home',$this->data);,base_url'backend/admin/test');
			//$this->data['subview'][] = 'backend/admin/customer/transaction-form';
			//$this->load->view('backend/admin/home',$this->data);

		}else
		{
			dump("wahahah error!..");
		}
	}

	public function saveImage()
	{
		// $images = $this->input->post("images");
		$transaction_id  = $_POST['trans_id'];
		$incrment        = $_POST['counter'];
		$file_name       = "image-".$transaction_id."-".$incrment.".png";		
		// if($images)
		// {
		// 	for ($i=0; $i < count($images) ; $i++) {
		// 		$uri  = $images[$i]['url'];
		// 		$file_name = "image-".$transaction_id."-".$i.".png";
		// 		  $uri = substr($uri,strpos($uri,",")+1);
		// 		  if(file_put_contents( $_SERVER['DOCUMENT_ROOT']."/img/orderimages/".$file_name, base64_decode($uri)))
		// 		  {
		// 		  	$imageData = array(
		// 		  					"transaction_id"=>$transaction_id,
		// 		  					"directory"=>$file_name);
		// 		  	$this->saveImagePath($imageData);
		// 		  }
		// 	}
		// }
		move_uploaded_file($_FILES["photo"]["tmp_name"], $_SERVER['DOCUMENT_ROOT']."/img/orderimages/".$file_name);
	  	$imageData = array(
	  					"transaction_id"=>$transaction_id,
	  					"directory"=>$file_name);
	  	$this->saveImagePath($imageData);	
		die(json_encode(array("success"=>true)));
	}

	public function saveImagePath($imageData)
	{
		$this->load->model("mobile/sm_transactions_model");
		$this->sm_transactions_model->saveImagePath($imageData);
	}


	/** CREATE NEW ORDER FUNCTIONS */

	public function searchCustomer()
	{
		$phone_number = $this->input->post("phone_number",true);
		$email = $this->input->post("email",true);
		//$phone_number = "98328890";
		//$email = "veronicafoo13@gmail.com";

		//get all customers contact numbers
		$contact_numbers = $this->sm_transactions_model->getAllCustomersContact();

		$customer = array();

		if ($phone_number != "" && $email != "") //both email and phone number
		{
			foreach ($contact_numbers as $contacts) {
				$cus_email  = $contacts->cus_email;
				$cus_email = json_decode($cus_email);

				$cus_fax_num = $contacts->cus_fax_num;
				$cus_fax_num = json_decode($cus_fax_num);
					if (in_array($phone_number, $cus_fax_num) && in_array($email, $cus_email)) {
						array_push($customer, $this->sm_transactions_model->getCustomer($contacts->id));
					}
				$cus_hom_num = $contacts->cus_hom_num;
				$cus_hom_num = json_decode($cus_hom_num);
					if (in_array($phone_number, $cus_hom_num) && in_array($email, $cus_email)) {
						array_push($customer, $this->sm_transactions_model->getCustomer($contacts->id));
					}
				$cus_han_num = $contacts->cus_han_num;
				$cus_han_num = json_decode($cus_han_num);
					if (in_array($phone_number, $cus_han_num) && in_array($email, $cus_email)) {
						array_push($customer, $this->sm_transactions_model->getCustomer($contacts->id));
					}
				$cus_off_num = $contacts->cus_off_num;
				$cus_off_num = json_decode($cus_off_num);
					if (in_array($phone_number, $cus_off_num) && in_array($email, $cus_email)) {
						array_push($customer, $this->sm_transactions_model->getCustomer($contacts->id));
					}
			}
		} else if ($phone_number != "" && $email == "") //phone number
		{
			//dump($contact_numbers);
			foreach ($contact_numbers as $contacts) {
				$cus_fax_num = $contacts->cus_fax_num;
				$cus_fax_num = json_decode($cus_fax_num);
					if (in_array($phone_number, $cus_fax_num)) {
						array_push($customer, $this->sm_transactions_model->getCustomer($contacts->id));
					}
				$cus_hom_num = $contacts->cus_hom_num;
				$cus_hom_num = json_decode($cus_hom_num);
					if (in_array($phone_number, $cus_hom_num)) {
						array_push($customer, $this->sm_transactions_model->getCustomer($contacts->id));
					}
				$cus_han_num = $contacts->cus_han_num;
				$cus_han_num = json_decode($cus_han_num);
					if (in_array($phone_number, $cus_han_num)) {
						array_push($customer, $this->sm_transactions_model->getCustomer($contacts->id));
					}
				$cus_off_num = $contacts->cus_off_num;
				$cus_off_num = json_decode($cus_off_num);
					if (in_array($phone_number, $cus_off_num)) {
						array_push($customer, $this->sm_transactions_model->getCustomer($contacts->id));
					}
			}
		} else if ($email != "" && $phone_number == "") // email
		{
			foreach ($contact_numbers as $contacts) {
				$cus_email  = $contacts->cus_email;
				$cus_email = json_decode($cus_email);
				if (in_array($email, $cus_email)) {
					array_push($customer, $this->sm_transactions_model->getCustomer($contacts->id));
				}
			}
		} else { //empty or null 
			$customer = array();
		}

		
	die(json_encode(array("success"=>true, "customer"=>$customer)));
	}


	public function getCustomerBasics()
	{
		$id = $this->input->post("customer_id",true);
		if ($id)
		{
			die(json_encode(array("success"=>true,"properties"=>$this->sm_transactions_model->getProperties($id),"customer_info"=>$this->sm_transactions_model->getCustomer($id))));
		} else {
			die(json_encode(array("success"=>true,"properties"=>null,"customer_info"=>null)));
		}
	}

	public function createIndirectAppointment()
	{

		$customer_id = $this->input->post("customer_id",true);  
		$customer_property = $this->input->post("customer_property",true);
		$sales_rep_id = $this->input->post("sales_rep_id",true);

		$data = array(
				"sales_rep_id"=>$sales_rep_id,
				"date"=>date("Y-m-d"),
				"time"=>date("H:i"),
				"user_id"=>$sales_rep_id,
				"customer_id"=>$customer_id,
				"customer_property"=>$customer_property,
				"direct"=>1
			);
		$appointment_id = $this->sm_transactions_model->createIndirectAppointment($data);
		
		if ((int)$appointment_id > 0)
		{
			die(json_encode(array("success"=>true,"appointment_id"=>$appointment_id)));
		} else {
			die(json_encode(array("success"=>false,"appointment_id"=>0)));
		}
  
	}
	
	/** END OF FUNCTIONS */

}

?>