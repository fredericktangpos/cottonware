<?php
	class Itemcontroller extends Admin_controller {
		public function __construct ()
		{
			parent::__construct();
			$this->load->model('joborder/m_tbl_job_subcat');
		}

		public function index()
		{
			//code here!..
		}

		public function ajaxSearchItem()
		{
			$keyword 	= $this->input->post("keyword",true);
			$keyMatch 	= "job_type_name";
			$where 		= $this->input->post('where');
			$where_col	= $this->input->post('whereCol');

			$return = $this->m_tbl_job_subcat->get_by(array('('.$keyMatch.' like "%'.$keyword.'%")'=>NULL));
			
			if(count($return) > 0) {
				echo json_encode(array('success'=>true, 'data'=>$return));
			}
			else {
				echo json_encode(array('success'=>false));
			}

		}

		public function saveItemUpdate()
		{
			$intItem_id = $this->input->post("intItem_id",TRUE);
			$chrItem_name = $this->input->post("chrItem_name",TRUE);
			$chrItem_type = $this->input->post("chrItem_type",TRUE);
			
			$data = array(
					'job_type_name'=>$chrItem_name,
					'sub_cat_type'=>$chrItem_type
				);

			$id = $this->m_tbl_job_subcat->save($data,$intItem_id);

			if($intItem_id==$id)
			{
				echo json_encode(array("success"=>"true"));
			}else
			{
				echo json_encode(array("success"=>"false"));
			}
		}

		public function softDeleteItem()
		{
			$intItem_id = $this->input->post("item_id",TRUE);

			$id = $this->m_tbl_job_subcat->save(array("status"=>0),$intItem_id);

			if($intItem_id==$id)
			{
				echo json_encode(array("success"=>"true"));
			}else
			{
				echo json_encode(array("success"=>"fail"));
			}
		}

		public function softDeleteSupplier()
		{
			
		}
	}
?>