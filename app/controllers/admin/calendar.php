<?php

class Calendar extends Admin_Controller 
{

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$user_id = $this->session->userdata('id');
		//console.log($this->session);
		$this->data['subview'][] = 'backend/admin/calendar';
		$this->load->view('backend/admin/home',$this->data);
	}

	public function event($id,$cust_id)
	{
		//$userRole = $this->session->userdata("u_role");
		//dump ($this->session);
		$this->load->model("customer/m_tbl_customer_appointment");
		$this->load->model("customer/m_tbl_users");
		$this->load->model("joborder/m_tbl_asset");

		//pending pani nga area
		$this->data["sales_rep"]= $this->m_tbl_users->get_by(array("u_role"=>3));
		//pending pani nga area
		$this->data['appointmentsview']=$this->m_tbl_customer_appointment->viewAppointment($id);

		$this->data['properties']= $this->m_tbl_asset->get_by(array('tbl_customer_id'=>$cust_id));
		$this->data['id']=$id;
        $this->data['subview'][]  = "backend/admin/customer/view-appointments";
        //$this->data['customerid'] = intVal($customerid);
        $this->load->view('backend/admin/home', $this->data, FALSE);
	}

}

?>