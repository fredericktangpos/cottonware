<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerController extends Admin_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('customer/m_customer');
        $this->load->model('customer/m_customer_contact');
        $this->data['active_nav'] = "customer";
    }


    //index function
    public function index()
    {
        $this->data['subview'][]  = "backend/admin/customer/customer-list";
        $this->load->view('backend/admin/home', $this->data, FALSE);
    }

   

    //edit function
    public function edit($customerid)
    {
        $this->load->model("customer/m_tbl_customer_appointment");
        $this->load->model("user/m_tbl_users");

        $this->load->library('pagination');
        $this->load->model('joborder/m_tbl_asset');
        $this->load->model('joborder/m_tbl_job_transaction');
        $this->data['customer_id']  = $customerid;
        $this->data['properties']   = $this->m_tbl_asset->get_by(array('tbl_customer_id'=>$customerid));
        $this->data['joborder_summary']   = $this->m_tbl_job_transaction->getCostumerTransactions($customerid);
        $this->data['appointments']         = $this->m_tbl_customer_appointment->getAppointment($customerid);
        $this->data["sales_rep"]             = $this->m_tbl_users->get_by(array("u_role"=>3));
        
        $this->data['subview'][]  = "backend/admin/customer/customer-edit";
        $this->data['customerid'] = intVal($customerid);
        $this->load->view('backend/admin/home', $this->data, FALSE);

    }

    //create a company via ajax
    public function customerCreationAjax()
    {
        $customerid      = intVal($this->input->post('customer_id',TRUE))===0 ? NULL: $this->input->post('customer_id',TRUE);;
        $_customer_id    = (int)$this->input->post('customer_id',TRUE);
        $cus_name        = $this->input->post('cus_name',TRUE);
        $cus_salutations = $this->input->post('cus_salutations',TRUE);
        $cus_role        = $this->input->post('cus_role',TRUE);

        $cus_hom_num     = json_encode($this->input->post('cus_hom_num',TRUE));
        $cus_off_num     = json_encode($this->input->post('cus_off_num',TRUE));
        $cus_fax_num     = json_encode($this->input->post('cus_fax_num',TRUE));
        $cus_han_num     = json_encode($this->input->post('cus_han_num',TRUE));
        $cus_email       = json_encode($this->input->post('cus_email',TRUE));

        $cus_blk         = $this->input->post('cus_blk',TRUE);
        $cus_street      = $this->input->post('cus_street',TRUE);
        $cus_bldg        = $this->input->post('cus_bldg',TRUE);
        $cus_unit        = $this->input->post('cus_unit',TRUE);
        $cus_postal      = $this->input->post('cus_postal',TRUE);
        $cus_remarks     = $this->input->post('cus_remarks',TRUE);

        $error           = false;
        $content         = "";


        $this->form_validation->set_rules('cus_salutations', 'Customer Salutations', 'required|trim|xss_clean');
        $this->form_validation->set_rules('cus_role', 'Customer Role', 'required|trim|xss_clean');
        $this->form_validation->set_rules('cus_name', 'Customer Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('cus_off_num[]', 'Office Number', 'trim|xss_clean');
        $this->form_validation->set_rules('cus_hom_num[]', 'Home Number', 'trim|xss_clean');
        $this->form_validation->set_rules('cus_fax_num[]', 'Fax Number', 'trim|xss_clean');
        $this->form_validation->set_rules('cus_han_num[]', 'Hand Phone', 'trim|xss_clean');
        $this->form_validation->set_rules('cus_email[]', 'Email Address', 'trim|xss_clean');
        $this->form_validation->set_rules('cus_postal', 'Postal', 'required|trim|xss_clean');

        if($this->form_validation->run()!==FALSE){
            $insert     = array
                            (
                                'cus_salutations'    => $cus_salutations,
                                'cus_role'           => $cus_role,
                                'cus_name'           => $cus_name,
                                'cus_fax_num'        => $cus_fax_num,
                                'cus_hom_num'        => $cus_hom_num,
                                'cus_han_num'        => $cus_han_num,
                                'cus_off_num'        => $cus_off_num,
                                'cus_email'          => $cus_email,
                                'cus_address_blk'    => $cus_blk,
                                'cus_address_street' => $cus_street,
                                'cus_address_unit'   => $cus_unit,
                                'cus_address_bldg'   => $cus_bldg,
                                'cus_address_postal' => $cus_postal,
                                'cus_remark'         => $cus_remarks,
                                'cus_status'         => 1
                            );

            $customerid     = $this->m_customer->save($insert,$customerid);
            if ($_customer_id == 0)
            {
                $this->load->model("joborder/m_tbl_asset");
                $this->load->model("customer/m_customer_contact");

                $default_property = array('tbl_customer_id' => $customerid,
                                        'asset_name' => $cus_street,
                                        'asset_add_unit' => $cus_unit,
                                        'asset_add_street' => $cus_street,
                                        'asset_add_bldg' => $cus_bldg,
                                        'asset_add_postal' => $cus_postal, 
                                        'tbl_contact_person' => 0);

                $this->m_tbl_asset->save($default_property);

                $contact_person_data = array(
                                        "customer_id"=>$customerid,
                                        "name"=>$cus_name,
                                        "role"=>'Owner',
                                        "status"=>1);
                $this->m_customer_contact->save($contact_person_data);

            }

            $customer = $this->m_customer->describeCustomer($this->m_customer->get($customerid));


            $error    = false;
            $content  = $customer;
        }
        else{
            $error    = true;
            $content  = validation_errors();
        }

        die(json_encode(array('error'=>$error,'content'=>$content)));
    }

    //save contact person ajax
    public function customerContactPersonAjax()
    {
        $customerid      = intVal($this->input->post('customerid',TRUE));
        $contactid       = $this->input->post('contact_id',TRUE);
        $contact_name    = $this->input->post('contact_name',TRUE);
        $contact_contact = $this->input->post('contact_contact',TRUE);
        $contact_role    = $this->input->post('contact_role',TRUE);
        $contact_email   = $this->input->post('contact_email',TRUE);

        $error   = false;
        $content = "";

        $this->form_validation->set_rules('contact_name[]', 'Contact\'s Name', 'trim|xss_clean');
        $this->form_validation->set_rules('contact_contact[]', 'Contact\'s Number', 'trim|xss_clean');
        $this->form_validation->set_rules('contact_email[]', 'Contact\'s Email Address', 'trim|xss_clean|valid_email');
        // $this->form_validation->set_rules('contact_name[]', 'Contact\'s Name', 'trim|required|xss_clean');
        // $this->form_validation->set_rules('contact_contact[]', 'Contact\'s Number', 'trim|min_length[5]|xss_clean');
        // $this->form_validation->set_rules('contact_email[]', 'Contact\'s Email Address', 'trim|required|min_length[5]|xss_clean|valid_email');

        if(count($contactid)!==0):
            if($this->form_validation->run()!==FALSE):
                $this->db->where("customer_id",$customerid);
                $this->db->update("tbl_customers_contact",array("status"=>0));
                $insert_batch = array();

                for($i=0;$i<count($contactid);$i++):
                    if(!empty($contact_name[$i])):
                        $tmp_contact_id = intVal($contactid[$i]);
                        $tmp_arr        = array(
                                                    "customer_id" => $customerid,
                                                    "name"        => $contact_name[$i],
                                                    "contacts"    => $contact_contact[$i],
                                                    "role"        => $contact_role[$i],
                                                    "email"       => $contact_email[$i],
                                                    "status"      => 1
                                                );
                        if($tmp_contact_id!==0):
                            $this->db->where("id",$tmp_contact_id);
                            $this->m_customer_contact->save($tmp_arr,$tmp_contact_id);
                        else:
                            $insert_batch[] = $tmp_arr;
                        endif;
                    endif;
                endfor;

                if(count($insert_batch)!==0):
                    $this->db->insert_batch("tbl_customers_contact",$insert_batch);
                endif;

                $customer = $this->m_customer->describeCustomer($this->m_customer->get($customerid));
                $error    = false;
                $content  = $customer;
            else:
                $error   = true;
                $content = validation_errors();
            endif;
        endif;

        die(json_encode(array('error'=>$error,'content'=>$content)));
    }

    //get customers
    public function getCustomers()
    {
		$orderCol     = $this->input->post('orderCol',TRUE);
		$orderVal     = $this->input->post('orderVal',TRUE) =='true' ? "ASC":"DESC" ;
		$searchTerm   = $this->input->post('searchTerm',TRUE);
		$perPage      = $this->input->post('perPage',TRUE);
        $start        = $this->input->get('per_page',TRUE);
		$paginate     = array();

        $paginate['table']       = "tbl_customers";
        $paginate['select']      = "*";
        $paginate['like_filter'] = array('cus_name'=>$searchTerm, "cus_status"=>1);

        $paginate['per_page'] = $perPage;
        $paginate['order']    = array('column_name'=>$orderCol,'order'=>$orderVal);
        $paginate['base_url'] = ' ';
        $paginate['start']    = $start;

        $customer        = $this->m_customer->general_paginator($paginate);
        $customer["msg"] = $this->m_customer->describeCustomer($customer["msg"]);

		die(json_encode(array('error'=>false,'content'=>$customer)));
    }

    /**
     * remove customer , remove from list change status to 0, 1 default
     */

    function removeCustomer()
    {
        $intCustomer_id = $this->input->post("intCustomer_id");
        
        $customer_update = array("cus_status"=>0);

        $confirmation_id = $this->m_customer->save($customer_update,$intCustomer_id);

        die(json_encode(array('error'=>false,'customer_id'=>$confirmation_id)));
    }
    //get customer
    public function getCustomer()
    {
        $customerid = $this->input->get('customerID',TRUE);
        $customer   = $this->m_customer->describeCustomer($this->m_customer->get($customerid));
        die(json_encode(array('error'=>false,'content'=>$customer)));
    }

    //check if the customer has any duplicates
    public function checkDuplicate()
    {
        $cus_id   = $this->input->get('cus_id',TRUE);
        $cus_name = $this->input->get('cus_name',TRUE);
        $customers = $this->m_customer->describeCustomer($this->m_customer->get_by(array("(cus_name like '%{$cus_name}%' and id != {$cus_id})"=>NULL)));
        die(json_encode(array('error'=>false,'content'=>$customers)));
    }

    public function saveAppointment() {
        $this->load->model("customer/m_tbl_customer_appointment");
        $this->load->model("user/m_tbl_users");

        $intSalesRep_id = $this->input->post("selIntRepresentative_id",TRUE);
        $appointment_date = $this->input->post("appointment_date",TRUE);
        $appointment_time = $this->input->post("appointment_time",TRUE);
        $customer_id = $this->input->post("customer_id",TRUE);
        $customer_property  = $this->input->post("customer_property",TRUE);

        $appointment_data = array(
                                'sales_rep_id'=>$intSalesRep_id,
                                'date' => date('Y-m-d', strtotime(str_replace('/', '-', $appointment_date))),
                                'time' => $appointment_time,
                                'user_id' => $this->session->userdata("id"),
                                'customer_id' => $customer_id,
                                'customer_property' => $customer_property);

        $appointment_id = $this->m_tbl_customer_appointment->save($appointment_data);
        if(((int)$appointment_id) > 0)
        {
            $sales_info = $this->m_tbl_users->get_by(array("id"=>$intSalesRep_id));
            $added_by = $this->m_tbl_users->get_by(array("id"=>$this->session->userdata("id")));
            die(json_encode(array("success"=>true,"appointment_id"=>$appointment_id,"sales_name"=>$sales_info, "added_by"=>$added_by)));
        }else {
             die(json_encode(array("success"=>false,"appointment_id"=>0)));
        }
        
    }


        public function saveAppointment2() {
        $this->load->model("customer/m_tbl_customer_appointment");
        $this->load->model("user/m_tbl_users");

        $intSalesRep_id = $this->input->post("selIntRepresentative_id",TRUE);
        $appointment_date = $this->input->post("appointment_date",TRUE);
        $appointment_time = $this->input->post("appointment_time",TRUE);
        $peter_id = $this->input->post("peter_id",TRUE);
        $customer_property  = $this->input->post("customer_property",TRUE);

        $appointment_data = array(
                                'sales_rep_id'=>$intSalesRep_id,
                                'date' => date('Y-m-d', strtotime(str_replace('/', '-', $appointment_date))),
                                'time' => $appointment_time,
                                'user_id' => $this->session->userdata("id"),
                                'customer_property' => $customer_property);

        $appointment_id = $this->m_tbl_customer_appointment->save($appointment_data,$peter_id);
        if(((int)$appointment_id) > 0)
        {
            $sales_info = $this->m_tbl_users->get_by(array("id"=>$intSalesRep_id));
            $added_by = $this->m_tbl_users->get_by(array("id"=>$this->session->userdata("id")));
            die(json_encode(array("success"=>true,"appointment_id"=>$appointment_id,"sales_name"=>$sales_info, "added_by"=>$added_by)));
        }else {
             die(json_encode(array("success"=>false,"appointment_id"=>0)));
        }
        
    }

    public function deleteAppointment() {

        $this->load->model("customer/m_tbl_customer_appointment");
        $peter_id= $this->input->post ("peter_id",TRUE);
        $this->m_tbl_customer_appointment->delete($peter_id);
        die(json_encode(array('id'=>$peter_id)));
    }


//pending pani nga part
    public function UpdateStatusAppointment() {
    $this->load->model("customer/m_tbl_customer_appointment");
    $peter_id = $this->input->post("peter_id",TRUE);
    $appointment_data = array(
                            'status'=>1);

    $appointment_id = $this->m_tbl_customer_appointment->save($appointment_data,$peter_id);
    if(((int)$appointment_id) > 0)
    {
        die(json_encode(array("success"=>true,"appointment_id"=>$appointment_id)));
    }else {
         die(json_encode(array("success"=>false,"appointment_id"=>0)));
    }  
}

}