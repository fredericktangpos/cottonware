<?php

class Settings extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();

		$this->load->model('supplier/m_tbl_supplier');
		$this->load->model('settings/m_tbl_job_subcat');
		$this->load->model('settings/m_tbl_job_classification');
		$this->load->model('joborder/m_tbl_job_category');

		$user_id = $this->session->userdata('id');
	}

	public function index()
	{
		$this->load->library('pagination');

		// load all the supplier in the list
		$table_name 				 = 'tbl_supplier';
		$total						 = count($this->db->get($table_name)->result());
		$per_pg						 = 6;
		$offset						 = $this->uri->segment(3);
		$data['base']				 = $this->config->item('base_url');
		$this->data['pagination'] 	 = $this->__ajax_paginate($data['base'].'admin/settings', $total, $per_pg);
									   $this->db->where(array("status"=>1));
		$this->data['supp_summary']  = $this->db->get($table_name,$per_pg,$offset)->result();

										$this->db->where(array("status"=>1));
		$this->data['supplier'] 	= 	$this->m_tbl_supplier->get();

		// load all the job category in the list
		$table_name1 				 = 'tbl_job_subcat';
		$total1						 = count($this->db->get($table_name1)->result());
		$per_pg1					 = 6;
		$offset1					 = $this->uri->segment(3);
		$data['base1']				 = $this->config->item('base_url');
		$this->data['pagination1']   = $this->__ajax_paginate($data['base1'].'admin/settings', $total1, $per_pg1);
									   $this->db->where(array("status"=>1));
		$this->data['job_cat1'] 	 = $this->db->get($table_name1,$per_pg1,$offset1)->result();

		// load all the job classification in the list
		$table_name2 				 = 'tbl_job_classification';
		$total2						 = count($this->db->get($table_name2)->result());
		$per_pg2					 = 6;
		$offset2					 = $this->uri->segment(3);
		$data['base2']				 = $this->config->item('base_url');
		$this->data['pagination2']   = $this->__ajax_paginate($data['base1'].'admin/settings', $total2, $per_pg2);
		$this->data['job_class'] 	 = $this->m_tbl_job_classification->get_classification($table_name2,$per_pg2,$offset2);

		// ==============
		//$this->data['supplier'] 	 = $this->m_tbl_supplier->get();
		//$this->data['job_category']  = $this->m_tbl_job_category->get();

		//$this->data['subview'][] 	 = 'backend/admin/single';
		$this->data['subview'][] = 'backend/admin/settings/setting_dashboard';
		$this->load->view('backend/admin/home',$this->data);
	}



	#redirect to suppler settings
	public function to_suppliers()
	{
		redirect("admin/supplier");
	}

	public function supplier_details()
	{
		$this->data['subview'][] = 'backend/admin/settings/supplier_details';
		$this->load->view('backend/admin/home',$this->data);
	}

	public function to_curtains()
	{
		redirect("admin/curtaincontroller");
	}

	public function to_users(){
		redirect("admin/user");
	}

	public function to_menus()
	{
		redirect("admin/menu");
	}

	# add customer via ajax
	public function ADD_SUPPLIER()
	{
		$supp_co_name 			= $this->input->post('supp_name');
		$supp_contact_person 	= $this->input->post('supp_contact_name');
		$supp_contact_num 		= $this->input->post('supp_contact_num');
		$supp_email 			= $this->input->post('supp_email');

		if(empty($supp_co_name))
		{
			echo json_encode(array('stat'=>false, 'msg'=>'Supplier name must not be empty'));
		}
		else
		{
			$data = array(
				'supp_co_name' 			=> $supp_co_name,
				'supp_contact_person' 	=> $supp_contact_person,
				'supp_contact_num' 		=> $supp_contact_num,
				'supp_email' 			=> $supp_email
			);

			$save_supplier = $this->m_tbl_supplier->save($data);
			$thedata = $this->m_tbl_supplier->get($save_supplier);

			if(count($save_supplier) > 0)
			{
				echo json_encode(array('stat'=>true, 'msg'=>'New supplier successfully added', 'content'=>$thedata));
			}
		}
	}

	# add job category via ajax
	# 
	
	public function ADD_JOB_ORDER()
	{
		$job_name 		= $this->input->post('job_name');
		$job_desc 		= $this->input->post('job_desc');
		$job_cat_stat 	= $this->input->post('cat_stat');
		$subCategory	= $this->input->post('subCategory');

		if(empty($job_name))
		{
			echo json_encode(array('stat'=>false, 'msg'=>'Category name must not be empty'));
		}
		else
		{
			$data = array(
				'transaction_name' 		=> $job_name,
				'transaction_description' 		=> $job_desc,
				'is_available' 	=> $job_cat_stat
			);

			$save_cat = $this->m_tbl_job_category->save($data);
			if(count($save_cat) > 0)
			{
				if(!empty($subCategory))
				{
					foreach ($subCategory as $subCategory)
					{
						if(!empty($subCategory))
						{
							$subcat = array(
								'job_type_name' 	=> $subCategory,
								'tbl_job_category_id'	=> $save_cat
							);
							$save_subcat = $this->m_tbl_job_subcat->save($subcat);
						}
					}
				}
			}
			echo json_encode(array('stat'=>true, 'msg'=>'New Job category successfully added'));
		}
	} 
	

	# get sub category via ajax
	public function GET_SUBCATEGORY()
	{
		$parent_jobcat_id = $this->input->post('jobcat_id');

		if(!empty($parent_jobcat_id))
		{
			$subcat_list = $this->m_tbl_job_subcat->get_by(array('tbl_job_category_id'=>$parent_jobcat_id));
			if(count($subcat_list) > 0)
			{
				echo json_encode(array('subcat_list'=>$subcat_list));
			}
		}
	}

	# add new job classification
	public function ADD_JOB_CLASSIFICATION()
	{
		$job_class_name  = $this->input->post('job_class_name');
		$tbl_supplier_id = $this->input->post('tbl_supplier_id');
		$job_category_id = $this->input->post('job_category_id');
		$sub_category    = $this->input->post('sub_category');
		$job_class_desc  = $this->input->post('job_class_desc');

		$data = array(
			'job_class_name' 		=> $job_class_name,
			'job_formula'	 		=> 0,
			'job_desc'	 			=> $job_class_desc,
			'tbl_job_category_id'	=> $job_category_id,
			'tbl_supplier_id'	 	=> $tbl_supplier_id
		);

		$save_job_class = $this->m_tbl_job_classification->save($data);
		if(count($save_job_class) > 0)
		{
			echo json_encode(array('msg'=>'Successfully adding Job Classification'));
		}

	}

	public function addItem()
	{
		$chrItem_name        = $this->input->post("chrItem_name",TRUE);
		$intSupplier_id      = $this->input->post("intSupplier_id",TRUE);
		$chrItem_category    = $this->input->post("chrItem_category",TRUE);
		$chrPrice_amount     = $this->input->post("chrPrice_amount",TRUE);
		$chrType_code     = $this->input->post("chrType_code",TRUE);
		//$chrItem_description = $this->input->post("chrItem_description",TRUE);

		$tbl_job_subcat_data = array(
			'job_type_name' => $chrItem_name,
			'sub_cat_type'  =>$chrItem_category
		);

		$save_item = $this->m_tbl_job_subcat->save($tbl_job_subcat_data);
		if((int)$save_item>0)
		{
			$tbl_job_classification_data = array(
			'cost_amount'         =>$chrPrice_amount,
			'tbl_supplier_id'     =>$intSupplier_id,
			'tbl_job_subcat_id'   =>$save_item,
			'type_code'		=>$chrType_code
			);
			
			$save_item_info = $this->m_tbl_job_classification->save($tbl_job_classification_data);
			
			echo json_encode(array('success'=>'true'));
		}else
		{
			echo json_encode(array('success'=>'false'));
		}
		
	}

	public function addItemSupplier()
	{
		$item_id             = $this->input->post("item_id",TRUE);
		$intSupplier_id      = $this->input->post("intSupplier_id",TRUE);
		$intItem_category    = $this->input->post("intItem_category",TRUE);
		$chrPrice_amount     = $this->input->post("chrPrice_amount",TRUE);
		$chrItem_description = $this->input->post("chrItem_description",TRUE);

		$tbl_job_classification_data = array(
			'cost_amount'         =>$chrPrice_amount,
			'tbl_supplier_id'     =>$intSupplier_id,
			'tbl_job_subcat_id'   =>$item_id,
			'type_code'		=>$chrType_code
		);

		$save_item_info = $this->m_tbl_job_classification->save($tbl_job_classification_data);

		if((int)$save_item_info > 0 )
		{	
			echo json_encode(array("success"=>"true"));
		}
		else
		{
			echo json_encode(array("success"=>"false"));
		}
	}

	public function getSubType()
	{
		//$intTransaction_type = $this->input->post("transaction_type",TRUE);
		$intTransaction_type= 1 ;
		$this->load->model("joborder/m_tbl_job_subcat");

		$result = $this->m_tbl_job_subcat->getSubType($intTransaction_type);

		die(json_encode(array('sub_type'=>$result)));

	}

	public function updateJobTransaction()
	{
		/*
		$this->load->model("joborder/m_tbl_job_transaction");
		$this->load->model("joborder/m_tbl_transaction_item");
		$this->load->model("joborder/m_tbl_miscellanous");

		$_transaction_list = $this->m_tbl_job_transaction->get();
		foreach ($_transaction_list as $transaction) {
			$transaction_id = $transaction->id;
			$transaction_basics = $this->m_tbl_transaction_item->getBasics($transaction_id);
			if (count($transaction_basics) > 0)//if result is not empty
			{
				$_update_data = array(
								"asset_id"=>$transaction_basics[0]->asset_id,
								"contact_person"=>$transaction_basics[0]->tbl_customer_contact_id);
				dump($this->m_tbl_job_transaction->save($_update_data,$transaction_id));
			} else {
				$_update_data = array(
								"asset_id"=>1,
								"contact_person"=>1);
				dump($this->m_tbl_job_transaction->save($_update_data,$transaction_id));
			}
		
			
		}

		*/
	}

	public function removeDummyTransactions()
		{
			$this->load->model("joborder/m_tbl_job_transaction");
			$this->load->model("joborder/m_tbl_transaction_images");
			$this->load->model("joborder/m_tbl_transaction_item");
			$this->load->model("joborder/m_tbl_asset");
			$this->load->model("joborder/m_tbl_asset_dtl");
			$this->load->model("joborder/m_tbl_transaction_type_selection");
			$this->load->model("joborder/m_tbl_quotation");
			$this->load->model("joborder/m_tbl_miscellanous");
			$this->load->model("customer/m_tbl_customer_appointment");

			//get all the transactions
			$transaction_list = $this->m_tbl_job_transaction->get_by(array("tbl_users_id"=>28));
			//dump($transaction_list);
			if( count($transaction_list) > 0 ) {
				foreach ($transaction_list as $transaction) {
					$transaction_id = (int)$transaction->id;

					//get all transaction item from this transaction id
					
					$transaction_items = $this->m_tbl_transaction_item->get_by(array("tbl_job_transaction_id"=>$transaction_id));
					if ( count($transaction_items) > 0 ) {

						foreach ($transaction_items as $trans_item) {
							$asset_dtl_id = $trans_item->asset_dtl_id;

							//get the transaction_item id to delete data on transaction_type_selection
							$item_id = $trans_item->id;

							$type_selections = $this->m_tbl_transaction_type_selection->get_by(array("tbl_transaction_item_id"=>$item_id));
							
							if ( count($type_selections) > 0 )
							{
								foreach ($type_selections as $selection) {
									$selection_id = $selection->id;
									$this->m_tbl_transaction_type_selection->deleteItem($selection_id);
								}
							}

							$this->m_tbl_transaction_item->deleteItem($item_id);
							$this->m_tbl_asset_dtl->deleteItem($asset_dtl_id);
							
						}

					}

					//get all miscellanous job for this transaction id
					
					$transaction_miscellanous = $this->m_tbl_miscellanous->get_by(array("transaction_id"=>$transaction_id));
					if ( count($transaction_miscellanous) > 0 )
					{
						foreach ($transaction_miscellanous as $misc) {
							$misc_id = $misc->id;
							$this->m_tbl_miscellanous->deleteItem($misc_id);
						}
					}
					

					$transaction_images = $this->m_tbl_transaction_images->get_by(array("transaction_id"=>$transaction_id));
					//dump($transaction_images);
					if ( count($transaction_images) > 0 )
					{
						foreach ($transaction_images as $images) {
							$images_id = $images->id;
							$this->m_tbl_transaction_images->deleteItem($images_id);
						}
					}

					$appointments_list = $this->m_tbl_customer_appointment->get_by(array("transaction_id"=>$transaction_id));
					if ( count($appointments_list) > 0 )
					{
						foreach ($appointments_list as $appointment) {
							$appointment_id = $appointment->id;
							$this->m_tbl_customer_appointment->deleteItem($appointment_id);
						}
					}

					$quotation_list = $this->m_tbl_quotation->get_by(array("tbl_job_transaction_id"=>$transaction_id));
					if ( count($quotation_list) > 0 )
					{
						foreach ($quotation_list as $quotation) {
							$quotation_id = $quotation->id;
							$this->m_tbl_quotation->deleteItem($quotation_id);
						}
					}

					$this->m_tbl_job_transaction->deleteItem($transaction_id);
				}
			}

		}


} # end of class

?>
