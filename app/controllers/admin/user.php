<?php
class User extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();
		$this->load->model('user/m_tbl_users');
	}

	public function index() 
	{
		$the_user = $this->session->all_userdata();
		$this->load->model('user/m_tbl_users');

		$this->data['ses_info']		= $the_user;
		$this->data['page_title'] 	= 'User Area';
		$this->data['subview'] 		= 'backend/admin/user/users-list';
		$this->data['dbArUser_list'] = $this->m_tbl_users->get();
		$this->load->view('backend/admin/home',$this->data);
	}

	public function login() {

		$dashboard = 'admin/dashboard';
		$this->m_tbl_users->loggedin() == FALSE || redirect($dashboard);
		$rules = $this->m_tbl_users->rules;
		$this->form_validation->set_rules($rules);
		#validate the input and process
		if($this->form_validation->run() == TRUE) {
			# if the credential is true then procced
			if($this->m_tbl_users->login() == TRUE) {
				redirect($dashboard);
			}
			else {
				$this->session->set_flashdata('error', 'Credential not match.');
				redirect('admin/user/login', 'refresh');
			}
		}
		// $this->data['subview'] = 'backend/admin/user/login';
		$this->load->view('backend/admin/login',$this->data);
	}


	public function logout() 
	{
		$this->m_tbl_users->logout();
		redirect('admin/user/login');
	}

	public function newUserForm()
	{
		$this->load->model("user/m_tbl_user_types");
		$this->data['user_types']       = $this->m_tbl_user_types->get();
		$this->load->model("user/m_tbl_menus");
		$dbArMenu_list = $this->m_tbl_menus->get();
		$this->data['dbArMenu_list'] = $dbArMenu_list;
		$this->data['subview'] 		= 'backend/admin/user/new-user-form';
		$this->load->view('backend/admin/home',$this->data);
	}

	public function addNew()
	{
		//log in credentials
		$chrUser_name = $this->input->post("txtChrUser_name",TRUE);
		$chrUser_password = $this->input->post("txtChrUser_password",TRUE);
		$chrUser_password_confirm = $this->input->post("txtChrUser_password_confirm",TRUE);
		$intUser_role = $this->input->post("selIntUser_role",TRUE);

		//personal information
		$chrUser_first_name = $this->input->post("txtChrFirst_name",TRUE);
		$chrUser_last_name = $this->input->post("txtChrLast_name",TRUE);
		$chrUser_email = $this->input->post("txtChrUser_email",TRUE);
		$chrUser_contact_number = $this->input->post("txtChrUser_contact",TRUE);
		$chrUser_address = $this->input->post("txtChrUser_contact",TRUE);

		$user_data = array(
							'u_fname'=>$chrUser_first_name,
							'u_lname'=>$chrUser_last_name,
							'u_email'=>$chrUser_email,
							'u_contact'=>$chrUser_contact_number,
							'u_address'=>$chrUser_address,
							'u_username'=>$chrUser_name,
							'u_pass'=>$this->m_tbl_users->hash($chrUser_password),
							'u_role'=>$intUser_role);
		$user_id  = $this->m_tbl_users->save($user_data);

		if((int)$user_id > 0)
		{
			$this->load->model("user/m_tbl_user_access");
			$user_access = $this->input->post("selIntUser_access",TRUE);
			if(count($user_access) > 1)
			{
				for($x = 0 ; $x < count($user_access); $x++) {
					$data = array('tbl_users_id'=>$user_id,'menu_id'=>(int)$user_access[$x]);
					$access_id = $this->m_tbl_user_access->save($data);
					if($access_id == 0 || $access_id == null)
					{
						exit;
					}
				}
			}

			redirect("admin/settings/to_users");

		}else{

			return false;

		}
		


	}

	public function deleteUser($intUserID)
	{
		$intUserID = (int)$intUserID;
		$user_id = $this->m_tbl_users->save(array("status"=>0),$intUserID);
		if((int)$user_id > 0 )
		{
			die(json_encode(array('status'=>"true",'user_id'=>$user_id)));
		}else 
		{
			die(json_encode(array('status'=>"false",'user_id'=>0)));
		}
	}

	public function updateUser($intUserID)
	{
		$intUserID = (int)$intUserID;
		$this->load->model("user/m_tbl_menus");
		$this->load->model("user/m_tbl_user_access");
		$this->data['dbArMenu_list'] = $this->m_tbl_menus->get();
		$this->data['userAccess'] = $this->m_tbl_user_access->get_by(array("tbl_users_id"=>$intUserID));
		$this->data['userDetails'] = $this->m_tbl_users->get_by(array("id"=>$intUserID));
		$this->data['subview'] 		= 'backend/admin/user/edit-user';
		$this->load->view('backend/admin/home',$this->data);

	}

	public function update()
	{
		$this->load->model("user/m_tbl_menus");
		$this->load->model("user/m_tbl_user_access");
		$intUser_id = $this->input->post("hdnIntUserId",TRUE);

		$user_access = $this->input->post("selIntUser_access",TRUE);
		$old_userAccess = $this->m_tbl_user_access->get_by(array("tbl_users_id"=>$intUser_id));

		//delete all the access..
		$this->m_tbl_user_access->_removeAccess($intUser_id);

		$chrUser_name = $this->input->post("txtChrUser_name",TRUE);
		$chrUser_password = $this->input->post("txtChrUser_password",TRUE);
		$chrUser_password_confirm = $this->input->post("txtChrUser_password_confirm",TRUE);
		$intUser_role = $this->input->post("selIntUser_role",TRUE);

		

		//personal information
		$chrUser_first_name = $this->input->post("txtChrFirst_name",TRUE);
		$chrUser_last_name = $this->input->post("txtChrLast_name",TRUE);
		$chrUser_email = $this->input->post("txtChrUser_email",TRUE);
		$chrUser_contact_number = $this->input->post("txtChrUser_contact",TRUE);
		$chrUser_address = $this->input->post("txtChrUser_contact",TRUE);
		$user_data = array();
		if($chrUser_password!= "")
		{
			$user_data = array(
							'u_fname'=>$chrUser_first_name,
							'u_lname'=>$chrUser_last_name,
							'u_email'=>$chrUser_email,
							'u_contact'=>$chrUser_contact_number,
							'u_address'=>$chrUser_address,
							'u_username'=>$chrUser_name,
							'u_pass'=>$this->m_tbl_users->hash($chrUser_password),
							'u_role'=>$intUser_role);
		}else if ($chrUser_password == "")
		{
			$user_data = array(
							'u_fname'=>$chrUser_first_name,
							'u_lname'=>$chrUser_last_name,
							'u_email'=>$chrUser_email,
							'u_contact'=>$chrUser_contact_number,
							'u_address'=>$chrUser_address,
							'u_username'=>$chrUser_name,
							'u_role'=>$intUser_role);
		}

		$this->m_tbl_users->save($user_data,$intUser_id);

		$this->load->model('users/m_tbl_user_access');
		if(count($user_access) > 1)
		{
			
			for ($i=0; $i < count($user_access); $i++) { 
				$access_id = (int)$user_access[$i];
				$test_data = array("menu_id"=>$access_id,"tbl_users_id"=>$intUser_id);
				$result = $this->m_tbl_user_access->_isExist($test_data);
				$_isExist = count($result);
				if($_isExist == 0)
				{
					$user_ac_id = $this->m_tbl_user_access->save($test_data);
					if($user_ac_id == 0)
					{
						die();
					}
				}
			}
		}
		redirect('admin/user/updateUser/'.$intUser_id);

	}

	public function userUpdate()
	{
		$this->load->model("user/m_tbl_menus");
		$this->load->model("user/m_tbl_user_access");
		$intUser_id = $this->input->post("hdnIntUserId",TRUE);

		$user_access = $this->input->post("selIntUser_access",TRUE);
		$old_userAccess = $this->m_tbl_user_access->get_by(array("tbl_users_id"=>$intUser_id));

		$chrUser_name = $this->input->post("txtChrUser_name",TRUE);
		$chrUser_password = $this->input->post("txtChrUser_password",TRUE);
		$chrUser_password_confirm = $this->input->post("txtChrUser_password_confirm",TRUE);
		$intUser_role = $this->input->post("selIntUser_role",TRUE);

		

		//personal information
		$chrUser_first_name = $this->input->post("txtChrFirst_name",TRUE);
		$chrUser_last_name = $this->input->post("txtChrLast_name",TRUE);
		$chrUser_email = $this->input->post("txtChrUser_email",TRUE);
		$chrUser_contact_number = $this->input->post("txtChrUser_contact",TRUE);
		$chrUser_address = $this->input->post("txtChrUser_address",TRUE);
		$user_data = array();
		if($chrUser_password!= "")
		{
			$user_data = array(
							'u_fname'=>$chrUser_first_name,
							'u_lname'=>$chrUser_last_name,
							'u_email'=>$chrUser_email,
							'u_contact'=>$chrUser_contact_number,
							'u_address'=>$chrUser_address,
							'u_username'=>$chrUser_name,
							'u_pass'=>$this->m_tbl_users->hash($chrUser_password),
							'u_role'=>$intUser_role);
		}else if ($chrUser_password == "")
		{
			$user_data = array(
							'u_fname'=>$chrUser_first_name,
							'u_lname'=>$chrUser_last_name,
							'u_email'=>$chrUser_email,
							'u_contact'=>$chrUser_contact_number,
							'u_address'=>$chrUser_address,
							'u_username'=>$chrUser_name,
							'u_role'=>$intUser_role);
		}

		$this->m_tbl_users->save($user_data,$intUser_id);

		$this->load->model('users/m_tbl_user_access');
		redirect('admin/user/userProfile/'.$intUser_id);

	}
	public function userSchedule()
	{
		$this->load->model("customer/m_tbl_customer_appointment");
		$userRole = $this->session->userdata("u_role");
		$scheduleList = array();
		if($userRole == 1)
		{
			$scheduleList = $this->m_tbl_customer_appointment->getAppointment(null);
		} else if ($userRole == 2) 
		{

		} else if ($userRole == 3)
		{
			$scheduleList = $this->m_tbl_customer_appointment->getAppointment($this->session->userdata("id"));

		}
		die(json_encode(array("schedules"=>$scheduleList)));
	}

	public function userProfile($id = null)
	{
		$intUserID = (int)$id;
		$this->load->model("user/m_tbl_menus");
		$this->load->model("user/m_tbl_user_access");
		$this->data['dbArMenu_list'] = $this->m_tbl_menus->get();
		$this->data['userAccess'] = $this->m_tbl_user_access->get_by(array("tbl_users_id"=>$intUserID));
		$this->data['userDetails'] = $this->m_tbl_users->get_by(array("id"=>$intUserID));
		$this->data['subview'] 		= 'backend/admin/user/user-profile';
		$this->load->view('backend/admin/home',$this->data);
	}

	function uploadImage()
	{
		$image = @$this->input->post("image_src");
		$user_id = @$this->input->post("user_id");
		$file_name = "image-".$user_id.".png";
		$uri = substr($image,strpos($image,",")+1);
		if(file_put_contents( $_SERVER['DOCUMENT_ROOT']."/img/userimages/".$file_name, base64_decode($uri)))
		{
			$imageData = array(
		  					"image"=>$file_name);
			$user_id = $this->m_tbl_users->save($imageData,$user_id);
			if($user_id > 0 ) {
				die(json_encode(array("success"=>true,"user_id"=>$user_id)));
			}
		}	
	}
} #end of class

?>