<?php
class Menu extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();
		$this->load->model('user/m_tbl_menus');
	}

	public function index() 
	{
		$the_user = $this->session->all_userdata();
		$dbArMenu_list = $this->m_tbl_menus->get();
		$this->data['ses_info']		= $the_user;
		$this->data['page_title'] 	= 'Menu Settings';
		$this->data['subview'] 		= 'backend/admin/user/menus';
		$this->data['dbArMenu_list'] = $dbArMenu_list;
		$this->load->view('backend/admin/home',$this->data);
	}

	public function addMenu()
	{
		$display_name = $this->input->post("menu_display_name",TRUE);
		$slug = $this->input->post("menu_slug",TRUE);
		$icon_class = $this->input->post("menu_icon_class",TRUE);

		$data = array(
					'display_name' => $display_name,
					'slug' => $slug,
					'icon_class' => $icon_class);
		$menu_id = $this->m_tbl_menus->save($data);

		if((int)$menu_id > 0)
		{
			redirect("admin/settings/to_menus");
		}
	}
	public function menuInformation($menu_id)
	{
		$this->load->model("user/m_tbl_menus");
		$data = $this->m_tbl_menus->get_by(array("id"=>$menu_id));
		die(json_encode($data));
	}
	
	public function saveChanges()
	{
		$this->load->model("user/m_tbl_menus");

		$menu_id = (int)$this->input->post("menu_id",TRUE);
		$menu_display_name = $this->input->post("display_name",TRUE);
		$menu_slug  = $this->input->post("slug",TRUE);
		$menu_icon_class = $this->input->post("icon_class",TRUE);

		$data = array(	"display_name" => $menu_display_name,
						"slug" => $menu_slug,
						"icon_class" => $menu_icon_class  );
		$result_id = (int)$this->m_tbl_menus->save($data,$menu_id);
		if($menu_id == $result_id)
		{
			die(json_encode(array("success"=>true,"result_id" => $result_id)));
		} else {
			die(json_encode(array("success"=>false,"result_id" => 0)));
		}
	}

} #end of class

?>