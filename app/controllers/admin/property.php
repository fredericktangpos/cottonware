<?php

class Property extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();

		$this->load->model('joborder/m_tbl_asset');
	}

	public function add() {

		$user_id = $this->session->userdata('id');
		$this->data['subview'][] = 'backend/admin/customer/property_form';
		$this->load->view('backend/admin/home',$this->data);
	}
	/**
	 * insert new property to specific customer
	 * @param  [int] $customer_id customers id
	 * @return [json]             return json encoded value
	 */
	public function insertProperty ($customer_id) {
		$this->load->helper('date');
		$intCustomer_id = $customer_id;
		$aRproperty_details = json_decode($this->input->post('aRproperty_details',TRUE));
		$image_name = $customer_id.'-'.time();
		$_FILES['fileProperty_image']['name'] = $image_name.'.'.strtolower(pathinfo($_FILES["fileProperty_image"]["name"], PATHINFO_EXTENSION));

		$aRproperty_data = array(

			'tbl_customer_id'    =>$aRproperty_details[0]->customer_id,
			'asset_name'         =>$aRproperty_details[0]->chrProperty_name,
			'asset_add_unit'     =>$aRproperty_details[0]->chrProperty_unit,
			'asset_add_street'   =>$aRproperty_details[0]->chrProperty_street,
			'asset_add_bldg'     =>$aRproperty_details[0]->chrProperty_building,
			'asset_add_postal'   =>$aRproperty_details[0]->chrProperty_postal,
			'tbl_contact_person' =>'0',
			'asset_photo'        =>'img/properties/'.$_FILES['fileProperty_image']['name']

		);

		$result = $this->m_tbl_asset->save($aRproperty_data);
		$this->upload_file('fileProperty_image','img/properties/');
		echo json_encode(array('file_name'=>$_FILES['fileProperty_image']['name'],'property_id'=>$result));

	}
}

?>