<?php 
	class Curtaincontroller extends Admin_Controller
	{
		public function __construct() {
			parent::__construct();

			$this->load->model('joborder/m_tbl_job_subcat');
			$user_id = $this->session->userdata('id');
		}

		public function index()
		{
			$user_id = $this->session->userdata('id');

			$this->data['page_title'] = 'Type of Curtains';
			$this->data['subview'][] = 'backend/admin/settings/setting_curtains';

			
			//dump($this->data['curtain_types']);
			$this->load->view('backend/admin/home',$this->data);
		}

		public function json_curtain()
		{
			$config = array(
					'table'=>'tbl_job_subcat',
					'select' =>'*',
					'start'=>@$_GET['per_page']!==NULL?$_GET['per_page']:0,
					'per_page'=>20,
					'filter'=>array(
								'status'=>1,
								'transaction_type_id'=>1
								),
					'base_url' =>'curtaincontroller'
				);
			$this->data['curtain_types'] 	= 	$this->m_tbl_job_subcat->general_paginator($config);			
			die(json_encode($this->data['curtain_types']));
		}
		/**
		 * [get the types subtypes like styles of a certain curtain type]
		 * @param  [type] $sub_id [types id]
		 * @return [NULL]         []
		 */
		public function typesSubType($sub_id)
		{
			$this->data['page_title'] = 'Sub Type';
			//$this->data['subview'][] = 'backend/admin/settings/setting_curtains';

			$config = array(
						'table'=>'tbl_job_classification',
						'select' =>'tbl_job_classification.id AS type_id,tbl_job_classification.* , tbl_supplier.*',
						'start'=>@$_GET['per_page']!==NULL?$_GET['per_page']:0,
						'per_page'=>15,
						'filter'=>array(
						'tbl_job_classification.status'=>1,
						'tbl_job_classification.tbl_job_subcat_id'=>$sub_id
						),
						'base_url' =>$sub_id,
						'join' => array
							(
								array
								(
									'table'=>'tbl_supplier',
									'condition'=>'tbl_supplier.id  = tbl_job_classification.tbl_supplier_id'
								)
							),
						'order'=>array('column_name'=>"tbl_job_classification.type_name",'order'=>"asc")
				);
			$this->data['sub_sub_type'] 	= 	$this->m_tbl_job_subcat->general_paginator($config);
			$this->load->model("supplier/m_tbl_supplier");
			$this->data['suppliers']=$this->m_tbl_supplier->get_by(array('status'=>1));
			$this->data['type_name']= $this->m_tbl_job_subcat->get_by(array('id'=>$sub_id));
			$this->data['type_id']= $sub_id;
			//dump($this->data['suppliers']);
			//dump($this->data['type_name']);
			$this->data['subview'][] = 'backend/admin/settings/view_curtain';
			$this->load->view('backend/admin/home',$this->data);
		}

		public function addType()
		{
			$chrType_name  = $this->input->post("chrType_name",TRUE);
			$result_id = $this->m_tbl_job_subcat->save(array('job_type_name'=>$chrType_name,'transaction_type_id'=>1));
			if($result_id!=NULL)
			{
				die(json_encode(array('success'=>'true','result_id'=>$result_id)));
			}else
			{
				die(json_encode(array('success'=>'false','result_id'=>NULL)));
			}
		}

		public function addSwatch()
		{
			$chrSwatch_name            =$this->input->post("chrSwatch_name",TRUE);
			$chrSwatch_type_code       =$this->input->post("chrSwatch_type_code",TRUE);
			$intSwatch_width           =$this->input->post("intSwatch_width",TRUE);
			$fltSwatch_price_per_meter =$this->input->post("fltSwatch_price_per_meter",TRUE);
			$intSupplier_id            =$this->input->post("intSupplier_id",TRUE);
			$main_sub_type_id             =$this->input->post("main_sub_type_id",TRUE);
			$blinds_min_width             = $this->input->post("min_width_panel",TRUE);

			$swatch_data =array(
							'cost_amount'=>$fltSwatch_price_per_meter,
							'tbl_supplier_id'=>$intSupplier_id,
							'tbl_job_subcat_id'=>$main_sub_type_id,
							'type_code'=>$chrSwatch_type_code,
							'width'=>$intSwatch_width,
							'type_name'=>$chrSwatch_name,
							'min_width'=>$blinds_min_width
						);
			$this->load->model("settings/m_tbl_job_classification");
			$result_id = $this->m_tbl_job_classification->save($swatch_data);

			if($result_id!=NULL)
			{
				die(json_encode(array('success'=>'true','result_id'=>$result_id)));
			}else
			{
				die(json_encode(array('success'=>'true','result_id'=>NULL)));
			}
		}


		public function updateType()
		{
			$intTypeID = $this->input->post("intTypeID",TRUE);
			$chrTypeName = $this->input->post("chrTypeName",TRUE);

			$data = array(
					'job_type_name'=>$chrTypeName
					);
			$result_id = $this->m_tbl_job_subcat->save($data,$intTypeID);

			if($result_id!=NULL)
			{
				die(json_encode(array('success'=>'true','result_id'=>$result_id)));
			}else
			{
				die(json_encode(array('success'=>'false','result_id'=>0)));
			}
		}

		public function deleteType($type_id)
		{
			$result_id = $this->m_tbl_job_subcat->save(array('status'=>'0'),$type_id);
			if($result_id!=NULL)
			{
				die(json_encode(array('status'=>'true','result_id'=>$result_id)));
			}else
			{
				die(json_encode(array('status'=>'false', 'result_id'=>0)));
			}
		}

		public function updateCurtainSwatch()
		{
			$intSwatch_id = $this->input->post("swatch_id",TRUE);
			$chrSwatch_name = $this->input->post("swatch_name",TRUE);
			$chrSwatch_code = $this->input->post("swatch_code",TRUE);
			$intSwatch_width = $this->input->post("swatch_width",TRUE);
			$fltSwatch_cost = $this->input->post("swatch_cost",TRUE);
			$intSupplier_id = $this->input->post("swatch_supplier_id",TRUE);
			$intMinWidth = $this->input->post("min_width",TRUE);

			$this->load->model("settings/m_tbl_job_classification");

			$data = array(
						'cost_amount' => $fltSwatch_cost, 
						'tbl_supplier_id' => $intSupplier_id, 
						'type_code' => $chrSwatch_code, 
						'width' => $intSwatch_width,
						'type_name' => $chrSwatch_name,
						"min_width"=>$intMinWidth
						);
			$result_id = $this->m_tbl_job_classification->save($data, $intSwatch_id);

			if($result_id!=NULL)
			{	
				die(json_encode(array('success'=>'true','result_id'=>$result_id)));
			}else 
			{
				die(json_encode(array('success'=>'false','result_id'=>NULL)));
			}
		}
		public function deleteCurtainSwatch()
		{
			$swatch_id = $this->input->post("id",TRUE);

			$this->load->model("settings/m_tbl_job_classification");
			$result_id = $this->m_tbl_job_classification->save(array('status'=>0),$swatch_id);

			if($result_id!=NULL)
			{
				die(json_encode(array('success'=>'true','result_id'=>$result_id)));
			}else
			{
				die(json_encode(array('success'=>'false','result_id'=>$result_id)));
			}
		}
	}
 ?>