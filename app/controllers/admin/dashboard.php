<?php

class Dashboard extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();

		$this->load->model('joborder/m_tbl_job_transaction');
		$this->load->model('customer/m_customer');
		

	}

	public function index()
	{	
		$user_id = $this->session->userdata('id');
		$Count=$this->m_customer->countcustomers();
		$counttransaction=$this->m_tbl_job_transaction->countTransaction();
		$this->data['CountTransaction']=count($counttransaction);
		$this->data['Count']=count($Count);
		$this->data['subview'][] = 'backend/admin/user/dashboard';
		$this->load->view('backend/admin/home',$this->data);
	}

	public function transactionActivity()
	{
		$this->load->model("joborder/m_tbl_job_transaction");
		$transactions = $this->m_tbl_job_transaction->transactionLatestTen();
		die(json_encode(array("transactions"=>$transactions)));
	}
}

?>
