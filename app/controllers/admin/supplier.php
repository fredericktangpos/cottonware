<?php

class Supplier extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();

		$this->load->model('supplier/m_tbl_supplier');
		$this->load->model('settings/m_tbl_job_classification');
		$user_id = $this->session->userdata('id');
	}

	public function index() {
		$this->load->library('pagination');
		$user_id = $this->session->userdata('id');

		$this->data['page_title'] = 'List of Suppliers';
		$this->data['subview'][] = 'backend/admin/settings/setting_suppliers';
		
		$this->load->view('backend/admin/home',$this->data);
	}

	# JSON encoded for supplier
	public function json_supplier()
	{
		if(count($_POST) != 0) {
			$config = array(
					'table'=>'tbl_supplier',
					'select' =>'*',
					'start'=>@$_GET['per_page']!==NULL?$_GET['per_page']:0,
					'per_page'=>5,
					'filter'=>array(
								'status'=>1
								),
					'base_url' =>'supplier'
				);
			$this->data['supplier'] 	= 	$this->m_tbl_supplier->general_paginator($config);		
			die(json_encode($this->data['supplier']));
		}
	}

	# add new supplier / edit existing supplier
	public function edit($id = NULL) {
		
		$this->data['supplier'] = $this->m_supplier->get($id);

		$supplier_first_name       = $this->input->post('supplier_first_name');
		$supplier_last_name        = $this->input->post('supplier_last_name');
		$supplier_contact_number   = $this->input->post('supplier_contact_number');
		$supplier_email            = $this->input->post('supplier_email');
		$supplier_address          = $this->input->post('supplier_address');
		$supplier_status           = $this->input->post('supplier_status');
		$now                       = date('Y-m-d H:i:s');

		$data = array(
			'supplier_first_name'     => $supplier_first_name,
			'supplier_last_name'      => $supplier_last_name,
			'supplier_contact_number' => $supplier_contact_number,
			'supplier_email'          => $supplier_email,
			'supplier_address'        => $supplier_address,
			'supplier_status'         => $supplier_status,
			'supplier_create_date'    => $now,
			'supplier_last_update'    => $now
		);

		if($id == NULL)
		{
			if($_POST)
			{
				$result	= $this->m_supplier->save($data);
				if(count($result) > 0)
				{
					$this->session->set_flashdata(array('msg' => 'Saved changes successfully', 'flag' => 'success'));
					redirect(base_url().'admin/supplier/edit/'.$id);
				}
			}

			$this->data['page_title'] = 'Add New Supplier';
			$this->data['subview'][] = 'backend/admin/user/supplier/supplier_form';
		}
		else
		{
			if($_POST)
			{
				$result	= $this->m_supplier->save($data, $id);
				if(count($result) > 0)
				{
					$this->session->set_flashdata(array('msg' => 'Update supplier successfully', 'flag' => 'success'));
					redirect(base_url().'admin/supplier/edit/'.$id);
				}
			}

			$this->data['page_title'] = 'Update Supplier';
			$this->data['subview'][] = 'backend/admin/user/supplier/supplier_form';
		}

		$this->load->view('backend/admin/home',$this->data);
	}
	public function softDeleteSupplier()
	{
		$intSupplier_id = $this->input->post("supplier_id",TRUE);

		$id = $this->m_tbl_supplier->save(array("status"=>0),$intSupplier_id);

		if($intSupplier_id==$id)
		{
			echo json_encode(array("success"=>"true"));
		}else
		{
			echo json_encode(array("success"=>"fail"));
		}
	}

	public function updateSupplierDetails()
	{
		$intSupplier_id = $this->input->post("intSupplier_id",true);
		$chrSupplier_name = $this->input->post("chrSupplier_name",true);
		$chrContact_number = $this->input->post("chrContact_number",true);
		$chrSupplier_email = $this->input->post("chrSupplier_email",true);
		$chrContact_person = $this->input->post("chrContact_name",true);

		$data=array(
				'supp_co_name'=>$chrSupplier_name,
				'supp_contact_person' => $chrContact_person,
				'supp_contact_num' => $chrContact_number,
				'supp_email'=>$chrSupplier_email
			);

		$id = $this->m_tbl_supplier->save($data,$intSupplier_id);

		if($intSupplier_id==$id)
		{
			echo json_encode(array("success"=>"true"));
		}else
		{
			echo json_encode(array("success"=>"fail"));
		}
	}

	public function supplier_swatches($supplier_id)
	{

		$intSupplier_id = (int)$supplier_id;

		$this->load->library('pagination');
		$user_id = $this->session->userdata('id');

		$this->data['page_title'] = 'List of Suppliers Swatches';
		$this->data['subview'][] = 'backend/admin/settings/supplier_details';
		$config = array(
				'table'=>'tbl_job_classification',
				'select' =>'*, tbl_job_classification.id AS swatch_id',
				'start'=>@$_GET['per_page']!==NULL?$_GET['per_page']:0,
				'per_page'=>5,
				'filter'=>array(
							'tbl_job_classification.status'=>1,
							'tbl_job_classification.tbl_supplier_id'=>$supplier_id
							),
				'base_url' =>$supplier_id,
				'join' => array(
							array(
								'table'=>'tbl_supplier',
								'condition'=>'tbl_supplier.id = tbl_job_classification.tbl_supplier_id'
								),
							array(
								'table' =>'tbl_job_subcat',
								'condition'=>'tbl_job_subcat.id= tbl_job_classification.tbl_job_subcat_id'
								),
							array(
								'table'=>'tbl_transaction_type',
								'condition'=>'tbl_job_subcat.transaction_type_id = tbl_transaction_type.id'	
								)
							)
			);
		$this->data['supplier_name'] = $this->m_tbl_supplier->get_by(array('id'=>$supplier_id));
		$this->data['supplier'] 	= 	$this->m_tbl_supplier->general_paginator($config);
		//dump($this->data['supplier']);
		$this->load->model('settings/m_tbl_transaction_type');

		$this->data['type'] = $this->m_tbl_transaction_type->get();
		$this->data['supplier_id'] = $supplier_id;
		//dump($this->data['type']);
		$this->load->view('backend/admin/home',$this->data);	

	}
	/**
	 * [get sub type depending on the type, like curtain types]
	 * @param  [int] $type [the main job type like curtain and sofa]
	 * @return [json]       [array of sub type list]
	 */
	public function getSubType($type)
	{
		$intType = (int)$type;

		$this->load->model('settings/m_tbl_job_subcat');
		$result =$this->m_tbl_job_subcat->getSubType($intType);
		dump($intType);
		echo json_encode($result);

	}	

	public function addSwatch()
	{

		$intSupplier_id = $this->input->post("intSupplier_id");
		$intSwatch_type = $this->input->post("intSwatch_type");
		$intSwatch_sub_type = $this->input->post("intSwatch_sub_type");
		$chrSwatch_name = $this->input->post("chrSwatch_name");
		$chrSwatch_type_code = $this->input->post("chrSwatch_type_code");
		$intSwatch_width = $this->input->post("intSwatch_width");
		$fltSwatch_price_per_meter = $this->input->post("fltSwatch_price_per_meter");
	
		$swatches_data = array(
						'cost_amount'=>$fltSwatch_price_per_meter,
						'tbl_supplier_id'=>$intSupplier_id,
						'tbl_job_subcat_id'=>$intSwatch_sub_type,
						'type_code'=>$chrSwatch_type_code,
						'width'=>$intSwatch_width,
						'type_name'=>$chrSwatch_name,
						'status'=>1
						);

		$result_id = $this->m_tbl_job_classification->save($swatches_data);

		if((int)$result_id > 0 && $result_id!=NULL)
		{
			die(json_encode(array('success'=>'true','result_id'=>$result_id)));
		}else
		{
			die(json_encode(array('success'=>'false')));
		}


	}

	public function updateSupplierSwatch()
	{
		$this->load->model("settings/m_tbl_job_classification");
		$intSwatch_ID = $this->input->post("id",TRUE);
        $chrNew_swatch_name = $this->input->post("new_swatch_name",TRUE);
        $chrNew_swatch_code = $this->input->post("new_swatch_code",TRUE);
        $chrNew_swatch_width = $this->input->post("new_swatch_width",TRUE);
        $intNew_swatch_cost = $this->input->post("new_swatch_cost",TRUE);

        $data = array(
        		'cost_amount'=>$intNew_swatch_cost,
        		'width'=>$chrNew_swatch_width,
        		'type_code'=>$chrNew_swatch_code,
        		'type_name'=>$chrNew_swatch_name
        	);

        $result_id = $this->m_tbl_job_classification->save($data, $intSwatch_ID);

        if($result_id!=NULL)
        {	
        	die(json_encode(array('success'=>'true','result_id'=>$result_id)));
        }else 
        {
        	die(json_encode(array('success'=>'false','result_id'=>NULL)));
        }

	}

	public function deleteSupplierSwatch($swatch_id)
	{
		$this->load->model("settings/m_tbl_job_classification");

		$result_id = $this->m_tbl_job_classification->save(array("status"=>0),$swatch_id);

		if($result_id!=NULL)
		{
			die(json_encode(array("success"=>"true","result_id"=>$result_id)));
		}else
		{
			die(json_encode(array("success"=>"false","result_id"=>NULL)));
		}
	}
}

?>