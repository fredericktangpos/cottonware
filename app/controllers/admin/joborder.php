<?php

class Joborder extends Admin_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->load->model('customer/m_customer');
		$this->load->model('customer/m_customer_contact');
		$this->load->model('joborder/m_tbl_asset');
		$this->load->model('joborder/m_tbl_asset_dtl');
		$this->load->model('joborder/m_tbl_asset_sub_dtl');
		$this->load->model('joborder/m_tbl_job_transaction');
		$this->load->model('joborder/m_tbl_transaction_item');
		$this->load->model('joborder/m_tbl_quotation');
		$this->load->model('joborder/m_tbl_miscellanous');
		$this->load->model('settings/m_tbl_transaction_type');
		$this->load->model('joborder/m_tbl_transaction_type_selection');
		$this->load->model('joborder/m_tbl_transaction_images');
		$this->load->model('settings/m_tbl_job_subcat');
		$this->load->model('settings/m_tbl_job_classification');
	}

	public function index()
	{

		$config             = array();
		$config["table"]    = "tbl_job_transaction";
		$config['filter']   = array('job_trans_status'=>0);
		$config["select"]   = "tbl_job_transaction.*, tbl_customers.cus_name, tbl_customers.id as customer_id";
		$config["start"]    = isset($_GET["per_page"]) ? intVal($_GET["per_page"]) : 0;
		$config["per_page"] = 1000;
		$config["order"] = array("column_name"=>"tbl_job_transaction.id","order"=>"DESC");
		$config["base_url"] = "joborder";
		$config["join"]     = 	array(
									array("table"=>"tbl_customers","condition"=>"tbl_job_transaction.tbl_customers_id = tbl_customers.id")
								);
		$joborders= $this->m_tbl_job_transaction->general_paginator($config);	

		$this->data["joborders"] = $joborders;
		$this->data['subview'][] = 'backend/admin/joborders/job_order_list';

		$this->load->view('backend/admin/home',$this->data);
	}
	public function fetching_asset_dtl()
	{
		$property_id = $this->input->post('property_id');
		$result		 = $this->m_tbl_asset_dtl->get_by(array('tbl_asset_id'=>$property_id));

		if(count($result) > 0) {
			echo json_encode(array('success'=>true, 'data'=>$result));
		}
		else echo json_encode(array('success'=>false));
	}

	public function createnew() 
	{
		$user_id 		= $this->session->userdata('id');
		$contact_id 	= $this->input->post('contact_id');
		$property_id 	= $this->input->post('property_id');
		$customer_id 	= $this->input->post('customer_id');
		$item 			= json_decode($this->input->post('item'));
		$retval			= FALSE;

		// handling the creation of transaction
		$jo_data = array(
			'tbl_users_id'		=> $user_id,
			'tbl_customers_id'	=> $customer_id,
			'job_trans_created'	=> date("Y-m-d H:i:s"),
			'contact_person'    => $contact_id
		);
		/*======================================================================================================
		// once the transaction is save, it will also save all the transaction item connected to the transaction id
		======================================================================================================*/
		$jo_transaction = $this->m_tbl_job_transaction->save($jo_data);
		if(count($jo_transaction) > 0)
		{
			// handling curtain data
			if(count($item->curtain) > 0):
				// loop and get each item posted
				foreach($item->curtain as $item_idx =>$item_val):
					// check the item_iD if exists in the db
					$check_exist = $this->m_tbl_asset_dtl->get($item_val->assetDTLid);
					
					//item_val->fh 			= str_replace('<br>', '', $item_val->fh);
					$item_val->fh 			= 0;
					$item_val->wh 			= str_replace('<br>', '', $item_val->wh);
					$item_val->ww 			= str_replace('<br>', '', $item_val->ww);
					$item_val->location 	= str_replace('<br>', '', $item_val->location);
					$item_val->room 		= str_replace('<br>', '', $item_val->room);
					$item_val->short_desc 	= str_replace('<br>', '', $item_val->short_desc);

					// if true, create a set varible array and store the item details
					$subjob_data = array(
						'tbl_asset_id'	=> $property_id,
						'location'		=> $item_val->location,
						'room'			=> $item_val->room,
						'short_desc'	=> $item_val->short_desc,
						'full_height'	=> $item_val->fh,
						'window_height'	=> $item_val->wh,
						'window_width'	=> $item_val->ww,
						'asset_type'	=> 1
					);

					// check if the item id is exist, if TRUE, it will update
					if(@$check_exist->id > 0):
						$asset_dtl = $this->m_tbl_asset_dtl->save($subjob_data, $check_exist->id);
					else :
					// if not exist, it wil create new one..
						$asset_dtl = $this->m_tbl_asset_dtl->save($subjob_data);
					endif;

					if(count($asset_dtl) > 0):
						// getting the main job trasaction data to store in the database
						$transaction_item = array(
							'tbl_job_transaction_id' 	=> $jo_transaction,
							'tbl_customer_contact_id' 	=> $contact_id,
							'asset_id' 					=> $property_id,
							'asset_dtl_id' 				=> $asset_dtl
						);
						// store data to table
						$x = $this->m_tbl_transaction_item->save($transaction_item);

						// once the data is stored, it will send a return value of SUCCESS == TRUE in AJAX
						if(count($x) > 0) $retval = TRUE;
						// otherwise, send FALSE
						else $retval = FALSE;
					endif;
				endforeach;
			endif;

			// handling sofa data
			if(count($item->sofa) > 0):
				// loop and get each item posted
				foreach($item->sofa as $item_idx =>$item_val):
					// check the item_iD if exists in the db
					$check_exist = $this->m_tbl_asset_dtl->get($item_val->assetDTLid);
					
					$item_val->fh 			= str_replace('<br>', '', $item_val->fh);
					$item_val->wh 			= str_replace('<br>', '', $item_val->wh);
					$item_val->ww 			= str_replace('<br>', '', $item_val->ww);
					$item_val->location 	= str_replace('<br>', '', $item_val->location);
					$item_val->room 		= str_replace('<br>', '', $item_val->room);
					$item_val->short_desc 	= str_replace('<br>', '', $item_val->short_desc);

					// if true, create a set varible array and store the item details
					$subjob_data = array(
						'tbl_asset_id'	=> $property_id,
						'location'		=> $item_val->location,
						'room'			=> $item_val->room,
						'short_desc'	=> $item_val->short_desc,
						'full_height'	=> $item_val->fh,
						'window_height'	=> $item_val->wh,
						'window_width'	=> $item_val->ww,
						'asset_type'	=> 2
					);

					// check if the item id is exist, if TRUE, it will update
					if(@$check_exist->id > 0):
						$asset_dtl = $this->m_tbl_asset_dtl->save($subjob_data, $check_exist->id);
					else :
					// if not exist, it wil create new one..
						$asset_dtl = $this->m_tbl_asset_dtl->save($subjob_data);
					endif;

					if(count($asset_dtl) > 0):
						// getting the main job trasaction data to store in the database
						$transaction_item = array(
							'tbl_job_transaction_id' 	=> $jo_transaction,
							'tbl_customer_contact_id' 	=> $contact_id,
							'asset_id' 					=> $property_id,
							'asset_dtl_id' 				=> $asset_dtl
						);
						// store data to table
						$x = $this->m_tbl_transaction_item->save($transaction_item);

						// once the data is stored, it will send a return value of SUCCESS == TRUE in AJAX
						if(count($x) > 0) $retval = TRUE;
						// otherwise, send FALSE
						else $retval = FALSE;
					endif;
				endforeach;
			endif;

			if($retval) echo json_encode(array('success'=> true, 'redirect'=>'joborder/edit/', 'trans_id'=>$jo_transaction));
			else echo json_encode(array('success'=> false));
		}	// end of curtain job transaction
	}

	// create and save joborder transaction
	// this part is intentionally separated for debugging purposes
	// since this part is very huge in processing transaction
	public function edit($id=NULL)
	{
		
		
					$user_id = $this->session->userdata('id');
					$this->load->model('customer/m_customer_contact');
					$this->load->model('joborder/m_tbl_locations');
					$this->load->model('joborder/m_tbl_rooms');
					$this->load->model('joborder/m_tbl_asset');
					$this->load->model('settings/m_tbl_job_subcat');

					$this->data['transaction_data'] 		= $this->m_tbl_job_transaction->get($id);

				if ($this->data['transaction_data']==null)
				{

					$this->data['subview'][] = 'backend/components/error_reporting';
				    $this->load->view('backend/admin/home',$this->data);
				
				}

				else
				{
								$select_join = 'tbl_transaction_item.*, 
												tbl_transaction_item.tbl_customer_contact_id AS CONTACT_PERSON_ID,
												tbl_transaction_item.id AS trans_id, 
												tbl_job_transaction.*, 
												tbl_asset_dtl.*, 
												tbl_customers.cus_name, 
												tbl_customers.id as CUSTOMER_ID,
												tbl_customers_contact.name,
												tbl_asset.asset_name,
												tbl_asset.asset_photo,
												tbl_asset.id AS ASSET_ID,
												tbl_rooms.room_name,
												tbl_locations.location_name';

								$joining_array = array();
								$joining_array[0] = new stdClass();
								$joining_array[1] = new stdClass();
								$joining_array[2] = new stdClass();
								$joining_array[3] = new stdClass();
								$joining_array[4] = new stdClass();
								$joining_array[5] = new stdClass();
								$joining_array[6] = new stdClass();
								

								$joining_array[0]->table 		= 'tbl_transaction_item';
								$joining_array[0]->condition 	= 'tbl_job_transaction.id = tbl_transaction_item.tbl_job_transaction_id';
								$joining_array[0]->method 		= 'left';

								$joining_array[1]->table 		= 'tbl_asset_dtl';
								$joining_array[1]->condition 	= 'tbl_asset_dtl.id = tbl_transaction_item.asset_dtl_id';
								$joining_array[1]->method 		= 'left';

								$joining_array[2]->table 		= 'tbl_customers';
								$joining_array[2]->condition 	= 'tbl_customers.id = tbl_job_transaction.tbl_customers_id';
								$joining_array[2]->method 		= 'left';

								$joining_array[3]->table 		= 'tbl_customers_contact';
								$joining_array[3]->condition 	= 'tbl_customers_contact.id = tbl_transaction_item.tbl_customer_contact_id';
								$joining_array[3]->method 		= 'left';

								$joining_array[4]->table 		= 'tbl_asset';
								$joining_array[4]->condition 	= 'tbl_asset.id = tbl_asset_dtl.tbl_asset_id';
								$joining_array[4]->method 		= 'left';

								$joining_array[5]->table 		= 'tbl_rooms';
								$joining_array[5]->condition 	= 'tbl_rooms.id = tbl_asset_dtl.room';
								$joining_array[5]->method 		= 'left';

								$joining_array[6]->table 		= 'tbl_locations';
								$joining_array[6]->condition 	= 'tbl_locations.id = tbl_asset_dtl.location';
								$joining_array[6]->method 		= 'left';

								$this->data['job_transaction_item'] = $this->m_tbl_job_transaction->get_by(array('tbl_job_transaction.id'=>$id, "tbl_transaction_item.status"=>1),FALSE,$joining_array,$select_join);
								
								if (count($this->data['job_transaction_item']) > 0)
								{
									$this->data['misc_only'] = false;
									$item_array[0] = new stdClass();
									$item_array[1] = new stdClass();
									$item_array[2] = new stdClass();

									$item_array[0]->table 		= 'tbl_transaction_type_selection';
									$item_array[0]->condition 	= 'tbl_transaction_type_selection.tbl_transaction_item_id = tbl_transaction_item.id';
									$item_array[0]->method 		= 'left';

									$item_array[1]->table 		= 'tbl_job_classification';
									$item_array[1]->condition 	= 'tbl_job_classification.id = tbl_transaction_type_selection.tbl_job_classification_id';
									$item_array[1]->method 		= 'left';

									$item_array[2]->table 		= 'tbl_supplier';
									$item_array[2]->condition 	= 'tbl_supplier.id = tbl_job_classification.tbl_supplier_id';
									$item_array[2]->method 		= 'left';


									$item_join = 	"tbl_transaction_item.*,
													tbl_transaction_item.id AS trans_item_id,
													tbl_transaction_type_selection.*,
													tbl_transaction_type_selection.id AS trans_type_sel_id,
													tbl_job_classification.*,
													tbl_job_classification.id AS job_class_id,
													tbl_supplier.*,
													tbl_supplier.id AS supplier_id";

									$this->data['item_order_list'] = $this->m_tbl_transaction_item->get_by(array('tbl_job_transaction_id'=>$id),FALSE,$item_array,$item_join);

									$this->data['transaction_id'] = (int)$id;
									//$this->data['job_transaction_item']['item_list'] = 
									//dump($this->data['item_order_list']);
									$dbaRquotations = $this->m_tbl_quotation->get_by(array('tbl_job_transaction_id'=>$this->data['transaction_id']));

									//$temp_storage = preg_split("/[][,]/",$dbaRquotations[1]->items);
									
									$order_basic_info = array();
									$item_basic_info = array();
									
									foreach ($this->data['item_order_list'] as $order) {
										$temp_array = array(
															'classification_id' => $order->tbl_job_subcat_id,
															'swatch_id' => $order->job_class_id,
															'transaction_item_id' => $order->tbl_transaction_item_id,
															'selection_id' =>$order->trans_type_sel_id );
										$order_basic_info[] = $temp_array;
									}
									foreach($this->data['job_transaction_item'] as $order_item)
									{	
										$temp_array = array(
															'item_id' => $order_item->trans_id,
															'height' => $order_item->window_height,
															'width' => $order_item->window_width);
										$item_basic_info[] = $temp_array;
									}

									$this->load->helper('file');
									$costing_list = $this->calculateBasicCost($item_basic_info , $order_basic_info);

									//dump($costing_list);

									$dbArCostumer_contacts = $this->m_customer_contact->getCustomersContact($this->data['job_transaction_item'][0]->CUSTOMER_ID);
									$dbArLocations = $this->m_tbl_locations->getLocations();
									$dbArRooms = $this->m_tbl_rooms->getRooms();
									$dbArTypes = $this->m_tbl_job_subcat->getType();

									$this->data['intUser_id']  = $this->session->userdata("id");
									$this->data['transaction_id'] = $id;
									$this->data['customer_contacts'] = $dbArCostumer_contacts;
									$this->data['locations'] = $dbArLocations;
									$this->data['rooms'] = $dbArRooms;
									$this->data['types'] = $dbArTypes;
									$this->data['properties'] = $this->m_tbl_asset->get_by(array('tbl_customer_id'=>$this->data['job_transaction_item'][0]->CUSTOMER_ID));

									//die(json_encode($this->data['item_order_list']));
									$this->data['dbaRquotations'] = $dbaRquotations;
									$this->data['ASSET_ID'] = $this->data['job_transaction_item'][0]->ASSET_ID ;
									$this->data['CONTACT_PERSON'] = $this->data['job_transaction_item'][0]->CONTACT_PERSON_ID ;
									$this->data['MISC_DATA'] = $this->getMiscOrder($id);
									$this->data['costing_list'] = $costing_list;
								} else {
									$this->data['dbaRquotations'] = array();
									$this->data['MISC_DATA'] = $this->getMiscOrder($id);
									$trans_data = $this->m_tbl_job_transaction->get_by(array("id"=>$id));
									$this->data['ASSET_ID'] = $trans_data[0]->asset_id;
									$this->data['CONTACT_PERSON'] =$trans_data[0]->contact_person;
									$this->data['transaction_id'] = $id;
									$this->data['misc_only'] = true;
									$dbArLocations = $this->m_tbl_locations->getLocations();
									$dbArRooms = $this->m_tbl_rooms->getRooms();
									$dbArTypes = $this->m_tbl_job_subcat->getType();

									$this->data['locations'] = $dbArLocations;
									$this->data['rooms'] = $dbArRooms;
									$this->data['types'] = $dbArTypes;
								}
								$this->data['transaction_images'] = $this->m_tbl_transaction_images->get_by(array("transaction_id"=>$id));
								//dump($this->data['transaction_images']);
								$this->data['transaction_id'] = $id;
								$this->data['customer_data']=$this->m_customer->getCustomer($id);
								$this->data['subview'][] = 'backend/admin/joborders/job_order';
								$this->load->view('backend/admin/home',$this->data);
				}
			
	}



	public function getMiscOrder($id)
	{
		$this->load->model("joborder/m_tbl_miscellanous");
		return $this->m_tbl_miscellanous->getMisc($id);
	}

	// update the details of the window/sofa
	public function save_update_assetdtl($selector)
	{
		if($selector == 1)
		{
			$data = $this->input->post('asset_data');
			$final_data = array(
				'tbl_asset_id'	=> $data[0]['propertyID'],
				'location'		=> $data[0]['location'],
				'room'			=> $data[0]['room'],
				'short_desc'	=> $data[0]['short_desc'],
				'full_height'	=> 0,
				'window_height'	=> $data[0]['wh'],
				'window_width'	=> $data[0]['ww'],
				'asset_type'	=> '2'
			);
			$result = $this->m_tbl_asset_dtl->save($final_data, $data[0]['assetDTLid']);
		}
		else if($selector ==2)
		{
			$newWindow = $this->input->post('newWindow');
			$newWindow_data = array(
				'tbl_asset_id'	=> $newWindow[0]['propID'],
				'location'		=> $newWindow[0]['location'],
				'room'			=> $newWindow[0]['room'],
				'short_desc'	=> $newWindow[0]['short_desc'],
				'full_height'	=> $newWindow[0]['fh'],
				'window_height'	=> $newWindow[0]['wh'],
				'window_width'	=> $newWindow[0]['ww'],
				'asset_type'	=> '1'
			);
			$result = $this->m_tbl_asset_dtl->save($newWindow_data);
		}

		if(count($result) > 0) {
			$data = $this->m_tbl_asset_dtl->get($result);
			echo json_encode(array('success'=>true, 'data'=>$data));
		}
		else {
			echo json_encode(array('success'=>false));
		}
	}

	// load customer's asset
	public function load_customer_assets()
	{
		$id 		= $this->input->post('id');
		$selector	= $this->input->post('selector');
		$result		= array();

		# selecting the customer asset
		if($selector == 1) {
			$result = $this->m_tbl_asset->get_by(array('tbl_customer_id'=>$id));
		}
		# selecting the property asset level
		else if($selector == 2) {
			$select_join = 'tbl_asset_dtl.asset_dtl_level, tbl_asset_sub_dtl.asset_sub_dtl_type, tbl_asset_sub_dtl.id AS window_id';
			$joining_array = array();
			$joining_array[0] = new stdClass();
			$joining_array[0]->table 		= 'tbl_asset_sub_dtl';
			$joining_array[0]->condition 	= 'tbl_asset_sub_dtl.tbl_asset_dtl_id=tbl_asset_dtl.id';
			$joining_array[0]->method 		= 'left';
			$result = $this->m_tbl_asset_dtl->get_by(array('tbl_asset_dtl.tbl_asset_id'=>$id), FALSE, $joining_array, $select_join);
		}
		
		if(count($result) > 0) {
			echo json_encode(array('success'=>true, 'data'=>$result));
		}
		else
			echo json_encode(array('success'=>false));
	}

	// ajax searching module
	public function search_module()
	{
		$keyword 	= $this->input->post('keyword');
		$keyMatch 	= $this->input->post('keyMatch');
		$selector 	= $this->input->post('selector');
		$where 		= $this->input->post('where');
		$where_col	= $this->input->post('whereCol');

		#search for customer
		if($selector == '1') {
			if(!empty($where)) {
				$return = $this->m_customer->get_by(array($where_col=>$where, '('.$keyMatch.' like "%'.$keyword.'%")'=>NULL));
			}
			else {
				$return = $this->m_customer->get_by(array('('.$keyMatch.' like "%'.$keyword.'%")'=>NULL));
			}
			
			$return = $this->m_customer->describeCustomer($return);

			if(count($return) > 0) {
				echo json_encode(array('success'=>true, 'data'=>$return));
			}
			else {
				echo json_encode(array('success'=>false));
			}
		}

		#search for customer
		if($selector == '2') {
			if(!empty($where)) {
				$return = $this->m_customer_contact->get_by(array($where_col=>$where, '('.$keyMatch.' like "%'.$keyword.'%")'=>NULL));
			}
			else {
				$return = $this->m_customer_contact->get_by(array('('.$keyMatch.' like "%'.$keyword.'%")'=>NULL));
			}
			
			if(count($return) > 0) {
				echo json_encode(array('success'=>true, 'data'=>$return));
			}
			else {
				echo json_encode(array('success'=>false));
			}
		}
	}

	// load supplier given the job sub-category
	public function get_supplier()
	{
		$sub_category = $this->input->post('sub_cat_id');

		// dump($sub_category);

		if(count($sub_category) > 0):
			foreach ($sub_category as $cat_idx => $cat_val):
				$joining_array = array();
				$joining_array[0] = new stdClass();
				$joining_array[0]->table     = 'tbl_job_subcat';
				$joining_array[0]->condition = 'tbl_job_subcat.id=tbl_job_classification.tbl_job_subcat_id';
				$joining_array[0]->method    = 'left';
				
				$joining_array[1]            = new stdClass();
				$joining_array[1]->table     = 'tbl_supplier';
				$joining_array[1]->condition = 'tbl_supplier.id=tbl_job_classification.tbl_supplier_id';
				$joining_array[1]->method    = 'left';

				$result->supplier[$cat_idx] = $this->m_tbl_job_subcat->get($cat_val);
				$result = $this->m_tbl_job_classification->get_by(array('tbl_job_subcat_id'=>$cat_val), 
					FALSE, $joining_array,'tbl_supplier.supp_co_name, tbl_job_classification.cost_amount');
				dump($result);
			endforeach;
		endif;
		die;
	}

	public function typesSubType($main_type_id)
	{
		$sub_type = $this->m_tbl_job_classification->getSubType($main_type_id);
		die(json_encode(array("content"=>$sub_type)));
	}

	public function subTypeInfo($sub_id)
	{
		$sub_type_detail = $this->m_tbl_job_classification->subTypeDetail($sub_id);
		die(json_encode(array("content"=>$sub_type_detail)));
	}

	function createQuotation($transaction_id)
	{
		$intTransaction_id = $transaction_id;
		
		$item_list = $this->m_tbl_transaction_item->getTransactionItems($intTransaction_id);


		$items = array();

			foreach ($item_list as $item) {
						$items[] = (int)$item->id;
			}

		$item_json = json_encode($items);

		$misc = array();
		$miscellanous = $this->m_tbl_miscellanous->get_by(array("transaction_id"=>$transaction_id));
		foreach ($miscellanous as $misc_item) {
			$misc[] = (int)$misc_item->id;
		}

		$data = array(
					'tbl_job_transaction_id' => $intTransaction_id,
					'items'=>$item_json,
					'misc'=>json_encode($misc));

		
		$_is_exist = $this->m_tbl_quotation->get_by(array('tbl_job_transaction_id'=>$intTransaction_id));

		$update_trans_type_data = array('job_trans_type'=>2);
		$this->m_tbl_job_transaction->save($update_trans_type_data,$intTransaction_id);
		$result_id = $this->m_tbl_quotation->save($data);
		redirect(base_url("admin/joborder/edit/".$transaction_id));
	}

	function calculateBasicCost($itemList, $orderList)
	{
		$arItem_list = $itemList;
		$arOrder_list = $orderList;
		$costs_list = array();
		foreach ($arItem_list as $item) {
			$temp_price = 0; 
			foreach ($arOrder_list as $order) {
				if ($item['item_id'] == $order['transaction_item_id']) {

					if($order['classification_id'] == 1)
					{
						$order_cost = $this->nightCurtainCalculation($item['width'], $item['height'], $order['swatch_id']);
						
						$sewing_cost = $this->sewingCost($item['width'], 1);
						$temp_price += ($sewing_cost+$order_cost) ; 
						$costs_list[] = array(
											'swatch_id' => $order['swatch_id'],
											'basic_price' => ($order_cost+$sewing_cost),
											'transaction_item_id' => $order['transaction_item_id'],
											'selection_id' => $order['selection_id']);
					} else if($order['classification_id'] == 2 || $order['classification_id'] == 5)
					{
						$order_cost = $this->dayCurtainCalculation($item['width'], $item['height'], $order['swatch_id']);
						
						$sewing_cost = $this->sewingCost($item['width'], 2);

						$temp_price += ($sewing_cost+$order_cost) ; 
						$costs_list[] = array(
											'swatch_id' => $order['swatch_id'],
											'basic_price' => ($order_cost+$sewing_cost),
											'transaction_item_id' => $order['transaction_item_id'],
											'selection_id' => $order['selection_id']);

					}else if($order['classification_id'] == 3)
					{
						$order_cost = $this->trackCost($order['swatch_id'], $item['width']);
						$temp_price += $order_cost;
						$costs_list[] = array(
											'swatch_id' => $order['swatch_id'],
											'basic_price' => $order_cost,
											'transaction_item_id' => $order['transaction_item_id'],
											'selection_id' => $order['selection_id']);

					}else if($order['classification_id'] == 4)
					{
						$order_cost = $this->blindsCost($item['width'], $item['height'], $order['swatch_id']);
						$temp_price += $order_cost;
						$costs_list[] = array(
											'swatch_id' => $order['swatch_id'],
											'basic_price' => $order_cost,
											'transaction_item_id' => $order['transaction_item_id'],
											'selection_id' => $order['selection_id']);
					}else if($order['classification_id'] == 6)
					{
						$order_cost = $this->romanBlindsCost($item['width'], $item['height'], $order['swatch_id']);
						$temp_price += $order_cost;
						$costs_list[] = array(
											'swatch_id' => $order['swatch_id'],
											'basic_price' => $order_cost,
											'transaction_item_id' => $order['transaction_item_id'],
											'selection_id' => $order['selection_id']);
					}

				}
			}

			$this->m_tbl_transaction_item->save(array('item_total_cost'=>$temp_price),$item['item_id']);
		}

		//dump($costs_list);

		return $costs_list;
	}
	public function deleteOrderItem()
	{
		$intItemOrderID = $this->input->post("itemOrderID",TRUE);
		$this->m_tbl_transaction_type_selection->delete($intItemOrderID);
		die(json_encode(array('success'=>'true')));
	}

	public function editOrderDescription()
	{
		$intOrderID = $this->input->post("intOrderID",TRUE);
		$chrOrderDescription  = $this->input->post("chrOrderDescription",TRUE);

		$update_data = array('description'=>$chrOrderDescription);

		$result_id = $this->m_tbl_transaction_type_selection->save($update_data,$intOrderID);
		if($result_id!=NULL)
		{
			die(json_encode(array('success'=>'true','result_id'=>$result_id)));
		}else
		{
			die(json_encode(array('success'=>'false','result_id'=>NULL)));
		}
	}

	public function adminVariables()
	{
		//load the database
		$this->load->model('joborder/m_tbl_admin_variables');

		$dbArAdmin_variables = $this->m_tbl_admin_variables->get();

		return $dbArAdmin_variables;
	}

	public function swatchDetails($swatch_id)
	{
		//load job classification database
		$this->load->model('settings/m_tbl_job_classification');
		$dbArSwatch_data = $this->m_tbl_job_classification->get_by(array('id'=>$swatch_id));

		return $dbArSwatch_data;
	}
	/**
	* calculation for day curtain cost
	*
	*
	**/

	public function dayCurtainCalculation($width, $height, $swatch_id)
	{

		$_WIDTH = (float)$width;
		$_HEIGHT = (float)$height;

		/****    get the GST FACTOR, DAY CURTAIN FABRIC BLEED (INCH), CURTAIN FACTOR    ****/

		$dbArAdmin_variables = $this->adminVariables();
		$_GST_FACTOR =	(float)$dbArAdmin_variables[0]->gst_factor; 
		$_DAY_CURTAIN_FABRIC_BLEED = (float)$dbArAdmin_variables[0]->day_curtain_fabric_bleed;
		$_CURTAIN_FACTOR = (float)$dbArAdmin_variables[0]->curtain_factor;
		
		/****************---------------------------------------------*******************/


		/******   get the swatch details from supplier i.e. width, price per meter ******/

		$dbArSwatch_data = $this->swatchDetails($swatch_id);
		$_CURTAIN_WIDTH = (float)$dbArSwatch_data[0]->width;
		$_PRICE_PER_METER = (float)$dbArSwatch_data[0]->cost_amount;
		//$_PRICE_PER_METER = (float)7.00;

		/**********-----------------------------------------------------------************/

		/**********      get day curtain fabric calculation   *********/

		$_DAY_CURTAIN_FABRIC_CALCULATION = (float)(($_WIDTH*2)+$_DAY_CURTAIN_FABRIC_BLEED)/$_CURTAIN_FACTOR;

		/***************************************************************/

		/**********   get day curtain fabric required   ************/
		$_DAY_CURTAIN_FABRIC_REQUIRED = ceil($_DAY_CURTAIN_FABRIC_CALCULATION);
		/***********************************************************/

		$_DAY_CURTAIN_COST = round(($_DAY_CURTAIN_FABRIC_REQUIRED * $_PRICE_PER_METER * $_GST_FACTOR),2);
		//dump($_DAY_CURTAIN_COST);
		return $_DAY_CURTAIN_COST;

	}

	public function nightCurtainCalculation($width, $height, $swatch_id)
	{
		$_WIDTH = (float)$width;
		$_HEIGHT = (float)$height;

		$dbArSwatch_data = $this->swatchDetails($swatch_id);
		$_CURTAIN_WIDTH = (float)$dbArSwatch_data[0]->width;
		//$_CURTAIN_WIDTH = (float)145.00;
		$_PRICE_PER_METER = (float)$dbArSwatch_data[0]->cost_amount;
		//$_PRICE_PER_METER = (float)8.00;

		$_NIGHT_CURTAIN_FABRIC_WIDTH = round(($_CURTAIN_WIDTH/2.54/2),2);
		$_NIGHT_PANELS = ceil($_WIDTH/$_NIGHT_CURTAIN_FABRIC_WIDTH);

		$dbArAdmin_variables = $this->adminVariables();
		$_GST_FACTOR = $dbArAdmin_variables[0]->gst_factor;
		$_NIGHT_CURTAIN_FABRIC_BLEED = $dbArAdmin_variables[0]->night_curtain_fabric_bleed;
		$_CURTAIN_FACTOR = $dbArAdmin_variables[0]->curtain_factor;
		$_NIGHT_CURTAIN_FABRIC_CALCULATION = (($_HEIGHT + $_NIGHT_CURTAIN_FABRIC_BLEED)*$_NIGHT_PANELS)/$_CURTAIN_FACTOR;	
		$_NIGHT_CURTAIN_FABRIC_REQUIRED = ceil($_NIGHT_CURTAIN_FABRIC_CALCULATION);
		$_NIGHT_CURTAIN_COST = round(($_NIGHT_CURTAIN_FABRIC_REQUIRED * $_PRICE_PER_METER * $_GST_FACTOR),2);
		//dump($_NIGHT_CURTAIN_COST);
		return $_NIGHT_CURTAIN_COST;
	}

	/***
	*@param $type, 1 or 2. 1 for night, and 2 for day
	*/
	public function sewingCost($window_width, $type)
	{	
		$_SEWING_COST = 0;

		$dbArAdmin_variables = $this->adminVariables();
		$_GST_FACTOR = (float)$dbArAdmin_variables[0]->gst_factor;
		$_DAY_SEWING_FACTOR = (float)$dbArAdmin_variables[0]->day_sewing_factor;
		$_DAY_SEWING_PRICE_PER_PANEL = (float)$dbArAdmin_variables[0]->day_sewing_price_per_panel;
		$_NIGHT_SEWING_PRICE_PER_PANEL = (float)$dbArAdmin_variables[0]->night_sewing_price_per_panel;

		$_SEWING_PANELS = ceil($window_width/$_DAY_SEWING_FACTOR);

		if($type==1)
		{	
			$_SEWING_COST = round(($_SEWING_PANELS * $_DAY_SEWING_PRICE_PER_PANEL * $_GST_FACTOR),2);
		}else if($type==2)
		{
			$_SEWING_COST = round(($_SEWING_PANELS * $_NIGHT_SEWING_PRICE_PER_PANEL * $_GST_FACTOR),2);
		}

		//dump($_SEWING_COST);

		return $_SEWING_COST;
	}

	public function trackCost($track_id, $window_width)
	{
		$track_id = (int)$track_id;
		$_WIDTH = (float)$window_width;
		$dbArSwatch_data = $this->swatchDetails($track_id);	
		$_PRICE_PER_FEET = 	$dbArSwatch_data[0]->cost_amount;
		//$_PRICE_PER_FEET = 	(float)5.00;

		$_TRACK_COST = 0;

		$_TRACK = round(($_WIDTH/12),2);

		$_TRACK_COST = round(($_TRACK * $_PRICE_PER_FEET),2);

		//dump($_TRACK_COST);

		return $_TRACK_COST;
	}

	public function blindsCost($window_width, $window_height, $blinds_id)
	{
		$this->load->model("settings/m_tbl_job_classification");
		$dbArAdmin_variables = $this->adminVariables();
		$roman_blind_width_fabric_bleed  = $dbArAdmin_variables[0]->roman_blind_width_fabric_bleed;
		$roman_blind_height_fabric_bleed  = $dbArAdmin_variables[0]->roman_blind_height_fabric_bleed;
		$blinds_data = $this->m_tbl_job_classification->get_by(array("id"=>$blinds_id));
		
		$window_height = $window_height;
		$_PRICE_PER_SQUARE_FEET = $blinds_data[0]->cost_amount;
		$_GST_FACTOR = $dbArAdmin_variables[0]->gst_factor;

		$_BLINDS_DIMENSION = round(($window_width * $window_height) / 144);

		$_BLINDS_DIMENSION = $_BLINDS_DIMENSION < $blinds_data[0]->min_width ? $blinds_data[0]->min_width : $_BLINDS_DIMENSION;

		$_BLINDS_COST = round(($_BLINDS_DIMENSION * $_PRICE_PER_SQUARE_FEET  * $_GST_FACTOR) ,2);
		//dump($_BLINDS_COST);
		return $_BLINDS_COST;
	}
		public function romanBlindsCost($window_width, $window_height, $blinds_id)
	{
		$this->load->model("settings/m_tbl_job_classification");
		$dbArAdmin_variables = $this->adminVariables();
		$roman_blind_width_fabric_bleed  = $dbArAdmin_variables[0]->roman_blind_width_fabric_bleed;
		$roman_blind_height_fabric_bleed  = $dbArAdmin_variables[0]->roman_blind_height_fabric_bleed;
		$blinds_data = $this->m_tbl_job_classification->get_by(array("id"=>$blinds_id));
		$window_width = $window_width > $blinds_data[0]->min_width ? $window_width : $blinds_data[0]->min_width;
		$window_height = $window_height;
		$_PRICE_PER_SQUARE_FEET = $blinds_data[0]->cost_amount;
		$_GST_FACTOR = $dbArAdmin_variables[0]->gst_factor;

		$_BLINDS_DIMENSION = round((($window_width+$roman_blind_width_fabric_bleed) * ($window_height+$roman_blind_height_fabric_bleed)/144));
	
		$_BLINDS_COST = round(($_BLINDS_DIMENSION * $_PRICE_PER_SQUARE_FEET  * $_GST_FACTOR) ,2);
		//dump($_BLINDS_COST);
		return $_BLINDS_COST;
	}
	function getTypes()
	{
		$this->load->model('settings/m_tbl_job_subcat');
		$dbArTypes = $this->m_tbl_job_subcat->getType();
		//dump($dbArTypes);
		die(json_encode($dbArTypes));
	}

	function getSwatch($typeID)
	{
		$intTypeID = (int)$typeID;
		$this->load->model('settins/m_tbl_job_classification');
		$dbArSwatches = $this->m_tbl_job_classification->getSubType($intTypeID);
		die (json_encode($dbArSwatches));
	}
	public function showJobOrderForm($asset_id,$customer_id)
	{
		$this->load->model('customer/m_customer_contact');
		$this->load->model('joborder/m_tbl_locations');
		$this->load->model('joborder/m_tbl_rooms');
		$this->load->model('settings/m_tbl_job_subcat');
		$dbArCostumer_contacts = $this->m_customer_contact->getCustomersContact($customer_id);
		$dbArLocations = $this->m_tbl_locations->getLocations();
		$dbArRooms = $this->m_tbl_rooms->getRooms();
		$dbArTypes = $this->m_tbl_job_subcat->getType();
		$intAsset_id = (int)$asset_id;
		$intCustomer_id = (int)$customer_id;
		$this->data['asset_id']  = $intAsset_id;
		$this->data['intCustomer_id']  = $intCustomer_id;
		$this->data['intUser_id']  = $this->session->userdata("id");
		$this->data['customer_contacts'] = $dbArCostumer_contacts;
		$this->data['locations'] = $dbArLocations;
		$this->data['rooms'] = $dbArRooms;
		$this->data['types'] = $dbArTypes;
		$this->data['subview'][] = 'backend/admin/joborders/joborder_form';
		$this->load->view('backend/admin/home',$this->data);
	}

	public function getOrders()
	{
		$intAsset_id = $this->input->post("hdnIntAsset_id",TRUE);
		$intCustomer_id = $this->input->post("hdnIntCustomer_id",TRUE);
		$intContact_person = $this->input->post("selIntContact_person",TRUE);
		$intWindow_location = $this->input->post("selIntWindow_location",TRUE);
		$chrWindow_location_others = $this->input->post("txtChrWindow_location_others",TRUE);
		$intWindow_room = $this->input->post("selIntWindow_room",TRUE);
		$chrWindow_room_others = $this->input->post("txtChrWindow_room_others",TRUE);

		$aRWindowBasicInfo = array(
								'intWindow_location' => (int)$intWindow_location,
								'chrWindow_location_others' => $chrWindow_location_others,
								'intWindow_room'=> (int)$intWindow_room,
								'chrWindow_room_others' => $chrWindow_room_others,
								'asset_id' => (int)$intAsset_id,
								'customer_id' => $intCustomer_id,
								'contact_person' => (int)$intContact_person
								);

		//dimensions
		
		$intWindow_width = $this->input->post("txtIntWindow_width",true);
		$intWindow_height = $this->input->post("txtIntWindow_height", true);

		//first style
		$intFirst_style = $this->input->post("1_intStyle",TRUE);
		$intFirst_style_swatch = $this->input->post("1_intStyleSwatch",TRUE);
		//second style
		$intSecond_style = $this->input->post("2_intStyle",TRUE);
		$intSecond_style_swatch = $this->input->post("2_intStyleSwatch",TRUE);
		//third style
		$intThird_style = $this->input->post("3_intStyle",TRUE);
		$intThird_style_swatch = $this->input->post("3_intStyleSwatch",TRUE);
		//fourth style
		$intFourth_style = $this->input->post("4_intStyle",TRUE);
		$intFourth_style_swatch = $this->input->post("4_intStyleSwatch",TRUE);
		//fifth style
		$intFifth_style = $this->input->post("5_intStyle",TRUE);
		$intFifth_style_swatch = $this->input->post("5_intStyleSwatch",TRUE);

		$windows_list =  array();

		for ($i=0; $i < 10 ; $i++) { 
			$window_width = (int)$intWindow_width[$i];
			$window_height = (int)$intWindow_height[$i];

			if($window_width > 0 && $window_height > 0)
			{
				$temp_styles = array(
								'style1' => $intFirst_style[$i],
								'style2' => $intSecond_style[$i],
								'style3' => $intThird_style[$i],
								'style4' => $intFourth_style[$i],
								'style5' => $intFifth_style[$i]
								);
				$temp_style_swatch = array(
								'swatch1' => (int)$intFirst_style_swatch[$i],
								'swatch2' => (int)$intSecond_style_swatch[$i],
								'swatch3' => (int)$intThird_style_swatch[$i],
								'swatch4' => (int)$intFourth_style_swatch[$i],
								'swatch5' => (int)$intFifth_style_swatch[$i]
								);
				$temp_var = array(
								1 => $temp_styles,
								2 => $temp_style_swatch,
								3 => $window_width,
								4 => $window_height
								);


				$windows_list[] = $temp_var;
			}
		}

		$this->addJobOrder($aRWindowBasicInfo, $windows_list);

	}

	function addJobOrder($aRWindowBasicInfo, $windows_list)
	{
		$this->load->model("joborder/m_tbl_transaction_type_selection");
		$this->load->model("joborder/m_tbl_job_transaction");
		$this->load->model("joborder/m_tbl_transaction_item");
		$this->load->model("joborder/m_tbl_asset_dtl");

		$windows_info = $aRWindowBasicInfo;
		$windows_list = $windows_list;

		/*** get if the new room location is added, if others insert others to the database 
		and then get the id from the database.. then reset the new data**/
		if($windows_info['intWindow_location'] == 0)
		{
			$this->load->model("joborder/m_tbl_locations");

			$new_location = $windows_info['chrWindow_location_others'];

			$location_id = $this->m_tbl_locations->insertLocation($new_location);
			//dump($location_id);

			$windows_info['intWindow_location'] = $location_id; 

		}

		/*** test if the room name exist, if not then add the new room to the database and 
		then get the id from the database ***/
		
		if($windows_info['intWindow_room'] == 0)
		{
			$this->load->model("joborder/m_tbl_rooms");

			$new_room = $windows_info['chrWindow_room_others'];

			$room_id = $this->m_tbl_rooms->insertRoom($new_room);
			//dump($room_id);

			$windows_info['intWindow_room'] = $room_id; 
		}

		//dump($windows_info);
		//dump($windows_list);
		
		/***  Insert new transaction in here!.. **/
		$user_id = $this->session->userdata('id');

		$arTransaction_data = array(
								'tbl_users_id' => $user_id,
								'tbl_customers_id' => $windows_info['customer_id'],
								'job_trans_created' => date("Y-m-d h:m:s")
								);
		$intTransaction_id = $this->m_tbl_job_transaction->insertTransaction($arTransaction_data);
		//dump(count($windows_list));
		if( count($windows_list) > 0 ){
			foreach ($windows_list as $w_list) {
				//dump($w_list);
				$arAsset_detail = array(
										'tbl_asset_id' => $windows_info['asset_id'],
										'location' => $windows_info['intWindow_location'],
										'room' => $windows_info['intWindow_room'],
										'short_desc' =>"NULL",
										'full_height' =>0,
										'window_height' => $w_list[3],
										'window_width' => $w_list[4],
										'asset_type' => 1);

				$result_id = $this->m_tbl_asset_dtl->insertAsset($arAsset_detail);
				if($result_id == null) {
					dump("error on adding asset detail..");
					exit;
				}	

				$arTransaction_item_data  = array(
								'tbl_job_transaction_id' => (int)$intTransaction_id,
								'tbl_customer_contact_id' => $windows_info['contact_person'],
								'asset_id' => $windows_info['asset_id'], 
								'asset_dtl_id' =>$result_id); 

				$trans_item_id = $this->m_tbl_transaction_item->insertTransactionItem($arTransaction_item_data);
				
				$orders = array();

				$orders_list = $w_list[2];
				//dump($orders_list);
				for ($i=1; $i <= 5; $i++) { 
					if($w_list[2]['swatch'.$i] > 0 )
					{
						$orders[] = $w_list[2]['swatch'.$i];
					}
				}

				foreach ($orders as $order) {

					$arType_selection_data = array(
											'tbl_transaction_item_id' => $trans_item_id,
											'tbl_job_classification_id' => $order);

					$this->m_tbl_transaction_type_selection->insertNewSelection($arType_selection_data);

				}

			}
		}

		redirect(base_url("admin/joborder/edit/".$intTransaction_id));  
				
	}
	function ajaxAddJobOrder()
	{
		$this->load->model("joborder/m_tbl_transaction_type_selection");

		$transaction_item_id = $this->input->post("trans_item_id",TRUE);
		$swatch_id = $this->input->post("subsub_type_id",TRUE);
		$order_description = $this->input->post("chrOrder_description",TRUE);

		$data = array(
			'tbl_transaction_item_id'=>$transaction_item_id,
			'tbl_job_classification_id'=>$swatch_id,
			'description'=>$order_description
		);

		$result_id = $this->m_tbl_transaction_type_selection->save($data);

		if($result_id!=NULL)
		{
			die(json_encode(array('success'=>'true','result_id'=>$result_id)));
		}else
		{
			die(json_encode(array('success'=>'false','result_id'=>NULL)));
		}

	}

	function createInvoice($transaction_id)
	{
		$intTransaction_id = (int)$transaction_id;
		if($intTransaction_id > 0)
		{
			$this->load->model("joborder/m_tbl_job_transaction");
			(int)$result_id = $this->m_tbl_job_transaction->createInvoice($intTransaction_id);
			if($result_id == $intTransaction_id){
				die(json_encode(array('success'=>'true','result_id' => $result_id)));
			}else{
				die(json_encode(array('success'=>'false', 'result_id' => null)));
			}
		}
	}

	function approvedTransaction()
	{
		$config             = array();
		$config["table"]    = "tbl_job_transaction";
		$config['filter']   = array('job_trans_status'=>1);
		$config["select"]   = "tbl_job_transaction.*, tbl_customers.cus_name, tbl_customers.id as customer_id";
		$config["start"]    = isset($_GET["per_page"]) ? intVal($_GET["per_page"]) : 0;
		$config["per_page"] = 1000;
		$config["order"] = array("column_name"=>"tbl_job_transaction.id" , "order"=>"DESC");
		$config["base_url"] = "joborder";
		$config["join"]     = 	array(
									array("table"=>"tbl_customers","condition"=>"tbl_job_transaction.tbl_customers_id = tbl_customers.id")
								);
		$joborders          = $this->m_tbl_job_transaction->general_paginator($config);	

		$this->data["joborders"] = $joborders;
		$this->data['subview'][] = 'backend/admin/joborders/approved_transactions';

		$this->load->view('backend/admin/home',$this->data);
	}


	function showRevisions($revision_id)
	{
		$intRevision = (int)$revision_id;
		$dbaRquotation = $this->m_tbl_quotation->get_by(array('id'=>$intRevision));
		$itemList_temp = preg_split("/[][,]/",$dbaRquotation[0]->items);
		$miscItems =  json_decode($dbaRquotation[0]->misc);

		$misc_data = array();
		if (count($miscItems) > 0 )
		{
			foreach ($miscItems as $misc) {
				$misc_id = $misc;
				$misc_info  = $this->m_tbl_miscellanous->get_by(array("id"=>$misc_id));
				$misc_data[] = $misc_info;
			}	
		}
		
		$item_data = array();
		$item_orders = array();
		for ($i=1; $i < count($itemList_temp)-1 ; $i++) { 
			$temp_item = $itemList_temp[$i];

			$item_array[0] = new stdClass();
			$item_array[1] = new stdClass();
			$item_array[2] = new stdClass();
			$item_array[3] = new stdClass();
			$item_array[4] = new stdClass();
			$item_array[5] = new stdClass();

			$item_array[0]->table 		= 'tbl_job_transaction';
			$item_array[0]->condition 	= 'tbl_transaction_item.tbl_job_transaction_id = tbl_job_transaction.id';
			$item_array[0]->method 		= 'left';

			$item_array[1]->table 		= 'tbl_customers';
			$item_array[1]->condition 	= 'tbl_transaction_item.tbl_customer_contact_id = tbl_customers.id';
			$item_array[1]->method 		= 'left';

			$item_array[2]->table 		= 'tbl_asset';
			$item_array[2]->condition 	= 'tbl_transaction_item.asset_id = tbl_asset.id';
			$item_array[2]->method 		= 'left';

			$item_array[3]->table 		= 'tbl_asset_dtl';
			$item_array[3]->condition 	= 'tbl_transaction_item.asset_dtl_id = tbl_asset_dtl.id';
			$item_array[3]->method 		= 'left';

			$item_array[4]->table 		= 'tbl_rooms';
			$item_array[4]->condition 	= 'tbl_asset_dtl.room = tbl_rooms.id';
			$item_array[4]->method 		= 'left';

			$item_array[5]->table 		= 'tbl_locations';
			$item_array[5]->condition 	= 'tbl_asset_dtl.location = tbl_locations.id';
			$item_array[5]->method 		= 'left';

			$item_join = 	"tbl_transaction_item.*,
							 tbl_transaction_item.id as ID_transaction_item,
							 tbl_job_transaction.*,
							 tbl_job_transaction.id as ID_transaction,
							 tbl_customers.*,
							 tbl_customers.id as ID_customer_contact,
							 tbl_asset.*,
							 tbl_asset.id as ID_asset,
							 tbl_asset_dtl.*,
							 tbl_asset_dtl.id as ID_asset_dtl,
							 tbl_rooms.*,
							 tbl_locations.*";

			$temp_item_data = $this->m_tbl_transaction_item->get_by(array('tbl_transaction_item.id'=>$temp_item),FALSE,$item_array,$item_join);
			$item_data[] = $temp_item_data;

			$item_array2[0] = new stdClass();

			$item_array2[0]->table 		= 'tbl_job_classification';
			$item_array2[0]->condition 	= 'tbl_transaction_type_selection.tbl_job_classification_id = tbl_job_classification.id';
			$item_array2[0]->method 		= 'left';

			$item_join2 = 	"tbl_transaction_type_selection.id AS tbl_type_sel_id , tbl_transaction_type_selection.description,tbl_transaction_type_selection.tbl_transaction_item_id as ID_transaction_item_id,,
							tbl_job_classification.*,
							tbl_job_classification.id as ID_job_classiification";

			$temp_item_orders = $this->m_tbl_transaction_type_selection->get_by(array('tbl_transaction_type_selection.tbl_transaction_item_id'=>$temp_item),FALSE,$item_array2,$item_join2);
			$item_orders[] = $temp_item_orders;
		}

		$order_basic_info = array();
		$item_basic_info = array();
		//dump($this->data['item_order_list']);
		foreach ($item_orders as $order) {
				for ($i=0; $i < count($order); $i++) { 
					//dump($order);
					$temp_array = array(
								'classification_id' => (int)$order[$i]->tbl_job_subcat_id,
								'swatch_id' => (int)$order[$i]->ID_job_classiification,
								'transaction_item_id' => (int)$order[$i]->ID_transaction_item_id,
								'selection_id' => (int)$order[$i]->tbl_type_sel_id );
					$order_basic_info[] = $temp_array;
				}
		}
		//dump($this->data['job_transaction_item']);
		foreach($item_data as $order_item)
		{	
			//dump($order_item);
			for ($i=0; $i < count($order_item); $i++) { 
				$temp_array = array(
								'item_id' => (int)$order_item[$i]->ID_transaction_item,
								'height' => (int)$order_item[$i]->window_height,
								'width' => (int)$order_item[$i]->window_width );
				$item_basic_info[] = $temp_array;
			}
		}

		//$this->load->helper(array('dompdf','file'));
		$this->load->helper('file');
		$costing_list = $this->calculateBasicCost($item_basic_info , $order_basic_info);
		//$html = $this->load->view('backend/admin/customer/transaction-form');
		//$html = "just a test1...";
		//$data = pdf_create($html, '', false);
	    //$test = write_file("quotation/test.pdf", $data);
	    $final_item_list = array();
	    $final_order_list = array();
	    foreach ($item_data as $item_list) {
	    	for ($i=0; $i <count($item_list) ; $i++) { 
	    		$final_item_list[] = $item_list[$i];
	    	}
	    }

	    foreach ($item_orders as $order) {
	    	for ($i=0; $i < count($order); $i++) { 
	    		$final_order_list[] = $order[$i];
	    	}
	    }
	    $this->data['miscellanous'] = $misc_data;
		$this->data['costing_list'] = $costing_list;
		$this->data['job_transaction_item'] = $final_item_list;
		$this->data['item_order_list'] = $final_order_list;
		$this->data['subview'][] = 'backend/admin/customer/revision';

		$this->load->view('backend/admin/home',$this->data);
	}

	public function getOrder ()
	{
		$this->load->model("joborder/m_tbl_transaction_type_selection");
		$this->load->model("joborder/m_tbl_job_transaction");
		$this->load->model("joborder/m_tbl_transaction_item");
		$this->load->model("joborder/m_tbl_asset_dtl");
		$this->load->model("joborder/m_tbl_rooms");
		$this->load->model("joborder/m_tbl_locations");

		$arOrders = $this->input->post("orders",TRUE);
		$contact_person = $this->input->post("contact_person",TRUE);
		$asset_id = $this->input->post("asset_id",TRUE);
		$customer_id = $this->input->post("customer_id",TRUE);
		$user_id = $this->input->post("user_id",TRUE);

		$arTransaction_data = array(
								'tbl_users_id' => $user_id,
								'tbl_customers_id' => $customer_id,
								'job_trans_created' => date("Y-m-d h:m:s"),
								'asset_id' => $asset_id
								);
		$intTransaction_id = $this->m_tbl_job_transaction->insertTransaction($arTransaction_data);

		foreach ($arOrders as $order) {
			$location_id = $order['location_id'];
			if($location_id == 0 ) {
				$location_id = $this->m_tbl_locations->save(array("location_name"=>$order['location_name']));
			}
			$room_data = $order['data'];

			foreach ($room_data as $room) {
				
				if($room['room_id'] != "") {
					$room_id = (int)$room['room_id'];
					
					if($room_id  == 0) {
						$room_id = (int)$this->m_tbl_rooms->save(array("room_name"=>$room['room_name']));

					}
					$dimensions = $room['dimension'];
					foreach ($dimensions as $dimension) {
						$dimension_width = $dimension['width'];
						$dimension_height = $dimension['height'];
						$dimension_orders = $dimension['orders'];
						$asset_dtl_data = array(
												"tbl_asset_id"=>$asset_id,
												"location"=>$location_id,
												"room" => $room_id,
												"window_height" =>$dimension_height,
												"window_width" =>$dimension_width,
												"asset_type" => 0 );
						$asset_dtl_id = (int)$this->m_tbl_asset_dtl->save($asset_dtl_data);
						if($asset_dtl_id > 0)
						{
							$transaction_item_data = array(
													"tbl_job_transaction_id"=>$intTransaction_id,
													"tbl_customer_contact_id"=>$contact_person,
													"asset_id" =>$asset_id, 
													"asset_dtl_id" =>$asset_dtl_id, 
													"item_total_cost"=>0.00);
							$transaction_item_id = $this->m_tbl_transaction_item->save($transaction_item_data);
							
							foreach ($dimension_orders as $d_order) {
								$type_selection_data = array(
														"tbl_job_classification_id" =>(int)$d_order["secondary_type"],
														"tbl_transaction_item_id"=>(int)$transaction_item_id);
								$this->m_tbl_transaction_type_selection->save($type_selection_data);
							}
						}
						
					}
				}
			}

		}

		die(json_encode(array("success"=>true,"transaction_item_id"=>$intTransaction_id)));
	}

	public function removeTransactionItem()
	{
		$this->load->model("joborder/m_tbl_transaction_item");
		$intTransactionItem_id = $this->input->post("transaction_item_id");
		$trans_item_id = (int)$this->m_tbl_transaction_item->save(array("status"=>0),$intTransactionItem_id);
		if($trans_item_id > 0)
		{
			die(json_encode(array("success"=>true, "trans_item_id" => $trans_item_id)));
		} else 
		{
			die(json_encode(array("success"=>false, "trans_item_id" => 0)));
		}


	}

	public function getOrderOnEdit() {
		$arRooms = $this->input->post("rooms",TRUE);
		$arPrimary_data = $this->input->post("primaryData",TRUE);

		$arRoom_details = array();
		foreach ($arRooms as $room) {
			
			$count = 0;
			$arRoom_details['dimension_orders'] = array();
			for ($i=0; $i < count($room['width_list']) ; $i++) { 

				$width = $room['width_list'][$i];
				$height = $room['height_list'][$i];
				$room_id = $room['room_id'];
				$room_name = $room['room_name'];
				//array_push($arRoom_details,$room_id);
				$arRoom_details['room_id'] = $room_id;	
				$arRoom_details['room_name'] = $room_name;
				//array_push($arRoom_details,$room_name);
				$order = array();
				$reverse_count = 5;
				$inc = count($room['basic_type_list']);
				if ($count < $inc)
	            {
	                $inc = $count+5;
	            }
	            for ($h = $count; $h < $inc; $h++) {
	            	if ($room['basic_type_list'][$h]!="") {
	            		$_temp = array(
	            					"basic_type_id"=>$room['basic_type_list'][$h],
	            					"basic_type_name"=>$room['basic_type_name_list'][$h],
	            					"secondary_type"=>$room["secondary_type_list"][$h],
	            					"secondary_type_name_list"=>$room["secondary_type_name_list"][$h]);
	            		//dump($_temp);
	            		array_push($order,$_temp);
	            	}
	            }
	            $count+=5;

	              $temp_data  = array(
					                "width" => $width,
					                "height" => $height,
					                "orders" => $order
					            );

	              //dump($temp_data);
	              array_push($arRoom_details['dimension_orders'],$temp_data);
	              //$arRoom_details['dimension_orders'] = $temp_data;
	              
			}
				$arRoom_details['transaction_id'] = $arPrimary_data['transaction_id'];
				$arRoom_details['contact_person'] = $arPrimary_data['contact_person'];
				$arRoom_details['location_id'] 	  = $arPrimary_data['location_id'];
				$arRoom_details['location_name']  = $arPrimary_data['location_name'];
				$arRoom_details['asset_id']  = $arPrimary_data['asset_id'];

			//dump($arRoom_details);
				$this->insertOrderOnEdit($arRoom_details);
		}
		die(json_encode(array("success" => true)));
	}


	function insertOrderOnEdit($arRoom_details)	
	{
		$this->load->model("joborder/m_tbl_transaction_item");
		
		$dimensions = $arRoom_details['dimension_orders'];
		

		foreach ($dimensions as $dimension) {

			$room_id = (int)$arRoom_details['room_id'];
			$height = $dimension['width'];
			$width = $dimension['height'];
			$orders  = $dimension['orders'];
			$location_id = (int)$arRoom_details['location_id'];
			$asset_id = $arRoom_details['asset_id'];
			$transaction_id = $arRoom_details['transaction_id'];
			$asset_dtl_id = 0 ; 
			$contact_person = $arRoom_details['contact_person'];
			if($room_id == 0 )
			{
				$this->load->model('joborder/m_tbl_rooms');
				$room_id = (int)$this->m_tbl_rooms->save(array("room_name"=>$arRoom_details['room_name']));
			}
			if($location_id == 0)
			{
				$this->load->model('joborder/m_tbl_locations');
				$location_id = (int)$this->m_tbl_locations->save(array("location_name"=>$arRoom_details['location_name']));
			}
				$this->load->model("joborder/m_tbl_asset_dtl");
				$this->load->model("joborder/m_tbl_transaction_item");
				$this->load->model("joborder/m_tbl_transaction_type_selection");

			$asset_dtl_id = (int)$this->m_tbl_asset_dtl->save(array(
													"tbl_asset_id"=> $asset_id, 
													"location" => $location_id,
													"room" => $room_id,
													"window_height" => $height, 
													"window_width" => $width, 
													"asset_type" => 0));
			$transaction_item_id = $this->m_tbl_transaction_item->save(array(
														"tbl_job_transaction_id" => $transaction_id, 
														"tbl_customer_contact_id" => $contact_person , 
														"asset_id" => $asset_id, 
														"asset_dtl_id" => $asset_dtl_id));

			foreach ($orders as $order) {
				$type_selection_data = array(
										"tbl_job_classification_id" =>(int)$order["secondary_type"],
										"tbl_transaction_item_id"=>(int)$transaction_item_id);
				$this->m_tbl_transaction_type_selection->save($type_selection_data);
			}

		}

		return true;
	}

	function newMiscJobOrder()
	{
		$transaction_id = $this->input->post("transaction_id",true);
		$misc_cost = $this->input->post("misc_cost",true);
		$misc_description = $this->input->post("misc_description",true);
		$misc_id = $this->input->post("misc_id",true);
		
		if ( $misc_id == 0 )
		{
			$misc_id = NULL;
		}
		$misc_data = array(
						"transaction_id"=>$transaction_id,
						"description"=>$misc_description,
						"cost"=>$misc_cost);
		$misc_id = $this->m_tbl_miscellanous->save($misc_data,$misc_id);
		if($misc_id > 0)
		{
			die(json_encode(array("success"=>true,"misc"=>$misc_id)));
		} else {
			die(json_encode(array("success"=>false,"misc"=>0)));
		}
		
	}

	function  removeMisc()
	{
		$misc_id = $this->input->post("misc_id");
		$data = array("status"=>0);
		$misc = $this->m_tbl_miscellanous->save($data,$misc_id);
		die(json_encode(array("success"=>true,"misc_id"=>$misc)));
	}

	function downloadPDF($transaction_id)
	{
		$intTransaction_id = $transaction_id;
		$this->data['transaction_id'] = $transaction_id;
		$this->data['customers_info'] = $this->m_tbl_job_transaction->getTransactionsCustomerData($transaction_id);

			$select_join = 'tbl_transaction_item.*, 
						tbl_transaction_item.id AS trans_id, 
						tbl_job_transaction.*, 
						tbl_asset_dtl.*, 
						tbl_customers.*, 
						tbl_customers_contact.name,
						tbl_asset.asset_name,
						tbl_asset.asset_photo,
						tbl_rooms.*,
						tbl_locations.*';

			$joining_array = array();
			$joining_array[0] = new stdClass();
			$joining_array[1] = new stdClass();
			$joining_array[2] = new stdClass();
			$joining_array[3] = new stdClass();
			$joining_array[4] = new stdClass();
			$joining_array[5] = new stdClass();
			$joining_array[6] = new stdClass();
			

			$joining_array[0]->table 		= 'tbl_transaction_item';
			$joining_array[0]->condition 	= 'tbl_job_transaction.id = tbl_transaction_item.tbl_job_transaction_id';
			$joining_array[0]->method 		= 'left';

			$joining_array[1]->table 		= 'tbl_asset_dtl';
			$joining_array[1]->condition 	= 'tbl_asset_dtl.id = tbl_transaction_item.asset_dtl_id';
			$joining_array[1]->method 		= 'left';

			$joining_array[2]->table 		= 'tbl_customers';
			$joining_array[2]->condition 	= 'tbl_customers.id = tbl_job_transaction.tbl_customers_id';
			$joining_array[2]->method 		= 'left';

			$joining_array[3]->table 		= 'tbl_customers_contact';
			$joining_array[3]->condition 	= 'tbl_customers_contact.id = tbl_transaction_item.tbl_customer_contact_id';
			$joining_array[3]->method 		= 'left';

			$joining_array[4]->table 		= 'tbl_asset';
			$joining_array[4]->condition 	= 'tbl_asset.id = tbl_asset_dtl.tbl_asset_id';
			$joining_array[4]->method 		= 'left';

			$joining_array[5]->table 		= 'tbl_rooms';
			$joining_array[5]->condition 	= 'tbl_asset_dtl.room = tbl_rooms.id';
			$joining_array[5]->method 		= 'left';

			$joining_array[6]->table 		= 'tbl_locations';
			$joining_array[6]->condition 	= 'tbl_asset_dtl.location = tbl_locations.id';
			$joining_array[6]->method 		= 'left';

			$this->data['job_transaction_item'] = $this->m_tbl_job_transaction->get_by(array('tbl_job_transaction.id'=>$transaction_id,"tbl_transaction_item.status"=>1),FALSE,$joining_array,$select_join);
			if (count($this->data['job_transaction_item']) > 0)
			{
				$item_array[0] = new stdClass();
				$item_array[1] = new stdClass();
				$item_array[2] = new stdClass();

				$item_array[0]->table 		= 'tbl_transaction_type_selection';
				$item_array[0]->condition 	= 'tbl_transaction_type_selection.tbl_transaction_item_id = tbl_transaction_item.id';
				$item_array[0]->method 		= 'left';

				$item_array[1]->table 		= 'tbl_job_classification';
				$item_array[1]->condition 	= 'tbl_job_classification.id = tbl_transaction_type_selection.tbl_job_classification_id';
				$item_array[1]->method 		= 'left';

				$item_array[2]->table 		= 'tbl_supplier';
				$item_array[2]->condition 	= 'tbl_supplier.id = tbl_job_classification.tbl_supplier_id';
				$item_array[2]->method 		= 'left';


				$item_join = 	"tbl_transaction_item.*,
								tbl_transaction_item.id AS trans_item_id,
								tbl_transaction_type_selection.id AS trans_type_sel_id,
								tbl_transaction_type_selection.*,
								tbl_job_classification.*,
								tbl_job_classification.id AS job_class_id,
								tbl_supplier.*,
								tbl_supplier.id AS supplier_id";

				$this->data['item_order_list'] = $this->m_tbl_transaction_item->get_by(array('tbl_job_transaction_id'=>$transaction_id,"tbl_transaction_item.status"=>1),FALSE,$item_array,$item_join);
				
				$order_basic_info = array();
				$item_basic_info = array();
				
				foreach ($this->data['item_order_list'] as $order) {
					$temp_array = array(
										'classification_id' => $order->tbl_job_subcat_id,
										'swatch_id' => $order->job_class_id,
										'transaction_item_id' => $order->tbl_transaction_item_id,
										'selection_id' =>$order->trans_type_sel_id );
					$order_basic_info[] = $temp_array;
				}
				foreach($this->data['job_transaction_item'] as $order_item)
				{	
					$temp_array = array(
										'item_id' => $order_item->trans_id,
										'height' => $order_item->window_height,
										'width' => $order_item->window_width);
					$item_basic_info[] = $temp_array;
				}

				$this->load->helper('file');
				$costing_list = $this->calculateBasicCost($item_basic_info , $order_basic_info);

				$this->data['costing_list'] = $costing_list;
			} else {
				$this->data['item_order_list'] = array();
				$this->data['job_transaction_item'] = array();
				$this->data['costing_list'] = array();
			}

			$this->data['miscellanous'] = $this->m_tbl_miscellanous->get_by(array("transaction_id"=>$transaction_id));

			//$this->load->view('backend/admin/customer/transaction-form', $this->data);
			$c_name = $this->data['customers_info'][0]->cus_name;
			$date_today = date('g:ia,l jS F Y');

			$date_today2 = date('jS F Y');
			$this->data['datekaron']=$date_today2;
			$pdf_name = $c_name.'[ '.$date_today.' ].pdf';
			$this->load->library('pdf');
			
			$this->pdf->set_paper( "letter", "portrait");
			$this->pdf->load_view('backend/admin/customer/transaction-form',$this->data);
			$this->pdf->render();
			$this->pdf->stream($pdf_name);
			
	}

} #end of class

