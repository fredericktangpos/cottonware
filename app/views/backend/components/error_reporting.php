<div class="box col-md-12" style="margin-bottom:36%;">
	<div class="box-inner" >
		<div data-original-title="" class="box-header well" >
			<h2><i class="glyphicon glyphicon-book"></i> Error </h2>
		</div>
		<div class="box-content" style="display: block; height:253px;">
			<div class="row" style="display: block;  margin-top:60px; ">		
				<center><div class="" role="alert" style="width:50%; "><h1>Sorry the page you requested cannot be found.</h1></div></center>
			</div>
		</div>
	</div>
</div>
