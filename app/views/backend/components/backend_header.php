<!DOCTYPE html>
<html lang="en" base-url = "<?php echo base_url(); ?>" ng-app="cw">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title><?php echo $meta_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="COTTONWARE, Customer Record Management">
    <meta name="author" content="Marnelle">
    
    <!-- The styles -->
    <?php 
    if(!is_null($styles) && count($styles) != 0) {
        foreach ($styles as $thestyles) {
            echo '<link href="'.base_url($thestyles).'" rel="stylesheet">'."\n";
        }
    }
    ?>
    <link href='<?php echo base_url();?>bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>  
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.ico">

</head>

<body>
<!-- topbar starts -->
<div class="navbar navbar-default" role="navigation">
    <div class="navbar-inner">
        <button type="button" class="navbar-toggle pull-left animated flip">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url('admin/dashboard'); ?>">
            <img alt="Cottonware CRM" style="width:200px;" src="<?php echo base_url();?>img/logo.png" />
        </a>
        <!-- user dropdown starts -->
        <div class="pull-right">
            
        </div>
        <div class="btn-group pull-right">
            <!--<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <i class=""></i><span class="hidden-sm hidden-xs"> <?php echo $this->session->userdata('name');?></span>
                <span class="caret"></span>
            </button>  -->
            <a href="#" class=""data-toggle="dropdown"><img class="media-object img-rounded img-responsive" src="<?php echo base_url("img/userimages/".$this->session->userdata('image')) ?> " alt="..." width="50" height="auto" style="    max-height: 45px;
            max-width: 45px;"></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('admin/user/userProfile/'.$this->session->userdata('id')); ?>"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Profile</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url('admin/user/logout'); ?>"><i class="glyphicon glyphicon-off"></i>&nbsp;&nbsp;Logout</a></li>
            </ul>
        </div>
        <!-- user dropdown ends -->
    </div>
</div>
<!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Main</li>
                        <?php foreach ($dbArUsers_access as $access): ?>
                            <?php 
                                if(((int)($access->id)) == 4)
                                {
                            ?>
                                <li class="accordion">
                                <!--<a href="#"><i class="glyphicon glyphicon-briefcase"></i><span> Accordion Menu</span></a> -->
                                <a href="#">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <i class="glyphicon <?php echo $access->icon_class ?>"></i>
                                            <?php echo $access->display_name ?>
                                        </div>
                                        <div class="col-md-3">
                                            <i class="fa fa-caret-down"></i>
                                        </div> 
                                    </div>   
                                </a>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li><a href="<?php echo base_url('admin/joborder'); ?>">Pending</a></li>
                                        <li><a href="<?php echo base_url('admin/joborder/approvedTransaction'); ?>">Approved</a></li>
                                    </ul>
                                </li>  
                            <?php
                                } else if(((int)($access->id)) == 5)
                                {
                            ?>
                                <li class="nav-header">SETTINGS / MODERATION</li>
                                <li class="accordion">
                                <!--<a href="#"><i class="glyphicon glyphicon-briefcase"></i><span> Accordion Menu</span></a> -->
                                <a href="#">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <i class="glyphicon <?php echo $access->icon_class ?>"></i>
                                            <?php echo $access->display_name ?>
                                        </div>
                                        <div class="col-md-3">
                                            <i class="fa fa-caret-down"></i>
                                        </div> 
                                    </div>   
                                </a>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li><a href="<?php echo base_url('admin/settings/to_suppliers'); ?>">Supplier</a></li>
                                        <?php foreach($transaction_types as $type){ ?>
                                            <li>
                                                <a href="<?php echo base_url($type->slug); ?>" ><?php echo $type->name ?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li> 
                            <?php
                                } else if(((int)($access->id)) == 6)
                                {
                            ?>  
                                <li class="accordion">
                                <!--<a href="#"><i class="glyphicon glyphicon-briefcase"></i><span> Accordion Menu</span></a> -->
                                <a href="#">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <i class="glyphicon <?php echo $access->icon_class?>"></i>
                                            <?php echo $access->display_name?>
                                        </div>
                                        <div class="col-md-3">
                                            <i class="fa fa-caret-down"></i>
                                        </div> 
                                    </div>   
                                </a>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li><a href="<?php echo base_url('admin/user') ?>">Users</a></li>
                                        <li><a href="<?php echo base_url('admin/menu'); ?>">Menus</a></li>
                                    </ul>
                                </li>      
                            <?php
                                }else
                                {

                            ?>
                                <li>
                                    <a class="ajax-link" href="<?php echo base_url("$access->slug"); ?>"><i class="glyphicon <?php echo $access->icon_class ?>"></i><span> <?php echo $access->display_name ?></span></a>
                                </li>
                            <?php
                                }
                             ?>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
        <!-- left menu ends -->
        <!-- start of the content -->
        <div id="content" class="col-lg-10 col-sm-10">


