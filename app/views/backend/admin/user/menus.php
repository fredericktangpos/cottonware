<div class="row">
    <div class="box col-md-7">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Menu List</h2>
            </div>
            <div class="box-content">
                <?php 
                    foreach ($dbArMenu_list as $menu) {
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="text" class="form-control menu_name" value="<?php echo $menu->display_name ?>" disabled="disabled">
                            <span class="input-group-addon menu_id_tag_<?php echo $menu->id ?>" theMenuID = "<?php echo $menu->id ?>" onclick="getMenuData(this);"><a href="#" id="<?php echo $menu->id ?>"><i class="glyphicon glyphicon-pencil"></i></a></span>
                       </div>
                    </div>
                </div><br>
                <?php
                    }
                 ?>
            </div>
        </div>
    </div>
    <div class="box col-md-5">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-plus-sign"></i> Add New Menu</h2>
            </div>
            <div class="box-content">
                <form action="<?php echo base_url("admin/menu/addMenu") ?>" method="post" role="form">
                    <div class="row">
                        <div class="col-md-12"><label for="">Display Name</label></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="form-group"><input type="text" class="form-control" name="menu_display_name" required></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><label for="">Slug</label></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"><input type="text" class="form-control" name="menu_slug" ></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><label for="">Twitter Icon Class</label></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"><input type="text" class="form-control" name="menu_icon_class" required></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><input type="submit" href="#" class="col-md-12 btn btn-primary" value="Add Menu to the List"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="viewMenuDetailModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-danger">Menu Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-label">
                            <label for="" class="form-label">Display Name</label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="m_display_name">
                            <input type="hidden" id="m_menu_id">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-label">
                            <label for="" class="form-label">Slug</label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="m_slug">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-label">
                            <label for="" class="form-label">Icon Class</label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="m_icon_class">
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <span class="text-danger _menu_update_err" style="display:none;"> Error on updating menu, please contact your database admin</span>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" id="cancel-delete-swatch" onclick="closeMenuModal()" >Close</button>
                <button class="btn btn-primary" id="persist-delete-swatch" onclick="saveChanges(this)">Save Changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->