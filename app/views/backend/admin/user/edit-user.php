<?php //dump($userAccess); ?>
<div>
	<div class="col-md-12">
		<h3>System Information</h3>
	</div>
	<form role="form" action="<?php echo base_url('admin/user/update')?>" method="post">
		<div class="row">
			<div class="col-md-6 customer-input-hs">
	            <label>
	                User Name
	                <input type="hidden" name="hdnIntUserId" value="<?php  echo $userDetails[0]->id?>">
	            </label>
	            <input type="text" class="form-control" id="txtChrUser_name" name="txtChrUser_name" placeholder="User Name" required value="<?php echo $userDetails[0]->u_username ?>">
	        </div>
			<div class="col-md-6 customer-input-hs">
	            <label>
	                Role
	            </label>
	            <select  id="selIntUser_role" name="selIntUser_role" class="form-control" required>
	            	<option value=""> -- Role -- </option>
	            	<option value="1" <?php 
	            		if($userDetails[0]->u_role == 1)
	            		{
	            			echo 'selected';
	            		}
	            	 ?>>Super Admin</option>
	            	<option value="2" <?php 
	            		if($userDetails[0]->u_role == 2)
	            		{
	            			echo 'selected';
	            		}
	            	 ?>>System Admin</option>
	            	<option value="3" <?php 
	            		if($userDetails[0]->u_role == 3)
	            		{
	            			echo 'selected';
	            		}
	            	 ?>>User</option>
	            </select>
	        </div>
        </div>
        <div class="row">
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                New Password <h5><i>(if you dont want to change the password, just leave this blank)</i></h5>
	            </label>
	            <input type="password" class="form-control" id="edit_txtChrUser_password" name="txtChrUser_password" placeholder="Password" >
	        </div>
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                Confirm New Password<h5><i>(if you dont want to change the password, just leave this blank)</i></h5>
	            </label>
	            <input type="password" class="form-control" id="edit_txtChrUser_password_confirm" name="txtChrUser_password_confirm" placeholder="Confirm Password" >
	        	<span class="text-danger passwordDontMatch" id="" style="display:none;">Password dont match, please try again!</span> 
	        </div>
		</div>
        <div class="col-md-12">
        	<h3>User Information</h3>
        </div>
        <div class="row">
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                First Name
	            </label> 
	            <input type="text" class="form-control" id="txtChrFirst_name" name="txtChrFirst_name" placeholder="First Name" value="<?php  echo $userDetails[0]->u_fname?>">
	        </div>
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                Last Name
	            </label>
	            <input type="text" class="form-control" id="txtChrLast_name" name="txtChrLast_name" placeholder="Last Name" value="<?php echo $userDetails[0]->u_lname ?>">
	        </div>
        </div>
        <div class="row">
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                Email
	            </label>
	            <input type="email" class="form-control" id="txtChrUser_email" name="txtChrUser_email" placeholder="Enter email" value="<?php echo $userDetails[0]->u_email ?>">
	        </div>	
	        <div class="col-sm-6 customer-input-hs">
	            <label>
	                Contact
	            </label>
	            <input type="text" class="form-control" id="txtChrUser_contact" name="txtChrUser_contact" placeholder="Contact #" value="<?php echo $userDetails[0]->u_contact ?>">
	        </div>	
        </div>
        <div class="row">	
	        <div class="col-sm-12 customer-input-hs">
	            <label>
	                Address
	            </label>
	            <input type="text" class="form-control" id="txtChrUser_address" name="txtChrUser_contact" placeholder="Address" value="<?php echo $userDetails[0]->u_address ?>">
	        </div>	
        </div><br>
        <div class="col-md-12">
        	<h3>User Access</h3>
        </div>
        <?php 
       			foreach ($dbArMenu_list as $menu) {
       		?>
			<div class="row">
				<div class="col-lg-6">
				    <div class="input-group">
				      <span class="input-group-addon">
				        <input type="checkbox" aria-label="..." name="selIntUser_access[]" value="<?php echo $menu->id ?>" <?php 
				        	foreach ($userAccess as $access) {
				        		if ($access->menu_id == $menu->id) {
				        			echo "checked";
				        		}
				        	}
				         ?>>
				      </span>
				      <input type="text" class="form-control" aria-label="..." value="<?php echo $menu->display_name ?>" disable="disable">
				    </div><!-- /input-group -->
				</div><!-- /.col-lg-6 -->
			</div><br>
       		<?php
       			}
       	 ?>
		<br>
       	<div class="row">
        	<div class="col-sm-12">
        		<button type="submit" onclick="return updateValidate();" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i><span>Update User</span></button>
			</div>
        </div>
    </form>
</div>