<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<div id="fb-root"></div>


<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Dashboard</a>
        </li>
    </ul>
</div>


<div class="row">

        <div class="col-md-4">
            <a data-toggle="tooltip" title="Customer List" class="well top-block" href="<?php echo base_url('admin/customer'); ?>">
                <i class="glyphicon glyphicon-user blue"></i>
                <div>Customers</div>
                <div><?php echo $Count ?></div>
            </a>
        </div>
        <div class="col-md-4">
             <a data-toggle="tooltip" title="View Customers" class="well top-block"   href="<?php echo base_url('admin/customer/edit/0'); ?>">
                <i class="glyphicon glyphicon-star green"></i>
                <div>Add Customers</div>
               
                <!-- <span class="notification green">4</span> -->
            </a>
        </div>
        <div class="col-md-4">
             <a data-toggle="tooltip" title="Job Order List" class="well top-block" href="<?php echo base_url('admin/joborder'); ?>">
                <i class="glyphicon glyphicon-shopping-cart yellow"></i>
                <div>Job Orders</div>
                <div><?php echo $CountTransaction;?></div>
               <!-- <span class="notification yellow">50</span> -->
            </a>
        </div>

</div>

</br>
<div class="row">

    <div class="col-md-8">
         <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-calendar"></i> Calendar</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div id="calendar"></div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="col-md-4">

        <div class="row">
            <div class="col-md-12">             
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-user"></i> Job Order Activity</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round btn-default"><i
                                    class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        <div class="box-content">
                            <ul class="dashboard-list dashboard-list-unique" style="overflow: auto;height:400px;" >
                                <li style="display:none;" class="list-src" >
                                    <strong>User :</strong> <a href="" class="users_name">Usman
                                    </a><br>
                                    <strong class="date">Date:</strong><br>
                                    <a href=""> <strong>Transaction #:</strong><b>000</b><b class="transaction_id"></b></a><br>
                                    <strong>Status:</strong> <span class="label-success label label-default trans_status">Approved</span>
                                    <hr>    
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
            </br>
        <div class="row">
                <div class="col-md-12">
                 <div class="fb-page" data-href="https://www.facebook.com/CottonWareServices" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Cottonware</a></blockquote></div></div>
                </div>
        </div>  


    </div>

</div>