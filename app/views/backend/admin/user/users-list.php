
<div class="row">
	<div class="col-md-12">
		<h2>Users List</h2>
	</div>
	<div class="col-md-12 text-right">
		<a href="<?php echo base_url("admin/user/newUserForm") ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i><span>Add New User</span></a>
	</div>
	<div class="col-md-12">
		<table class="table table-hover responsive">
			<thead>
				<tr>
					<th class="text-center">Full Name</th>
					<th class="text-center">System Username</th>
					<th class="text-center">Role</th>
					<th class="text-center">Status</th>
					<th class="text-center">View</th>
					<th class="text-center">Edit</th>
				</tr>
				<tbody>
					<?php foreach ($dbArUser_list as $user) {
					?>
					<tr>
						<td><?php echo $user->u_fname.' '.$user->u_lname ?></td>
						<td><?php echo $user->u_username ?></td>
						<td><?php
							$role = (int)($user->u_role); 
							if($role == 1)
							{
								echo "Super Admin";
							}else if($role == 2)
							{
								echo "System Admin";
							}else if($role == 3) {
								echo "System User";
							}
						 ?></td>
						 <td><?php
							$role = (int)($user->status); 
							if($role == 1)
							{
								echo "Active User";
							}else if($role == 0)
							{
								echo "Not Active";
							}
						 ?></td>
						<td class="text-center"><a theID = "<?php echo $user->id ?>" theName="<?php echo $user->u_fname.' '.$user->u_lname ?>" href="<?php echo base_url("admin/user/updateUser/".$user->id) ?>"> <i class="fa fa-edit"></i></a></td>
						<td class="text-center"><a theID = "<?php echo $user->id ?>" theName="<?php echo $user->u_fname.' '.$user->u_lname ?>" onclick="showUserDeleteModal(this);" href="" ><i class="fa fa-times red"></i></a></td>
					</tr>
					<?php
					} ?>
					
				</tbody>
			</thead>
		</table>
	</div>
</div>

<div class="modal fade in" id="deleteUserModal"  aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					Are you sure you want to remove <b class="del_user_name">No Name</b> from the Users List?. <br> <i>Reminder: Only soft delete, user can only be deactivated.</i>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary delete-user-confirm" onclick="deleteUser(this);" >Confirm Delete</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>