<div>
	<div class="col-md-12">
		<h3>System Information</h3>
	</div>
	<form role="form" action="<?php echo base_url('admin/user/addNew') ?>" method="post">
		<div class="row">
			<div class="col-md-6 customer-input-hs">
	            <label>
	                User Name
	            </label>
	            <input type="text" class="form-control" id="txtChrUser_name" name="txtChrUser_name" placeholder="User Name" required>
	        </div>
			<div class="col-md-6 customer-input-hs">
	            <label>
	                Role
	            </label>
	            <select  id="selIntUser_role" name="selIntUser_role" class="form-control" required>
	            	<option value=""> -- Role -- </option>
	            	<?php 
	                    foreach ($user_types as $types) {
	                 ?>
	                        <option value="<?php print_r($types->id) ?> "> <?php print_r($types->role_name) ?></option>
	                 <?php 
	                     }
	                  ?>
	            </select>
	        </div>
        </div>
        <div class="row">
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                Password
	            </label>
	            <input type="password" class="form-control" id="txtChrUser_password" name="txtChrUser_password" placeholder="Password" required>
	        </div>
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                Confirm Password
	            </label>
	            <input type="password" class="form-control" id="txtChrUser_password_confirm" name="txtChrUser_password_confirm" placeholder="Confirm Password" required>
	        	<span class="text-danger" id="passwordDontMatch" style="display:none;">Password dont match, please try again!</span> 
	        </div>
	        <div>
	        	
	        </div>
		</div>
        <div class="col-md-12">
        	<h3>User Information</h3>
        </div>
        <div class="row">
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                First Name
	            </label> 
	            <input type="text" class="form-control" id="txtChrFirst_name" name="txtChrFirst_name" placeholder="First Name">
	        </div>
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                Last Name
	            </label>
	            <input type="text" class="form-control" id="txtChrLast_name" name="txtChrLast_name" placeholder="Last Name">
	        </div>
        </div>
        <div class="row">
			<div class="col-sm-6 customer-input-hs">
	            <label>
	                Email
	            </label>
	            <input type="email" class="form-control" id="txtChrUser_email" name="txtChrUser_email" placeholder="Enter email">
	        </div>	
	        <div class="col-sm-6 customer-input-hs">
	            <label>
	                Contact
	            </label>
	            <input type="text" class="form-control" id="txtChrUser_contact" name="txtChrUser_contact" placeholder="Contact #">
	        </div>	
        </div>
        <div class="row">	
	        <div class="col-sm-12 customer-input-hs">
	            <label>
	                Address
	            </label>
	            <input type="text" class="form-control" id="txtChrUser_address" name="txtChrUser_contact" placeholder="Address">
	        </div>	
        </div><br>
        <div class="col-md-12">
        	<h3>User Access</h3>
        </div>
        <?php 
       			foreach ($dbArMenu_list as $menu) {
       		?>
			<div class="row">
				<div class="col-lg-6">
				    <div class="input-group">
				      <span class="input-group-addon">
				        <input type="checkbox" aria-label="..." name="selIntUser_access[]" value="<?php echo $menu->id ?>">
				      </span>
				      <input type="text" class="form-control" aria-label="..." value="<?php echo $menu->display_name ?>" disable="disable">
				    </div><!-- /input-group -->
				</div><!-- /.col-lg-6 -->
			</div><br>
       		<?php
       			}
       	 ?>
		<br>
       	<div class="row">
        	<div class="col-sm-12">
        		<button type="submit" class="btn btn-primary" onclick="return validate();"><i class="glyphicon glyphicon-plus-sign"></i><span>Add User</span></button>
			</div>
        </div>
    </form>
</div>