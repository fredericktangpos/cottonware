<div class="container">	
<form role="form" action="<?php echo base_url('admin/user/userUpdate')?>" method="post">
	<div class="row">
		<div class="col-md-4">
			<div style="position:relative;" class="user-image-container" >
				<img onmouseover="toggleUploadButton(this)" onmouseout="toggleDownButton(this)" class="preview-image-panel media-object img-thumbnail" src="<?php echo base_url("img/userimages/".$this->session->userdata('image')) ?> " alt="..." style="    max-height: 200px;
    max-width: 400px;">
				
			</div>
			<input  type="file" class="toggle-upload" style="visibility:hidden;width:80px;float:left; !important" accept="image/x-png, image/gif, image/jpeg">
			<input type="button" onclick="saveImagetoDB(this)" class="saveNewImage"value="save" style="visibility:hidden;none;height: 21px;
														    font-size: 11px;
														    float: left;
														    margin-left: 3%;">
			<input type="button" onclick="removeImage(this)" class="removeNewImage" value="remove" style="visibility:hidden;height: 21px;
														    font-size: 11px;
														    margin-left: 3%;">
			<input type="hidden" id="_img_user_id" value='<?php  echo $this->session->userdata('id');  ?>'>
		</div>
		<div class="col-md-8">
			<input type="hidden" name="hdnIntUserId" value="<?php echo $this->session->userdata('id'); ?>">
			<div class="row">
				<div class="col-md-2">
					<label for="" class="form-label">First Name</label>
				</div>
				<div class="col-md-4">
					<input type="text" class="form-control" id="txtChrFirst_name" name="txtChrFirst_name" placeholder="First Name"  value="<?php  echo $userDetails[0]->u_fname?>">
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-2">
					<label for="" class="form-label">Last Name</label>
				</div>
				<div class="col-md-4">
					<input type="text" class="form-control" id="txtChrLast_name" name="txtChrLast_name" placeholder="Last Name"  value="<?php echo $userDetails[0]->u_lname ?>">
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-2">
					<label for="" class="form-label">Email</label>
				</div>
				<div class="col-md-4">
					<input type="text" class="form-control" id="txtChrUser_email" name="txtChrUser_email" placeholder="Location"  value="<?php echo $userDetails[0]->u_email ?>">
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-2">
					<label for="" class="form-label">Contact</label>
				</div>
				<div class="col-md-4">
					<input type="text" class="form-control" id="txtChrUser_contact" name="txtChrUser_contact" placeholder="Contact number"  value="<?php echo $userDetails[0]->u_contact ?>">
				</div>
			</div>
		</div>
	</div><br>
	<div class="row">
		<div class="col-md-2">
			<label for="" class="form-label">Address</label>
		</div>
		<div class="col-md-6">
			<input type="text" class="form-control" id="txtChrUser_address" name="txtChrUser_address" placeholder="Address"  value="<?php echo $userDetails[0]->u_address ?>">
		</div>
	</div>
	<hr class="divider">
	<h3>System Information</h3>
	<div class="row">
		<div class="col-md-2">
			<label for="" class="form-label">Username</label>
		</div>
		<div class="col-md-6">
			<input type="text" class="form-control" id="txtChrUser_name" name="txtChrUser_name" placeholder="Username" value="<?php echo $userDetails[0]->u_username ?>" readonly>
		</div>
	</div><br>
	<div class="row">
		<div class="col-md-2">
			<label for="" class="form-label">Role</label>
		</div>
		<div class="col-md-6">
			 <select  id="selIntUser_role" name="selIntUser_role" class="form-control" readonly>
	            	<option value=""> -- Role -- </option>
	            	<option value="1" <?php 
	            		if($userDetails[0]->u_role == 1)
	            		{
	            			echo 'selected';
	            		}
	            	 ?>>Super Admin</option>
	            	<option value="2" <?php 
	            		if($userDetails[0]->u_role == 2)
	            		{
	            			echo 'selected';
	            		}
	            	 ?>>System Admin</option>
	            	<option value="3" <?php 
	            		if($userDetails[0]->u_role == 3)
	            		{
	            			echo 'selected';
	            		}
	            	 ?>>User</option>
	            </select>
		</div>
	</div>
	<hr class="divider">
	<h3>System Access</h3>
	<div class="row">
		<?php 
       			foreach ($dbArMenu_list as $menu) {
       				foreach ($userAccess as $access) {
				        		if ($access->menu_id == $menu->id) {
				   		?>
						<div class="row">
							<div class="col-lg-6">
							    <div class="input-group">
							      <span class="input-group-addon">
							        <input type="checkbox" aria-label="..." name="selIntUser_access[]" value="<?php echo $menu->id ?>" checked readonly = "readonly">
							      </span>
							      <input type="text" class="form-control" aria-label="..." value="<?php echo $menu->display_name ?>" readonly>
							    </div><!-- /input-group -->
							</div><!-- /.col-lg-6 -->
						</div><br>
			       		<?php
       				}
       			}
       		}
       	 ?>
	</div><br>
	<div class="row">
		<div class="col-md-2">
			<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i><span>Update User</span></button>
		</div>
		<input type="hidden" value="1" name="userprofile">
	</div>
</form>
</div>