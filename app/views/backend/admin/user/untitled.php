<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Dashboard</a>
        </li>
    </ul>
</div>

<div class="row center">
        <div class="col-md-4 col-sm-3 col-xs-6">
            <a data-toggle="tooltip" title="Customer List" class="well top-block" href="<?php echo base_url('admin/customer'); ?>">
                <i class="glyphicon glyphicon-user blue"></i>

                <div>Customers</div>
                <div><?php echo $Count ?></div>
            </a>
        </div>
      

          <div class="col-md-4 col-sm-3 col-xs-6">
            <a data-toggle="tooltip" title="View Customers" class="well top-block"   href="<?php echo base_url('admin/customer/edit/0'); ?>">
                <i class="glyphicon glyphicon-star green"></i>

                <div>Add Customers</div>
                <div>228</div>
                <!-- <span class="notification green">4</span> -->
            </a>
        </div> 

        <div class="col-md-4 col-sm-3 col-xs-6">
            <a data-toggle="tooltip" title="Job Order List" class="well top-block" href="<?php echo base_url('admin/joborder'); ?>">
                <i class="glyphicon glyphicon-shopping-cart yellow"></i>
                <div>Job Orders</div>
                <div>205</div>
               <!-- <span class="notification yellow">50</span> -->
            </a>
        </div>

</div>

<div class="row">
    <div class="box col-md-8">
       <!--  <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Quick Links</h2>
                <div class="box-icon"> -->
                    <!--<a class="btn btn-setting btn-round btn-default" href="#"><i class="glyphicon glyphicon-cog"></i></a>-->
                 <!--    <a class="btn btn-minimize btn-round btn-default" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a class="btn btn-close btn-round btn-default" href="#"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
        		<div class="row">
        			<div class="col-md-12 center">
        				<h3><a href="<?php echo base_url('admin/joborder'); ?>">Go to Transaction Area.</a></h3>
        				<hr>
        				<a href="<?php echo base_url('admin/customer/'); ?>">View Customer</a> | <a href="<?php echo base_url('admin/customer/edit/0'); ?>">Add Customer</a>
                        |<a href="<?php echo base_url('admin/customer/edit/0'); ?>">Add Customer</a>
        			</div>
        		</div>
        	</div>
        </div> -->
    </div>
    
    <!-- Calendar Area -->
    <div class="box col-md-8">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-calendar"></i> Calendar</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div id="calendar"></div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<!-- Calendar Area -->




    <div class="row">
        <div class="box col-md-4">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-user"></i> Member Activity</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <div class="box-content">
                        <ul class="dashboard-list">
                            <li>
                                <a href="#">
                                    <img class="dashboard-avatar" alt="Usman"
                                         src="http://www.gravatar.com/avatar/f0ea51fa1e4fae92608d8affee12f67b.png?s=50"></a>
                                <strong>Name:</strong> <a href="#">Usman
                                </a><br>
                                <strong>Since:</strong> 17/05/2014<br>
                                <strong>Status:</strong> <span class="label-success label label-default">Approved</span>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="dashboard-avatar" alt="Sheikh Heera"
                                         src="http://www.gravatar.com/avatar/3232415a0380253cfffe19163d04acab.png?s=50"></a>
                                <strong>Name:</strong> <a href="#">Sheikh Heera
                                </a><br>
                                <strong>Since:</strong> 17/05/2014<br>
                                <strong>Status:</strong> <span class="label-warning label label-default">Pending</span>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="dashboard-avatar" alt="Abdullah"
                                         src="http://www.gravatar.com/avatar/46056f772bde7c536e2086004e300a04.png?s=50"></a>
                                <strong>Name:</strong> <a href="#">Abdullah
                                </a><br>
                                <strong>Since:</strong> 25/05/2014<br>
                                <strong>Status:</strong> <span class="label-default label label-danger">Banned</span>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="dashboard-avatar" alt="Sana Amrin"
                                         src="http://www.gravatar.com/avatar/hash"></a>
                                <strong>Name:</strong> <a href="#">Sana Amrin</a><br>
                                <strong>Since:</strong> 17/05/2014<br>
                                <strong>Status:</strong> <span class="label label-info">Updates</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
             </br> 
             <div class="row">
                <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2F501103543319611&width=600&colorscheme=light&show_faces=true&border_color&stream=true&header=true&height=435" scrolling="yes" frameborder="0" style="border:none; overflow:hidden; width:600px; height:430px; background: white; float:left; " allowtransparency="true"></iframe>
                 </div>
             </div>
        </div>
    </div>

                


</div>