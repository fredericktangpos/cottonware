                        
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM"></script> <!-- remote -->
    <div>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">Customer</a>
            </li>
        </ul>
    </div>

        <!--
        <div class="row">
            <div class="col-md-12 text-right">
                <a href="#" ng-if="customer.id!==0" data-toggle="modal" data-target = "#addappointmentmodal" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i>Add Appointment</a>
            </div>
        </div>
        -->

    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-hover">
                        <tr>
                            <th>Customer Name</th>
                            <th>Rep. in Charge</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        <tbody class="appointment_list_body">
                            <tr id="clone-origin" class="" style="display:none;">
                                <td class="customer"></td>
                                <td class="name"></td>
                                <td class="date"></td>
                                <td class="time"></td>
                                <td class="added_by"></td>
                                <td class="status"></td>
                            </tr>
                       <?php 
                        if(count($appointmentsview) > 0) {
                            foreach ($appointmentsview as $appn) {
                         ?>
                                <tr class="appointment><?php echo $appn->id;?>   ">
                                    <td class="customer"><?php echo $appn->cus_name?></td>
                                    <td class="name"><?php echo $appn->sales_fname .'  '.$appn->sales_lname?></td>
                                    <td class="date"><?php echo $appn->date; ?></td>
                                    <td class="time"><?php echo $appn->time; ?></td>
                                    <td class="added_by"><?php 
                                        if ($appn->status == 0)
                                        {
                                            echo 'Pending';
                                        } else if ($appn->status == 1)
                                        {
                                            echo 'Done';
                                        } ?></td>
                                    <?php if($appn->status==0) { ?>
                                    <td class="status">

                                        <button class="btn btn-primary" id="persist-delete-type" data-toggle="modal" data-target = "#updateappointmentmodal">edit</button>
                                        
                                        <button class="btn btn-success" data-toggle="modal" data-target = "#updateStatus" id="persist-delete-type">Mark as Done</button>
                                        
                                        <button class="btn btn-danger" id="persist-delete-type" data-toggle="modal" data-target = "#delete_event">delete</button>
                                    </td>
                                    <?php } ?>
                                </tr>
                         <?php 
                                 }
                             } else {
                          ?>
                            <span class="text-center">No appointment...</span>
                          <?php 
                                }
                           ?>
                          </tbody>
                </table>
            </div>
            
        </div>
    </div>



    <div class="modal fade" id="delete_event">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-danger">Warning!!.</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this <b><i id="modal-supplier-name">Event</i></b>?
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal" id="cancel">Cancel</button>
                    <button class="btn btn-primary" id="confirm" onclick="deleted()">Confirm</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


     <div class="modal fade" id="updateStatus">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-danger">Warning!!.</h4>
                </div>
                <div class="modal-body">
                    update status to <b><i id="modal-supplier-name">Done</i></b>?
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal" id="cancel">Cancel</button>
                    <button class="btn btn-primary" id="confirm" onclick="updatestatus()">Confirm</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    <!-- ------ -->


        <div class="modal fade" id="delete_supplier2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-danger">Warning!!.</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this <b><i id="modal-supplier-name">Event</i></b>?
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal" id="cancel">Cancel</button>
                    <button class="btn btn-primary" id="confirm" onclick="deleted()">Confirm</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



<!-- ////////////// -->
  <div class="modal fade" id="updateappointmentmodal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Appointment</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"><label for="" class="control-label">Customer Property</label></div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="selIntCustomer_property" id="selIntCustomer_property" class="form-control input-sm">
                                        <option value=""> -- choose customer property -- </option>
                                        <?php 
                                            if( count($properties) > 0 )
                                            {
                                                foreach ($properties as $prop) {
                                                    ?>
                                                        <option value="<?php print_r($prop->id) ?> "><?php print_r($prop->asset_name); ?></option>
                                                    <?php
                                                }
                                            }
                                         ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="">Sales Rep. in Charge</label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select  class="form-control input-sm" name="selIntSales_representative" id="selIntSales_representative">
                                        <option value="0">-- <?php echo $appn->sales_fname .'  '.$appn->sales_lname?> --</option>
                                        <?php 
                                            if(count($sales_rep) > 0)
                                            {
                                                foreach ($sales_rep as $sales) {
                                         ?>
                                                <option value="<?php print_r($sales->id) ?>" ><?php print_r($sales->u_fname . '  '.$sales->u_lname ); ?></option>
                                         <?php 
                                                }
                                            }
                                          ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="">Date</label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="date" class="form-control input-sm" value="<?php echo $appn->date; ?>" name="chrDateAppointment_date" id="chrDateAppointment_date">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="">Time</label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="time" class="form-control input-sm" value="<?php echo $appn->time; ?>" name="chrTimeAppointment_time" id="chrTimeAppointment_time">
                                    <input type="hidden" value="<?php echo $id; ?>" name="hdnIntCustomer_id" id="hdnIntCustomer_id">
                                </div>
                                <div>
                                    <span class="text-warning show_in_warning" style="display:none;">Please fill the necessary information<i class="glyphicon glyphicon-warning"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-warning" id="cancel-delete-type" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary" id="persist-delete-type" onclick="updateAppointment(this)" >Update Appointment</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
    </div>