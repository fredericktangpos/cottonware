<form id="add-property-form">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group marg-top">
				<label for="txtInputName">Property Name: </label>
				<input type="text" class="form-control" id="txtChrProperty_name" placeholder="Asset Name">
				<span class="text-danger _no_property_name" style="display:none;"> Please fill the property name.</span>
			</div>
			<div class="form-group">
				<div class="inline-address">
					<div class="col-md-6">
						<label for="">Unit: </label>
						<input type="text" class="form-control" id="txtChrProperty_unit" placeholder="Unit">
						<label for="">Street: </label>
						<input type="text" class="form-control" id="txtChrProperty_street" placeholder="Street">
						<label for="">Building: </label>
						<input type="text" class="form-control" id="txtChrProperty_building" placeholder="Building">
						<label for="">Postal: </label>
						<input type="text" class="form-control" id="txtChrProperty_postal" placeholder="Postal">
						<span class="text-danger _no_postal_code" style="display:none;"> Please fill the postal code.</span>
					</div>
					<div class="col-md-6">
						<div class="fileinput fileinput-new" data-provides="fileinput">
					  		<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; margin-top: 25px;">
					  			
					  		</div>
				            <span class="btn btn-primary btn-file"><span class="fileinput-new" style="width: 100%;">Select image</span><span class="fileinput-exists">Change</span><input type="file" id="fileProperty_image" name="fileProperty_image"></span>
				       		<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</form>