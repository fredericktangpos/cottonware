<form id="edit-supplier-form">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<div class="inline-address">
					<div class="col-md-12">
						<div class="form-group">
							<label for="" class="control-label">Type Name</label>
							<input type="text" class="form-control input-sm" id="edit_swatch_name">
						</div>
						<div class="form-group">
							<input type="hidden" id="edit_supplier_id">
							<label class="control-label" for="">Type Code </label>
							<input type="text" class="form-control input-sm" id="edit_swatch_code" >
						</div>
						<div class="form-group">
							<label class="control-label" for="">Width </label>
							<input type="text" class="form-control input-sm" id="edit_swatch_width">
						</div>
						<div class="form-group">
							<label class="control-label" for="">Price per Meter </label>
							<input type="text" class="form-control input-sm" id="edit_swatch_cost">
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</form>