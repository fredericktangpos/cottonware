<form id="edit-item-form">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<div class="inline-address">
					<div class="col-md-12">
						<label>Item Name</label>
						<input type="text" class="form-control" id="txtChrItem_name" placeholder="Item Name">
						<input type="hidden" id="htxtItem_id" name="htxtItem_id" />
						<label for="">Item Type</label>
						<select name="" id="slctItem_category" class="form-control input-sm">
							<option value="">-- Select Category --</option>
							<option value="curtain">Curtain</option>
							<option value="sofa">Sofa</option>
							<option value="blind">Blind</option>
							<option value="track">Track</option>
							<option value="accessories">Accessories</option>
						</select>
					</div>
				</div>
			</div>
		</div>	
	</div>
</form>