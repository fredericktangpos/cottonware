
  <div style="margin-left:510px;" class="col-md-8"><p style="font-size:10px;">Date Created: <?php echo $datekaron; ?></p></div>

	<img style="width:100%;" src="img/printheader.png">
		
		<table style="margin-top:10px;" width="100%"> 
			<tr>
				<th style=" padding-right:100px; font-weight:normal;"><code> <b>Customer Name: </b><?php echo $customers_info[0]->cus_name;?> </code></th>
				<th style="margin-right:300px;padding: 15px; font-weight:normal;"><code><b>Ref: CW - <?php echo str_pad($transaction_id, 7, '0', STR_PAD_LEFT);?></b></code></th>
			</tr>
		</table>
		<br>
	<!-- 	<center><div><p>Customer Information</p></div></center> -->
	<div>
			<table style="font-size:13px; margin-left:400px;">
						 <tr>
						 	<td>FAX #:</td>
						 	<td>
						 		
						<?php 
							foreach (json_decode($customers_info[0]->cus_fax_num) as $fax_num) {
								print_r($fax_num);
								print_r("<br />");
							}
						 ?>
						 	</td>
						 </tr>
						 <tr>
						 	<td>HOME #:</td>
						 	<td>
						 	<?php 
							foreach (json_decode($customers_info[0]->cus_hom_num) as $home_num) {
								print_r($home_num);
								print_r("<br />");
								}
							 ?>
						 	</td>
						 </tr>
						 <tr>
						 	<td>HAND #:</td>
						 	<td>
						 	<?php 
							foreach (json_decode($customers_info[0]->cus_han_num) as $hand_num) {
								print_r($hand_num);
								print_r("<br />");
								}
							 ?>
						 	</td>
						 </tr>
						 <tr>
						 	<td>OFFICE #:</td>
						 	<td>
						 	<?php 
							foreach (json_decode($customers_info[0]->cus_off_num) as $office_num) {
								print_r($office_num);
								print_r("<br />");
								}
							 ?>
						 	</td>
						 </tr>
			</table>
			<br>
			<table>
				<tr>
					<th style=" padding-left:0px; font-weight:normal;">Email:</th>
					<th style=" padding-left:200px; font-weight:normal;">Address:</th>
				</tr>
				<tr>
					<td style=" padding-left:90px; font-weight:normal;" ><?php 
						foreach (json_decode($customers_info[0]->cus_email) as $email) {
							print_r($email);
							print_r("<br />");
						}
					 ?></td>
					<td style=" padding-left:200px; font-weight:normal;"><code><b><?php echo $customers_info[0]->cus_address_blk.' '.$customers_info[0]->cus_address_street.' '.$customers_info[0]->cus_address_unit;?></b></code> </td>
				</tr>
			</table>
	</div>
<hr style="color:grey;">

		<div class="row">
			<div class="col-md-12">
				<center><p>We are pleased to append herewith our quotation for your kind consideration.</p></center>
			</div>
		</div>

		

<table width="100%">
	<tr>
		<td><h5>ORDERED ITEMS</h5></td>
		<td></td>
	</tr>
	 <?php 
	 $total =  0 ;
	 if( count($job_transaction_item) > 0) {
	 
	 foreach ($job_transaction_item as $trans_item) {
	 ?>
		<tr style="border-top:1px solid black;">
			<td colspan="200px">
				<h5><b><code><?php print_r($trans_item->location_name . '   ,   ' . $trans_item->room_name);?></code></b></h2>
			</td>
			<td colspan="200px"></td>
		</tr>
			<?php 
				foreach ($item_order_list as $order_detail) {
					if($order_detail->trans_item_id == $trans_item->trans_id) {
			?>
				<tr>
					<td colspan="200px" style="padding-left:40px;"><code><?php print_r($order_detail->type_name . ' [' .$order_detail->type_code. ']') ?></code></td>
					<td colspan="200px" style=""><code style="align:right;">
						<?php foreach ($costing_list as $cost): ?>
							<?php if ($order_detail->trans_type_sel_id == $cost['selection_id']&& $cost['swatch_id'] == $order_detail->job_class_id && $cost['transaction_item_id'] == $order_detail->trans_item_id): ?>
								<b>$ </b><?php 
								$total +=(float)$cost['basic_price'];
								echo (number_format($cost['basic_price'], 2, '.', ''));
								 ?>
							<?php endif ?>
						<?php endforeach ?>
						</code>
					</td>
				</tr>
			<?php
					}
				}
			 ?>
		<tr>
			<td></td>
			<td></td>
		</tr>
	 <?php
	 }

	} else {
		?>
		<tr>
			<td colspan="200px" style="padding-left:40px;"><code>-- NO ITEMS --</code> </td>
			<td></td>
		</tr>

	<?php	
	}
	  ?>
	  	<tr>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><h5>OTHER SERVICES</h5></td>
			<td></td>
		</tr>
		<?php 
		if ( count($miscellanous) > 0 )
		{
			foreach ($miscellanous as $misc) {
		?>
			<tr>
				<td colspan="200px" style="padding-left:40px;"><code><?php echo $misc->description; ?></code></td>
				<td colspan="200px" style=""><code><b>$ </b> <?php echo number_format($misc->cost, 2, '.', '') ?></code></td>
			</tr>
		<?php
		$total+=(float)$misc->cost;
			}
		} else {
		?>
			<tr>
				<td colspan="200px" style="padding-left:40px;"><code>-- NO OTHER SERVICES --</code> </td>
				<td></td>
			</tr>
		<?php
			}
		 ?>

	  <tr style="background-color:#C2C2C2">
	  	<td colspan="200px" style="text-align:center;"><b><i><code>TOTAL</code></i></b></td>
	  	<td colspan="200px"><code><b>$ </b><?php print_r(number_format($total, 2, '.', '')) ?></code></td>
	  </tr>
</table>
<br>
<hr style="border-top: dotted 1px;">
<div width="1000px">
	<p><b><i>Remarks</i></b></p>
	<div style="margin-left:10px;">
		<p>
			<h5>1.) Validity: 30 days from date of quotation.<br>2.) Terms of Payment: Cash on delivery or cheque made payable to Cotton Ware LLP.<br>3.) The above cost is not subjected to GST.<br>4.) Deposit Required: 10%</h5>
		</p>

	</div>
	<div>
		<center>
		<p>
			<strong>Lovely Agero <br><h6>Cotton Ware LLP</h6></strong>
			<h6>53 Ubi Ave 1 Paya Ubi Industrial Park #01-29 Singapore 408934 <br>Tel: 6743 4048   Fax: 6848 1149 Website: www.cottonware.com.sg<br>Email: enquire@cottonware.com.sg</h6>
			<h6></h6><h6></h6>
		</p>
	</center>
	</div>
</div>