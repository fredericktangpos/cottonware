<form id="edit-supplier-form">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<div class="inline-address">
					<div class="col-md-12">
						<div class="form-group">
							<input type="hidden" id="edit_supplier_id">
							<label class="control-label" for="">Name </label>
							<input type="text" class="form-control input-sm" id="edit_supp_name" placeholder="Supplier Name">
							</div>
							<div class="form-group">
								<label class="control-label" for="">Representative </label>
								<input type="text" class="form-control input-sm" id="edit_supp_contact_name" placeholder="Contact Person">
							</div>
							<div class="form-group">
								<label class="control-label" for="">Contact Number </label>
								<input type="text" class="form-control input-sm" id="edit_supp_contact_num" placeholder="Contact Number">
							</div>
							<div class="form-group">
								<label class="control-label" for="">Email Address </label>
								<input type="email" class="form-control input-sm" id="edit_supp_email" placeholder="Email Address">
							</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</form>