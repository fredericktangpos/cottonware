<div class="row">
	<div class="col-md-12 table_joborder" >
		<table class="table table-responsive table-hover">
			<thead>
				<tr>
					<td class="text-center"><h4>JOB ID</h4></td>
					<td class="text-center"><h4>Transaction Date</h4></td>
					<td class="text-center"><h4>Status</h4></td>
					<td class="text-center"><h4>Type</h4></td>
				</tr>
			</thead>
			<tbody>
				<?php if(count($joborder_summary) > 0):?>
				<?php foreach($joborder_summary as $joborder_list): ?>
					<tr>
						<td class="text-center"><a href="<?php echo base_url("admin/joborder/edit/".$joborder_list->id); ?>"><?php echo str_pad($joborder_list->id, 7, '0', STR_PAD_LEFT); ?></a></td>
						<td class="text-center"><?php echo $joborder_list->job_trans_created; ?></td>
						<td class="text-center"><?php if($joborder_list->job_trans_status == 0) echo 'Pending'; else echo 'Completed'; ?></td>
						<td class="text-center"><?php if($joborder_list->job_trans_type == 1) echo 'New Transaction'; else echo 'Follow Up'; ?></td>
					</tr>
				<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="5">No Transaction yet.</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>