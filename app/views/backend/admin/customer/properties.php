<br />
<div class="row" id="propertyTab">
	<div class="col-md-12" >
		<div class="table-responsive" >
			<table class="table table-hover">
				<thead>
					<tr>
						<td class="text-left">
							<a class="btn btn-primary btn-xs" data-toggle="modal" href='#addnew_property'><i class="fa fa-plus"></i></a>
							<!-- <a href="<?php //echo base_url('admin/property/add'); ?>" target=blank class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></a>  -->
							<strong>Property Name</strong>
						</td>
						<td class="text-left"><strong>Address</strong></td>
					</tr>
				</thead>
				<tbody>
					<?php if(count($properties) > 0):?>
						<?php foreach($properties as $properties_idx => $properties_value):?>
							<tr>
								<td style="width:40%">
									<div class="clearfix">
										<?php 
				                            echo '<img class="img-responsive img-thumbnail pull-left" src="'.base_url($properties_value->asset_photo).'" width="20%" />
				                            <span class="pull-left" style="margin-left:10px;">'.$properties_value->asset_name.'</span>';
				                        ?>
			                        </div>
								</td>
								<td>
									<?php echo @$properties_value->asset_add_unit.' '.@$properties_value->asset_add_street.' '.@$properties_value->asset_add_bldg.' '.@$properties_value->asset_add_postal; 
									?> 
								</td>
								<td class="text-center">
									<!-- <a onclick="getpropID(this);" prop_id="<?php echo $properties_value->id; ?>" class="btn btn-success btn-xs" data-toggle="modal" href='#createnew_joborder'><i class="fa fa-plus-circle"></i> Job Order</a>
									<a href="<?php echo base_url('admin/joborder/showJobOrderForm/').'/'.$properties_value->id.'/'.$customer_id; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i>Add Job Order</a>-->
								</td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>
						<tr id="no-property">
							<td colspan="3" class="text-left">
								No property yet is registered to this customer. Do you want to add one? <a data-toggle="modal" href="#addnew_property">Click here</a>
							</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- modal area for adding new property -->
<div class="modal fade" id="addnew_property">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<input type="hidden" value="<?php echo $customer_id; ?>" id="customerID_property" />
				<h4 class="modal-title">Add New Property</h4>
			</div>
			<div class="modal-body">
				<?php $this->load->view('backend/admin/customer/property_form'); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary " id="submit_property">Save Property</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal for create job order for property -->
<div class="modal fade" id="createnew_joborder">
	<div class="modal-dialog" style="width:75%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<input type="hidden" value="<?php echo $customer_id; ?>" class="customerID" />
				<input type="hidden" value="" class="test" id="propoertyID" />
				<h4 class="modal-title">Create New Job Order</h4>
			</div>
			<div class="modal-body">
				<?php $this->load->view('backend/admin/joborders/new_joborder'); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button class="btn btn-primary" data-toggle="modal" href='#newWindow'><i class="fa fa-plus-circle"></i> Add Item</button>
				<button id="createBtn" type="button" onclick="SaveJobOrderTransaction();" class="btn btn-primary" disabled>Create Job Order</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- adding new window -->
<div class="modal fade" id="newWindow" asset_prop_id="" itemType="">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Adding New Item</h4>
			</div>
			<div class="modal-body">
				<form action="" method="POST" class="form-horizontal">
					<div class="form-group">
						<label class="col-md-2">Location</label>
						<div class="col-md-10">
							<input id="newLocation" type="text" name=""class="form-control" value="" placeholder="Location">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2">Room</label>
						<div class="col-md-10">
							<input id="newRoom" type="text" name=""class="form-control" value="" placeholder="Room">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2">Short Description</label>
						<div class="col-md-10">
							<textarea id="newDesc" class="form-control" rows="5" placeholder="Description"></textarea>

						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2">Dimension</label>
						<div class="col-md-3">
							<input id="newWW" type="text" class="form-control" placeholder="Window Width">
						</div>
						<div class="col-md-4">
							<input id="newWH" type="text" class="form-control" placeholder="Window Height">
						</div>
					</div>
					
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Add New Item</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->