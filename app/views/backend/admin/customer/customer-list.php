<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Customer</a>
        </li>
    </ul>
</div>
<div class="box-inner">
    <div class="box-header well">
        <h2><i class="glyphicon glyphicon-user"></i> Customer Area</h2>
        <div class="box-icon">
            <!-- <a class="btn btn-setting btn-round btn-default" href="#"><i class="glyphicon glyphicon-cog"></i></a> -->
            <a class="btn btn-minimize btn-round btn-default" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
            <!-- <a class="btn btn-close btn-round btn-default" href="#"><i class="glyphicon glyphicon-remove"></i></a> -->
        </div>
    </div>
    <div class="box-content">
		<div ng-controller="CustomerList" ng-cloak="" class="clearfix" >
			
			<!--  header -->
			<div class="customer-list row">
				<!-- searching of customer -->
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<input type="text" ng-model="filter" class="form-control" placeholder="Search">
						<div class="input-group-btn">
							<button type="button" class='btn btn-primary' ng-click="loadCustomer();">
								<i class="fa fa-search"></i>
							</button>
						</div>
						<div class="input-group-btn">
							<div class="btn-group">
							  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							    New <span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu" role="menu">
							    <li>
							    	<a href="<?php echo base_url("admin/customer/edit/0"); ?>">
							    		<i class="fa fa-group"></i>
							    		&nbsp;Customer
							    	</a>
							    </li>
							  </ul>
							</div>
						</div>
					</div>
				</div>
				<!--  /.  -->
				
				<!--  pagination -->
				<div class="col-sm-6 col-xs-6 customer-pagination-container text-center">
				</div>
				<!--  /.  -->
				
				<!--  items per page -->
				<div class="col-sm-2 col-xs-6" ng-show="customers.length!=0">
					<div class="input-group">
						<input type="text" class="form-control" ng-model="per_page">
						<div class="input-group-addon">
							<i class="fa fa-file"></i>
						</div>
					</div>
				</div>
				<!--  /.  -->
			</div>
			<!--  /. -->
			
			<!--  customer list  -->
			<div class="customer-list row" ng-cloak ng-show="!loading">
				<div class="col-xs-12">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th ng-click="orderVal=!orderVal; orderCol='cus_name';">
									Customer
								</th>
								<th ng-click="orderCol='datetime_created';orderVal=!orderVal">
									<i class="fa fa-calendar"></i>
								</th>
								<th>
									
								</th>
							</tr>
						</thead>
						<tbody ng-if="customers.length!=0">
							<tr class="customer-row" ng-repeat="customer in customers track by $index">
								<td>
									<a  href="<?php echo base_url("admin/customer/edit/{{customer.id}}"); ?>">
										<span class="company_information">
											{{customer.cus_name}}
										</span>
									</a>
								</td>
								<td>
									{{customer.datetime_created}}
								</td>
								<td>
									<i style="cursor:pointer" class="glyphicon glyphicon-remove" id="{{customer.id}}" ng-click="removeCustomerModal($event);"></i>
								</td>
							</tr>
						</tbody>

						<tbody ng-if="customers.length==0">
							<tr>
								<td colspan='2'>
									<div class="text-center general-empty">
										<i class="fa fa-trash"></i>
										<p>
											0 Customers
										</p>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!--  /.  -->
			
			<!--  show loading -->
			<div class="customer-list row" ng-show="loading">
				<div class="col-xs-12">
					<div class="general-empty text-center">
						<i class="fa fa-spinner fa-pulse"></i>
						<p>
							Loading Customers
						</p>
					</div>
				</div>
			</div>
			  	<div class="modal fade" id="removeCustomerModal" >
			      <div class="modal-dialog">
			        <div class="modal-content">
			          <div class="modal-header">
			            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			            <h4 class="modal-title">Remove from Order List</h4>
			          </div>
			          <div class="modal-body">
			            <div class="form-group">
			              Are you sure you want to remove this customer from the list?. <br>
			              <span class="text-danger"><i>* This action cannot be undone.</i></span>
			            </div>
			          </div>
			          <div class="modal-footer">
			            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
			            <button type="button" class="btn btn-primary removeCustomerConfirmBtn" ng-click="removeCustomer($event);">Confirm Remove</button>
			          </div>
			        </div><!-- /.modal-content -->
			      </div><!-- /.modal-dialog -->
			    </div><!-- /.modal -->
			<!--  /.  -->
		</div>
    </div>
</div>
