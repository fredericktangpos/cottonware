<div class="row">
	<div class="col-md-2">
		<div class="form-group">
			<label class="control-label" for="">Contact Person </label>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<input type="hidden" class="asset_id" value="<?php print_r($asset_id) ?>">
			<input type="hidden" class="customer_id" value="<?php print_r($intCustomer_id) ?>">
			<input type="hidden" class="user_id" value="<?php print_r($intUser_id) ?>">

			<select name="" id="contact_person_id" class="form-control">
				<option value="">----</option>
				<?php 
					foreach ($customer_contacts as $contact) {
						?>
							<option value="<?php print_r($contact->id)?>"><?php print_r($contact->name) ?></option>
						<?php
					}
				 ?>
			</select>
			<span class="text text-danger _no_contact_person" style="display:none;">Please choose customer contact person. </span>
		</div>
	</div>
	<div class="col-md-6">
		<a href="#" class="btn btn-primary " data-toggle="modal" data-target="#addOrd"><i class="glyphicon glyphicon-plus-sign"></i>   ADD</a>
	</div>
</div>
<div class="mock_clone_src" style="display:none;">
	<div class="location_area">
		<div class="row">
				<div class="col-md-12">
					<h3>Location: <i class="location_name">2nd Floor</i> <input type="hidden" name="location[]" class="location_id"></h3>
				</div>
		</div>
		<div class="row room_area">
			<div class="room_clone_src">
				<div class="col-md-12">
					<i class="room_name"></i> <input type="hidden" name="room[]" class="room_id">
				</div>
				<div class="col-md-12 dimension_area">
					<div class="dimension_clone_src" style="display:none;">
						<i class="room_width">12</i> x <i class="room_height">12</i>
						<ol class="mock_order_list">
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="order_area">


</div>
<a href="#" class="btn btn-primary saveOrderBtn" style="display:none;" onclick="saveOrder()">Save Order</a>

<!-- the new window modal  -->
<div class="modal fade" id="addOrd">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="well">
					<div class="row">
						<div class="col-md-2">
							<div class="form-group">
								<label for="" class="control-label">Choose Level</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<select name="" class="form-control order_location" >
									<option value=""></option>
									<?php 
										foreach ($locations as $location) {
											?>
												<option value="<?php  print_r($location->id)?>"><?php print_r($location->location_name) ?></option>
											<?php
										}
									 ?>
								</select>
								<span class="text text-danger _no_location" style="display:none;">Please choose level.</span>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<input type="text" class="form-control _other_location"  placeholder = "others if not specified" >
							</div>
						</div>
						<div class="col-md-4">
							<a href="#" class="btn btn-primary" onclick = "showNewRoom()" ><i class="glyphicon glyphicon-plus-sign"></i> ROOM</a>
						</div>
					</div>
					<div class="one-room clone-src" style="display:none">
							<div class="row">
								<hr>
								<div class="" style="text-align:left;margin-left:1%;">
									<a href="#" class="text-danger" onclick="_REMOVEROOM(this)"><i class="glyphicon danger glyphicon-remove">remove.room</i></a>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label for="" class="control-label">Room Description</label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<select name="" id="order_room" class="form-control">
											<option value=""></option>
											<?php 
												foreach ($rooms as $room) {
													?>
														<option value="<?php print_r($room->id) ?>"><?php print_r($room->room_name) ?></option>
													<?php
												}
											 ?>
										</select>
										<span class="text-danger _no_room_err" style="display:none;"> Please choose room description</span>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<input type="text" class="form-control _other_room" placeholder = "others if not specified" >
									</div>
								</div>
								

							</div>
							<!--
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="file" class="form-control" multiple>
									</div>
								</div>
							</div> -->
							<div class="row">
								<div class="col-md-2">
									<a class="btn btn-success btn-xs" onclick="showNewDimension(this)"><i class="fa fa-plus-circle"></i>&nbsp;	Add Window Dimension</a>
								</div><br><br>
							</div>
							<div class="dimension-area">
								<div class="row one-dimension dclone-src" style="display:none;">
									<div class="" style="margin-left:1%;">
										<a href="#" class="text-danger" onclick="_REMOVEDIMENSION(this)"><i class="glyphicon glyphicon-remove">remove</i></a>
									</div>
									<div class="col-md-1">
										<div class="form-group">
											<input type="text" id="order_width" class="form-control" placeholder="width(in)">
										</div>
										
									</div>
									<div class="col-md-1">
										<div class="form-group">
											<input type="text" id="order_height" class="form-control" placeholder="height(in)">
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<select name=""  onchange = "chooseStyle(this)" class="form-control basic_type">
												
												<option value="">--Curtain Type--</option>
												<?php 
													foreach ($types as $type) {
														?>
															<option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
														<?php
													}
												 ?>
											</select>
										</div>
										<div class="form-group" id="order_style">
											<select name="" id="" class="form-control type_sel_swatch secondary_type">
											</select>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<select name="" id="" onchange = "chooseStyle(this)" class="form-control basic_type">
												<option value="">--Curtain Type--</option>
												<?php 
													foreach ($types as $type) {
														?>
															<option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
														<?php
													}
												 ?>
											</select>
										</div>
										<div class="form-group">
											<select name="" id="" class="form-control type_sel_swatch secondary_type">
											</select>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<select name="" id="" onchange = "chooseStyle(this)" class="form-control basic_type">
												
												<option value="">--Curtain Type--</option>
												<?php 
													foreach ($types as $type) {
														?>
															<option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
														<?php
													}
												 ?>
											</select>
										</div>
										<div class="form-group">
											<select name="" id="" class="form-control type_sel_swatch secondary_type">
											</select>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<select name="" id="" onchange = "chooseStyle(this)" class="form-control basic_type">
												
												<option value="">--Curtain Type--</option>
												<?php 
													foreach ($types as $type) {
														?>
															<option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
														<?php
													}
												 ?>
											</select>
										</div>
										<div class="form-group">
											<select name="" id="" class="form-control type_sel_swatch secondary_type">
											</select>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<select name="" id="" onchange = "chooseStyle(this)" class="form-control basic_type">
												
												<option value="">--Curtain Type--</option>
												<?php 
													foreach ($types as $type) {
														?>
															<option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
														<?php
													}
												 ?>
											</select>
										</div>
										<div class="form-group">
											<select name="" id="" class="form-control type_sel_swatch secondary_type">
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					<div class="room-area">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" id="cancel-delete-supplier" onclick="hideModal()">Cancel</button>
				<button class="btn btn-primary" id="persist-delete-supplier" onclick="getOrder(this)">Confirm</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->