<form enctype="multipart/form-data" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('admin/joborder/edit'); echo @$joborder->id !== NULL ? '/'. @$joborder->id : '' ; ?>">
	<!-- search customer -->
	<div class="form-group">
		<!-- <label class="col-md-2">Customer Name</label> -->
		<div class="col-md-12">
			<input onblur="search_customer(this);" type="text" class="form-control thekeyword" value="" placeholder="Please search customer" />
			<input name="tbl_customer_id" type="hidden" id="customer_id">
		</div>
		<div class="col-md-12">
			<div style="display:none;" class="showResult">
				<ul class="list-group">
				</ul>
			</div>
		</div>
	</div>
	<!-- divider and the Details of the trasaction-->
	<div class="form-group">
		<div class="col-md-12">
			<hr style="border:2px dashed #999; margin-bottom:0px;" />
			<h3 style="margin:5px 0px 0px 0px; font-size:18px;">Transaction Details</h3>
		</div>
	</div>

	<!-- property/asset details -->
	<div class="form-group add_property">
		<div class="col-md-12">
			<div class="panel panel-default">
				  <div class="panel-heading" style="background:#EBC137;">
						<div class="clearfix">
							<h3 class="panel-title property_title pull-left">Property</h3>
							<a href="javascript:void(0);" onclick="" class="pull-right property_clone" style="color:#fff; cursor:pointer;font-size:15px;" ><i class="fa fa-plus-circle"></i></a>
							<a href="javascript:void(0);" onclick="$(this).parent().parent().parent().find('.panel-body').toggle('slow');" class="pull-right" style="color:#fff; cursor:pointer;font-size:15px;margin-right:5px;" ><i class="fa fa-minus-circle"></i></a>
						</div>
				  </div>
				  <div class="panel-body">
						<!-- search contact person here -->
						<div class="form-group">
							<div class="col-md-12">
								<input type="hidden" id="property_id">
								<input onblur="search_customer_contact(this);" type="text" class="form-control contact_key" value="" placeholder="Please search contact person" />
								<input name="property[0][customer_contact_id]" type="hidden" class="customer_contact_id">
							</div>
							<div class="col-md-12">
								<div style="display:none;" class="showResult">
									<ul class="list-group">
									</ul>
								</div>
							</div>
						</div>

						<!-- property level here-->
						<div class="form-group">
							<div class="col-md-12">
								<select name="property[0][property_id]" onchange="load_asset_sub_dtl(this);" class="form-control display_prop">
									<option value="0">Please asset/property</option>
								</select>
							</div>
						</div>

						<!-- window details here-->
						<div class="form-group">
							<div class="col-md-12">
								<div class="panel panel-primary thewindow">
									  <div class="panel-heading">
										  <div class="clearfix">
											<h3 onclick="$(this).parent().parent().parent().find('.panel-body').toggle('slow');" style="cursor:pointer;" class="panel-title window_title pull-left">Window</h3>
											<a href="javascript:void(0);" onclick="add_window(this);" class="pull-right wind_plus" style="color:#fff; cursor:pointer;font-size:15px;" ><i class="fa fa-plus-circle"></i></a>
										  </div>
									  </div>
									  <div class="panel-body">
											<div class="row">
												<div class="col-md-2" style="background:#fff;">
													<img src="<?php echo base_url('images/h_default.png'); ?>" class="img-responsive" alt="Image" style="border:2px solid #999;" width="150" />
													<p class="text-center">Upload Photo</p>		
												</div>
												<div class="col-md-10">
													<!-- window description -->
													<div class="form-group">
														<div class="col-md-12">
															<select onchange="copyToWindowheader(this);" prop_idx="0" name="property[0][window][0][asset_sub_dtl_id]" class="form-control tbl_asset_sub_dtl_id">
																<option value="" selected>Select window</option>
															</select>
														</div>
													</div>
													<!-- window ceiling height -->
													<div class="input-group">
														<div class="input-group-addon">
															<input name="property[0][window][0][fh]" type="text" class="form-control fh" value="" placeholder="Full Height (inch)">
														</div>
														<div class="input-group-addon">
															<input name="property[0][window][0][wh]" type="text" class="form-control wh" value="" placeholder="Window Height (inch)">
														</div>
														<div class="input-group-addon">
															<input name="property[0][window][0][ww]" type="text" class="form-control ww" value="" placeholder="Width (inch)">
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<!-- window curtain type -->
													<div class="well well_custom">
														<div win_idx="0" class="add_item">
															<div class="input-group clearfix">
																<button onclick="clone_trigger(this);" type="button" class="btn btn-primary thebutton"><i class="fa fa-plus"></i></button>
																<button style="display:none;" onclick="remove_item(this,'.clone_container .add_item');" type="button" class="btn btn-danger thebutton"><i class="fa fa-times"></i></button>
															</div>
															<div class="input-group">
																<span class="input-group-addon">Curtain</span>
																<select name="property[0][window][0][curt][0][thecurt]" class="form-control curt">
																	<option value="" selected>Select Curtain</option>
																	<?php if(count($curtain) > 0):?>
																		<?php foreach ($curtain as $curtains): ?>
																				<option value="<?php echo $curtains->id; ?>"><?php echo $curtains->job_type_name; ?></option>
																		<?php endforeach; ?>
																		<?php else: ?>
																				<option value="">No curtain Type</option>
																	<?php endif; ?>
																</select>
															
																<span class="input-group-addon">Blind</span>
																<select name="property[0][window][0][blnd][0][theblnd]" class="form-control blnd">
																	<option value="" selected>Select Blinds</option>
																	<?php if(count($blind) > 0):?>
																		<?php foreach ($blind as $blinds): ?>
																				<option value="<?php echo $blinds->id; ?>"><?php echo $blinds->job_type_name; ?></option>
																		<?php endforeach; ?>
																		<?php else: ?>
																				<option value="">No blind Type</option>
																	<?php endif; ?>
																</select>
															
																<span class="input-group-addon">Track</span>
																<select name="property[0][window][0][trck][0][thetrck]" class="form-control trck">
																	<option value="" selected>Select track</option>
																	<?php if(count($track) > 0):?>
																		<?php foreach ($track as $tracks): ?>
																				<option value="<?php echo $tracks->id; ?>"><?php echo $tracks->job_type_name; ?></option>
																		<?php endforeach; ?>
																		<?php else: ?>
																				<option value="">No track Type</option>
																	<?php endif; ?>
																</select>
															</div>
														</div>
														<div class="clone_container"></div>
													</div>
												</div>
											</div>
									  </div>
								</div>
								<div class="thewindow_add"></div>
							</div>
						</div>
				  </div>
			</div>
		</div>
	</div>

	<!-- form button  -->
	<div class="form-group">
		<div class="col-md-12">
			<button type="submit" class="btn btn-primary pull-right form-control"> Save Job Order</button>
		</div>
	</div>
</form>
