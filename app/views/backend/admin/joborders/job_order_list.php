<div class="row">
	<h2>Pending Transactions</h2>
</div><br>

<div class="row">
	
	<div class="col-xs-12">
		<div class="table-responsive">
			<table class="table table-hover">	
				<thead>
					<tr>
						<th>Transaction Number</th>
						<th>Customer</th>
						<th></th>
					</tr>
				</thead>

				<?php 
					if(count($joborders["msg"])!==0):
				?>
				<tbody>
					<?php 
					foreach($joborders["msg"] as $page):
					?>
					<tr>
						<td>
							<a href="<?php echo base_url("admin/joborder/edit/".$page->id); ?>">
								CW - <?php echo str_pad($page->id, 7, '0', STR_PAD_LEFT);?>
							</a>
						</td>
						<td>
							<a href="<?php echo base_url("admin/customer/edit/".$page->customer_id); ?>">
								<?php 
									echo $page->cus_name;
								?>
							</a>
						</td>
						<td>
							<?php 
								echo $page->job_trans_created;
							?>
						</td>
					</tr>
					<?php
					endforeach;
					?>
				</tbody>
				<?php
					endif;
				?>

				<tbody>
					
				</tbody>

			</table>
		</div>
	</div>
</div>

<!--  pagination  -->
<div class="row">
	<div class="col-xs-10 text-center" >
		<?php 
			echo $joborders["page_links"];
		?>
	</div>
</div>
<!-- /. -->