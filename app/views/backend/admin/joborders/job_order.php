<div ng-controller="JobOrder">
  <div class="fluid-container">
    <div class="row">
      <?php 
          if($transaction_data->job_trans_status == 0){
          ?>
            <div class="col-md-2">
             <a href="<?php echo base_url('admin/joborder/createQuotation/').'/'.$transaction_id ?>" onclick = "createQuotation(this)" theTransactionID = "" id="create_quotation" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-floppy-save"></i>&nbsp;Create Quotation</a>  
            </div>
            <div class="col-md-2">
              <a href="" theTransactionID = "<?php echo $transaction_id; ?>" onclick="createInvoice(this);" id="create_invoice" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-file"></i>&nbsp;Create Invoice</a>
            </div>
          <?php 
          }
        ?>
      <div class="col-md-2">
        <div class="btn-group">
          <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="glyphicon glyphicon-repeat"></i>&nbsp;Revisions <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <?php 
                $count = 0;
                if(count($dbaRquotations)>0)
                {
                  foreach ($dbaRquotations as $versions) {
                    $count++;
                ?>
                    <li><a target="_blank" href="<?php echo base_url('admin/joborder/showRevisions/'.$versions->id)?>">Version <?php echo $count ?></a></li>
                <?php 

                  }
                }
               ?>
          </ul>
        </div>
      </div>
      <div class="col-md-2">
        <a href="<?php echo base_url('admin/joborder/downloadPDF/').'/'.$transaction_id ?>" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-download-alt"></i>&nbsp;Download PDF</a>
      </div>
    </div>
    <hr>
  </div>
  <div class="fluid-container">
    <div class="row">
      <div class="col-md-2">Customer Name:</div>
      <div class="col-md-4" ><?=$customer_data[0]['cus_name']?></div>
      <div class="col-md-2">Date Created:</div>
      <div class="col-md-4" ><?=date("M d, Y", strtotime($transaction_data->job_trans_created))?></div>
    </div>
      <div class="row">
      <div class="col-md-2">Property Name:</div>
      <div class="col-md-4" ><?=$customer_data[0]['asset_name']?></div>
      <div class="col-md-2">Last Updated:</div>
      <div class="col-md-4" ><?=date("M d, Y", strtotime($transaction_data->job_trans_modify))?></div>
    </div>
    </div>
    <div class="row">
      <div class="col-md-2">Status:</div>
      <div class="col-md-4" ><code><?php if($transaction_data->job_trans_status == 0) echo '<strong style="color:red;">PENDING</strong>'; else  echo '<strong style="color:red;">ON PROCESS</strong>'; ?></code></div>
      <div class="col-md-2">Trans Type:</div>
      <div class="col-md-4" ><?php if($transaction_data->job_trans_type == 1) echo '<strong style="color:red;">FIRST TIME</strong>'; else echo '<strong style="color:red;">FOLLOW UP</strong>';?></div>
    </div>
    <div class="row">
      <div class="col-md-2">Current Total:</div>
      <div class="col-md-4" ><code>0.00</code></div>
      <div class="col-md-2">Tracking #:</div>
      <div class="col-md-4" ><?=$transaction_data->id?></div>
    </div>
    <hr>
    <div class="row">
    <div class="box col-md-12">
      <div class="box-inner">
        <div data-original-title="" class="box-header well">
            <i class="glyphicon glyphicon-picture"></i> Transaction Images
           <div class="box-icon">
            <a class="btn btn-minimize btn-round btn-default" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
           </div>
        </div>
        <div class="box-content image-set" style="display:block;">
          <div class="image-set">
          <?php 
          if ( count($transaction_images) > 0 )
          {
            foreach ($transaction_images as $images) {
          ?>
            <a style="height:100px;width:200px;float:left;" href="<?php echo base_url("img/orderimages/".$images->directory) ?> " data-lightbox="order-images" data-title="" rel="lightbox[gallery]"><img data-lightbox="order-images" style="width:auto;height:inherit;" src="<?php echo base_url("img/orderimages/".$images->directory) ?> " alt=""></a>
          <?php

              }
            } else {
              echo "NO IMAGES UPLOADED IN THIS TRANSACTION";
            }
           ?>
               </div>
          <div style="clear:both;"></div>
        </div>
      </div>
    </div>
  </div>
  <hr>
    <div class="row">
      <?php 
        if($transaction_data->job_trans_status == 0) {
      ?>
         <div class="col-md-12">
            <a class="btn  btn-primary btn-xs" data-toggle="modal" href='#addOrdOnEdit'><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Add new location order</a>
          </div><br><br>
      <?php
        }
       ?>
      <?php 
        if (count($job_transaction_item) > 0) {
          foreach($job_transaction_item as $item){ 
      ?>
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><b>Location :</b> <?=$item->location_name?>, &nbsp;<b>Room:</b><?=$item->room_name?>,&nbsp;<b>Width (ft):</b> <?=$item->window_width?> <b>&nbsp;Height (ft):</b><?=$item->window_height?>  
            <?php 
              if($transaction_data->job_trans_status == 0){
            ?>
                <a class="btn btn-link btn-xs pull-right danger" assetDetailID = "<?php echo $item->asset_dtl_id; ?>" transactionItemID = "<?php echo $item->trans_id; ?>" onclick="showConfirmModal(this)"><i class="glyphicon glyphicon-trash"></i>&nbsp;Discard Dimension</a>
                <a class="btn btn-link btn-xs pull-right" theTransactionTypeID = "<?php echo $item->asset_type?>" theTransactionItemID="<?php echo $item->trans_id ?>" ng-click="showItemOrderDialog(<?php echo $item->trans_id ?>,<?php echo $item->asset_type ?>);"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Order</a>
            <?php
              }
            ?>
          </div>
          <div class="panel-body">
            <?php if ( count($item_order_list) > 0 ): ?>
            <table class="table table-hover">
              <tbody class="order-list-<?=$item->trans_id?>">
                <?php
                  foreach($item_order_list as $order){
                    if($item->trans_id == $order->trans_item_id) {
                      if($order->type_name!='' || $order->type_name!=NULL){
                ?>
                        <tr class="order-item-<?=$order->trans_type_sel_id?>">
                          <td><?=$order->type_name?> [<?=$order->type_code?>]</td>
                          <td><b>$</b> 
                          <?php 
                              foreach ($costing_list as $cost) {
                                if ($cost['swatch_id'] == $order->tbl_job_classification_id 
                                  && $cost['transaction_item_id'] == $order->tbl_transaction_item_id
                                  && $cost['selection_id'] == $order->trans_type_sel_id)
                                {
                                  echo $cost['basic_price'];
                                }
                              }
                           ?>
                          </td>
                          <td><?php if ($transaction_data->job_trans_status == 0) {
                            ?>
                              <a class="btn btn-default btn-xs btn-link" href="" onclick="deleteOrderItem(this)" theOrderID = "<?php echo $order->trans_type_sel_id ?>" theItemID = "<?php echo $item->trans_id ?>"> <i class="glyphicon glyphicon-remove"></i></a>
                            <?php
                          } ?>
                        </td>
                        </tr>
                <?php
                      }
                    }
                  }
                 ?>
              </tbody>
            </table>
            <?php endif ?>
          </div>
        </div>
      </div>
      <?php
          }
        } else {
          echo 'No Order for this transaction';
        }
       ?>
    </div>
    <div class="row">
      <?php 
        if ($transaction_data->job_trans_status == 0) {
      ?>
          <div class="col-md-12">
            <a class="btn  btn-primary btn-xs" data-toggle="modal" href='#add-misc-dialog'><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Add new miscellanous</a>
          </div><br><br>
      <?php
        }
       ?>
      
      <div class="col-md-12 misc-order-container">
        <?php 
            if ( count($MISC_DATA)  > 0 ) {
              echo '<table class="table table-hover" >';
              foreach ($MISC_DATA as $misc) { 
        ?>
                
                  <tr id="<?=$misc['id']?>">
                    <td class="misc_description"><?=$misc['description']?></td>
                    <td><b>$</b><span class="misc_cost"><?=$misc['cost']?></span> </td>
                    <td><a class="btn btn-default btn-xs btn-link" href="" onclick="editMisc(this)"> <i class="glyphicon glyphicon-edit"></i></a>&nbsp;&nbsp;<a class="btn btn-default btn-xs btn-link" href="" onclick="deleteMisc(this)"> <i class="glyphicon glyphicon-remove"></i></a></td>
                  </tr>
        <?php
              }
              echo '</table>';
            } else {
              echo 'No miscellanous added in this order.';
            }
         ?>
      </div>
      
    </div>
      <div class="modal fade" id="item-order-dialog" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Choose Order</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label" for="">Choose Order Type</label>
            <select class="form-control input-sm" onchange="searchSubType(this);" id="order-main-type">
              <option value=""> -- Choose Order Type -- </option>
              <option value="{{ type.id}}" ng-repeat="type in sub_type track by $index">{{ type.job_type_name }}</option>
              
            </select>
          </div>    
          <div class="form-group">
            <label class="control-label" for="">Choose Style</label>
            <select class="form-control input-sm" id="subsub_style" onchange="getSubTypeDetail(this)">
              <option value="0"> -- Style -- </option>
            </select>
          </div>

          <div class="form-group">
            <label for="" class="control-label">Description</label>
            <textarea name="" class="form-control input-sm" id="txtChrOrder_description" cols="20" rows="5" style="resize:none"></textarea>
          </div>
          <p class="show-add-order-edit text-warning" style="display:none;">* Please chose an order!</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary order-item-submit" onclick="addJobOrder(this);">Add Order</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  </div>
  <!-- the new window modal  -->  
  <div class="modal fade" id="addOrdOnEdit">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="well">
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <input class="order_asset_id" type="hidden" value="<?php echo $ASSET_ID ?>"> 
                  <input type="hidden" class="order_contact_person" value="<?php echo $CONTACT_PERSON ?>"> 
                  <input type="hidden" id="addOnOrderTransaction_id" value="<?php echo $transaction_id; ?>">
                  <label for="" class="control-label">Choose Level </label>
                  <p style="display:none;" class="text-danger no-room-level-validation">* No room added for this level.</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <select name="" class="form-control order_location" >
                    <option value=""></option>
                    <?php 
                      foreach ($locations as $location) {
                        ?>
                          <option value="<?php  print_r($location->id)?>"><?php print_r($location->location_name) ?></option>
                        <?php
                      }
                     ?>
                  </select>
                  <span class="text text-danger _no_location" style="display:none;">Please choose level.</span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <input type="text" class="form-control _other_location"  placeholder = "others if not specified" >
                </div>
              </div>
              <div class="col-md-4">
                <a href="#" class="btn btn-primary" onclick = "showNewRoom()" ><i class="glyphicon glyphicon-plus-sign"></i> ROOM</a>
              </div>
            </div>
            <div class="one-room clone-src" style="display:none">
                <div class="row">

                  <hr>
                  <div class="col-md-2">
                    <p style="display:none;" class="text-danger no-dimension-room-validation">* No dimension added for this room</p>
                    <div class="form-group">
                      <label for="" class="control-label">Room Description</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <select name="" id="order_room" class="form-control">
                        <option value=""></option>
                        <?php 
                          foreach ($rooms as $room) {
                            ?>
                              <option value="<?php print_r($room->id) ?>"><?php print_r($room->room_name) ?></option>
                            <?php
                          }
                         ?>
                      </select>
                      <span class="text-danger _no_room_err" style="display:none;"> Please choose room description</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control _other_room" placeholder = "others if not specified" >
                    </div>
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-2">
                    <a class="btn btn-success btn-xs" onclick="showNewDimension(this)"><i class="fa fa-plus-circle"></i>&nbsp;  Add Window Dimension</a>
                  </div><br><br>
                </div>
                <div class="dimension-area">
                  <div class="row dclone-src" style="display:none;">
                    <p style="display:none;" class="text-danger no-order-dimension-validation">* No order selected for this dimension.</p>
                    <div class="col-md-1">
                      <div class="form-group">
                        <input type="number" id="order_width" class="form-control" placeholder="width(in)">
                      </div>
                    </div>
                    <div class="col-md-1">
                      <div class="form-group">
                        <input type="number" id="order_height" class="form-control" placeholder="height(in)">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <select name=""  onchange = "chooseStyle(this)" class="form-control basic_type">
                          
                          <option value="">--Curtain Type--</option>
                          <?php 
                            foreach ($types as $type) {
                              ?>
                                <option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
                              <?php
                            }
                           ?>
                        </select>
                      </div>
                      <div class="form-group" id="order_style">
                        <select name="" id="" class="form-control type_sel_swatch secondary_type">
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <select name="" id="" onchange = "chooseStyle(this)" class="form-control basic_type">
                          <option value="">--Curtain Type--</option>
                          <?php 
                            foreach ($types as $type) {
                              ?>
                                <option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
                              <?php
                            }
                           ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <select name="" id="" class="form-control type_sel_swatch secondary_type">
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <select name="" id="" onchange = "chooseStyle(this)" class="form-control basic_type">
                          
                          <option value="">--Curtain Type--</option>
                          <?php 
                            foreach ($types as $type) {
                              ?>
                                <option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
                              <?php
                            }
                           ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <select name="" id="" class="form-control type_sel_swatch secondary_type">
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <select name="" id="" onchange = "chooseStyle(this)" class="form-control basic_type">
                          
                          <option value="">--Curtain Type--</option>
                          <?php 
                            foreach ($types as $type) {
                              ?>
                                <option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
                              <?php
                            }
                           ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <select name="" id="" class="form-control type_sel_swatch secondary_type">
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <select name="" id="" onchange = "chooseStyle(this)" class="form-control basic_type">
                          
                          <option value="">--Curtain Type--</option>
                          <?php 
                            foreach ($types as $type) {
                              ?>
                                <option value="<?php print_r($type->id); ?> "><?php print_r($type->job_type_name); ?></option>
                              <?php
                            }
                           ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <select name="" id="" class="form-control type_sel_swatch secondary_type">
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <div class="room-area">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-warning" id="cancel-delete-supplier" onclick="hideAddOnEdit()">Cancel</button>
          <button class="btn btn-primary" id="persist-delete-supplier" onclick="addOrderOnEdit(this)">Confirm</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <div class="modal fade" id="removeAssetFromListModal" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Remove from Order List</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              Are you sure you want to remove this area from your order list?. <br>
              <span class="text-danger"><i>* All orders within this area will be remove as well.</i></span>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary removeAreaConfirmBtn" onclick="removeTransactionItem(this);">Confirm Remove</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>
  <!-- item modal selection -->
<div class="modal fade" id="delete-order-item-confirm" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title text-warning">Warning!</h4>
      </div>
      <div class="modal-body">
        <i>This order is about to be deleted "<code class="del-item-show">This Order</code>" from the item order list. This action cannot be undo.</i>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary confirm-delete-order-item" onclick="confirmDeleteItem(this);">Confirm Delete</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="add-misc-dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add Miscellanous</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="misc-id" value="0"> 
        <input type="hidden" class="misc-transaction_id" value="<?php echo $transaction_id; ?>"> 
        <div class="form-group">
          <div class="row">
            <div class="col-md-4">
              <div class="form-label">
                <label for="" class="form-label">COST</label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="number" class="form-control misc-cost"  placeholder="0.00">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-label">
              <label for="" class="form-label">Description</label>
            </div>  
          </div>
          <div class="col-md-8">
            <div class="form-group">
              <textarea name="" id="" cols="30" rows="3" class="form-control misc-description"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary new-misc-submit" onclick="newMisc(this)">Add</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

