
<div class="form-group" style="padding: 10px;">
	<div class="row">
		<div class="classdropdown">
			<div style="margin-left: -13px;">
				<div class="col-md-11">
					<select class="form-control dropdown-selector" style="margin-bottom: 10px; height: 30px;">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
				<div class="col-md-1">
					<button onclick="removeDropDown(this);" style="cursor:pointer; display: none;" class="btn btn-sm btn-danger forclose"><i class="fa fa-times"></i></button>
					<button onclick="addDropDown(this);" style="cursor:pointer;" class="btn btn-sm btn-primary foradd"><i class="fa fa-plus-circle"></i></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="cloned">
		</div>
	</div>
</div>		