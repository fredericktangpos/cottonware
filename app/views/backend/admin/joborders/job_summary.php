<div class="row">
	<div class="col-md-12">
	  <?php 
	        if($this->session->flashdata('flag') == 'success')
	        {
	            echo '<div class="alert alert-success">'.$this->session->flashdata('msg').'</div>';
	        }
	        if($this->session->flashdata('flag') == 'error')
	        {
	            echo '<div class="alert alert-danger">'.$this->session->flashdata('msg').'</div>';
	        }
		    ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			  <div style="background:#F0F0F0;" class="panel-heading">
					<h3 class="panel-title">JOBORDER CONTROL No: <?php echo $joborder->id; ?></h3>
			  </div>
			  <div class="panel-body">
					<h4>Customer: <a href="<?php echo base_url('admin/customer/edit').'/'.$customer->id; ?>"><?php echo $customer->cus_name; ?></a></h4>
					<br />
					<?php //dump($property); ?>

					<?php foreach ($property as $the_property):?>
						<div class="panel panel-default">
							  <div style="background:#DEDEDE;" class="panel-heading clearfix">
									<h3 class="panel-title pull-left"><?php echo $the_property->property_name->asset_name; ?></h3> <!-- property name -->
									<h3 class="panel-title pull-right"><?php echo $the_property->contact_person->name; ?> | <i class="fa fa-mobile"></i> <?php echo $the_property->contact_person->contacts; ?></h3>
							  </div>

								<?php //dump($the_property->window);?>

								<?php if(count($the_property->window) > 0): ?>
										<?php foreach($the_property->window as $window_idx =>$window_value):?>
											<div style="background:#F6F6F6;" class="panel-body">
												<div class="row" style="border-bottom:1px dashed #9D9D96;padding:5px 0 5px 0;">
													<div class="col-md-3 clearfix">
														<img src="<?php echo base_url('images/h_default.png'); ?>" class="img-responsive pull-left" alt="Image" style="border:2px solid #999;" width="40" />
														<div style="margin-left:5px;" class="pull-left">
															<p><strong><?php echo $window_value->window_dtl->asset_sub_dtl_type.' - window '.($window_idx+1); ?></strong><br />
															Size: <?php echo $window_value->window_dtl->full_height.' x '.
															$window_value->window_dtl->window_height.' x '.$window_value->window_dtl->window_width; ?></p>
														</div> 
													</div>
													<div class="col-md-3">
														<h5 style="margin-top:0px; font-weight:bold;">CURTAINS</h5>
														<select name="the_curtain[]" class="form-control the_curtain" style="font-size:11px; padding:10px;" multiple>
															<?php if(count($curtain) > 0):?>
																<?php foreach($curtain as $thecurtain): ?>
																	<option value="<?php echo $thecurtain->id;?>" <?php if(in_array($thecurtain->id, $window_value->curtain)) echo " selected=selected"; ?>><?php echo $thecurtain->job_type_name; ?></option>
																<?php endforeach; ?>
															<?php else: ?>
																<option value="">Select Curtain Type</option>
															<?php endif; ?>
														</select>
														<button onclick="get_suppliers(this);" style="margin:0px !important;line-height:5px !important;width:100%;font-style:italic;font-size:11px;" class="btn btn-primary" type="button">show supplier</button>
														<div class="curtain_supplier" style="border:1px solid #ccc;margin:2px 0 2px 0; padding:0 5px 0 5px;display:none;">
															<div class="suppliers">
																<p>Day Curtain</p>
																<p>Night Curtain</p>
															</div>
														</div>
													</div>
													<div class="col-md-3">
														<h5 style="margin-top:0px; font-weight:bold;">BLINDS</h5>
														<select name="the_blind" class="form-control the_blind" style="font-size:11px; padding:10px;margin-bottom:3px;" multiple>
															<?php if(count($blind) > 0):?>
																<?php foreach($blind as $theblind): ?>
																	<option value="<?php echo $theblind->id;?>" <?php if(in_array($theblind->id, $window_value->blind)) echo " selected=selected"; ?>><?php echo $theblind->job_type_name; ?></option>
																<?php endforeach; ?>
															<?php else: ?>
																<option value="">Select Blind Type</option>
															<?php endif; ?>
														</select>
													</div>
													<div class="col-md-3">
														<h5 style="margin-top:0px; font-weight:bold;">TRACKS</h5>
														<select name="the_track" class="form-control the_track" style="font-size:11px; padding:10px;margin-bottom:3px;" multiple>
															<?php if(count($track) > 0):?>
																<?php foreach($track as $thetrack): ?>
																	<option value="<?php echo $thetrack->id;?>" <?php if(in_array($thetrack->id, $window_value->track)) echo " selected=selected"; ?>><?php echo $thetrack->job_type_name; ?></option>
																<?php endforeach; ?>
															<?php else: ?>
																<option value="<?php echo $thetrack->id;?>"><?php echo $thetrack->job_type_name; ?></option>
															<?php endif; ?>
														</select>
													</div>
												</div>
											</div>
										<?php endforeach; ?>
									<?php else: ?>
											<div style="background:#F6F6F6;" class="panel-body">
												<div style="border-bottom:1px dashed #9D9D96;padding:5px 0 5px 0;" class="row">
													<h3>No window in this properties for this transaction</h3>
												</div>
											</div>
								<?php endif; ?>
						</div>
					<?php endforeach; ?>
			  </div>
		</div>
	</div>
</div>