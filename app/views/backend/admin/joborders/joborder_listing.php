
<div class="clearfix">
	<h2 class="pull-left">Job Order Listing</i></h2>
	<a href="<?php echo base_url('admin/joborder/edit'); ?>" class="btn btn-primary pull-right">CREATE NEW JOB ORDER</a>
</div>
<hr>
<?php //dump($joborder_summary); ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-responsive table-striped">
			<thead>
				<tr>
					<td style="padding-left: 25px;"><h5>Job Order Id</h5></td>
					<td><h5>Customer Name</h5></td>
					<td><h5>Transaction Created</h5></td>
					<td><h5>Status</h5></td>
					<td><h5>Type</h5></td>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($joborder_summary as $job_summary => $value):
				?>
				<tr>
					<td  style="padding-left: 25px;"><a href="#"><?php echo  $value->id; ?></a></td>
					<td><?php echo  $value->cus_name; ?></td>
					<td><?php echo  $value->job_trans_created; ?></td>
					<td><?php echo  $value->job_trans_status; ?></td>
					<td style="text-align: center;"><?php echo  $value->job_trans_type; ?></td>
				</tr>
				<?php 
					endforeach;
				?>
			</tbody>
		</table>
	</div>
</div>