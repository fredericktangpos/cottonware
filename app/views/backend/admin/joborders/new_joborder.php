<div class="row">
	<div class="col-md-12">
		<div class="row">
			<label for="input-id" class="col-sm-1">Contact Person</label>
			<div class="col-md-11">
				<div class="input-group">
					<input type="text" class="form-control contact_key" value="" placeholder="Search contact Person..." />
					<input name="customer_contact_id" type="hidden" class="customer_contact_id">
					<span class="input-group-btn">
						<button onclick="search_customer_contact(this,2);" class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
					</span>
				</div>
				<div style="display:none;" class="showResult">
					<ul class="list-group"></ul>
				</div>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<input type="hidden" value="0" id="selector">
				<!--<button id="new_itemcurtain" type="button" onclick="addNewItem(this, 1);" class="btn btn-warning btn-xs pull-right" style="display:block;"><i class="fa fa-plus"></i> New Window</button>
				<button id="new_itemsofa" type="button" onclick="addNewItem(this, 2);" class="btn btn-warning btn-xs pull-right" style="display:none;"><i class="fa fa-plus"></i> New Sofa</button> -->
				<ul class="nav nav-tabs" id="myTab">
					<li onclick="switchButton(1);"><a href="#property_window">Curtains</a></li>
					<li onclick="switchButton(2);" class="active"><a href="#property_sofa">Sofa</a></li>
				</ul>
				<div class="tab-content">
					<!-- window area -->
					<div class="tab-pane" id="property_window">
						<div class="row">
							<div class="col-md-12" style="display:block;height:300px;overflow-y:scroll;">
								<table class="table table-condensed table-responsive table-hover windowTable" id="windowTable">
									<thead>
										<tr>
											<th>Location</th>
											<th>Room</th>
											<th>Short Description</th>
											<th colspan="3" class="text-center">Measurement(ft.)</th>
											<th class="text-center">Action</th>
										</tr>
										<tr class="tableItems" style="display:none;" assetDtlid="">
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style location">Ex: Ground floor</td>
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style room">Ex: Living Room</td>
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style short_desc">Ex: left side window in the living room</td>
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style text-center ww">window width</td>
											<td style="width:20px;">x</td>
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style text-center wh">window height</td>
											
											<td class="text-center">
												<input type="hidden" value="0" class="inTransaction" />
												<a onclick="remove($(this).parent().parent().remove());" 
													href="javascript:void(0);" 
													style="font-size:20px;color:red;display:none;margin-right:10px;" 
													class="pull-right remover">
													<i class="fa fa-times-circle"></i>
												</a>
												<a onclick="addToTransaction(this);" 
													href="javascript:void(0);" 
													style="font-size:20px;color:green;margin-right:10px;" 
													class="pull-right remover">
													<i class="fa fa-check-circle"></i>
												</a>
												<a onclick="Update_asset_details(this);" 
													href="javascript:void(0);" 
													style="font-size:20px;color:#CD9541;margin-right:10px;" 
													class="pull-right remover">
													<i class="fa fa-pencil-square-o"></i>
												</a>
											</td>
										</tr>
									</thead>
									<tbody class="thetbody">
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- sofa area -->
					<div class="tab-pane active" id="property_sofa">
						<div class="row">
							<div class="col-md-12" style="display:block;height:300px;overflow-y:scroll;">
								<table class="table table-condensed table-responsive table-hover sofaTable" id="sofaTable">
									<thead>
										<tr>
											<th>Location</th>
											<th>Room</th>
											<th>Short Description</th>
											<th colspan="5" class="text-center">Measurement(ft.)</th>
											<th class="text-center">Action</th>
										</tr>
										<tr class="tableItems" style="display:none;" assetDtlid="">
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style location">Ex: Ground floor</td>
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style room">Ex: Living Room</td>
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style short_desc">Ex: left side window in the living room</td>
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style text-center fh">full height</td>
											<td style="width:20px;">x</td>
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style text-center wh">window height</td>
											<td style="width:20px;">x</td>
											<td contenteditable="false" onclick="updateData(this);" class="joborderTable_style text-center ww">window width</td>
											<td class="text-center">
												<input type="hidden" value="0" class="inTransaction" />
												<a onclick="remove($(this).parent().parent().remove());" 
													href="javascript:void(0);" 
													style="font-size:20px;color:red;display:none;margin-right:10px;" 
													class="pull-right remover">
													<i class="fa fa-times-circle"></i>
												</a>
												<a onclick="addToTransaction(this);" 
													href="javascript:void(0);" 
													style="font-size:20px;color:green;margin-right:10px;" 
													class="pull-right remover">
													<i class="fa fa-check-circle"></i>
												</a>
												<a onclick="" 
													href="javascript:void(0);" 
													style="font-size:20px;color:#CD9541;margin-right:10px;" 
													class="pull-right remover">
													<i class="fa fa-pencil-square-o"></i>
												</a>
											</td>
										</tr>
									</thead>
									<tbody class="thetbody">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 	
	</div>
</div>