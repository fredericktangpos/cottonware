<div class="row">
	<h2><?php print_r($type_name[0]->job_type_name); ?></h2>
	<input type="hidden" value="<?php echo $type_id; ?>" id="htxtMain_type_id" theName="<?php print_r($type_name[0]->job_type_name); ?>"> 
	<div class="col-md-12">
		<div class="col-md-10">
		</div>
		<div class="col-md-2">
			<button type="button" data-toggle="modal" class="btn btn-primary btn-sm" data-target="#add-new-style" thisID="<?php echo $type_id; ?>" ><i class="fa fa-plus-circle"></i> Add New Style</button>
		</div>
	</div>
</div>
<br>
<div class="row">	
	<div class="col-md-12">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Type Code</th>
					<th>Width <?php 
						switch ($type_name[0]->id) {
							case 1:
							case 2:
								echo '&nbsp;<i>(cm)</i>';
								break;
							case 3:
								echo '&nbsp;<i>(ft)</i>';
								break;
							case 4:
								echo '&nbsp;<i>(in)</i>';
								break;
							case 5:
								echo '&nbsp;';
								break;
							case 6:
								echo '&nbsp;<i>(in)</i>';
								break;
							default:
								echo '&nbsp;';
								break;
						}
					 ?></th>
					<th>Price/Meter</th>
					<?php 
						if (strtolower($type_name[0]->job_type_name) == "blinds")
						{
					?>
						<th>Min. Area <i>(sq ft)</i></th>
					<?php
						}
					 ?>
					<th>Supplier</th>
					<th>Edit</th>
					<th>Remove</th>
				</tr>
			</thead>
			<tbody class="curtain-swatch-table-body">
				<tr id="curtain-swatch-clone-origin" style="display:none !important"> 
					<td class="cu-sw-id"></td>
					<td class="cu-sw-name"></td>
					<td class="cu-sw-code"></td>
					<td class="cu-sw-width"></td>
					<td class="cu-sw-cost"></td>
					<?php 
						if (strtolower($type_name[0]->job_type_name) == "blinds")
						{
					?>
							<td class="cu-sw-min-width"></td>
					<?php
						}
					?>
					<td><a class="cu-sw-supplier" theSupplier_id="" href="" target="_blank"></a></td>
					<td><a onclick="viewEditCurtainSwatchModal(this);" class="edit-curtain-swatch-1"theID = "" theName="<?php print_r($type_name[0]->job_type_name); ?>"href="#"><i class="glyphicon glyphicon-edit icon-white"></i></a></td>
					<td><a onclick="viewDeleteCurtainSwatchModal(this);" data-toggle="modal" data-target="#delete_blinds" href="#"><i class="glyphicon glyphicon-remove icon-white red"></i></a></td>
				</tr>

				<?php 
				if(count($sub_sub_type['msg'])>0){
					foreach($sub_sub_type['msg'] as $style) {?>
				<tr class="curtain-swatch-<?php echo $style->type_id; ?>">
					<td class="cu-sw-id"><?php echo $style->type_id; ?></td>
					<td class="cu-sw-name"><?php echo $style->type_name; ?></td>
					<td class="cu-sw-code"><?php echo $style->type_code; ?></td>
					<td class="cu-sw-width"><?php echo $style->width; ?></td>
					<td class="cu-sw-cost"><?php echo $style->cost_amount; 
					?></td>
					<?php 
						if (strtolower($type_name[0]->job_type_name) == "blinds")
						{
					?>
							<td class="cu-sw-min-width"><?php echo $style->min_width; ?> </td>
					<?php
						}
					?>
					<td><a class="cu-sw-supplier" theSupplier_id="<?php echo $style->id?>" href="<?php echo base_url('admin/supplier/supplier_swatches/').'/'.$style->id ?>" target="_blank"><?php echo $style->supp_co_name; ?></a></td>
					<td><a onclick="viewEditCurtainSwatchModal(this);" theName="<?php print_r($type_name[0]->job_type_name); ?>" theID = "<?php echo $style->type_id; ?>" href="#"><i class="glyphicon glyphicon-edit icon-white"></i></a></td>
					<td><a onclick="viewDeleteCurtainSwatchModal(this);"  theID="<?php echo $style->type_id; ?>" theName = "<?php echo $style->type_name; ?>" href="#"><i class="glyphicon glyphicon-remove icon-white red"></i></a></td>
				</tr>
				<?php 
					}
				} else {?>
					<tr><td colspan="7" class="text-center">No data Fetched from Server</td></tr>
				<?php } ?>
			</tbody>
		</table>
		<div class="text-center">
			<?php echo $sub_sub_type['page_links']; ?>
		</div>
	</div>
</div>
<div class="modal fade" id="add-new-style">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h2>Add new Swatch</h2>
			</div>
			<div class="modal-body">
				<div class="panel-body">
						<form action="" method="POST" class="form-horizontal" role="form">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label" for="">Style Name</label>
									<input type="text" class="form-control input-sm" id="txtChrSwatch_name" placeholder="Swatch Name">
								</div>
								<div class="form-group">
									<label class="control-label" for="">Type Code </label>
									<input type="text" class="form-control input-sm" id="txtChrSwatch_type_code" placeholder="Type Code">
								</div>
								<div class="form-group">
									<label class="control-label" for="">Width </label>
									<input type="text" class="form-control input-sm" id="txtChrSwatch_width" placeholder="Width">
								</div>
								<div class="form-group">
									<label class="control-label" for="">Price/meter </label>
									<input type="text" class="form-control input-sm" id="txtChrSwatch_pricer_per_meter" placeholder="Price per Meter">
								</div>
								<div class="form-group" id="sw-sub_type-input">
									<label for="" class="control-label">Supplier Name</label>
									<select name="" id="cmbIntSupplier_id"  class="form-control input-sm">
										<option value="">Select Supplier</option>
										<?php 
											foreach($suppliers as $supplier){
										 ?>
											<option value="<?php echo $supplier->id?>"><?php echo $supplier->supp_co_name ?></option>
										 <?php } ?>
									</select>
								</div>
								<?php 
									if (strtolower($type_name[0]->job_type_name) == "blinds")
									{
								?>
									<div class="form-group" id="blinds-min-width">
										<label for="" class="control-label">Minimum Area (sq ft)</label>
										<input type="text" id="intBlindsMinWidth" class="form-control input-sm">
									</div>

								<?php
									}
								 ?>
								<div class="form-group">
									<input type="hidden" id="htxtIntSupplier_id" name="htxtIntSupplier_id" value="">
									<button onclick = addSwatch(this); type="button" class="form-control btn btn-primary"><span id="add_supplier_btn">Add</span></button>
								</div>
							</div>
						</form>
				  </div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="edit-curtain-swatch-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h2>View this Swatch</h2>
			</div>
			<div class="modal-body">
				<div class="panel-body">
						<form action="" method="POST" class="form-horizontal" role="form">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label" for="">Style Name</label>
									<input type="text" class="form-control input-sm" id="txtChrSwatch_name_edit" placeholder="Swatch Name">
								</div>
								<div class="form-group">
									<label class="control-label" for="">Type Code </label>
									<input type="text" class="form-control input-sm" id="txtChrSwatch_type_code_edit" placeholder="Type Code">
								</div>
								<div class="form-group">
									<label class="control-label" for="">Width </label>
									<input type="text" class="form-control input-sm" id="txtChrSwatch_width_edit" placeholder="Width">
								</div>
								<div class="form-group">
									<label class="control-label" for="">Price/meter </label>
									<input type="text" class="form-control input-sm" id="txtChrSwatch_pricer_per_meter_edit" placeholder="Price per Meter">
								</div>
								<?php 
									if (strtolower($type_name[0]->job_type_name) == "blinds")
									{
								?>
									<div class="form-group" id="blinds-min-width">
										<label for="" class="control-label">Minimum Area (sq ft)</label>
										<input type="text" id="intBlindsMinWidth_edit" class="form-control input-sm">
									</div>

								<?php
									}
								 ?>
								<div class="form-group" id="sw-sub_type-input">
									<label for="" class="control-label">Supplier Name</label>
									<select name="" id="cmbIntSupplier_id_edit"  class="form-control input-sm">
										<option value="">Select Supplier</option>
										<?php 
											foreach($suppliers as $supplier){
										 ?>
											<option value="<?php echo $supplier->id?>"><?php echo $supplier->supp_co_name ?></option>
										 <?php } ?>
									</select>
								</div>
								<div class="form-group">
									<input type="hidden" id="htxtIntSupplier_id_edit" name="htxtIntSupplier_id" value="">
								</div>
							</div>
						</form>
					</div>			
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    			<button type="button" class="btn btn-primary confirm-curtain-swatch-update" onclick="confirmCurtainSwatchEdit(this);" theName = "<?php echo $type_name[0]->job_type_name?>">Save changes</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delete-supplier-swatch-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-danger">Warning!!.</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete <b><i id="modal-curtain-swatch-name"><?php echo $style->type_code; ?></i></b> from supplier's list?
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" id="cancel-delete-supplier" onclick="cancelDelete(this,5);">Cancel</button>
				<button class="btn btn-primary" id="persist-delete-supplier" onclick="persistDelete(this,5);">Confirm</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->