<div ng-controller="CurtainList" ng-cloak="">
	
<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo base_url('admin/dashboard'); ;?>">Home</a>
			</li>
			<li>
				<a href="<?php echo base_url('admin/curtaincontroller'); ?>">Settings</a>
			</li>
			<li>
				<a href="<?php echo base_url('admin/curtaincontroller'); ?>">Curtains</a>
			</li>
			<li class="active">View</li>
		</ol>
	</div>
</div>
<div class="row">	
	<div class="col-md-7"></div>
	<div class="col-md-3">
		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-primary btn-sm" type="button"><i class="fa fa-search"></i></button>
			</div>
			<div class="col-md-10">
				<input type="text" class="form-control" id="search" ng-model="searching" placeholder="search">
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<button type="button" data-toggle="modal" data-target="#addType" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Curtain Type</button>
	</div>
	<div class="modal fade" id="addType">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2>Add new Curtain Type</h2>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label" for="">Type Name </label>
						<input type="text" class="form-control input-sm" id="txtChrType_name" placeholder="Curtain Type">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        			<button type="button" class="btn btn-primary" onclick="addType(this);">Add</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="table table-hover" style="text-align: center;">
			<thead>
				<tr>
					<th ng-click="orderVal=!orderVal; orderCol='id';">Id</th>
					<th ng-click="orderVal=!orderVal; orderCol='job_type_name';">Name</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody class="type-table-body">
				<tr id="type-origin" style="display:none !important">
					<td id="type_id"></td>
					<td id="type_name"></td>
					<td><a  onclick="showEditCurtainModal(this);" href=""> <i class="fa fa-edit"></i></a></td>
					<td><a onclick="showCurtainDeleteModal(this);" href="" ><i class="fa fa-times red"></i></a></td>
				</tr>
				<tr class="tr-type-id-{{ curtain.id }}>" ng-repeat="curtain in filtered = (curtains | filter:searching | orderBy: orderCol) track by $index">
					<td class="tr-type-id-{{ curtain.id }}>">{{ curtain.id }}</td>
					<td class="tr-type-id-{{ curtain.id }}>"><a href="<?php echo base_url('admin/curtaincontroller/typesSubType'); ?>/{{ curtain.id }}" target="_blank">{{ curtain.job_type_name }}</a></td>
					<td><a onclick="showEditCurtainModal(this);" href="" theID="{{ curtain.id }}" theName="{{ curtain.job_type_name }}"> <i class="fa fa-edit"></i></a></td>
					<td><a onclick="showCurtainDeleteModal(this);" theID="{{ curtain.id }}" theName="{{ curtain.job_type_name }}" href="" ><i class="fa fa-times red"></i></a></td>
				</tr>
				<tr ng-if="filtered.length == 0">
					<td colspan="4" style="text-align:center;">
						<h3>No result found</h3>
					</td>
				</tr>
			</tbody>	
		</table>
		<div class="text-center"></div>
	</div>
</div>
<div class="modal fade" id="editCurtainModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h2>Update</h2>
			</div>
			<div class="modal-body">
				<input class="form-control curtain-type-name" type="text" placeholder="" value="">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning update-curtain-cancel" data-dismiss="modal">Close</button>
    			<button type="button" class="btn btn-primary update-curtain-confirm" onclick="saveCurtainEdit(this);">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="deleteCurtainModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-danger">Warning!!.</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete <b><i id="modal-type-name">Sample</i></b> from the list?
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" id="cancel-delete-type" onclick="cancelDelete(this,3);">Cancel</button>
				<button class="btn btn-primary" id="persist-delete-type" onclick="persistDelete(this,3);">Confirm</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</div>