<div class="row">
	<div class="col-md-12">
		<h2><?php print_r($supplier_name[0]->supp_co_name) ?></h2>
	</div>
	<div class="col-md-10"></div>
	<div class="modal fade" id="add-supplier-dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2>Add new Swatch</h2>
				</div>
				<div class="modal-body">
					<div class="panel-body">
							<form action="" method="POST" class="form-horizontal" role="form">
								<div class="col-md-12">
									<div class="form-group" id="sw-type-input">
										<label for="" class="control-label">Type</label>
										<select name="" id="cmbIntSupplier_swatch_type" class="form-control input-sm" onchange="getSubType(this);">
											<option value="">Select Type</option>
											<?php 
												foreach ($type as $tp) {
													echo '<option value="'.$tp->id.'">'.$tp->name.'</option>';
												}
											 ?>
										</select>
									</div>
									<div class="form-group" id="sw-sub_type-input">
										<label for="" class="control-label">Sub Type</label>
										<select name="" id="cmbIntSupplier_swatch_sub_type"  class="form-control input-sm">
											<option value="">Select Sub Type</option>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label" for="">Swatch Name </label>
										<input type="text" class="form-control input-sm" id="txtChrSwatch_name" placeholder="Swatch Name">
									</div>
									<div class="form-group">
										<label class="control-label" for="">Type Code </label>
										<input type="text" class="form-control input-sm" id="txtChrSwatch_type_code" placeholder="Type Code">
									</div>
									<div class="form-group">
										<label class="control-label" for="">Width </label>
										<input type="text" class="form-control input-sm" id="txtChrSwatch_width" placeholder="Width">
									</div>
									<div class="form-group">
										<label class="control-label" for="">Price/meter </label>
										<input type="text" class="form-control input-sm" id="txtChrSwatch_pricer_per_meter" placeholder="Price per Meter">
									</div>
									<div class="form-group">
										<input type="hidden" id="htxtIntSupplier_id" name="htxtIntSupplier_id" value="<?php echo $supplier_id; ?>">
										<button onclick = supplierAddSwatch(this); type="button" class="form-control btn btn-primary"><span id="add_supplier_btn">Add</span></button>
									</div>
								</div>
							</form>
					  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="table table-hover" style="text-align: center;" id="supplier-table">
			<thead>
				<tr>
					<th>Type Code</th>
					<th>Width</th>
					<th>Price per Meter</th>
					<th>Actions</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="supplier-swatches-body">
				<tr id="" class="clone-origin" style="display:none !important">
					<td class="co-swatch-type-code"></td>
					<td class="co-swatch-width"></td>
					<td class="co-swatch-cost"></td>
					<td><a data-toggle="modal" data-target="#viewSupplier" href="#"><i class="glyphicon glyphicon-edit icon-white"></i></a></td>
					<td><a href="#"><i class="glyphicon glyphicon-remove icon red"></i></a></td>
				</tr>
			<?php 
				//dump($supplier['msg']);
			if(count($supplier['msg'])){ 
				foreach($supplier['msg'] as $supp){
			 ?>
				<tr id="co-<?php echo $supp->swatch_id; ?>">
					<td class="co-swatch-type-code"><?php print_r($supp->type_code) ?></td>
					<td class="co-swatch-width"><?php print_r($supp->width) ?></td>
					<td class="co-swatch-cost"><?php print_r($supp->cost_amount) ?></td>
					<td><a onclick="showEditSupplierSwatchModal(this);" theID="<?php echo $supp->swatch_id; ?>" theName = "<?php echo $supp->type_name; ?>" href="#"><i class="glyphicon glyphicon-edit icon-white"></i></a></td>
					<td><a onclick="showDeleteSupplierSwatchModal(this);" theID="<?php echo $supp->swatch_id; ?>" theName = "<?php echo $supp->type_name; ?>" href="#"><i class="glyphicon glyphicon-remove icon red"></i></a></td>
				</tr>
			<?php 
				}
			}else{
			 ?>
				<tr>
					<td colspan="5" class="text-center">No data fetched from server</td>
				</tr>
			<?php } ?>
				
			</tbody>	

		</table>
		<div class="text-center"><?php echo $supplier['page_links'] ?></div>
	</div>
</div>
<div class="modal fade" id="edit-supplier-swatch-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Update</h4>
			</div>
			<div class="modal-body">
				<?php $this->load->view('backend/admin/customer/editswatchesform'); ?>				
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" id="cancel-edit-swatch" onclick="cancelEditSwatch(this);">Cancel</button>
				<button class="btn btn-primary" id="persist-edit-swatch" onclick="confirmEditSwatch(this);">Confirm</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="delete-supplier-swatch-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-danger">Warning!!.</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete <b><i id="modal-swatch-code"></i></b> from supplier's list?
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" id="cancel-delete-swatch" onclick="cancelDelete(this,4);">Cancel</button>
				<button class="btn btn-primary" id="persist-delete-swatch" onclick="persistDelete(this,4);">Confirm</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->