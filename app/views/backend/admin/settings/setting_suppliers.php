<div ng-controller="SupplierList" ng-cloak="">
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url('admin/dashboard'); ;?>">Home</a>
				</li>
				<li>
					<a href="<?php echo base_url('admin/supplier'); ?>">Settings</a>
				</li>
				<li>
					<a href="<?php echo base_url('admin/supplier'); ?>">Suppliers</a>
				</li>
				<li class="active">View</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-7"></div>
		<div class="col-md-3">
			<div class="row">
				<div class="col-md-2">
					<button class="btn btn-primary btn-sm" type="button"><i class="fa fa-search"></i></button>
				</div>
				<div class="col-md-10">
					<input type="text" class="form-control" id="search" ng-model="searching" placeholder="search">
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<button type="button" data-toggle="modal" data-target="#add-supplier-dialog" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add New Supplier</button>
		</div>
		<div class="modal fade" id="add-supplier-dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h2>Add new Supplier</h2>
					</div>
					<div class="modal-body">
						<div class="panel-body">
								<form action="" method="POST" class="form-horizontal" role="form">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label" for="">Name </label>
											<input type="text" class="form-control input-sm" id="supp_name" placeholder="Supplier Name" required>
										</div>
										<div class="form-group">
											<label class="control-label" for="">Representative </label>
											<input type="text" class="form-control input-sm" id="supp_contact_name" placeholder="Contact Person">
										</div>
										<div class="form-group">
											<label class="control-label" for="">Contact Number </label>
											<input type="text" class="form-control input-sm" id="supp_contact_num" placeholder="Contact Number">
										</div>
										<div class="form-group">
											<label class="control-label" for="">Email Address </label>
											<input type="email" class="form-control input-sm" id="supp_email" placeholder="Email Address">
										</div>

										<div class="form-group">
											<button onclick = ADD_NEW_SUPPLIER(); type="button" class="form-control btn btn-primary"><span id="add_supplier_btn">Add</span></button>
										</div>
									</div>
								</form>
						  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover" style="text-align: center;" id="supplier-table">
				<thead>
					<tr>
						<th ng-click="orderVal=!orderVal; orderCol='id';">Id</th>
						<th ng-click="orderVal=!orderVal; orderCol='supp_co_name';">Name</th>
						<th ng-click="orderVal=!orderVal; orderCol='supp_contact_person';">Contact Person</th>
						<th ng-click="orderVal=!orderVal; orderCol='supp_contact_num';">Contact Number</th>
						<th ng-click="orderVal=!orderVal; orderCol='supp_email';">Contact Email</th>
						<th>Edit</th>
						<th>Remove</th>
					</tr>
				</thead>
				<tbody id="supplier-list-body">
					<tr id="sup-{{ customer.id }}" ng-repeat="customer in filtered = (customers | filter:searching | orderBy: orderCol) track by $index">
						<td>{{ customer.id }}</td>
						<td id="s-supplier-name"><a href="<?php echo base_url('admin/supplier/supplier_swatches/')?>/{{ customer.id }}" target="_blank">{{ customer.supp_co_name }}</a></td>
						<td id="s-contact-person">{{ customer.supp_contact_person }}</td>
						<td id="s-contact-number">{{ customer.supp_contact_num }}</td>
						<td id="s-contact-email">{{ customer.supp_email }}</td>
						<td><a href="" theName="{{ customer.supp_co_name }}" theSupplierID="{{ customer.id }}" theContactNumber="{{ customer.supp_contact_num }}" theContactPerson="{{ customer.supp_contact_person }}" theContactEmail="{{ customer.supp_email }}" onclick="showEditSupplierModal(this)"><i class="fa fa-edit"></i></a></td>
						<td><a href="" theName="{{ customer.supp_co_name }}" theSupplierID="{{ customer.id }}" theContactNumber="{{ customer.supp_contact_num }}" theContactPerson="{{ customer.supp_contact_person }}" theContactEmail="{{ customer.supp_email }}" onclick="showConfirmDeleteDialog(this,1)"><i class="fa fa-times red"></i></a></td>
					</tr>
					<tr ng-if="filtered.length == 0">
						<td colspan="7" style="text-align:center;"><h4>No data found</h4></td>
					</tr>
				</tbody>

			</table>
			<div class="text-center">{{ page_links }}</div>
		</div>
	</div>
	<div class="modal fade" id="edit_supplier_form">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Edit Supplier Details</h4>
				</div>
				<div class="modal-body">
					<?php $this->load->view('backend/admin/customer/editsupplierform'); ?>
				</div>
				<div class="modal-footer">
					<button class="btn btn-warning" id="cancel-edit-supplier" onclick="editSupplier(this,1);">Cancel</button>
					<button class="btn btn-primary" id="persist-edit-supplier" onclick="editSupplier(this,2);">Confirm</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div class="modal fade" id="delete_supplier">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title text-danger">Warning!!.</h4>
				</div>
				<div class="modal-body">
					Are you sure you want to delete <b><i id="modal-supplier-name">Sample</i></b> from supplier's list?
				</div>
				<div class="modal-footer">
					<button class="btn btn-warning" id="cancel-delete-supplier" onclick="cancelDelete(this,1);">Cancel</button>
					<button class="btn btn-primary" id="persist-delete-supplier" onclick="persistDelete(this,1);">Confirm</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>