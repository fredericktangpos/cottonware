<div class="row">
	<!-- first part of page -->
	<div class="col-md-12">
		<div class="col-md-8">
				<div class="panel panel-default">
					  <div class="panel-heading panel_header_color">
							<h3 class="panel-title">Add Item</h3>
					  </div>
						<div class="panel-body">
							<form action="" method="POST" class="form-horizontal" role="form" id="add_item_form">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label" for="" >Name </label>
										<input type="hidden" name="htxtItem_id" id="htxtItem-id" value="" />
										<input type="text"  onkeyup="search_item(this);" class="form-control input-sm" id="item_name"  placeholder="Job Classification Name">
									</div>
									<div class="result" style="display:none;max-height:400px;overflow:scroll;">
										<ul class="list-group"></ul>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<?php //dump($job_category); ?>
												<label class="control-label" for="">Supplier </label>
												<select name="" id="slctSupplier_id" class="form-control input-sm">
													<option value="">-- Select Supplier --</option>
													<?php 
														if(count($supplier) > 0):
														foreach ($supplier as $suppliers):
													?>
														<option value="<?php echo $suppliers->id; ?>"><?php echo $suppliers->supp_co_name; ?></option>
													<?php 
														endforeach;
														else:
													?>
														<option value="">No Supplier in the system</option>
													<?php
														endif;
													?>
												</select>
											</div>
											<div class="col-md-6">
												<label class="control-label" for="">Item Category</label>
												<select name="" id="slctItem_category" class="form-control input-sm">
													<option value="">-- Select Category --</option>
													<option value="curtain">Curtain</option>
													<option value="sofa">Sofa</option>
													<option value="blind">Blinds</option>
													<option value="accessories">Accessories</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<label class="control-label" for="">$ Price Amount</label><div style="color:red;display:none;" id="disp"></div>
												<input type="text" class="form-control input-sm" id="cost_amount" placeholder="Amount">
											</div>
											<div class="col-md-6">
												<label class="control-label" for="">Type Code</label><div style="color:red;display:none;" id="disp"></div>
												<input type="text" class="form-control input-sm" id="chrType_code" placeholder="Amount">
											</div>
										</div>
										
									</div>
									<div class="form-group">
										<button onclick = addItemSupplier(); type="button" class="btn btn-primary"><span id="add_jobclass_btn">Add</span></button>
									</div>
									
								</div>
							</form>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<!-- Supplier moderation -->
				<div class="panel panel-default">
					  <div class="panel-heading panel_header_color">
							<h3 class="panel-title">Supplier</h3>
					  </div>
					  <div class="panel-body">
							<form action="" method="POST" class="form-horizontal" role="form">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label" for="">Name </label>
										<input type="text" class="form-control input-sm" id="supp_name" placeholder="Supplier Name">
									</div>
									<div class="form-group">
										<label class="control-label" for="">Representative </label>
										<input type="text" class="form-control input-sm" id="supp_contact_name" placeholder="Contact Person">
									</div>
									<div class="form-group">
										<label class="control-label" for="">Contact Number </label>
										<input type="text" class="form-control input-sm" id="supp_contact_num" placeholder="Contact Number">
									</div>
									<div class="form-group">
										<label class="control-label" for="">Email Address </label>
										<input type="email" class="form-control input-sm" id="supp_email" placeholder="Email Address">
									</div>
									
									<div class="form-group">
										<button onclick = ADD_NEW_SUPPLIER(); type="button" class="form-control btn btn-primary"><span id="add_supplier_btn">Add</span></button>
									</div>
								</div>
							</form>
					  </div>
				</div>
			</div>
	</div>
	<div class="col-md-12">
		<div class="row">
		<div class="col-md-8">
			<div class="panel panel-default">
			  <div class="panel-heading panel_header_color">
					<h3 class="panel-title">Job Order Category</h3>
			  </div>
			  <div class="panel-body">
					<form action="" method="POST" class="form-horizontal" role="form">
						<div class="col-md-12">
							<div style="margin-bottom:5px;" class="form-group">
								<label class="control-label" for="">Name </label>
								<input type="text" class="form-control input-sm" id="category_name" placeholder="Category Name">
								<small id="show_subcat"><a href="javascript:void(0);">Add Sub Category</a></small>
								<div style="margin-top:5px;display:none;" id="add_subCat">
									<input onkeyup=get_subcat(fevent); type="text" class="form-control input-sm" id="subCat_name" placeholder="Sub Category Name" />
								</div>
								<div class="subcat_list">
									<ul class="list-group" style="margin-top:5px;"></ul>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label" for="">Description </label>
								<textarea class="form-control input-sm" rows="2" id="cat_description"placeholder="Category Description"></textarea>
							</div>

							<div class="form-group pull-right">
								<div class="btn-group" data-toggle="buttons">
								  <label class="btn btn-warning btn-xs active">
								    <input type="radio" id="" class="cat_stat" value="1" autocomplete="off"> Enable
								  </label>
								  <label class="btn btn-warning btn-xs">
								    <input type="radio" id="" class="cat_stat" value="0" autocomplete="off"> Disable
								  </label>
								</div>
							</div>

							<div class="form-group">
								<button onclick=ADD_JOB_CATEGORY(); type="button" class="form-control btn btn-primary"><span id="add_jobcat_btn">Add</span></button>
							</div>
						</div>
					</form>
			  </div>
		</div>
		</div>
			<!-- first in second part of page -->
			<!-- second in second part of page -->
			
		</div>
		<hr style="border:1px dashed #999;" />
		<div class="row">
			<div class="col-md-12">
				<div class="custom_tab" role="tabpanel" >
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#supplier" aria-controls="supplier" role="tab" data-toggle="tab">View All Supplier</a></li>
						<li role="presentation"><a href="#jo_order_cat" aria-controls="jo_order_cat" role="tab" data-toggle="tab">Item List</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content custom_tab_content">
						<div role="tabpanel" class="tab-pane active" id="supplier">
							<div id="table-container" class="table-responsive" style="height:400px !important"> 
								<h3>Supplier Listing</h3>
								<table class="table table-bordered table-hover">
									<thead>
										<tr style="background:#ccc;">
											<th class="text-center">ID</th>
											<th class="text-center">Supplier name</th>
											<th>Contact Person</th>
											<th>Contact #</th>
											<th>Contact Email</th>
											<th class="text-center">Edit</th>
											<th class="text-center">Remove</th>
										</tr>
									</thead>
									<tbody id="supplier_table">
										<?php if(count($supp_summary) > 0): ?>
											<?php foreach($supp_summary as $supplier1): ?>
												<tr id="sup-<?php echo $supplier1->id; ?>">
													<td class="text-center" ><?php echo $supplier1->id; ?></td>
													<td id="s-supplier-name"><?php echo $supplier1->supp_co_name; ?></td>
													<td id="s-contact-person"><?php echo $supplier1->supp_contact_person; ?></td>
													<td id="s-contact-number"><?php echo $supplier1->supp_contact_num; ?></td>
													<td id="s-contact-email"><?php echo $supplier1->supp_email; ?></td>
													<td class="text-center"><a href="" theName="<?php echo $supplier1->supp_co_name; ?>" theSupplierID="<?php echo $supplier1->id; ?>" theContactNumber="<?php echo $supplier1->supp_contact_num; ?>" theContactPerson="<?php echo $supplier1->supp_contact_person; ?>" theContactEmail="<?php echo $supplier1->supp_email; ?>" onclick="showEditSupplierModal(this)"><i class="fa fa-edit"></i></a></td>
													<td class="text-center"><a href="" theName="<?php echo $supplier1->supp_co_name; ?>" theSupplierID="<?php echo $supplier1->id; ?>" theContactNumber="<?php echo $supplier1->supp_contact_num; ?>" theContactPerson="<?php echo $supplier1->supp_contact_person; ?>" theContactEmail="<?php echo $supplier1->supp_email; ?>" onclick="showConfirmDeleteDialog(this,1)"><i class="fa fa-times"></i></a></td>
												</tr>
											<?php endforeach; ?>
										<?php else: ?>
												<tr>
													<td>No Supplier Found in the System</td>
												</tr>
										<?php endif; ?>
									</tbody>
								</table>
								<?php echo '<div id="ajax-table">'.$pagination.'</div>'; ?>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="jo_order_cat">
							<div id="table-container1" class="table-responsive" style="height:400px !important">
								<h3>Item Listing</h3>
								<table class="table table-bordered table-hover">
									<thead>
										<tr style="background:#ccc;">
											<th class="text-center">ID</th>
											<th class="text-center">Item Name</th>
											<th>Item Type</th>
											<th class="text-center">Edit</th>
											<th class="text-center">Remove</th>
										</tr>
									</thead>
									<tbody>
										<?php if(count($job_cat1) > 0): ?>
											<?php foreach($job_cat1 as $job_cat11): ?>
												<tr id="<?php echo $job_cat11->id; ?>" class="sample">
													<td class="text-center"><?php echo $job_cat11->id; ?></td>
													<td id="s_job_item_name"><?php echo $job_cat11->job_type_name; ?></td>
													<td id="s_job_item_type"><?php echo $job_cat11->sub_cat_type; ?></td>
													<td class="text-center"><a theName="<?php echo $job_cat11->job_type_name; ?>" theType="<?php echo $job_cat11->sub_cat_type; ?>" theItemID="<?php echo $job_cat11->id; ?>"  data-toggle="modal" href="" class="edit-item-btn" onclick="editItem(this);"><i class="fa fa-edit"></i></a></td>
													<td class="text-center"><a href="" theName="<?php echo $job_cat11->job_type_name; ?>" theType="<?php echo $job_cat11->sub_cat_type; ?>" theItemID="<?php echo $job_cat11->id; ?>" onclick="showConfirmDeleteDialog(this,2);"><i class="fa fa-times"></i></a></td>
												</tr>
											<?php endforeach; ?>
										<?php else: ?>
												<tr>
													<td>No Job Category Found in the System </td>
												</tr>
										<?php endif; ?>
									</tbody>
								</table>
								<?php echo '<div id="ajax-table1">'.$pagination1.'</div>'; ?>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="job_classification">
							<div id="table-container2" class="table-responsive">
								<h3>Job Classification Listing</h3>
								<table class="table table-bordered table-hover">
									<thead>
										<tr style="background:#ccc;">
											<th class="text-center">ID</th>
											<th class="text-center">Category</th>
											<th class="text-center">Supplier</th>
											<th class="text-center">Class Name</th>
											<th class="text-center">Formula</th>
											<th>Description</th>
											<th class="text-center">Edit</th>
											<th class="text-center">Remove</th>
										</tr>
									</thead>
									<tbody>
										<?php if(count($job_class) > 0): ?>
											<?php foreach($job_class as $job_class1): ?>
												<tr>
													<td class="text-center"><?php echo $job_class1->job_class_id; ?></td>
													<td><?php echo $job_class1->job_name; ?></td>
													<td><?php echo $job_class1->supp_co_name; ?></td>
													<td><?php echo $job_class1->job_class_name; ?></td>
													<td><?php echo $job_class1->job_formula; ?></td>
													<td><?php echo $job_class1->job_desc; ?></td>
													<td class="text-center"><a href=""><i class="fa fa-edit"></i></a></td>
													<td class="text-center"><a href=""><i class="fa fa-times"></i></a></td>
												</tr>
											<?php endforeach; ?>
										<?php else: ?>
												<tr>
													<td>No Job Category Found in the System </td>
												</tr>
										<?php endif; ?>
									</tbody>
								</table>
								<?php echo '<div id="ajax-table1">'.$pagination2.'</div>'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br />
<div class="modal fade" id="edit_item">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Item</h4>
			</div>
			<div class="modal-body">
				<?php $this->load->view('backend/admin/customer/edititemform'); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning " id="save-edited-item" onclick="saveEditedItem(this,1);">Cancel</button>
				<button type="button" class="btn btn-primary " id="save-edited-item" onclick="saveEditedItem(this,2);">Save Changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="delete_item">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-danger">Warning!!.</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete <b><i id="modal-item-name">Sample</i></b> from item list?
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" id="cancel-delete-item" onclick="cancelDelete(this,2);">Cancel</button>
				<button class="btn btn-primary" id="persist-delete-item" onclick="persistDelete(this,2);">Confirm</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--
<div class="modal fade" id="delete_supplier">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-danger">Warning!!.</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to delete <b><i id="modal-supplier-name">Sample</i></b> from supplier's list?
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" id="cancel-delete-supplier" onclick="cancelDelete(this,1);">Cancel</button>
				<button class="btn btn-primary" id="persist-delete-supplier" onclick="persistDelete(this,1);">Confirm</button>
			</div>
		</div><!-- /.modal-content -->
	<!--</div><!-- /.modal-dialog -->
<!--</div><!-- /.modal -->
-->
<div class="modal fade" id="edit_supplier_form">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Supplier Details</h4>
			</div>
			<div class="modal-body">
				<?php $this->load->view('backend/admin/customer/editsupplierform'); ?>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" id="cancel-delete-supplier" onclick="editSupplier(this,1);">Cancel</button>
				<button class="btn btn-primary" id="persist-delete-supplier" onclick="editSupplier(this,2);">Confirm</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->