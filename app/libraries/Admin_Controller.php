<?php 
class Admin_Controller extends MY_controller 
{
	function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *'); 
		//load all the item type transaction

		$this->load->model('settings/m_tbl_transaction_type');//load model 
		$this->data['transaction_types'] = $this->m_tbl_transaction_type->get();
		//dump($this->data['transaction_types']);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('user/m_tbl_users');
		$this->data['meta_title'] 	= 'Cottonware CRM System | Secure Area';

		# login check
		$exception_uris = array('admin/user/login', 'admin/user/logout');

		if(in_array(uri_string(), $exception_uris) == FALSE) {
			if($this->m_tbl_users->loggedin() == FALSE) {
				redirect('admin/user/login');
			}else
			{
				$this->load->model("user/m_tbl_user_access");
				$this->load->model("user/m_tbl_menus");
				$user_id = $this->session->userdata('id');
				//$dbArUsers_access = $this->m_tbl_user_access->get_by(array("tbl_users_id"=>(int)$user_id));

				$select_join = '
								menu.*,
								menu.id AS menu_id';

		$joining_array = array();
		$joining_array[0] = new stdClass();
		

		$joining_array[0]->table 		= 'menu';
		$joining_array[0]->condition 	= 'menu.id = user_access.menu_id';
		$joining_array[0]->method 		= 'left';

		$this->data['dbArUsers_access'] = $this->m_tbl_user_access->get_by(array('user_access.tbl_users_id'=>$user_id),FALSE,$joining_array,$select_join);
			}
		}
		// declaring styles
		$this->data['styles'] = array(
			'hovereffects/css/default.css',
			'hovereffects/css/component.css',
			'css/bootstrap.css',
			'css/styles.css',
			'css/plugins/metisMenu/metisMenu.min.css',
			'css/plugins/timeline.css',
			'css/jasny-bootstrap.css',
			'css/plugins/morris.css',
			'css/bootstrap-cerulean.min.css',
			'css/charisma-app.css',
			'css/noty_theme_default.css',
			'css/elfinder.min.css',
			'css/elfinder.theme.css',
			'css/jquery.iphone.toggle.css',
			'css/uploadify.css',
			'css/animate.min.css',
			'css/jquery-ui-1.8.21.custom.css',
			'css/jquery.noty.css',
			'css/lightbox.css',
			'css/jquery.datetimepicker.css',
			'font-awesome-4.1.0/css/font-awesome.min.css',
			'bower_components/fullcalendar/dist/fullcalendar.css',
			'bower_components/chosen/chosen.min.css',
			'bower_components/colorbox/example3/colorbox.css',
			'bower_components/responsive-tables/responsive-tables.css',
			'bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css'
		);


		// declaring scripts
		$this->data['scripts'] = array(
			'bower_components/bootstrap/dist/js/bootstrap.min.js',
			'js/jquery.cookie.js',
			'bower_components/moment/min/moment.min.js',
			'bower_components/fullcalendar/dist/fullcalendar.min.js',
			'js/jquery.dataTables.min.js',
			'bower_components/chosen/chosen.jquery.min.js',
			'bower_components/colorbox/jquery.colorbox-min.js',
			'js/jquery.noty.js',
			'bower_components/responsive-tables/responsive-tables.js',
			'bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js',
			'js/jquery.raty.min.js',
			'js/jquery.iphone.toggle.js',
			'js/jquery.autogrow-textarea.js',
			'js/jquery.uploadify-3.1.min.js',
			'js/jquery.history.js',
			'js/charisma.js',
			'js/jasny-bootstrap.js',
			'js/bootbox.min.js',
			'js/custom/settings.js',
			'js/custom/joborder.js',
			'js/custom/calendar.js',
			'js/custom/user.js',
			'js/lightbox.js',
			'angular/angular.js',
      		'js/customer/ng/app.js',
      		'js/customer/ng/controller.js',
      		'js/customer/ng/service.js',
      		'js/customer/property.js',
      		'js/customer/addjoborder.js',
      		'js/numeric-input-example.js',
      		'hovereffects/js/modernizr.custom.js',
      		'hovereffects/js/toucheffects.js',
      		'js/jquery.datetimepicker.min.js',
      		'js/jquery.datetimepicker.full.min.js'
		);

	}

	# upload file function
	public function upload_file($fieldName, $dir)
	{

		# load upload library
		$this->load->library('upload');

		// Configure upload.
	    $this->upload->initialize(array(
	        "file_name"     => @$_FILES[$fieldName]['name'],
	        "upload_path"   => $dir,
	        'allowed_types' => 'gif|jpg|png'
	    ));

	   // dump($this->upload->do_multi_upload($fieldName));
	    if($this->upload->do_multi_upload($fieldName)) 
    	{
	        return $this->upload->get_multi_upload_data();
		}
		else
		{
			return $this->upload->display_errors();
		}

	}





} #end of class	
?>